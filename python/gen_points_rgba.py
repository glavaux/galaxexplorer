import GalaxExplorer as ge
import numpy as np

dsh = ge.DataSetHandler()
ch = ge.Connectors()
rh = ge.Representation("ColorPointRepresentation")
rh.setParameter("auxFieldRed", "testred")
rh.setParameter("auxFieldGreen", "testgreen")
rh.setParameter("auxFieldBlue", "testred")
rh.setParameter("auxFieldAlpha", "testalpha")
rh.setParameter("distanceCorrection", "1")
rh.setParameter("pointSize", "40.0")

p = np.random.rand(3,1000000)*10.-5

ared = np.sqrt((p**2).sum(axis=0))
ared /= ared.max()

agreen = np.zeros(ared.size)

a = np.array([ared,agreen,(1-ared)**5])

dsh.newData("test", p, attribute_names=np.array(["testred","testgreen","testalpha"]), attribute_data=a)
dsh.waitData("test")

ch.create("test", "test", None, "ColorPointRepresentation")
