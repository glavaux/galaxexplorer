#+
# This is GalaxExplorer (./python/GalaxExplorer/data_io.py) -- Copyright (C) Guilhem Lavaux (2007-2013)
#
# guilhem.lavaux@gmail.com
#
# This software is a computer program whose purpose is to interpret and
# represent astromical data or N-body simulations in three-dimensions.
# It is a visualization tool.
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#+
import os.path
import socket
import dbus as d
import struct

class DataSetHandler:

    def __init__(self):
        self.bus = d.SessionBus()
        self.data_object = self.bus.get_object("org.GalaxExplorer", "/Data")

    """
    This function remotely removes a dataset from GalaxExplorer. It may
    do so only if all connections has been destroyed.

    - dataname: string 
    - force: force the destruction of data and its attached connections
    """
    def dropData(self,dataname,force=False):

        if type(dataname).__name__ != 'str':
            raise ValueError("Dataname must be a string")
        
        if not self.data_object.dropData(dataname,force):
            raise ValueError("Impossible to destroy dataset " + dataname)
        
    """
    This function creates remotely a new datagrid in GalaxExplorer.
    If the dataset already exists it will not be created and an exception
    is thrown.

    - dataname: a string giving the name of the dataset to create
    - data: a (Nx,Ny,Nz) numpy array with scalar value
    """
    def newGrid(self, dataname, data, boundaries=[-1,1,-1,1,-1,1]):
        import numpy as np

        if type(data) != np.ndarray:
            raise ValueError("data must be NumPy array")
        if len(data.shape) != 3: 
            raise ValueError("Invalid array shape")

        Nx,Ny,Nz = data.shape
        bnd = d.Struct([float(b) for b in boundaries[:6]], signature='dddddd') 
        try:
          port_id, cookie = self.data_object.createDataGrid(dataname, Nx, Ny, Nz)
        except d.DBusException as e:
          import sys
          x = sys.exc_info()
          raise x[1],None,x[2]
        
        port_id = int(port_id)
        cookie = int(cookie)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(('localhost', port_id))
        sock.send(struct.pack('i',cookie))
        sock.send(data.astype(np.float32).tostring(), socket.MSG_WAITALL)
        sock.close()
        self.waitData(dataname)
        

    """
    This function creates remotely a new dataset in GalaxExplorer.
    If the dataset already exists it will not be created and an exception
    is thrown.
    
    - dataname: a string giving the name of the dataset to create
    - trilist: a (3,N) numpy array with triangles vertices
    """
    def newMesh(self, dataname, trilist):
        import numpy as np

        if type(trilist) != np.ndarray:
            raise ValueError("trilist must be NumPy array")

        if len(trilist.shape) != 2 or trilist.shape[0] != 3:
            raise ValueError("Invalid array shape")

        if type(dataname) != str:
            raise ValueError("Dataname must be a string")

        N = trilist.shape[1]

        try:
            port_id,cookie = \
                self.data_object.createDataMesh(dataname, N, 0)
        except d.DBusException as e:
            raise e

        if port_id < 0:
            raise ValueError("Impossible to create dataset (already exists ?)")

        port_id = int(port_id)
        cookie = int(cookie)

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(('localhost', port_id))
        sock.send(struct.pack('i',cookie))

        s = ""
        for i in range(N):
            s = s + struct.pack('iii', trilist[0,i], trilist[1,i], trilist[2,i])
            
        sock.send(s, socket.MSG_WAITALL)

        sock.close()

        self.waitData(dataname)

    """
    This function creates remotely a new dataset in GalaxExplorer.
    If the dataset already exists it will not be created and an exception
    is thrown.
    
    - dataname: a string giving the name of the dataset to create
    - positions: a (3,N) numpy array with particle positions
    - multiplier: the particle position multiplier for visualization
    """
    def newData(self, dataname, positions, attribute_names=None, attribute_data=None, multiplier=10.0):
        import numpy as np

        if len(positions.shape) != 2 or positions.shape[0] != 3:
            raise ValueError("Invalid array shape")

        if type(dataname).__name__ != 'str':
            raise ValueError("Dataname must be a string")

        if attribute_names != None:

            if len(attribute_names.shape) != 1 or attribute_names.dtype.kind != 'S':
                raise ValueError("Invalid attribute names.")


            if attribute_data == None:
                raise ValueError("Attributes need data.")

            if len(attribute_data.shape) != 2 or attribute_data.shape[1] != positions.shape[1]:
                raise ValueError("Invalid number of elements in attribute_data.")

            a = []
            for i in range(len(attribute_names)):
                a.append(d.String(attribute_names[i]))

            Na = len(a)

            attribute_names = d.Array(a, 's')

        else:
            attribute_names = d.Array([], 's')
            attribute_data = None
            Na = 0

        N = positions.shape[1]

        try:
            port_id,cookie = \
                self.data_object.createPointDataSet(dataname, N, 0, attribute_names, multiplier)
        except d.DBusException as e:
            raise IOError("Lost DBus connection")

        if port_id < 0:
            raise ValueError("Impossible to create dataset (already exists ?)")

        port_id = int(port_id)
        cookie = int(cookie)

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(('localhost', port_id))
        sock.send(struct.pack('i',cookie))

        a = np.zeros((N,3+Na),dtype=np.float32)
        a[:,:3] = positions[:3,:].transpose()
        if attribute_data is not None:
            a[:,3:] = attribute_data.transpose()
        s = a.tostring()
        print N
        print len(s)
        
#        for i in range(N):
#          s = s + struct.pack('3f', *positions[0:3,i])
#          for j in range(Na):
#            s = s + struct.pack('f', attribute_data[j,i])
            
        sock.send(s, socket.MSG_WAITALL)

        sock.close()

        self.waitData(dataname)

    def waitData(self, dataname):

        ret = self.data_object.waitData(dataname)

        if ret==1:
            return

        if ret==0:
            raise IOError("Invalid return value. Pending reply timedout.")

        if ret==-1:
            raise ValueError("Data not pending creation.")

        if ret==-2:
            raise ValueError("Data creation aborted.")

        if ret==-3:
            raise ValueError("Data creation already finished or aborted.")

        raise IOError("Unknown return value.")

    def createMultiGadget(self, dataname, pattern, N=0, sx=0, sy=0, sz=0):
        ret = self.data_object.createMultiGadget(dataname, pattern, N, float(sx), float(sy), float(sz))
        if ret == 1:
           return

        raise IOError("Problem while loading data files")
 
    def createMultiRamses(self, dataname, basepath, N=0, sx=0, sy=0, sz=0):
        basepath = os.path.abspath(basepath)
        ret = self.data_object.createMultiRamses(dataname, basepath, N, float(sx), float(sy), float(sz))
        if ret == 1:
           return

        raise IOError("Problem while loading data files")

    def loadRamses(self, dataname, path, id):
        path = os.path.abspath(path)
        ret = self.data_object.loadRamses(dataname, path, id)
        if ret == 1:
           return
        raise IOError("Problem while loading ramses file")

    def setTrajectoryTime(self, dataname, t):
        ret = self.data_object.setTrajectoryTime(dataname, t)
        if ret == 1:
           return
        if ret == -1:
           raise IOError("Problem while loading data files.")
        if ret == -2:
           raise IOError("Dataset is not a trajectory.")
        raise IOError("Unknown error.")

    def loadGridFromFile(self, dataname, filename):
       filename = os.path.abspath(filename)
       ret = self.data_object.loadGridFromFile(dataname, filename)
       if ret == 1:
         return
       if ret == -1:
         raise IOError("Problem with loading data files.")
       raise IOError("Unknown error.")
