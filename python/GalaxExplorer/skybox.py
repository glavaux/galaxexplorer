#+
# This is GalaxExplorer (./python/GalaxExplorer/skybox.py) -- Copyright (C) Guilhem Lavaux (2007-2013)
#
# guilhem.lavaux@gmail.com
#
# This software is a computer program whose purpose is to interpret and
# represent astromical data or N-body simulations in three-dimensions.
# It is a visualization tool.
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#+
import dbus as b
import numpy as np
import results


GalaxResult=results.Enumerate('SUCCESS INVAL EXISTS INV_FILE')

class SkyBox:
    """
    This class allows to interact with the SkyBox functionality of GalaxExplorer.
    It supports adding a box from an Healpix map, removing it, reordering the 
    drawing of boxes, setting the scale and value-to-color transformation.
    """

    def __init__(self):
        """
        Constructor.
        """

        self.bus = b.SessionBus()
        self.handler = self.bus.get_object("org.GalaxExplorer", "/world/SkyBox")

    def add(self, name, path):
        """
        Add 
        """

        if (type(name) != str) or (type(path) != str):
            raise ValueError("Invalid type in parameters")

        return int(self.handler.add(name, path))

    def remove(self, name):
        
        if (type(name) != str):
            raise ValueError("Invalid type for name");

        return int(self.handler.remove(name))

    def reorder(self, namelist):

        if not np.iterable(namelist):
            raise ValueError("namelist must be iterable")

        a = []
        for i in namelist:
            if type(i) != str:
                raise ValueError("namelist must be a list of string")
            a.append(i)

        return int(self.handler.reorder(b.Array(a,signature='s')))


    def setColorValue(self, name, min_color=(0,0,0,0), max_color=(1,1,1,1),
                      palette=[]):


        if not np.iterable(palette):
            raise ValueError("palette must be iterable")

        if len(min_color) != 4 or len(max_color) != 4:
            raise ValueError("The number of color components must be four.")

        a=[]
        for i in palette:
            if len(i) != 5:
                raise ValueError("palette must be list of 5 doubles")
            a.append((float(i[0]),(float(i[1]),float(i[2]),float(i[3]),float(i[4]))))

	min2 = ()
	for i in min_color:
		min2 = min2 + (float(i),)
	max2 = ()
	for i in max_color:
		max2 = max2 + (float(i),)

        return int(self.handler.setColorValue(name, min2, max2, b.Array(a, signature="(d(dddd))")))


    def setMinMax(self, name, minval, maxval):

        return int(self.handler.setMinMax(name, float(minval), float(maxval)))

    def getList(self):

        a = self.handler.getList()

        l = []
        for i in a:
            l.append(str(i))

        return l
