#+
# This is GalaxExplorer (./python/GalaxExplorer/representation.py) -- Copyright (C) Guilhem Lavaux (2007-2013)
#
# guilhem.lavaux@gmail.com
#
# This software is a computer program whose purpose is to interpret and
# represent astromical data or N-body simulations in three-dimensions.
# It is a visualization tool.
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#+
import dbus as b
import numpy as np
import results

ParameterResult = results.Enumerate('SUCCESS NO_PROPERTY INVALID_TYPE INVALID_VALUE')


class RepresentationHandler:

    def __init__(self):
        self.bus = b.SessionBus()
        self.handler = self.bus.get_object("org.GalaxExplorer", "/world/RepresentationHandler")


    def getRepresentationListName(self):
        
        out=[]
        for i in self.handler.getRepresentationList():
            out.append(str(i))
        return out
        
    def getRepresentationList(self):

        nlist = self.getRepresentationListName()

        out=[]
        for i in nlist:
            out.append(Representation(i))

        return out
    
    def duplicateRepresentation(self, base, new_name):

        return bool(self.handler.duplicateRepresentation(new_name, base))

class Representation:

    def __init__(self,name):
        self.bus = b.SessionBus()
        self.name = name

        try:
            self.rep = self.bus.get_object("org.GalaxExplorer", "/world/representations/" + name)
        except DBusException:
            raise ValueError("Unknown representation")

    def getType(self):

        return str(self.rep.getType())


    def getName(self):
        return self.name

    def setParameter(self,param,value):
        # the value must be packaged in a variant type        

        tn = type(value).__name__
        
        var_dict = { 'str' : b.String,
                     'int' : b.Int32,
                     'float' : b.Double,
                     'numpy.float64' : b.Double,
                     };
        
        try:
            x = var_dict[tn](value, variant_level=1)
        except KeyError:
            raise ValueError("Unknown adequate type for packaging in " +
                             "DBus variant (arrays are not supported)")
        
        return int(self.rep.setParameter(param, x)[0])

    def getParameterList(self):
        
        out=[]
        for i in self.rep.getParameterList():
            out.append(str(i))

        return out

    def getParameterType(self, paramName):
        
        t,r = self.rep.getParameterType(paramName)

        if int(r[0]) != 0:
            raise ValueError("Unknown parameter or invalid type")

        return str(t)

    def getParameterListAndType(self):

        out=[]
        for i in self.rep.getParameterList():
            out.append((str(i), self.getParameterType(i)))

        return out
