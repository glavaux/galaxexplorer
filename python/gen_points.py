import GalaxExplorer as ge
import numpy as np

dsh = ge.DataSetHandler()
ch = ge.Connectors()
rh = ge.Representation("BWPointRepresentation")
rh.setParameter("auxFieldName", "testaux")
rh.setParameter("useTransfer", "1")
rh.setParameter("distanceCorrection", "1")
rh.setParameter("pointSize", "40.0")

p = np.random.rand(3,1000000)*10.-5
a = np.sqrt((p**2).sum(axis=0)).reshape((1,p.shape[1]))
a /= a.max()
print a.shape

dsh.newData("test", p, attribute_names=np.array(["testaux"]), attribute_data=a)
dsh.waitData("test")

ch.create("test", "test", None, "BWPointRepresentation")
