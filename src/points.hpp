/*+
This is GalaxExplorer (./src/points.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef POINTSTATUS_H
#define POINTSTATUS_H

#include <stdlib.h>
#include <exception>

namespace GalaxExplorer
{
  class PointCastException: public std::exception  
  {
  public:
    virtual const char * what() const throw()
    {
      return "An invalid cast to a PointStatus has been detected";
    }
  };

  class BarePoint
  {
  public:
    float x, y, z;

    float operator[](int i) const
    {
      switch (i) {
      case 0:
        return x;
      case 1:
        return y;
      case 2:
        return z;
      }
      abort();
      return -1;
    }
  };

  class PointStatus: public BarePoint
  {
  public:
    float vx, vy, vz;
    double lum;
    int selected;
    qint8 velPresent, colPresent, namePresent;
    double radius;
    float color[3];
    QString name;

    PointStatus()
    {
    }
    
    PointStatus(const BarePoint& p) throw (PointCastException) {
       this->operator=(p);
    }

    PointStatus& operator=(const BarePoint& p)
    {
      const PointStatus& pbis = reinterpret_cast<const PointStatus&>(p);
      this->x = pbis.x;
      this->y = pbis.y;
      this->z = pbis.z;
      this->vx = pbis.vx;
      this->vy = pbis.vy;
      this->vz = pbis.vz;
      this->lum = pbis.lum;
      this->selected = pbis.selected;
      this->velPresent = pbis.velPresent;
      this->colPresent = pbis.colPresent;
      this->namePresent = pbis.namePresent;
      this->radius = pbis.radius;
      this->name = pbis.name;
      for (int i = 0; i < 3; i++)
	this->color[i] = pbis.color[i];

      return *this;
    }
  };
  
  typedef PointStatus DataPoint;
  typedef BarePoint DataBarePoint;

  

};

#endif
