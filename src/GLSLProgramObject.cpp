/*+
This is GalaxExplorer (./src/GLSLProgramObject.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
/*
 * Adapted from an original code by Louis Bavoil
   Copyright (c) NVIDIA Corporation. All rights reserved.
 */
#define NV_REPORT_COMPILE_ERRORS
//#define NV_REPORT_UNIFORM_ERRORS

#include "includeGL.hpp"
#include <boost/format.hpp>
#include <iostream>
#include <QTextStream>
#include <QString>
#include <QFile>
#include "GLSLProgramObject.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cassert>
#include "glerror.hpp"

using std::cout;
using std::endl;
using boost::format;
using namespace GalaxExplorer;

static QString readGLSLFile(QString fname)
  throw (ShaderCompileError)
{
  QFile code;
  QString fname_full;

  cout << "Reading file " << fname.toAscii().constData() << endl;
  fname_full = QString(":/shaders/").append(fname);
  code.setFileName(fname_full);
  if (!code.open(QIODevice::ReadOnly | QIODevice::Text))
   {
      fname_full = QString("shaders:") + fname;
      code.setFileName(fname_full);
      if (!code.open(QIODevice::ReadOnly | QIODevice::Text))
        {
          std::cout << "Impossible to open " << fname.toAscii().constData() << std::endl;
          throw ShaderCompileError(fname);
        }
   }

  QTextStream s(&code);
  QString buf;
  QString line;
  
  while (!(line = s.readLine()).isNull()) {
    cout << " -- line: " << line.toAscii().constData() << endl;
    if (line.startsWith("#include")) {
      // Include a file here
      int iQuote, eQuote;
      cout << " --> file inclusion " << endl;
      
      line = line.simplified();
      
      iQuote = line.indexOf("\"");
      if (iQuote < 0)
        throw ShaderCompileError(fname);
      eQuote = line.indexOf("\"", iQuote+1);
      
      cout << format(" -->  iQuote = %d, eQuote = %d, size = %d") % iQuote % eQuote % line.size() << endl;
      if (eQuote != line.size()-1)
        throw ShaderCompileError(fname);
        
      line = line.mid(iQuote+1, eQuote-iQuote-1);
      cout << " --> Going to include '" << line.toAscii().constData() << "'" << endl;
      buf += readGLSLFile(line);
    } else {
      // Insert it into buf
      buf += line + "\n";
    }
  }
  return buf;
}

static GLuint loadAndCompileShader(GLuint type, std::string fname)
  throw (ShaderCompileError)
{  
  QString buf = readGLSLFile(QString(fname.c_str()));
  
  QByteArray bArra = buf.toAscii();
  const char *stringbuf_buf = bArra.data();

//  if (!GLEE_VERSION_2_0)
//    throw ShaderCompileError(QString(fname.c_str()));

  GLuint shader;
  GLint isCompiled;
  
  shader = glCreateShader(type);
  glShaderSource(shader, 1, &stringbuf_buf, 0);
  glCompileShader(shader);
  glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
  if (!isCompiled)
   {
     char *infoBuf;
     GLint infoLen;

     glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
     infoBuf = new char[infoLen];
     
     glGetShaderInfoLog(shader, infoLen, &infoLen, infoBuf);

     std::cout << "== Result of compilation of " << fname << std::endl;
     std::cout << infoBuf << std::endl;
     delete[] infoBuf;
     throw ShaderCompileError(QString(fname.c_str()));
   }
  return shader;
}

GLSLProgramObject::GLSLProgramObject() throw ():
  _progId(0) 
{
  texunit_allocator = new GLSLSimpleAllocator(0);
}

GLSLProgramObject::GLSLProgramObject(const GLSLProgramObject& p) throw()
  : _progId(0), texunit_allocator(0)
{
  operator=(p);
}

GLSLProgramObject::~GLSLProgramObject() throw() 
{
  destroy();
  delete texunit_allocator;
}

void GLSLProgramObject::setAllocator(GLSLTextureUnitAllocator *allocator)
{
  delete texunit_allocator;
  texunit_allocator = allocator;
}

void GLSLProgramObject::destroy()
  throw()
{
  if (_progId != 0) {
    glDeleteProgram(_progId);
    _progId = 0;
    texunit_allocator->free_all();
    _vertexShaders.clear();
    _fragmentShaders.clear();
    _geometryShaders.clear();
    _texunitMap.clear();
  }
}

void GLSLProgramObject::link()
{
  _progId = glCreateProgram();
  
  for (unsigned i = 0; i < _vertexShaders.size(); i++) {
    glAttachShader(_progId, _vertexShaders[i].getId());
  }
  
  for (unsigned i = 0; i < _fragmentShaders.size(); i++) {
    glAttachShader(_progId, _fragmentShaders[i].getId());
  }

  for (unsigned i = 0; i < _geometryShaders.size(); i++) {
    glAttachShader(_progId, _geometryShaders[i].getId());
  }
  
  glLinkProgram(_progId);
  
  GLint success = 0;
  glGetProgramiv(_progId, GL_LINK_STATUS, &success);
  
  if (!success) {
    char temp[1024];
    glGetProgramInfoLog(_progId, 1024, NULL, temp);
    printf("Failed to link program:\n%s\n", temp);
    abort();
  }
}

void GLSLProgramObject::bind()
{
  glUseProgram(_progId);
}

void GLSLProgramObject::unbind()
{
  glUseProgram(0);
}


void GLSLProgramObject::setUniform(const std::string& name, const GLfloat* val, int count)
{
  GLint id = glGetUniformLocation(_progId, name.c_str());
  if (id == -1) {
#ifdef NV_REPORT_UNIFORM_ERRORS
    std::cerr << m_vName << std::endl << m_fName << ":" << std::endl;
    std::cerr << "Warning: Invalid uniform parameter " << name << std::endl;
#endif
    return;
  }
  switch (count) {
  case 1:
    glUniform1fv(id, 1, val);
    break;
  case 2:
    glUniform2fv(id, 1, val);
    break;
  case 3:
    glUniform3fv(id, 1, val);
    break;
  case 4:
    glUniform4fv(id, 1, val);
    break;
  }
}

void GLSLProgramObject::setIntUniform(const std::string& name, const GLint* val, int count)
{
  GLint id = glGetUniformLocation(_progId, name.c_str());
  if (id == -1) {
#ifdef NV_REPORT_UNIFORM_ERRORS
    std::cerr << m_vName << std::endl << m_fName << ":" << std::endl;
    std::cerr << "Warning: Invalid uniform parameter " << name << std::endl;
#endif
    return;
  }
  switch (count) {
  case 1:
    glUniform1iv(id, 1, val);
    break;
  case 2:
    glUniform2iv(id, 1, val);
    break;
  case 3:
    glUniform3iv(id, 1, val);
    break;
  case 4:
    glUniform4iv(id, 1, val);
    break;
  }
}


void GLSLProgramObject::setMatrixUniform(std::string name, const GLfloat* val, int rank)
{
  GLint id = glGetUniformLocation(_progId, name.c_str());
  if (id == -1) {
#ifdef NV_REPORT_UNIFORM_ERRORS
    std::cerr << m_vName << std::endl << m_fName << ":" << std::endl;
    std::cerr << "Warning: Invalid uniform parameter " << name << std::endl;
#endif
    return;
  }
  switch (rank) {
  case 2:
    glUniformMatrix2fv(id, 1, 0, val);
    break;
  case 3:
    glUniformMatrix3fv(id, 1, 0, val);
    break;
  case 4:
    glUniformMatrix4fv(id, 1, 0, val);
    break;
  }
  CHECK_GL_ERRORS;
}

void GLSLProgramObject::setTextureUnit(const std::string& texname, int texunit)
{
  GLint linked;
  glGetProgramiv(_progId, GL_LINK_STATUS, &linked);
  if (linked != GL_TRUE) {
    std::cerr << "Error: setTextureUnit needs program to be linked." << std::endl;
    exit(1);
  }
  GLint id = glGetUniformLocation(_progId, texname.c_str());
  if (id == -1) {
#ifdef NV_REPORT_UNIFORM_ERRORS
    std::cerr << "Warning: Invalid texture " << texname << std::endl;
#endif
    return;
  }
  CHECK_GL_ERRORS;
  glUseProgram(_progId);
  glUniform1i(id, texunit);
  CHECK_GL_ERRORS;
}

void GLSLProgramObject::bindTexture(GLenum target, const std::string& texname, GLuint texid)
{
  GLuint texunit;

  if (_texunitMap.find(texname) == _texunitMap.end())
    _texunitMap[texname] = texunit_allocator->alloc();

  texunit = _texunitMap[texname];
  
  glActiveTexture(GL_TEXTURE0 + texunit);
  glBindTexture(target, texid);
  setTextureUnit(texname, texunit);
  CHECK_GL_ERRORS;
}

GLint GLSLProgramObject::getAttribLocation(const std::string& attribname)
{
  return glGetAttribLocation(_progId, attribname.c_str());
}

void GLSLProgramObject::pushShaders(GLSLProgramObject& p)
{
  if (_progId != 0)
    {
      std::cerr << "Program has already been linked. Abort" << std::endl;
      abort();
    }
  
  _vertexShaders.insert(_vertexShaders.end(),
			p._vertexShaders.begin(), p._vertexShaders.end());
  _fragmentShaders.insert(_fragmentShaders.end(),
			  p._fragmentShaders.begin(), p._fragmentShaders.end());
  _geometryShaders.insert(_geometryShaders.end(),
			  p._geometryShaders.begin(), p._geometryShaders.end());
}

void GLSLProgramObject::attachShader(GLSLShaderObject& o)
{
  switch (o.getType()) {
  case GL_VERTEX_SHADER:
    _vertexShaders.push_back(o);
    break;
  case GL_FRAGMENT_SHADER:
    _fragmentShaders.push_back(o);
    break;
    /*
  case GL_GEOMETRY_SHADER:
    _geometryShaders.push_back(o);
    break;*/
  default:
    abort();
  }
}
  
GLSLProgramObject& GLSLProgramObject::operator=(const GLSLProgramObject& p)
{
  if (_progId != 0)
    {
      std::cerr << "Program has already been linked. Abort" << std::endl;
      abort();
    }
  if (texunit_allocator)
    delete texunit_allocator;

  _vertexShaders = p._vertexShaders;
  _fragmentShaders = p._fragmentShaders;
  _geometryShaders = p._geometryShaders;
  texunit_allocator = p.texunit_allocator->duplicate();
  return *this;
}


GLSLShaderObject::GLSLShaderObject()
  throw()
{
  shaderId = 0;
  used = 0;
}

GLSLShaderObject::~GLSLShaderObject()
  throw()
{
  if (used != 0)
    {
      (*used)--;
      if ((*used) == 0)
	release();
    }
}

void GLSLShaderObject::release()
{
  delete used;
  glDeleteShader(shaderId);
}


GLSLShaderObject::GLSLShaderObject(const std::string& filename, GLuint type)
  throw (ShaderCompileError)
{
  assert((type == GL_VERTEX_SHADER) || (type == GL_FRAGMENT_SHADER));/* || (type == GL_GEOMETRY_SHADER));*/

  this->type = type;
  shaderId = loadAndCompileShader(type, filename);

  used = new int;
  *used = 1;
}

GLSLTextureUnitAllocator::~GLSLTextureUnitAllocator()
{
}
