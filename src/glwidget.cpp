/*+
This is GalaxExplorer (./src/glwidget.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include "includeGL.hpp"
#include <fstream>
#include <sstream>
#include <cassert>
#include <iostream>
#include <QtGui>
#include <QtOpenGL>
#include <list>
#include <cmath>
#include <algorithm>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_linalg.h>

#include "glwidget.hpp"
#include "representations/dataRepresentation.hpp"
#include "points.hpp"
#include "datasets.hpp"
#include "gltools.hpp"
#include "renderers/renderer.hpp"
#include "window.hpp"
#include "glerror.hpp"
#include "object_helper.hpp"
#include "back_grid.hpp"
#include "renderers/render_vs.hpp"

using namespace std;
using namespace GalaxExplorer;

static void normalizeAngle(float *angle)
{
  while (*angle < 0)
    *angle += 360;
  while (*angle > 360)
    *angle -= 360;
}

static GLRendererAutoFactory<GLBasicRenderer> gs_basicRendererFactory("0");

GalaxExplorer::UniverseWidget::UniverseWidget(Window *parent, QGLWidget *glw, QGraphicsView *v)
  : glWindow(parent), g_view(v), glWidget(glw)
{
  object = 0;
  disabledPainting = false;
  gl_initialized = false;
  
  crossSize = 0.2;
  
  fractionVeloc = 1.0;
  travelFraction = 100.0;
  
  vs = new ViewState(this);
  connect(vs,SIGNAL(colorUpdated()),this,SLOT(updateBgColor()));
  connect(vs,SIGNAL(stateUpdated()),this,SLOT(update()));
  connect(vs,SIGNAL(cubeUpdated()),this,SLOT(updateCube()));
  connect(this,SIGNAL(sceneRectChanged(const QRectF&)), this, SLOT(resizeScene(const QRectF&)));

  capturing = false;

  renderer = 0;

  fbWidth = 500;
  fbHeight = 500;  

  projection = PerspectiveProj;

  r_factory = parent->getRendererFactory();
  if (r_factory == 0)
    {
      r_factory = &gs_basicRendererFactory;
    }

  glOptions = new QPushButton(tr("Options"));
  glOptions->setWindowOpacity(0.8);
  addWidget(glOptions);

  createLightSource();

  // ================================
  // HACK FOR MENU POSITION AND SCENE
  menuOpened = false;

  grid_builder = boost::shared_ptr<GridBuilder>(new ScalingCube(UNIVERSE_BOXSIZE));
}

void UniverseWidget::setGridBuilder(boost::shared_ptr<GridBuilder> builder)
{
  grid_builder = builder;
  updateCube();
}

void UniverseWidget::setLightSourceVisible(bool v)
{
  lightItem->setVisible(v);
}

void UniverseWidget::setOptionMenu(QMenu *menu)
{
  glOptions->setMenu(menu);
  // ================================
  // HACK FOR MENU POSITION AND SCENE
  disconnect(glOptions, SIGNAL(pressed()), 0, 0);
  connect(glOptions, SIGNAL(pressed()), this, SLOT(handleOptionsMenu()));  
}

void UniverseWidget::handleOptionsMenu()
{
  if (menuOpened)
    return;

  QMenu *menu = glOptions->menu();
  QSize menuSize = menu->sizeHint();
  QRect r = glOptions->geometry();
  QPoint tl = (r.topLeft());
  QPoint globalPos = glWidget->mapToGlobal(tl);
  int x, y;

  x = globalPos.x();
  y = globalPos.y();

  if (globalPos.y() + r.height() + menuSize.height() <= QApplication::desktop()->availableGeometry(glWidget).height()) {
    y += r.height();
  } else {
    y -= menuSize.height();    
  }

  QPoint menuPos(x, y);

  glOptions->setDown(true);
  menu->exec(menuPos);
  menuOpened = false;
  glOptions->setDown(false);
}

void UniverseWidget::updateCube()
{
  grid_builder->make(*vs, gridChain);
  update();
}

bool UniverseWidget::restartRenderer()
{
  GLRenderer *new_renderer;
  GLRendererFactory *new_factory = glWindow->getRendererFactory();

  glWidget->makeCurrent();

  try
    {
      new_renderer = new_factory->newRenderer(*this);
    }
  catch (const RendererInitFailure& e)
    {
      cerr << "New renderer invalid. Staying with the old one." << endl;
      return false;
    }

  if (!gl_initialized)
    {
      delete new_renderer;
      r_factory = new_factory;

      return true;
    }

  for (GLConnectorVector::iterator iter = connectors.begin(); iter != connectors.end(); iter++)
    {
      iter->unprepare();
    }

  // More tricky
  // Destroy renderer
  delete renderer;
  renderer = new_renderer;
  r_factory = new_factory;
  renderer->notifySize(fbWidth, fbHeight);
  setProjection(projection);

  // rebuild skyboxes
  QList<SkyBox *>::iterator iter = skybox.begin();
  while (iter != skybox.end())
    {
      (*iter)->rebuildTextures();
      ++iter;
    }

  // Recompile connections
  for (GLConnectorVector::iterator iter = connectors.begin(); iter != connectors.end(); iter++)
    {
      if (!capturing || iter->isShown())
        compileConnector(*iter);
    }

  // Done.
  glWidget->doneCurrent();
  return true;
}

UniverseWidget::~UniverseWidget()
{
  glWidget->makeCurrent();

  QList<SkyBox *>::iterator iter = skybox.begin();
  while (iter != skybox.end())
    {
      delete (*iter);
      ++iter;
    }

  skybox.clear();

  if (renderer != 0)
    delete renderer;

  glWidget->doneCurrent();
}

QSize UniverseWidget::minimumSizeHint() const
{
  return QSize(50, 50);
}

QSize UniverseWidget::sizeHint() const
{
  return QSize(400, 400);
}

void UniverseWidget::capture(const QString& fname, int w, int h)
{ 
  GLint gg;
  int oldw = fbWidth, oldh = fbHeight;
  int bpl = (w+3)&~3; // There is a 4 bytes alignment in OpenGL
  bool b2 = disabledPainting;

  disabledPainting = false;

  glGetIntegerv(GL_PACK_ALIGNMENT, &gg);
  cout << "gg=" << gg << endl;

  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glPixelStorei(GL_PACK_ROW_LENGTH, bpl);

  glWidget->makeCurrent();
  fbWidth = w;
  fbHeight = h;
  getRenderer()->beginSnapshot(w, h);
  uchar *pixels = new uchar[3*bpl*h];
  glReadPixels(0,0,w,h,GL_RGB,GL_UNSIGNED_BYTE, pixels);
  QImage img(pixels, w, h, 3*bpl, QImage::Format_RGB888); 
  img = img.mirrored();
  getRenderer()->endSnapshot();

  img.save(fname, "PNG");
  delete[] pixels;
  fbWidth = oldw;
  fbHeight = oldh;

  disabledPainting = b2;
}

void UniverseWidget::setXRotation(float angle)
{
  vs->setRotationX(angle);
}

void UniverseWidget::setYRotation(float angle)
{
  vs->setRotationY(angle);
}

void UniverseWidget::setZRotation(float angle)
{
  vs->setRotationZ(angle);
}

void UniverseWidget::setTravel(int percent)
{
  travelFraction = 100.0 * ((GLdouble)percent / 500.0);
  vs->setDistance(travelFraction);
}

void UniverseWidget::setActiveConnectors(const ConnectorVector& v)
{
  connectors.clear();
  copy(v.begin(), v.end(), back_inserter(connectors));
  recompileAll();
}

void UniverseWidget::compileConnector(GLConnector& connector)
{
  executeConnection(*this, connector);
}


void UniverseWidget::recompileConnection(int j)
{
  if (j < 0 || j >= connectors.size())    
    {
      qWarning("UniverseWidget: Invalid connector %d", j);
      return;
    }

  compileConnector(connectors[j]);
  update();
}


void UniverseWidget::lazyInitialize()
{ 
  try
    {
      renderer = r_factory->newRenderer(*this);
    }
  catch (const RendererInitFailure& e)
    {
      qFatal("Failed to initialize renderer");
    }
   
  setProjection(projection);

  grid_builder->make(*vs, gridChain);
    
  for (GLConnectorVector::iterator iter = connectors.begin(); iter != connectors.end(); iter++)
    {
      if (!capturing || iter->isShown())
        compileConnector(*iter);
    }

  renderer->notifySize(fbWidth, fbHeight);

  gl_initialized = true;

  forceUpdateLightSource = true;

  emit gl_ready();
}

QList<SkyBox *>& UniverseWidget::getSkyBoxList()
{
  return skybox;
}

void UniverseWidget::drawScene()
{
  // Draw the scene
  //  cout << "-- Drawing scene." << endl;
  if (vs->axisShown())
   {
      glColor4f(1.0,1.0,1.0,1.0);
      cout << "-- Drawing grid." << endl;
      if (renderer->usingShaders())
        renderer->useDefaultShader();
      gridChain.draw(); 
   }
  QList<SkyBox *>::iterator iter = skybox.begin();
  while (iter != skybox.end())
    {
      (*iter)->draw(renderer);
      ++iter;
    }

  for (GLConnectorVector::iterator iter = connectors.begin(); iter != connectors.end(); iter++)
    {
      if (iter->isShown())
	drawConnection(*this, *iter);
    }
  //  cout << "-- Finished drawing scene" << endl;
}

void UniverseWidget::setProjection(UniverseProjection proj)
{
  RenderViewState rvs;
  
  if (renderer == 0)
    return;

  projection = proj;

  float aspect = float(fbWidth)/fbHeight;
  
  switch (projection)
    {
    case PerspectiveProj:
      renderer->viewState().setFOV(vs->getFOV(), aspect, 1., 300.0);
      break;
    case OrthographicProj:
      {
        double r = 50*vs->getDistance()/100.0;
        renderer->viewState().setOrtho(-r*aspect,r*aspect,-r,r, -vs->getDistance()*sqrt(3.0)*0.5, 100.0);
        break;
      }
    }

  update();
}

void UniverseWidget::drawBackground(QPainter *painter, const QRectF& rect)
{
  if (painter->paintEngine()->type() != QPaintEngine::OpenGL &&
      painter->paintEngine()->type() != QPaintEngine::OpenGL2) {
    qWarning("OpenGLScene: drawBackground needs a QGLWidget to be set as viewport on the graphics view");
    return;
  }

  QGraphicsScene::drawBackground(painter, rect);

  painter->beginNativePainting();
  CHECK_GL_ERRORS;

  if (!gl_initialized)
    lazyInitialize();
  
  paintScene();

  painter->endNativePainting();
}

void UniverseWidget::createLightSource()
{
  QRadialGradient gradient(20, 20, 20, 20, 20);
  gradient.setColorAt(0.2, Qt::yellow);
  gradient.setColorAt(1, Qt::transparent);
  
  lightItem = new QGraphicsRectItem(0, 0, 40, 40);
  lightItem->setPen(Qt::NoPen);
  lightItem->setBrush(gradient);
  lightItem->setFlag(QGraphicsItem::ItemIsMovable);
  lightItem->setPos(100, 100);

  addItem(lightItem);
}

void UniverseWidget::paintScene()
{
  if (disabledPainting)
    return;

  RenderViewState::VSVector3 UX, UY, UZ;
  RenderViewState& rvs = renderer->viewState();
  
  UX << 1,0,0;
  UY << 0,1,0;
  UZ << 0,0,1;
  
  rvs.resetModelView();
  rvs.translate(0,0, -vs->getDistance());
  rvs.rotate(vs->getRotationX(), UX);
  rvs.rotate(vs->getRotationY(), UY);
  rvs.rotate(vs->getRotationZ(), UZ);
  {
    float x, y, z;
    vs->getCenter(x,y,z);
    rvs.translate(x,y,z);
  }
  

  if (forceUpdateLightSource)
    updateLightSourcePosition();
  else
    recoverLightSourcePosition();

  // Paint everything
  renderer->paint();
  
  CHECK_GL_ERRORS;
}

void UniverseWidget::recoverLightSourcePosition()
{
  Point3d p = renderer->getLightSource();

  RenderViewState rvs = renderer->viewState();
  RenderViewState::VSVector3 light_pos;
  RenderViewState::VSVector4 screenlight_pos;
  
  rvs.resetModelView();

  light_pos << -p.x,p.y,p.z;
  screenlight_pos = rvs.positionToScreen4(light_pos);
  screenlight_pos(0) = lightItem->x();
  screenlight_pos(1) = lightItem->y();
  light_pos = rvs.screen4ToPosition(screenlight_pos);  
  
  p.x = -light_pos(0);
  p.y = light_pos(1);
  p.z = light_pos(2);

  renderer->setLightSource(p);
}

void UniverseWidget::updateLightSourcePosition()
{
  GLdouble wx, wy, wz;
  Point3d p = renderer->getLightSource();

  RenderViewState rvs = renderer->viewState();
  RenderViewState::VSVector3 vs_p;
  
  rvs.resetModelView();
  
  vs_p << -p.x,p.y,p.z;
  vs_p = rvs.positionToScreen(vs_p);
  
  lightItem->setPos(vs_p(0), vs_p(1));
  forceUpdateLightSource = false;
}

void UniverseWidget::resizeScene(const QRectF& rect)
{
  fbWidth = rect.width();
  fbHeight = rect.height();

  QSize s = glOptions->size();

  glOptions->move(0, fbHeight-s.height());

  // Not yet active renderer
  if (renderer == 0)
    return;
  
  renderer->notifySize(fbWidth, fbHeight);

}

void UniverseWidget::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
  QGraphicsScene::mousePressEvent(event);
  if (event->isAccepted())
    return;

  event->accept();

  lastPos = event->scenePos();
  renderer->setInteractiveMode(true);
  refresh();
}

void UniverseWidget::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
  QGraphicsScene::mouseReleaseEvent(event);
  if (event->isAccepted())
    return;

  event->accept();
  renderer->setInteractiveMode(false);
  refresh();
}

void UniverseWidget::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
  QGraphicsScene::mouseMoveEvent(event);
  if (event->isAccepted())
    return;

  QPointF delta = event->scenePos() - event->lastScenePos();

  if (event->buttons() & Qt::RightButton) {
    setXRotation(vs->getRotationX() + 0.5 * delta.x());
    setYRotation(vs->getRotationY() + 0.5 * delta.y());
  } else if (event->buttons() & Qt::LeftButton) {
    setXRotation(vs->getRotationX() + 0.5 * delta.y());
    setZRotation(vs->getRotationZ() + 0.5 * delta.x());
  }
  lastPos = event->pos();
  event->accept();
}

void UniverseWidget::showConnector(const QString& name)
{
  for (GLConnectorVector::iterator iter = connectors.begin(); iter != connectors.end(); iter++)
    {
      if (iter->getName() == name)
        iter->setShow(true);
    }
}

void UniverseWidget::hideConnector(const QString& name)
{
  for (GLConnectorVector::iterator iter = connectors.begin(); iter != connectors.end(); iter++)
    {
      if (iter->getName() == name)
        iter->setShow(false);
    }
}

void UniverseWidget::recompileAll()
{
  for (GLConnectorVector::iterator iter = connectors.begin(); iter != connectors.end(); iter++)
    {
      compileConnector(*iter);
    }
  update();
}

void UniverseWidget::updateBgColor()
{
  update();
}

void UniverseWidget::setState(const QObject *o)
{
  const ViewState *new_vs = qobject_cast<const ViewState *>(o);
  if (new_vs == 0)
    return;

  *vs = *new_vs;

  update();
}

ViewState *UniverseWidget::getState()
{
  return vs;
}

ViewState::ViewState(QObject *parent)
  : QObject(parent)
{
  QSettings settings;

  uwidget = qobject_cast<UniverseWidget *>(parent);
  rx = ry = rz = distance = 0;
  centerX = centerY = centerZ = 0;
  shownAxis = settings.value("axisPresent", true).toBool();
  bgColor = settings.value("backgroundColor", QColor::fromRgbF(0.0,0.0,0.0)).value<QColor>();  
  multiplier = 1.0;
  fov = 60.0;
}

ViewState::~ViewState()
{
  QSettings settings;

  settings.setValue("backgroundColor", bgColor);
  settings.setValue("axisPresent", shownAxis);
}

void ViewState::saveState(QMap<QString,QVariant>& qmap)
{
  qmap["rx"] = rx;
  qmap["ry"] = ry;
  qmap["rz"] = rz;
  qmap["centerX"] = centerX;
  qmap["centerY"] = centerY;
  qmap["centerZ"] = centerZ;
  qmap["distance"] = distance;
  qmap["bgColor"] = bgColor;
  qmap["axisShown"] = shownAxis;
  qmap["distanceMultiplier"] = multiplier;
}

void ViewState::loadState(const QMap<QString,QVariant>& qmap)
{
  rx = qmap["rx"].toDouble();
  ry = qmap["ry"].toDouble();
  rz = qmap["rz"].toDouble();
  centerX = qmap["centerX"].toDouble();
  centerY = qmap["centerY"].toDouble();
  centerZ = qmap["centerZ"].toDouble();
  distance = qmap["distance"].toDouble();
  bgColor = qmap["bgColor"].value<QColor>();
  shownAxis = qmap["axisShown"].toBool();
  multiplier = qmap["distanceMultiplier"].toDouble();
  emit stateUpdated();
}

void ViewState::setRotationCenter(float x, float y, float z)
{
  centerX = x;
  centerY = y;
  centerZ = z;

  emit stateUpdated();
}

void ViewState::moveCube(float x, float y, float z)
{
  cubeX = x;
  cubeY = y;
  cubeZ = z;
  
  emit cubeUpdated();
}

void ViewState::setRotationX(float rx)
{
  this->rx = rx;
  normalizeAngle(&this->rx);
  emit stateUpdated();
}

void ViewState::setRotationY(float ry)
{
  this->ry = ry;
  normalizeAngle(&this->ry);
  emit stateUpdated();
}

void ViewState::setRotationZ(float rz)
{
  this->rz = rz;
  normalizeAngle(&this->rz);
  emit stateUpdated();
}

void ViewState::setDistance(float f)
{
  this->distance = f;
  emit stateUpdated();
}

void ViewState::setBackgroundColor(const QColor& col)
{
  bgColor = col;
  emit colorUpdated();
}

void ViewState::setMultiplier(float m)
{
  multiplier = m;
}

void ViewState::getCameraPosition(float position[3])
{
  RenderViewState::VSVector3 p = uwidget->getRenderer()->viewState().getCameraPosition();
  for (int i = 0; i < 3; i++)
    position[i] = p(i);
}

void ViewState::getViewMatrix(float matrix[4][4])
{
  GLfloat mat[4][4];

  uwidget->getRenderer()->viewState().getGLProjectionMatrix(&mat[0][0]);

  for (int i = 0; i < 4; i++)
    {
      for (int j = 0; j < 4; j++)
        {
          matrix[i][j] = mat[j][i];
        }
    }
}

void ViewState::getRotationMatrix(float matrix[4][4])
{
  RenderViewState::VSMatrix3 mat = uwidget->getRenderer()->viewState().getRotationMatrix();
  for (int i = 0; i < 4; i++)
    {
      for (int j = 0; j < 4; j++)
        {
          matrix[i][j] = mat(i,j);
        }
    }
}

void UniverseWidget::updateLight()
{
  forceUpdateLightSource = true;
}
