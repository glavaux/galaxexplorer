/*+
This is GalaxExplorer (./src/connectorDialog.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <cmath>
#include <QtDebug>
#include <QtGui>
#include "connectorDialog.hpp"
#include "inits.hpp"
#include "window.hpp"
#include "glconnect.hpp"
#include "ui_newSelector.h"
#include "createEditRepresentation.hpp"
#include <iostream>

using namespace std;
using namespace GalaxExplorer;


SelectorPropertyTracking::~SelectorPropertyTracking()
{
} 

void SelectorPropertyTracking::modified()
{
  emit propertyModified(gui, id);
}

SelectorPropertyEditing::SelectorPropertyEditing(DataSetSelector *s)
  : selector(s)
{
}

SelectorPropertyEditing::~SelectorPropertyEditing()
{
}

void SelectorPropertyEditing::propertyModified(QObject *gui, int id)
{
  const QMetaObject *meta = selector->metaObject();
  QMetaProperty metaprop = meta->property(id);
  QVariant variant;

  switch (metaprop.type())
    {
    case QVariant::String:
      {
	QLineEdit *line = qobject_cast<QLineEdit *>(gui);

	variant = QVariant(line->text());
	break;
      }
    case QVariant::Double:
      {
	QDoubleSpinBox *box = qobject_cast<QDoubleSpinBox *>(gui);

	variant = QVariant(box->value());
	break;
      }
    case QVariant::Bool:
      {
	QCheckBox *box = qobject_cast<QCheckBox *>(gui);

	variant = QVariant(box->checkState() == Qt::Checked);
	break;
      }
    case QVariant::Int:
      {
        QLineEdit *line = qobject_cast<QLineEdit *>(gui);

        variant = QVariant(line->text().toInt());
        break;
      }
    default:
      qFatal("Unexpected property type");
      break;	
    }
  
  selector->setProperty(metaprop.name(), variant);
  emit modified(selector);
}

void SelectorPropertyEditing::addTracking(SelectorPropertyTracking *tracking)
{
  connect(tracking, SIGNAL(propertyModified(QObject*,int)), this, SLOT(propertyModified(QObject*,int)));
  tracking->setParent(this);
}

ConnectorDialog::ConnectorDialog(Window *parent, AllDataSelector *selectors)
  : QDialog(parent)
{
  this->selectors = selectors;

  setupBaseUI();
}

void ConnectorDialog::setupBaseUI(const Connector *oldConnector)
{
  int cur;

  connectorUi.setupUi(this);    
  connectorUi.dataRepresentationList->addItems(g_allRepresentations.keys());
  
  cur = 0;
  for (DataSourceMap::iterator iter = g_allDataSources.begin(); 
       iter != g_allDataSources.end(); ++iter)
    {
      DataSource *ds = qobject_cast<DataSource *>(*iter);

      connectorUi.dataSetList->addItem(ds->getName());
      if (oldConnector != 0 && oldConnector->getConnector()->getDataSource()->getName() == ds->getName())
	connectorUi.dataSetList->setCurrentRow(cur);

      cur++;
    }
  
  cur = 0;
  for (AllDataSelector::iterator iter = selectors->begin(); iter != selectors->end(); ++iter)
    {
      connectorUi.dataSelectorList->addItem((*iter)->getName());

      if (oldConnector != 0)
	{
	  if (const SetConnector *sc = qobject_cast<const SetConnector *>(oldConnector->getConnector()))
	    {
	      if (sc->getSelector()->getName() == (*iter)->getName())
		connectorUi.dataSelectorList->setCurrentRow(cur);
	    }
	}

      cur++;
    }

  cur = 0;
  QString connRep = (oldConnector != 0) ? oldConnector->getConnector()->getRepresentation()->getName() : "";
  for (DataRepresentationMap::iterator iter = g_allRepresentations.begin();
       iter != g_allRepresentations.end();
       ++iter)
    {
      if (oldConnector != 0 & connRep == (*iter)->getName())
	connectorUi.dataRepresentationList->setCurrentRow(cur);
      
      cur++;
    }

  connect(connectorUi.createRep, SIGNAL(clicked(bool)), this, SLOT(createRepDialog()));
  connect(connectorUi.createSelector, SIGNAL(clicked(bool)), this, SLOT(createSelectorDialog()));
  connect(connectorUi.editSelector, SIGNAL(clicked(bool)), this, SLOT(editSelector()));
}

ConnectorDialog::ConnectorDialog(Window *parent, AllDataSelector *selectors, const Connector& oldConnector)
  : QDialog(parent)
{
  this->selectors = selectors;

  setupBaseUI(&oldConnector);

  connectorUi.connectorName->insert(oldConnector.getName());
}

ConnectorDialog::~ConnectorDialog()
{
}

Connector *ConnectorDialog::fullExecution()
{
  QString cName;
  QString dataSetName;
  QString selectorName;
  QString representationName;

  do
    {
      if (!exec())
	return 0;

      cName = connectorUi.connectorName->displayText();
      QListWidgetItem *dataSetItem = connectorUi.dataSetList->currentItem();
      QListWidgetItem *dataSelItem = connectorUi.dataSelectorList->currentItem();
      QListWidgetItem *dataRepItem = connectorUi.dataRepresentationList->currentItem();

      if (cName == "")
	{
	  QMessageBox::warning(this, tr("Warning"), tr("No name was given."));
	  continue;
	}

      if (dataSetItem == 0 || dataSelItem == 0 || dataRepItem == 0)
	{
	  QMessageBox::warning(this, tr("Warning"), tr("All lists must have hold a selected item"));
	  continue;
	}

      dataSetName = dataSetItem->text();
      selectorName = dataSelItem->text();
      representationName = dataRepItem->text();
      
      // At the moment this is efficient but in future ?
      if (qobject_cast<DataSet *>(g_allDataSources[dataSetName]))
	{
	  if (qobject_cast<DataSetCapableRepresentation *>(g_allRepresentations[representationName]) == 0)
	    {
	      QMessageBox::warning(this, tr("Warning"), tr("Data source and representation are incompatible"));
	      continue;
	    }
	}
      if (qobject_cast<DataGrid *>(g_allDataSources[dataSetName]))
	{
	  if (qobject_cast<DataGridCapableRepresentation *>(g_allRepresentations[representationName]) == 0)
	    {
	      QMessageBox::warning(this, tr("Warning"), tr("Data source and representation are incompatible"));
	      continue;
	    }
	}

     break;
    }
  while (1);
  
  if (DataSet *ds = qobject_cast<DataSet *>(g_allDataSources[dataSetName]))
    {
      return 
	new Connector(buildConnector(representationName, cName, ds,
				     (*selectors)[selectorName]));
    }
  if (DataGrid *dg = qobject_cast<DataGrid *>(g_allDataSources[dataSetName]))
    {
      return 
	new Connector(buildConnector(representationName, cName, dg));
    }
  return 0;
}

void ConnectorDialog::createSelectorDialog()
{
  Ui::NewSelectorDialog c;
  QDialog *dlg = new QDialog();

  c.setupUi(dlg);

  for (AllSelectors::iterator iter = g_allDataSelectors.begin();
       iter != g_allDataSelectors.end();
       ++iter)
    c.selectorTypeList->addItem((*iter)->getName());
 
  QString newName;
  QString selected;

  do
    {
      if (!dlg->exec())
	{
	  delete dlg;
	  return;
	}  
      if (c.newSelectorName->displayText() == "")
	{
	  QMessageBox::warning(dlg, tr("Warning"), tr("No name was given."));
	  continue;
	}
      if (c.selectorTypeList->currentItem() == 0)
	{
	  QMessageBox::warning(dlg, tr("Warning"), tr("No item was selected."));
	  continue;
	}
      newName = c.newSelectorName->displayText();
      selected = c.selectorTypeList->currentItem()->text();
      if ((*selectors)[newName] != 0)
	{
	  QMessageBox::warning(dlg, tr("Warning"), tr("A selector already exists under this name."));
	  continue;	  
	}
      break;
    }
  while (1);

  cout << "NewName=" << qPrintable(selected) << endl;

  (*selectors)[newName] = g_allDataSelectors[selected]->newSelector();
  (*selectors)[newName]->setName(newName);

  QListWidget *listSelectors = connectorUi.dataSelectorList;

  listSelectors->clear();
  for (AllDataSelector::iterator iter = selectors->begin(); iter != selectors->end(); ++iter)
    listSelectors->addItem((*iter)->getName());

  delete dlg;
}

void ConnectorDialog::createRepDialog()
{
  CreateRepresentationDialog dlg;

  dlg.run();

  connectorUi.dataRepresentationList->clear();
  connectorUi.dataRepresentationList->addItems(g_allRepresentations.keys());
}

static void editSelectorBox(ConnectorDialog *connections, DataSetSelector *s)
{
  QDialog *dlg = new QDialog();
  QVBoxLayout *vbox = new QVBoxLayout;
  const QMetaObject *meta = s->metaObject();

  dlg->setWindowTitle("Selector " + s->property("name").toString());

  SelectorPropertyEditing *edition = new SelectorPropertyEditing(s);

  QTableWidget *table = new QTableWidget(meta->propertyCount()-2,2);
  
  dlg->setWindowTitle(QObject::tr("Edit a selector"));
  
  int tableId = 0;
  for (int i = 0; i < meta->propertyCount(); i++)    
    {
      QMetaProperty metaprop = meta->property(i);
      SelectorPropertyTracking *tracking;
      

      if (metaprop.name() == QString("objectName") || metaprop.name() == QString("name"))
	continue;

      table->setCellWidget(tableId, 0, new QLabel(metaprop.name()));

      switch (metaprop.type())
	{
	case QVariant::String:
        case QVariant::Int:
	  {
	    QLineEdit *line = new QLineEdit(s->property(metaprop.name()).toString());
	    table->setCellWidget(tableId, 1, line);

	    tracking = new SelectorPropertyTracking(line, i);
	    edition->addTracking(tracking);
	    QObject::connect(line, SIGNAL(returnPressed()), tracking, SLOT(modified()));
	    break;
	  }
	case QVariant::Double:
	  {
	    QDoubleSpinBox *spinBox = new QDoubleSpinBox;
	    double value = s->property(metaprop.name()).toDouble();

	    table->setCellWidget(tableId, 1, spinBox);
	    spinBox->setMinimum(-INFINITY);
	    spinBox->setMaximum(INFINITY);
	    spinBox->setSingleStep(value / 10);
	    spinBox->setValue(value);

	    tracking = new SelectorPropertyTracking(spinBox, i);
	    edition->addTracking(tracking);
	    QObject::connect(spinBox, SIGNAL(valueChanged(double)), tracking, SLOT(modified()));

	    break;
	  }
	case QVariant::Bool:
	  {
	    QCheckBox *button = new QCheckBox;
	    bool value = s->property(metaprop.name()).toBool();
	    
	    table->setCellWidget(tableId, 1, button);
	    button->setCheckState((value) ? Qt::Checked : Qt::Unchecked);

	    tracking = new SelectorPropertyTracking(button, i);
	    edition->addTracking(tracking);
	    QObject::connect(button, SIGNAL(stateChanged(int)), tracking, SLOT(modified()));
	    break;
	  }
	default:
	  break;
	}
      tableId++;
    }
  vbox->addWidget(table);

  QPushButton *quitButton = new QPushButton(QObject::tr("Finish"));
  vbox->addWidget(quitButton);
  QObject::connect(quitButton, SIGNAL(clicked(bool)), dlg, SLOT(accept()));
  QObject::connect(edition, SIGNAL(modified(DataSetSelector*)), connections, SIGNAL(selectorModified(DataSetSelector*)));

  dlg->setLayout(vbox);

  dlg->exec();

  delete dlg;

  delete edition;
}

void ConnectorDialog::editSelector()
{
  QListWidget *listSelectors = connectorUi.dataSelectorList;
  DataSetSelector *s = (*selectors)[listSelectors->currentItem()->text()];
  assert(s != 0);

  editSelectorBox(this, s);
}
