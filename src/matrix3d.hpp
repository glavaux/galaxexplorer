/*+
This is GalaxExplorer (./src/matrix3d.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __MATRIX3D_HPP
#define __MATRIX3D_HPP

#include "vector3d.hpp"

namespace GalaxExplorer
{

  template<typename T>
  class Matrix3D
  {  
  public:
    T matrix[9];
    
    Matrix3D() {}    

    void unity()
    {
      for (int i = 0; i < 3; i++)
	for (int j = 0; j < 3; j++)
	  matrix[i+3*j] = (i==j) ? 1 : 0;
    }

    T& operator()(int i, int j)
    {
      return matrix[i+3*j];
    }

    const T& operator()(int i, int j) const
    {
      return matrix[i+3*j];
    }

    Matrix3D<T> nonNorm_inverse() const
    {
      Matrix3D<T> invM;
      const Matrix3D<T>& M = *this;

      // We use direct computation of minor
      // determinants in this case.
      invM(0,0) =
	M(1,1) * M(2,2) - M(1,2)*M(2,1);
      invM(1,1) =
	M(0,0) * M(2,2) - M(0,2)*M(2,0);
      invM(2,2) =
	M(0,0) * M(1,1) - M(0,1)*M(1,0);

      invM(1,0) =
	-(M(1,0)*M(2,2) - M(2,0)*M(1,2));
      invM(0,1) =
	-(M(0,1)*M(2,2) - M(0,2)*M(2,1));
      
      invM(2,0) =
	M(1,0)*M(2,1) - M(2,0)*M(1,1);
      invM(0,2) =
	M(0,1)*M(1,2) - M(0,2)*M(1,1);

      invM(2,1) =
	-(M(0,0)*M(2,1) - M(2,0)*M(0,1));
      invM(1,2) =
	-(M(0,0)*M(1,2) - M(0,2)*M(1,0));

      return invM;
    }    

    Matrix3D<T> inverse() const
    {
      return nonNorm_inverse()/det();
    }

    T det() const
    {
      const Matrix3D<T>& M = *this;

      return
	M(0,0)*M(1,1)*M(2,2) +
	M(0,1)*M(1,2)*M(2,0) +
	M(0,2)*M(1,0)*M(2,1) -
	M(0,2)*M(1,1)*M(2,0) -
	M(0,0)*M(1,2)*M(2,1) -
	M(0,1)*M(1,0)*M(2,2);
    }

    Matrix3D<T> operator/(T alpha) const
    {
      Matrix3D<T> M;

      for (int i = 0; i < 9; i++)
	M.matrix[i] = matrix[i] / alpha;
      return M;
    }
    
    Matrix3D<T> operator*(T alpha) const
    {
      Matrix3D<T> M;

      for (int i = 0; i < 9; i++)
	M.matrix[i] = matrix[i] * alpha;
      return M;
    }

    Matrix3D<T> operator*(const Matrix3D<T>& M) const
    {
      Matrix3D<T> outM;

      for (int i = 0; i < 3; i++)
	for (int j = 0; j < 3; j++)
	  {
	    outM(i,j) = 0;
	    for (int k = 0; k < 3; k++)
	      {
		outM(i,j) += operator()(i,k)*M(k,j);
	      }
	  }
      return outM;
    }

    Vector3D<T> operator*(const Vector3D<T>& V) const
    {
      Vector3D<T> v;

      for (int i = 0; i < 3; i++)
	{
	  v(i) = 0;
	  for (int k = 0; k < 3; k++)
	    {
	      v(i) += operator()(i,k)*V(k);
	    }
	}
      return v;
    }
  };  
};

#endif
