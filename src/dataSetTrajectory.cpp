/*+
This is GalaxExplorer (./src/dataSetTrajectory.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <QtCore>
#include "dataSetTrajectory.hpp"
#include "datasets.hpp"

using namespace GalaxExplorer;

DataSetTrajectory::DataSetTrajectory()
{
}

DataSetTrajectory::~DataSetTrajectory()
{
}

SimpleDataSetTrajectory::SimpleDataSetTrajectory()
{
  numTime = numTracers = 0;
  trajectories = 0;
  timeSampling = 0;
  dataPoints = 0;
}

SimpleDataSetTrajectory::SimpleDataSetTrajectory(DataTrajectory *points, uint32_t numP, float *t,
				     uint32_t numTime)
{
  trajectories = new DataTrajectory[numP * numTime];
  this->numTracers = numP;
  this->numTime = numTime;
  this->timeSampling = new float[numTime];
  memcpy(this->timeSampling, t, sizeof(float)*numTime);
  memcpy(trajectories, points, numP  * numTime * sizeof(DataTrajectory));

  dataPoints = new BarePoint[numP];
  numDataPoints = numP;
  setTimePosition(0.0);
}

SimpleDataSetTrajectory::~SimpleDataSetTrajectory()
{
  delete[] trajectories;
  delete[] timeSampling;
  delete[] dataPoints;
}

void SimpleDataSetTrajectory::setTimePosition(float t)
{
  uint32_t it1 = 0;

  if (t > 1.0)
    t = 1.0;
  if (t < 0.0)
    t = 0.0;

  while (t < timeSampling[it1] && it1 < (numTime-2))
    it1++;

  double alpha = (t - timeSampling[it1])/(timeSampling[it1+1]-timeSampling[it1]);
  double beta = 1 - alpha;

  for (uint32_t i = 0; i < numTracers; i++)
    {
      DataTrajectory &p1 = trajectories[it1*numTracers + i],
	&p2 = trajectories[(it1+1)*numTracers + i];
      BarePoint& pnt = dataPoints[i];

      pnt.x = beta*p1.x + alpha * p2.x;
      pnt.y = beta*p1.y + alpha * p2.y;
      pnt.z = beta*p1.z + alpha * p2.z;
    }

  emit dataUpdated();
}

void SimpleDataSetTrajectory::saveData(QDataStream& d)
{
  d << numTracers << numTime << getDistanceMultiplier();
  for (uint32_t i = 0; i < numTime; i++)
    d
      << timeSampling[i];

  for (uint32_t i = 0; i < numTracers*numTime; i++)
    d
      << trajectories[i].x
      << trajectories[i].y
      << trajectories[i].z;
}

void SimpleDataSetTrajectory::loadData(QDataStream& d)
{
  if (timeSampling != 0)
    delete[] timeSampling;
  if (trajectories != 0)
    delete[] trajectories;

  double mul;
  d >> numTracers >> numTime;
  d >> mul;
  setDistanceMultiplier(mul);

  timeSampling = new float[numTime];
  trajectories = new DataTrajectory[numTime*numTracers];

  for (uint32_t i = 0; i < numTime; i++)
    d >> timeSampling[i];

  for (uint32_t i = 0; i < numTracers*numTime; i++)
    d
      >> trajectories[i].x
      >> trajectories[i].y
      >> trajectories[i].z;
  
  if (dataPoints != 0)
    delete[] dataPoints;
  dataPoints = new DataPoint[numTracers];
  numDataPoints = numTracers;
  setTimePosition(0.0);
}
