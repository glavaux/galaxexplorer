/*+
This is GalaxExplorer (./src/selectorByDistance.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <cmath>
#include <QObject>
#include "datasets.hpp"
#include "dataSelectors.hpp"
#include "selectorByDistance.hpp"
#include "simpleDataFilter.hpp"
#include <iostream>

using namespace std;
using namespace GalaxExplorer;


DataSelectionByDistance::DataSelectionByDistance()
{
}

DataSelectionByDistance::~DataSelectionByDistance()
{
}

SelectByDistance::SelectByDistance(DataSelectionByDistance *s)
  : DataSetSelector(s)
{
  keepInsideRange = true;
  minDistance = 0;
  maxDistance = 20000;
}

SelectByDistance::~SelectByDistance()
{
}

void SelectByDistance::setKeepInside(bool k)
{  
  keepInsideRange = k;
}

bool SelectByDistance::keepInside()
{
  return keepInsideRange;
}

void SelectByDistance::setMinDistance(double mindist)
{
  minDistance = mindist;
}

double SelectByDistance::getMinDistance()
{
  return minDistance;
}

void SelectByDistance::setMaxDistance(double maxdist)
{
  maxDistance = maxdist;
}

double SelectByDistance::getMaxDistance()
{
  return maxDistance;
}
  
uint32_t SelectByDistance::getFirst(const DataSet& d, uint32_t cur) const
{
  if (keepInsideRange)
    {
      while (cur < d.size())
	{
	  const BarePoint& p = d[cur];

	  double dist = sqrt(p.x*p.x + p.y*p.y + p.z*p.z);
	  if (dist > minDistance && dist < maxDistance)
	    break;
	  cur++;
	}
    }
  else
    {
      while (cur < d.size())
	{
	  const BarePoint& p = d[cur];

	  double dist = sqrt(p.x*p.x + p.y*p.y + p.z*p.z);
	  if (dist < minDistance || dist > maxDistance)
	    break;
	  cur++;
	}
    }
  return cur;
}

void SelectByDistance::loadState(QDataStream& s)
{
  s >> keepInsideRange >> minDistance >> maxDistance;
}


void SelectByDistance::saveState(QDataStream& s)
{
  s << keepInsideRange << minDistance << maxDistance;
}

DataSetSelector *DataSelectionByDistance::newSelector()
{
  return new SelectByDistance(this);
}

