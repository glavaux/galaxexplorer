/*+
This is GalaxExplorer (./src/object_helper.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <iostream>
#include "object_helper.hpp"
#include "vboObject.hpp"
#include "gltools.hpp"

using namespace GalaxExplorer;
using namespace std;

static void matrixMultiply(GLdouble* M, double* v, double* v2)
{
  for (int i = 0; i < 3; i++)
    {
      v2[i] = 0;
      for (int j = 0; j < 3; j++)
	{
	  v2[i] += M[i+4*j] * v[j];
	}
    }
}

void ConeDrawer::draw(VBO_Object& obj,
		      GLfloat ux, GLfloat uy, GLfloat uz,
		      GLfloat x, GLfloat y, GLfloat z,
		      GLfloat R, GLfloat h)
{
  GLdouble M[16];

  buildMatrixLookAt(M, ux, uy, uz);

  obj.newSequence();

  obj.addPoint( x, y, z);
  if (colorSet && colornum == 3)
    obj.setColor(r, g, b);
  if (colorSet && colornum == 4)
    obj.setColor(r, g, b, a);
  for (int i = 0; i <= nFace; i++)
    {
      double v[3] = {  R * cosTable[i],
		       R * sinTable[i],
		       -h };
      double v2[3];

      matrixMultiply(M, v, v2);

      obj.addPoint(x + v2[0],
		   y + v2[1],
		   z + v2[2]);
      if (colorSet && colornum == 3)
	obj.setColor(r, g, b);
      if (colorSet && colornum == 4)
	obj.setColor(r, g, b, a);
    }
  
  obj.newSequence();
  
  {
    double v0[3] = { 0, 0, -h };
    double v2[3];

    matrixMultiply(M, v0, v2);     
    obj.addPoint(x+v2[0], y+v2[1], z+v2[2]);
    if (colorSet && colornum == 3)
      obj.setColor(r, g, b);
    if (colorSet && colornum == 4)
      obj.setColor(r, g, b, a);
  }

  for (int i = 0; i <= nFace; i++)
    {
      double v[3] = {  R * cosTable[nFace-i],
		       R * sinTable[nFace-i],
		       -h };
      double v2[3];

      matrixMultiply(M, v, v2);     

      obj.addPoint(x + v2[0],
		   y + v2[1],
		   z + v2[2]);
      if (colorSet && colornum == 3)
	obj.setColor(r, g, b);
      if (colorSet && colornum == 4)
	obj.setColor(r, g, b, a);
    }

}

void SphereDrawer::draw(VBO_Object& vbo, 
			GLfloat x, GLfloat y, GLfloat z, 
			GLfloat R)
{

  for (int i_phi = 0; i_phi < nFace; i_phi++)
    {
      float phi_c1 = cosTable[i_phi], phi_c2 = cosTable[i_phi+1],
            phi_s1 = sinTable[i_phi], phi_s2 = sinTable[i_phi+1];

      //      vbo.newSequence();

      vbo.addPoint(x, y, z - R);
      if (colorSet && colornum == 3)
        vbo.setColor(r, g, b);
      if (colorSet && colornum == 4)
        vbo.setColor(r, g, b, a);

      for (int j_theta = nFace; j_theta >= 1; j_theta--)
        {	 
          float theta_c1 = cosTable[j_theta], theta_c2 = cosTable[j_theta-1];
          float theta_s1 = sinTable[j_theta], theta_s2 = sinTable[j_theta-1];
          
          vbo.addPoint(x + R * phi_c2 * theta_s2,
	               y + R * phi_s2 * theta_s2,
          	       z + R * theta_c2);
          if (colorSet && colornum == 3)
            vbo.setColor(r, g, b);
          if (colorSet && colornum == 4)
            vbo.setColor(r, g, b, a);
          vbo.addPoint(x + R * phi_c1 * theta_s2,
	               y + R * phi_s1 * theta_s2,
	               z + R * theta_c2);
          if (colorSet && colornum == 3)
            vbo.setColor(r, g, b);
          if (colorSet && colornum == 4)
            vbo.setColor(r, g, b, a);
        }
      
      vbo.addPoint(x, y, z + R);
      if (colorSet && colornum == 3)
        vbo.setColor(r, g, b);
      if (colorSet && colornum == 4)
        vbo.setColor(r, g, b, a);
      
      for (int j_theta = 0; j_theta < nFace; j_theta++)
        {	 
          float theta_c1 = cosTable[j_theta+1], theta_c2 = cosTable[j_theta];
          float theta_s1 = -sinTable[j_theta+1], theta_s2 = -sinTable[j_theta];
	  
          vbo.addPoint(x + R * phi_c2 * theta_s2,
                       y + R * phi_s2 * theta_s2,
                       z + R * theta_c2);
          if (colorSet && colornum == 3)
            vbo.setColor(r, g, b);
          if (colorSet && colornum == 4)
            vbo.setColor(r, g, b, a);
          vbo.addPoint(x + R * phi_c1 * theta_s2,
                       y + R * phi_s1 * theta_s2,
                       z + R * theta_c2);
                       
          if (colorSet && colornum == 3)
            vbo.setColor(r, g, b);
          if (colorSet && colornum == 4)
            vbo.setColor(r, g, b, a);
        }
    }

  vbo.addPoint(x, y, z - R);
  if (colorSet && colornum == 3)
    vbo.setColor(r, g, b);
  if (colorSet && colornum == 4)
    vbo.setColor(r, g, b, a);
}



void CubeDrawer::draw(VBO_Object& c, GLfloat x, GLfloat y, GLfloat z, GLfloat L)
{
  float points[6][4][3] = {
    { 
      { -1, -1, -1 },
      {  1, -1, -1 },
      {  1,  1, -1 },
      { -1,  1, -1 },
    },{
      { -1, -1, 1 },
      { -1,  1, 1 },
      {  1,  1, 1 },
      {  1, -1, 1 },
    },{
      { -1, -1, -1 },
      { -1, -1,  1 },
      {  1, -1,  1 },
      {  1, -1, -1 }
    },{
      { -1,  1,  1 },
      { -1,  1, -1 },
      {  1,  1, -1 },
      {  1,  1,  1 },
    },{
      {  1,  1,  1 },
      {  1,  1, -1 },
      {  1, -1, -1 },
      {  1, -1,  1 }
    },{
      { -1,  1,  1 },
      { -1, -1,  1 },
      { -1, -1, -1 },
      { -1,  1, -1 }
    }
  };

  float cubeSize = L;
  bool tex3d = c.getAccessor().hasTexture3D();

  c.begin();

  for (int face = 0; face < 6; face++)
    {
      for (int v = 0; v < 4; v++)
        {
          c.addPoint(points[face][v][0]*cubeSize*0.5+x, 
                     points[face][v][1]*cubeSize*0.5+y, 
                     points[face][v][2]*cubeSize*0.5+z);

          GLfloat pos[3] = { 
                (points[face][v][0]+1)*0.5, 
                (points[face][v][1]+1)*0.5, 
                (points[face][v][2]+1)*0.5 };
          if (tex3d)
            c.setTexture3D(pos[0], pos[1], pos[2]);
          if (tex3dset)
            c.setAttrib(tex3dattr, pos);
      	}
      c.newSequence();
    }

  c.end();

}

