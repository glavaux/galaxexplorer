/*+
This is GalaxExplorer (./src/skybox.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <iostream>
#include "includeGL.hpp"
#include <QtCore>
#include <QtGui>
#include "skybox.hpp"
#ifdef HEALPIX_PRESENT
#include <healpix_map.h>
#endif
#include "glwidget.hpp"

using namespace GalaxExplorer;
using namespace std;

SkyBox::SkyBox(Healpix_Map<float>& map)
  : resolution(512), delayedInit(false)
{
#ifdef HEALPIX_PRESENT
  skymap = new Healpix_Map<float>();
  *skymap = map;

  skymap->minmax(vmin, vmax);
#endif
}

SkyBox::~SkyBox()
{
#ifdef HEALPIX_PRESENT
  if (skymap)
    delete skymap;

  glDeleteTextures(1, face_textures);
#endif

}

void SkyBox::buildCube(GLRenderer *renderer)
{
  float points[6][4][3] = {
    { 
      { -1, -1, -1 },
      {  1, -1, -1 },
      {  1,  1, -1 },
      { -1,  1, -1 },
    },{
      {  1, -1, 1 },
      { -1, -1, 1 },
      { -1,  1, 1 },
      {  1,  1, 1 },
    },{
      { -1, -1, -1 },
      { -1, -1,  1 },
      {  1, -1,  1 },
      {  1, -1, -1 }
    },{
      { -1,  1,  1 },
      { -1,  1, -1 },
      {  1,  1, -1 },
      {  1,  1,  1 },
    },{
      {  1,  1,  1 },
      {  1,  1, -1 },
      {  1, -1, -1 },
      {  1, -1,  1 }
    },{
      { -1,  1,  1 },
      { -1, -1,  1 },
      { -1, -1, -1 },
      { -1,  1, -1 }
    }
  };

  VBO_Descriptor desc;

  if (!renderer->usingShaders())
    desc.tex3d = true;

  VBO_Object c(desc);
  float cubeSize = 100.0;

  c.begin();

  for (int face = 0; face < 6; face++)
    {
      for (int v = 0; v < 4; v++)
        {
          c.addPoint(points[face][v][0]*cubeSize, points[face][v][1]*cubeSize, points[face][v][2]*cubeSize);
          if (!renderer->usingShaders()) 
            c.setTexture3D(points[face][v][0]*cubeSize, points[face][v][1]*cubeSize, points[face][v][2]*cubeSize);
        }
    }

  c.end();

  cube.transfer(c);
}

#define CHECK_GL_ERRORS \
{ \
    GLenum err = glGetError(); \
    if (err) \
      { \
        QString s; \
	cout << "Error " << err << " at line " << __LINE__ << endl; \
      } \
}

void SkyBox::prepareTextures(GLRenderer *renderer)
{
#ifdef HEALPIX_PRESENT
  float basis[6][3] = {
    { 1, 0, 0 },
    { 0, 1, 0 },
    { 0, 0, 1 },
    { -1, 0, 0 },
    { 0, -1, 0 },
    { 0, 0, -1 }
  };

  // We have to form 6 textures 
  int dx_direction[6] = {
    0+0,  // POSITIVE_X
    0+1,  // POSITIVE_Y
    0+2,  // POSITIVE_Z
    3+0,  // NEGATIVE_X
    3+1,  // NEGATIVE_Y
    3+2   // NEGATIVE_Z
  };

  int dy_direction[6] = {
    3+2, // POSITIVE_X
    0+0, // POSITIVE_Y
    0+0, // POSITIVE_Z
    0+2, // NEGATIVE_X
    0+0, // NEGATIVE_Y
    3+0, // NEGATIVE_Z
  };
  
  int dz_direction[6] = {
    3+1,
    0+2,
    3+1,
    3+1,
    3+2,
    3+1
  };

  GLint texture_face[6] = {
    GL_TEXTURE_CUBE_MAP_POSITIVE_X,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
  };

  unsigned char *textures[6];
  double deg2rad = M_PI/180;
  float delta;

  needTextureRebuilding = false;

  delta = vmax-vmin;

  for (int j = 0; j < 6; j++)
    {
      textures[j] = new unsigned char[4*resolution*resolution];

      int fx = dx_direction[j];
      int fy = dy_direction[j];
      int fz = dz_direction[j];

      for (int ix = 0; ix < resolution; ix++)
	{
	  double bx = -1 + 2.0*(ix+0.5)/resolution;

	  for (int iy = 0; iy < resolution; iy++)
	    {
	      double by = -1.0 + 2.0*(iy+0.5)/resolution;
	      double dx = 1/sqrt(1+bx*bx+by*by);
	      double dy = bx*dx;
	      double dz = by*dx;
	      float v[3] = {0,0,0};

	      for (int k = 0; k < 3; k++)
		v[k] = basis[fx][k]*dx + basis[fy][k]*dy + basis[fz][k]*dz;

	      vec3 v3(v[0],v[1],v[2]);

	      int pix;
	      int idx = ix + resolution*iy;
	      float value;
	      GColor col;

	      // Use colatitude convention in Healpix
	      pix = skymap->vec2pix(v3);
	      value = ((*skymap)[pix]-vmin)/delta;
	      
	      currentPalette.getColor(value, col);
	      for (int q = 0; q < 4; q++)
		textures[j][4*idx + q] = (unsigned char)(255*col.rgba[q]);
	    }
	}
    }

  glGenTextures(1, face_textures);

  glBindTexture(GL_TEXTURE_CUBE_MAP,face_textures[0]);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  //  glTexParameteri(GL_TEXTURE_CUBE_MAP_EXT, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  //  glTexParameteri(GL_TEXTURE_CUBE_MAP_EXT, GL_GENERATE_MIPMAP, GL_TRUE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP);

  for (int f = 0; f < 6; f++)
    {
      glTexImage2D(texture_face[f], 0, GL_RGBA, 
		   resolution, resolution, 0, GL_RGBA, GL_UNSIGNED_BYTE,
		   textures[f]);
      delete[] textures[f];
      CHECK_GL_ERRORS;
    }

#endif
  
  if (renderer->usingShaders())
    {
      GLSLShaderObject skyboxF("skybox/skyboxFragment.glsl", GL_FRAGMENT_SHADER);
      GLSLShaderObject skyboxV("skybox/skyboxVertex.glsl", GL_VERTEX_SHADER);
      GLSLProgramObject program;
      
      program.attachShader(skyboxF);
      program.attachShader(skyboxV);
      
      renderer->assembleProgramShader(shaders, program);
    }
}

void SkyBox::draw(GLRenderer *renderer)
{
  if (skymap == 0)
    return;
  
  if (!delayedInit)
    {
      buildCube(renderer);
      delayedInit = true;
      needTextureRebuilding = true;
    }
  if (needTextureRebuilding)
    prepareTextures(renderer);

  GLfloat camera_pos[3];

  // Grab the current camera position 
  renderer->getScene().getState()->getCameraPosition(camera_pos);

//  cout << "Position = " << camera_pos[0] << " " << camera_pos[1] << " " << camera_pos[2] << endl;

  if (renderer->usingShaders())
    {
      renderer->useShader(shaders);
      renderer->getShader(shaders).bindTextureCUBE("skybox_cube", face_textures[0]);
      renderer->getShader(shaders).setUniform("camera_position", camera_pos, 3);
      cube.draw(GL_QUADS);
    }
  else
    {
      glColor4f(1,1,1,1);
      glMatrixMode(GL_TEXTURE);
      glPushMatrix();
      glTranslated(-camera_pos[0], -camera_pos[1], -camera_pos[2]);

      glEnable(GL_TEXTURE_CUBE_MAP);
      glBindTexture(GL_TEXTURE_CUBE_MAP, face_textures[0]);
      cube.draw(GL_QUADS);
      glDisable(GL_TEXTURE_CUBE_MAP);

      glMatrixMode(GL_TEXTURE);
      glPopMatrix();
     
    }
}

void SkyBox::rebuildTextures()
{
  glDeleteTextures(1, face_textures);

  needTextureRebuilding = true;    
}

bool SkyBox::setPalette(const GPalette& palette)
{
  GPalette old_palette = this->currentPalette;

  this->currentPalette = palette;

  if (!this->currentPalette.rebuild())
    {
      this->currentPalette = old_palette;
      this->currentPalette.rebuild();
      return false;
    }

  rebuildTextures();
  return true;
}

void SkyBox::setMinMax(double min_val, double max_val)
{
  vmin = min_val;
  vmax = max_val;

  rebuildTextures();
}
