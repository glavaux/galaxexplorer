/*+
This is GalaxExplorer (./src/dataConnector.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include "datasets.hpp"
#include "representations/dataRepresentation.hpp"
#include "dataConnector.hpp"
#include "datagrid.hpp"
#include "datasource.hpp"
#include "datamesh.hpp"

using namespace GalaxExplorer;

SetConnector::SetConnector()
{
  source = 0;
  representation = 0;
  selector = 0;
}

SetConnector::SetConnector(const SetConnector& s)
  : selector(s.selector)
{
  source = s.source;
  representation = s.getRepresentation();
}

SetConnector::SetConnector(DataSet *dataset, DataRepresentation *representation,
	     DataSetSelector *selector)
{
  this->source = dataset;
  this->representation = representation;
  this->selector = selector;
}

SetConnector::~SetConnector()
{
}

SetConnector *SetConnector::copyConnector() const
{
  SetConnector *c = new SetConnector(*this);

  c->additional_data = additional_data;
  return c;
}

DataSet *SetConnector::getDataset() const
{
  return qobject_cast<DataSet *>(source);
}

GridConnector::GridConnector()
{
  representation = 0;
  source = 0;
}

GridConnector::GridConnector(const GridConnector& s)
{
  representation = s.representation;
  source = s.source;
}

GridConnector::GridConnector(DataGrid *datasrc, DataRepresentation *representation)
{
  this->source = datasrc;
  this->representation = representation;
}

GridConnector::~GridConnector()
{
}

DataConnector *GridConnector::copyConnector() const
{
  GridConnector *c = new GridConnector(*this);
  c->additional_data = additional_data;
  return c;
}

DataGrid *GridConnector::getDataGrid() const
{
  return qobject_cast<DataGrid *>(source);
}


MeshConnector::MeshConnector()
{
  representation = 0;
  source = 0;
  particles_source = 0;
}

MeshConnector::MeshConnector(const MeshConnector& s)
{
  representation = s.representation;
  source = s.source;
  particles_source = s.particles_source;
}

MeshConnector::MeshConnector(DataMesh *datasrc, DataSet *otherset,
			     DataRepresentation *representation)
{
  this->source = datasrc;
  this->representation = representation;
  this->particles_source = otherset;
}

MeshConnector::~MeshConnector()
{
}

DataConnector *MeshConnector::copyConnector() const
{
  return new MeshConnector(*this);
}

DataMesh *MeshConnector::getDataMesh() const
{
  return qobject_cast<DataMesh *>(source);
}

DataSet *MeshConnector::getDataParticles() const
{
  return particles_source;
}
