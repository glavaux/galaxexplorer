/*+
This is GalaxExplorer (./src/vboObject.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <iostream>
#include "vboObject.hpp"
#include "glerror.hpp"

using namespace std;
using namespace GalaxExplorer;

VBO_Object::VBO_Object(const VBO_Descriptor& desc, int numpoints)
   throw (VBO_OutOfMemory)
  : accessor(desc), dynamicDraw(desc.dynamicDraw)
{
  vboId = ~(GLuint)0;
  reordered = false;

  if (numpoints > 0)
    {
      data.resize(accessor.getNumFloats(numpoints));
      currentNumPoints = numpoints;
      allocVBO();
    }
  else
    currentNumPoints = 0;

  currentPosPoint = 0;  
}

VBO_Object::VBO_Object()
  : accessor(VBO_Descriptor())
{
  vboId = ~(GLuint)0;
  currentNumPoints = 0;
  currentPosPoint = 0;  
}

void VBO_Object::allocVBO()
  throw(VBO_OutOfMemory)
{
  GLsizei dataSize = accessor.getSize(currentNumPoints);
  GLsizei outBufferSize;

  glGenBuffers(1, &vboId);
  cout << "Alloc vboId = " << vboId << endl;

  glBindBuffer(GL_ARRAY_BUFFER, vboId);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &data[0], dynamicDraw ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);

  GLenum err = glGetError();

  glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &outBufferSize);
  if (dataSize != outBufferSize)
    {    
      cerr << "dataSize = " << dataSize << endl;
      cerr << "outBufferSize = " << outBufferSize << endl;

      glDeleteBuffers(1, &vboId);

      throw VBO_OutOfMemory();
    }  

  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

VBO_Object::~VBO_Object()
{
  if (currentNumPoints > 0)
    {
      assert(vboId != 0);
      cout << "Delete vboId = " << vboId << endl;
      glDeleteBuffers(1, &vboId);
    }
}

void VBO_Object::draw(GLint mode)
{
  bind();

  glEnableClientState(GL_VERTEX_ARRAY);  
  glVertexPointer(3, GL_FLOAT, accessor.getStride(), 0);
  
  if (accessor.hasTexture2D() || accessor.hasTexture3D())
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  CHECK_GL_ERRORS;

  if (accessor.hasTexture2D())
    glTexCoordPointer(2, GL_FLOAT, accessor.getStride(), (void *)accessor.getOffset(FlexiblePointAccessor::TEX2D_X));
  CHECK_GL_ERRORS;

  if (accessor.hasTexture3D())
    glTexCoordPointer(3, GL_FLOAT, accessor.getStride(), (void*)accessor.getOffset(FlexiblePointAccessor::TEX3D_X));
  CHECK_GL_ERRORS;

  for (int i = 0; i < accessor.numMultiTex(); i++)
    {
      // Only 2D texture supported here
      GLint texid = accessor.multiTexID(i);
#if 0
      glMultiTexCoordPointerEXT(texid, 2, GL_FLOAT,
				accessor.getStride(), 
				(void*)accessor.getMultiTexCoordOffset(i));
#endif
    }

  for (int i = 0; i < accessor.getNumAttrib(); i++)
    {
      glEnableVertexAttribArray (accessor.getAttribIndex(i));
      CHECK_GL_ERRORS;
      glVertexAttribPointer(accessor.getAttribIndex(i),
			    accessor.getAttribSize(i), GL_FLOAT, false,
			    accessor.getStride(), 
			    (void *)accessor.getAttribOffset(i));
      CHECK_GL_ERRORS;
    }

  if (reordered)
    {
      glEnableClientState(GL_INDEX_ARRAY);
      glIndexPointer(GL_INT, 1, order_indexes.get());
    }
  CHECK_GL_ERRORS;

  if (accessor.hasColors())
    {
      int num = accessor.numColorComponents();
      
      glEnableClientState(GL_COLOR_ARRAY);
      glColorPointer(num, GL_FLOAT, accessor.getStride(), (GLvoid*)accessor.getOffset(FlexiblePointAccessor::COLOR_R));
    }
  CHECK_GL_ERRORS;

  if (accessor.hasNormals())
    {
      glEnableClientState(GL_NORMAL_ARRAY);
      glNormalPointer(GL_FLOAT, accessor.getStride(), (void*)accessor.getOffset(FlexiblePointAccessor::NORMAL_X));
    }
  if (sequencesLen.size() == 1)
    {
      glDrawArrays(mode, 0, currentNumPoints);      
    }
  else
    {
      glMultiDrawArrays(mode, &sequencesStart[0], &sequencesLen[0], sequencesLen.size());
    }
  CHECK_GL_ERRORS;
  
  glDisableClientState(GL_VERTEX_ARRAY);
  if (reordered)
    glDisableClientState(GL_INDEX_ARRAY);
  if (accessor.hasNormals())
    glDisableClientState(GL_NORMAL_ARRAY);
  if (accessor.hasColors())
    glDisableClientState(GL_COLOR_ARRAY);
  if (accessor.hasTexture2D() || accessor.hasTexture3D())
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);

  for (int i = 0; i < accessor.getNumAttrib(); i++)
    glDisableVertexAttribArray(accessor.getAttribIndex(i));

  unbind();
}

void VBO_Object::bind()
{
  assert(vboId != 0);
  assert(currentNumPoints > 0);
  glBindBuffer(GL_ARRAY_BUFFER, vboId);
}

void VBO_Object::unbind()
{     
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void VBO_Object::begin()
{
  currentPosPoint = -1;
  data.clear();
  sequencesStart.clear();
  sequencesLen.clear();
  sequencesStart.push_back(0);
}

void VBO_Object::end()
  throw (VBO_OutOfMemory)
{
  GLsizei seqLen;

  if (currentNumPoints != 0)
    glDeleteBuffers(1, &vboId);

  cout << "Got " << currentPosPoint << " points" << endl;
  
  currentNumPoints = currentPosPoint+1;
  allocVBO();

  seqLen = currentPosPoint - sequencesStart[sequencesStart.size()-1] + 1;
  sequencesLen.push_back(seqLen);
}

void VBO_Object::newSequence()
{
  GLsizei seqLen;
 
  seqLen = currentPosPoint - sequencesStart[sequencesStart.size()-1] + 1;
  
  if (seqLen == 0)
    return;

  sequencesStart.push_back(currentPosPoint+1);
  sequencesLen.push_back(seqLen);
}

void VBO_Object::addPoint(GLfloat x, GLfloat y, GLfloat z)
{
  currentPosPoint++;

  data.resize(accessor.getNumFloats(currentPosPoint+1));

  data[accessor(currentPosPoint,FlexiblePointAccessor::VERTEX_X)] = x;
  data[accessor(currentPosPoint,FlexiblePointAccessor::VERTEX_Y)] = y;
  data[accessor(currentPosPoint,FlexiblePointAccessor::VERTEX_Z)] = z;
}

void VBO_Object::setColor(GLfloat r, GLfloat g, GLfloat b)
{
  data[accessor(currentPosPoint,FlexiblePointAccessor::COLOR_R)] = r;
  data[accessor(currentPosPoint,FlexiblePointAccessor::COLOR_G)] = g;
  data[accessor(currentPosPoint,FlexiblePointAccessor::COLOR_B)] = b;  
}

void VBO_Object::setColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a)
{
  data[accessor(currentPosPoint,FlexiblePointAccessor::COLOR_R)] = r;
  data[accessor(currentPosPoint,FlexiblePointAccessor::COLOR_G)] = g;
  data[accessor(currentPosPoint,FlexiblePointAccessor::COLOR_B)] = b;  
  data[accessor(currentPosPoint,FlexiblePointAccessor::COLOR_A)] = a;  
}

void VBO_Object::setTexture2D(GLfloat x, GLfloat y)
{
  data[accessor(currentPosPoint,FlexiblePointAccessor::TEX2D_X)] = x;
  data[accessor(currentPosPoint,FlexiblePointAccessor::TEX2D_Y)] = y;
}

void VBO_Object::setTexture3D(GLfloat x, GLfloat y, GLfloat z)
{
  data[accessor(currentPosPoint,FlexiblePointAccessor::TEX3D_X)] = x;
  data[accessor(currentPosPoint,FlexiblePointAccessor::TEX3D_Y)] = y;
  data[accessor(currentPosPoint,FlexiblePointAccessor::TEX3D_Z)] = z;
}

void VBO_Object::setNormal(GLfloat x, GLfloat y, GLfloat z)
{
  data[accessor(currentPosPoint,FlexiblePointAccessor::NORMAL_X)] = x;
  data[accessor(currentPosPoint,FlexiblePointAccessor::NORMAL_Y)] = y;
  data[accessor(currentPosPoint,FlexiblePointAccessor::NORMAL_Z)] = z;
}

void VBO_Object::setMultiTexture(int id, GLfloat x, GLfloat y)
{
  data[accessor.getMultiTexFloat(currentPosPoint, id, 0)] = x;
  data[accessor.getMultiTexFloat(currentPosPoint, id, 1)] = y;
}

void VBO_Object::setAttrib(int id, GLfloat *a)
{
  memcpy(&data[accessor.getAttribFloat(currentPosPoint, id)], a, sizeof(GLfloat)*accessor.getAttribSize(id));
}

void VBO_Object::transfer(VBO_Object& vbo)
{
  if (currentNumPoints != 0)
    glDeleteBuffers(1, &vboId);
  
  vboId = vbo.vboId;
  data = vbo.data;
  sequencesStart = vbo.sequencesStart;
  sequencesLen = vbo.sequencesLen;
  currentNumPoints = vbo.currentNumPoints;
  currentPosPoint = vbo.currentPosPoint;
  accessor = vbo.accessor;
  order_indexes = vbo.order_indexes;
  reordered = vbo.reordered;

  vbo.sequencesStart.clear();
  vbo.sequencesLen.clear();
  vbo.currentNumPoints = 0;
  vbo.currentPosPoint = 0;
  vbo.vboId = 0;
  vbo.data.clear();
}

VBO_Chain::VBO_Chain()
{
}

VBO_Chain::~VBO_Chain()
{
  for (int i = 0; i < chain.size(); i++)
    delete chain[i].second;
}

void VBO_Chain::reset()
{
  for (int i = 0; i < chain.size(); i++)
    delete chain[i].second;
  chain.clear();
}

void VBO_Chain::push(GLint dtype, VBO_Object& vbo)
{
  VBO_Object *o = new VBO_Object();

  o->transfer(vbo);

  chain.push_back(make_pair(dtype, o));
}

void VBO_Chain::draw()
{
  for (int i = 0; i < chain.size(); i++)
    if (chain[i].second->numPoints() != 0)
      chain[i].second->draw(chain[i].first);
}


void VBO_Object::setOrdering(boost::shared_array<int>& order)
{
  order_indexes = order;
  reordered = (order.get() != 0);
}
