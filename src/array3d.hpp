/*+
This is GalaxExplorer (./src/array3d.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __ARRAY3D_HPP
#define __ARRAY3D_HPP

#include <cmath>
#include <exception>
#include <QDataStream>
#include "vector3d.hpp"

namespace GalaxExplorer
{

  class NotConformableException: public std::exception
  {
  };

  class CannotAllocateArray: public std::exception
  {
  };
  
  template<typename T, bool checkedArray = false>
  class Array3D
  {
  protected:
    T *data;
    int _nx, _ny, _nz;
  public:
    Array3D() throw()
    {
      _nx = _ny = _nz = 0;
      data = 0;
    }

    Array3D(T *d, int nx, int ny, int nz) throw()
    {
      data = 0;
      alloc(nx,ny,nz);
      memcpy(data, d, sizeof(T)*(long)(nx)*ny*nz);
    }
    
    Array3D(const Array3D& a) throw()
    {
      data = 0;
      alloc(a._nx, a._ny, a._nz);
      memcpy(data, a.data, sizeof(T)*(long)(_nx)*_ny*_nz);     
    }

    ~Array3D()
    {
      if (data != 0)
	delete[] data;
    }

    void fill(const T& a)
    {
      long N = (long)(_nx)*_ny*_nz;
      for (int i = 0; i < N; i++)
	{
	  data[i] = a;
	}
    }

    const Array3D& operator=(const Array3D& a)
      throw()
    {
      alloc(a._nx, a._ny, a._nz);
      memcpy(data, a.data, sizeof(T)*(long)(_nx)*_ny*_nz);     
      return *this;
    }

    void alloc(int nx, int ny, int nz)
      throw(CannotAllocateArray)
    {
      if (data != 0)
        delete[] data;
      _nx = nx;
      _ny = ny;
      _nz = nz;
      if (nx*ny*nz != 0)
        {
          data = new T[nx*ny*nz];
          if (data == 0)
            {
              _nx=_ny=_nz = 0;
              throw CannotAllocateArray();
            }
        }
    };

    void free()
    {
      if (data == 0)
        return;

      delete[] data;
      data = 0;
      _nx = _ny = _nz = 0;
    }

    template<typename T2>
    Array3D<T2> *astype() const 
    {
      Array3D<T2> *m = new Array3D<T2>();
      long S = _nx*_ny*_nz;
      
      m->alloc(_nx, _ny, _nz);
      for (long l = 0; l < S; l++)
        (*m)[l] = T2(data[l]);
      return m;
    }

    void reshape(int nx, int ny, int nz)
      throw(NotConformableException)
    {
      long oldsize = _nx*_ny*_nz;
      long newsize = nx*ny*nz;

      if (oldsize != newsize)
        throw NotConformableException();
    }
    
    T& operator()(int x, int y, int z)
    {
      if (checkedArray)
        {
          if (x < 0 || x >= _nx ||
              y < 0 || y >= _ny || 
              z < 0 || z >= _nz)
            {
              throw NotConformableException();
            }
        }
      return data[x+_nx*(y+_ny*z)];
    }

    void getIGradient(int x, int y, int z, T d[3]) const
    {
      if (x <= 0 || y <= 0 || z <= 0 || x >= (_nx-1) ||
        y >= (_ny-1) || z >= (_nz-1))
        {
          d[0] = d[1] = d[2] = 0;
          return;
        }
            
      d[0] = 0.5*(operator()(x+1,y,z)-operator()(x-1,y,z));
      d[1] = 0.5*(operator()(x,y+1,z)-operator()(x,y-1,z));
      d[2] = 0.5*(operator()(x,y,z+1)-operator()(x,y,z-1));
    }

    void getGradient(float x, float y, float z, T d[3]) const
    {
      if (x <= 0 || y <= 0 || z <= 0 || x >= (_nx-1) ||
          y >= (_ny-1) || z >= (_nz-1))
        {
          d[0] = d[1] = d[2] = 0;
          return;
        }

      // We must do a trilinear interpolation here. 
      // First compute the three weights.
      float alpha, beta, gamma;
      int ix, iy, iz;
      T tmp_d[8][3];

      alpha = floor(x);
      beta = floor(y);
      gamma = floor(z);

      ix = (int)alpha;
      iy = (int)beta;
      iz = (int)gamma;

      alpha = (x-alpha);
      beta = (y-beta);
      gamma = (z-gamma);

      for (int kz=0;kz<2;kz++)
	for (int ky=0;ky<2;ky++)
	  for (int kx=0;kx<2;kx++)
	    {
	      int i = kx+2*ky+4*kz;
	      getIGradient(ix+kx,iy+ky,iz+kz,tmp_d[i]);
	    }
      
      for (int j = 0; j < 3; j++)
	{
	  d[j] = 
	    (1-alpha)*(1-beta)*(1-gamma)*tmp_d[0][j] +
	    alpha    *(1-beta)*(1-gamma)*tmp_d[1][j] +
	    (1-alpha)*beta    *(1-gamma)*tmp_d[2][j] +
	    alpha    *beta    *(1-gamma)*tmp_d[3][j] +
	    (1-alpha)*(1-beta)*gamma    *tmp_d[4][j] +
	    alpha    *(1-beta)*gamma    *tmp_d[5][j] +
	    (1-alpha)*beta    *gamma    *tmp_d[6][j] + 
	    alpha    *beta    *gamma    *tmp_d[7][j];
	}
    }
  
    template<typename T2>
    void getGradient(const Vector3D<T2>& pos, T d[3]) const
    {
      getGradient(pos(0), pos(1), pos(2), d);
    }    
    
    const T& operator()(int x, int y, int z) const
    {
      if (checkedArray)
	{
	  if (x < 0 || x >= _nx ||
	      y < 0 || y >= _ny || 
	      z < 0 || z >= _nz)
	    {
	      throw NotConformableException();
	    }
	}
      return data[x+_nx*(y+_ny*z)];
    }    

    T& operator[](int i)
    {
      return data[i];
    }

    const T& operator[](int i) const
    {
      return data[i];
    }

    int Nx() const { return _nx; }
    int Ny() const { return _ny; }
    int Nz() const { return _nz; }

  };

  template<typename T>
  QDataStream& operator<<(QDataStream& out, const Array3D<T>& a)
  {
    long N = a.Nx()*a.Ny()*a.Nz();
    qint32 Nx = a.Nx();
    qint32 Ny = a.Ny();
    qint32 Nz = a.Nz();

    if (N == 0)
      {
	out << (qint32)0;
	return out;
      }

    out << Nx << Ny << Nz;
    for (long i = 0; i < N; i++)
      {
	out << a[i];
      }
    return out;
  }
  
  template<typename T, bool checkedArray>
  QDataStream& operator>>(QDataStream& in, Array3D<T,checkedArray>& a)
  {
    qint32 Nx, Ny, Nz;

    in >> Nx;

    if (Nx == 0)
      return in;

    in >> Ny >> Nz;
    

    a.alloc(Nx, Ny, Nz);
    long N = Nx*Ny*Nz;

    for (long i = 0; i < N; i++)
      {
	in >> a[i];
      }
    return in;
  }
};

#endif
