/*+
This is GalaxExplorer (./src/glwidget.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __MYGLWIDGET_H
#define __MYGLWIDGET_H

#include "includeGL.hpp"
#include <QMetaType>
#include <QPushButton>
#include <QGraphicsSceneMouseEvent>
#include <QGLWidget>
#include <list>
#include <utility>
#include <map>
#include <string>
#include "points.hpp"
#include "datasets.hpp"
#include "representations/dataRepresentation.hpp"
#include "glconnect.hpp"
#include "vboObject.hpp"
#include "skybox.hpp"

namespace GalaxExplorer
{
  static const double UNIVERSE_BOXSIZE = 10.0;

  class GLRenderer;
  class Window;
  class GridBuilder;
  class UniverseWidget;
  
  class ViewState: public QObject
  {
    Q_OBJECT

    Q_PROPERTY(float xRotation READ getRotationX WRITE setRotationX)
    Q_PROPERTY(float yRotation READ getRotationY WRITE setRotationY)
    Q_PROPERTY(float zRotation READ getRotationZ WRITE setRotationZ)
    Q_PROPERTY(float distance READ getDistance WRITE setDistance)
    Q_PROPERTY(QColor bgColor READ getBackgroundColor WRITE setBackgroundColor)
    Q_PROPERTY(bool axis READ axisShown WRITE setAxis)
    Q_PROPERTY(float distanceMultiplier READ getMultiplier WRITE setMultiplier)
    Q_PROPERTY(float fov READ getFOV WRITE setFOV)
  public:
    ViewState(QObject *parent = 0);
    virtual ~ViewState();

    void getCenter(float& x, float& y, float& z) const {
      x = centerX;
      y = centerY;
      z = centerZ;
    }

    void getCubeCenter(float& x, float& y, float& z) const {
      x = cubeX;
      y = cubeY;
      z = cubeZ;
    }

    float getDistance() const { return distance; }
    void setDistance(float d);

    float getMultiplier() const { return multiplier; }
    void setMultiplier(float m);

    void setRotationCenter(float x, float y, float z);

    void setFOV(float fov) { this->fov = fov; }
    float getFOV() const { return fov; }

    void moveCube(float x, float y, float z);
    
    ViewState& operator=(const ViewState& v) {
      uwidget = v.uwidget;
      rx = v.rx;
      ry = v.ry;
      rz = v.rz;
      centerX = v.centerX;
      centerY = v.centerY;
      centerZ = v.centerZ;
      distance = v.distance;
      bgColor = v.bgColor;
      shownAxis = v.shownAxis;
      multiplier = v.multiplier;
      fov = v.fov;
      stateUpdated();

      return *this;
    }
    void saveState(QMap<QString,QVariant>& qmap);
    void loadState(const QMap<QString,QVariant>& qmap);

    void setRotationX(float rx);
    void setRotationY(float ry);
    void setRotationZ(float rz);
    
    float getRotationX() const { return rx; }
    float getRotationY() const { return ry; }
    float getRotationZ() const { return rz; }

    const QColor& getBackgroundColor() const { return bgColor; }
    void setBackgroundColor(const QColor& col);

    void setAxis(bool shown) { shownAxis = shown; stateUpdated(); }
    bool axisShown() const { return shownAxis; }

    void getEyeVector(float& x, float& y, float& z);
    void getCameraPosition(float pos[3]);

    void getViewMatrix(float matrix[4][4]);
    void getRotationMatrix(float matrix[4][4]);    

  signals:
    void stateUpdated();
    void cubeUpdated();
    void colorUpdated();
  private:
    UniverseWidget *uwidget;
    QColor bgColor;
    float rx, ry, rz;
    float centerX, centerY, centerZ;
    float cubeX, cubeY, cubeZ;
    float distance;
    bool shownAxis;
    float multiplier;
    float fov;
  };


  class UniverseWidget : public QGraphicsScene
  {
   Q_OBJECT

  public:
    enum UniverseProjection {
      OrthographicProj, PerspectiveProj
    };

    UniverseWidget(Window *parent, QGLWidget *glw, QGraphicsView *view);
    ~UniverseWidget();
    
    QSize minimumSizeHint() const;
    QSize sizeHint() const;

    void setActiveConnectors(const ConnectorVector& v);			  
    void recompileConnection(int c);

    GLRenderer *getRenderer() { return renderer; }
    bool restartRenderer();

    void setProjection(UniverseProjection proj);
    GalaxExplorer::ViewState *getState();

    QList<SkyBox *>& getSkyBoxList();

    QGLWidget *glwidget() { return glWidget; }
    QPushButton *getOptionsButton() { return glOptions; }

    void setOptionMenu(QMenu *menu);

    void setGridBuilder(boost::shared_ptr<GridBuilder> builder);
    boost::shared_ptr<GridBuilder> getGridBuilder() { return grid_builder; }

  public slots:
    void setXRotation(float angle);
    void setYRotation(float angle);
    void setZRotation(float angle);
    void setTravel(int percent);
    void showConnector(const QString& name);
    void hideConnector(const QString& name);
    void recompileAll();
    void setState(const QObject *state);
    void capture(const QString& fileName, int w, int h);
    void updateBgColor();
    void updateLight();
    QObject *getCurrentState() { return getState(); }

  signals:
    void xRotationChanged(float angle);
    void yRotationChanged(float angle);
    void zRotationChanged(float angle);    
    void gl_ready();
    
  public:
    virtual void drawBackground(QPainter *painter, const QRectF& rect);

  protected slots:
    void resizeScene(const QRectF& rect);
    void handleOptionsMenu();

  protected:
    void paintScene();
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

    friend class GLRenderer;

    void drawScene();

 public slots:
    void refreshGL() { paintScene(); }
    void refresh() { update(); }
    void setScreenRefreshing(bool b) { disabledPainting = b; }
    void updateCube();
    void setLightSourceVisible(bool v);
    
 private:
    void lazyInitialize();
    void createLightSource();
    void updateLightSourcePosition();
    void recoverLightSourcePosition();

    void compileConnector(GLConnector& c);

    UniverseProjection projection;
    bool showCube, menuOpened;
    int fbWidth, fbHeight;
    GLRenderer *renderer;

    QGraphicsRectItem *lightItem;
    bool forceUpdateLightSource;

    GLuint program;
    GLuint object;
    VBO_Chain gridChain;
    GLuint velocityList;
    GLdouble travelFraction;
    int xRot;
    int yRot;
    int zRot;
    QPointF lastPos;
    
    GLdouble crossSize;
    GLdouble fractionVeloc;
    
    GLConnectorVector connectors;

    bool capturing, useFrameBuffer;
    ViewState *vs;
    QColor bgColor;
    bool disabledPainting;

    QList<SkyBox *> skybox;

    bool gl_initialized;

    GLRendererFactory *r_factory;
    Window *glWindow;
    QGraphicsView *g_view;
    QGLWidget *glWidget;
    QPushButton *glOptions;

    boost::shared_ptr<GridBuilder> grid_builder;
  };


};

#endif

/*
 * Local variables:
 * mode:c++
 */
