/*+
This is GalaxExplorer (./src/back_grid.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include "includeGL.hpp"
#include <QtOpenGL>
#include <list>
#include <cmath>
#include <algorithm>
#include "glwidget.hpp"
#include "gltools.hpp"
#include "renderers/renderer.hpp"
#include "object_helper.hpp"
#include "vboObject.hpp"

#include "glwidget.hpp"
#include "back_grid.hpp"

using namespace GalaxExplorer;

ScalingCube::ScalingCube(double size)
  : GridBuilder(), sideSize(size)
{
}

void ScalingCube::make(ViewState& vs, VBO_Chain& chain)
{
  static const float cubePoints[][3] = {
    { 1, 1, -1 },
    { -1, 1, -1 },

    { 1, 1, -1 },
    { 1, -1, -1 },

    { -1, -1, 1},
    { -1, 1, 1 },

    { -1, 1, 1 },
    {  1, 1, 1 },

    {  1, 1, 1 },
    {  1,-1, 1 },

    {  1,-1, 1 },
    { -1,-1, 1 },

    {  1,-1, -1},
    {  1,-1, 1 },

    { -1, 1, -1},
    { -1, 1, 1 },

    {  1, 1,-1 },
    {  1, 1, 1 }
  };
  static const int numCubePoints = sizeof(cubePoints)/(3*sizeof(float));
  float cx, cy, cz;

  vs.getCubeCenter(cx, cy, cz);
  VBO_Descriptor desc;
  desc.numcomponents = 4;
  desc.subcolors = true;

  VBO_Object scalingCube(desc), axesCones(desc);

  scalingCube.begin();

  for (int i = 0; i < numCubePoints; i++)
    {
      scalingCube.addPoint(sideSize*cubePoints[i][0] - cx, 
			   sideSize*cubePoints[i][1] - cy, 
			   sideSize*cubePoints[i][2] - cz);
      scalingCube.setColor(0, 1, 0, 1);
    }

  scalingCube.addPoint(-sideSize - cx, -sideSize - cy, -sideSize - cz);
  scalingCube.setColor(1, 0, 0, 1);
  scalingCube.addPoint(-sideSize - cx, -sideSize - cy, sideSize - cz);
  scalingCube.setColor(1, 0, 0, 1);


  scalingCube.addPoint(-sideSize - cx, -sideSize - cy, -sideSize - cz);
  scalingCube.setColor(0, 1, 0, 1);
  scalingCube.addPoint(-sideSize - cx, sideSize - cy, -sideSize - cz);
  scalingCube.setColor(0, 1, 0, 1);


  scalingCube.addPoint(-sideSize - cx, -sideSize - cy, -sideSize - cz);
  scalingCube.setColor(0, 0, 1, 1);
  scalingCube.addPoint(sideSize - cx, -sideSize - cy, -sideSize - cz);
  scalingCube.setColor(0, 0, 1, 1);

  scalingCube.end();

  ConeDrawer cone(100);
  
  axesCones.begin();
  cone.setColor(1, 0, 0, 1);
  cone.draw(axesCones, 0, 0, 1,
	    -sideSize - cx, -sideSize - cy, sideSize - cz, 
	    0.2, 1.0);

  cone.setColor(0, 1, 0, 1);
  cone.draw(axesCones, 0, 1, 0,
	    -sideSize - cx, sideSize - cy, -sideSize - cz, 
	    0.2, 1.0);

  cone.setColor(0, 0, 1, 1);
  cone.draw(axesCones, 1, 0, 0,
	    sideSize - cx, -sideSize - cy, -sideSize - cz, 
	    0.2, 1.0);

  axesCones.end();

  chain.reset();
  chain.push(GL_LINES, scalingCube);
  chain.push(GL_TRIANGLE_FAN, axesCones);
}

ScalingCylinder::ScalingCylinder(double r, double sep)
  : GridBuilder(), radius(r), cylSep(sep)
{
}

void ScalingCylinder::make(ViewState& vs, VBO_Chain& chain)
{
  static const int NUM_CYL_STEP = 16;
  float cx, cy, cz;
  VBO_Descriptor desc;
  desc.numcomponents = 4;
  desc.subcolors = true;

  VBO_Object cylTop(desc), cylBottom(desc), cylBottomTop(desc);

  vs.getCubeCenter(cx, cy, cz);

  cylTop.begin();
  cylBottom.begin();
  cylBottomTop.begin();

  for (int i = 0; i < NUM_CYL_STEP; i++)
    {
      double a = radius*cos(2*M_PI*i/NUM_CYL_STEP), b = radius*sin(2*M_PI*i/NUM_CYL_STEP);

      cylTop.addPoint(a - cx, b - cy, cylSep - cz);
      cylTop.setColor(1,0,0,1);

      cylBottom.addPoint(a - cx, b - cy, - cylSep - cz);
 
     cylBottom.setColor(1,0,0,1);

      if ((i % (NUM_CYL_STEP/8)) == 0)
        {
          cylTop.addPoint(- cx, -cy, cylSep-cz);
          cylTop.setColor(1,0,0,1);
          cylTop.addPoint(a- cx, b-cy, cylSep-cz);
          cylTop.setColor(1,0,0,1);
          cylBottom.addPoint(- cx, -cy, -cylSep-cz);
          cylBottom.setColor(1,0,0,1);
          cylBottom.addPoint(a- cx, b-cy,-cylSep-cz);
          cylBottom.setColor(1,0,0,1);

          cylBottomTop.addPoint(a-cx,b-cy,cylSep-cz);
          cylBottomTop.setColor(1,0,0,1);
          cylBottomTop.addPoint(a-cx,b-cy,-cylSep-cz);
          cylBottomTop.setColor(1,0,0,1);
        }
    }

  cylTop.addPoint(radius - cx,  - cy, cylSep - cz);
  cylTop.setColor(1,0,0,1);
  cylBottom.addPoint(radius - cx,  - cy, - cylSep - cz);
  cylBottom.setColor(1,0,0,1);

  cylTop.end();
  cylBottom.end();
  cylBottomTop.end();

  chain.reset();
  chain.push(GL_LINE_STRIP, cylTop);
  chain.push(GL_LINE_STRIP, cylBottom);
  chain.push(GL_LINES, cylBottomTop);
}



void GalaxExplorer::buildGridMapper(GridBuilderMapper& builders)
{
  builders[QObject::tr("Cube")] = 
    boost::shared_ptr<GridBuilder>(new ScalingCube(UNIVERSE_BOXSIZE));

  builders[QObject::tr("Cylinder")] = 
    boost::shared_ptr<GridBuilder>(new ScalingCylinder(UNIVERSE_BOXSIZE, 2.0));
}
