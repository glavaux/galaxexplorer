/*+
This is GalaxExplorer (./src/representationDialog.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <cmath>
#include <QtGui>
#include "representationDialog.hpp"
#include "representations/transferFunction.hpp"
#include "representations/transferRepresentation.hpp"
#include "connectorDialog.hpp"
#include <iostream>
#include "inits.hpp"
#include "qtfe/Qtfe.h"

using namespace std;
using namespace GalaxExplorer;


RepresentationConfigDialog::RepresentationConfigDialog(DataRepresentation *rep, QWidget *parent)
  : QDialog(parent), representation(rep)
{
  createDialog();
 
}

RepresentationConfigDialog::~RepresentationConfigDialog()
{
}

void RepresentationConfigDialog::createDialog()
{
  QVBoxLayout *vbox = new QVBoxLayout;
  const QMetaObject *meta = representation->metaObject();

  setWindowTitle(tr("Representation ") + representation->property("name").toString());

  table = new QTableWidget(meta->propertyCount()-2,2);

  int tableId = 0;
  for (int i = 0; i < meta->propertyCount(); i++)    
    {
      QMetaProperty metaprop = meta->property(i);
      QString propName = metaprop.name();

      if (propName == QString("objectName") || propName == QString("name"))
        continue;

      cout << "Property name = " << qPrintable(propName) << endl;

      QVariant propValue = representation->property(qPrintable(propName));
      QTableWidgetItem *item = new QTableWidgetItem(propName);

      table->setItem(tableId, 0, item);

      switch (metaprop.type())
      {
      case QVariant::String:
        {
          QLineEdit *line = new QLineEdit(propValue.toString());
          table->setCellWidget(tableId, 1, line);

          addTracking(line, i, SIGNAL(returnPressed()));
          break;
        }
      case QVariant::Double:
        {
          QDoubleSpinBox *spinBox = new QDoubleSpinBox;
          double value = propValue.toDouble();

          table->setCellWidget(tableId, 1, spinBox);
          spinBox->setMinimum(-INFINITY);
          spinBox->setMaximum(INFINITY);
          spinBox->setSingleStep(value / 10);
                spinBox->setDecimals(5);
          spinBox->setValue(value);

          addTracking(spinBox, i, SIGNAL(valueChanged(double)));
          break;
        }
      case QVariant::Bool:
        {
          QCheckBox *button = new QCheckBox;
          bool value = propValue.toBool();

          table->setCellWidget(tableId, 1, button);
          button->setCheckState((value) ? Qt::Checked : Qt::Unchecked);

          addTracking(button, i, SIGNAL(stateChanged(int)));
          break;
        }
      case QVariant::Color:
        {
          QColor color = propValue.value<QColor>();
          QPushButton *colorButton = new QPushButton();
          QPalette palette = QApplication::palette();

          colorButton->setFlat(true);
          colorButton->setAutoFillBackground (true);
          colorButton->setObjectName("colorButton");
          palette.setColor(QPalette::Button, color);

          colorButton->setPalette(palette);

          table->setCellWidget(tableId, 1, colorButton);

          SelectorPropertyTracking *tracking = addTracking(colorButton, i, SIGNAL(clicked()));
          connect(tracking, SIGNAL(propertyModified(QObject*,int)), this, SLOT(changeColor(QObject*)));
          break;
        }
      case QVariant::Int:
        {
          QSpinBox *spinBox = new QSpinBox;
          int value = propValue.toInt();

          table->setCellWidget(tableId, 1, spinBox);
          spinBox->setMinimum(1);
          spinBox->setMaximum(100);
          spinBox->setValue(value);

          addTracking(spinBox, i, SIGNAL(valueChanged(int)));
          break;
        }
      case QVariant::UserType:
        {
          if (QString(metaprop.typeName()) == "GalaxExplorer::TransferFunction")
            {
              TransferFunction f = propValue.value<TransferFunction>();
              TransferRepresentation *tr = new TransferRepresentation(f);
              
              table->setCellWidget(tableId, 1, tr);
              SelectorPropertyTracking *tracking = addTracking(tr, i, SIGNAL(clicked()));
              connect(tracking, SIGNAL(propertyModified(QObject*,int)), this, SLOT(changeTransferFunction(QObject*)));
            }
        }
      default:
        break;
      }
     tableId++;
   }
  vbox->addWidget(table);

  QHBoxLayout *buttonLayout = new QHBoxLayout;
  vbox->addLayout(buttonLayout);  
  
  QPushButton *quitButton = new QPushButton(QObject::tr("Finish"));
  QPushButton *applyButton = new QPushButton(QObject::tr("Apply"));
  applyButton->setObjectName("applybutton");
  buttonLayout->addWidget(applyButton);
  buttonLayout->addWidget(quitButton);

  connect(quitButton, SIGNAL(clicked(bool)), this, SLOT(accept()));
  connect(applyButton, SIGNAL(clicked(bool)), this, SLOT(applyModifications()));
  
  setLayout(vbox);
}

bool RepresentationConfigDialog::runConfig()
{
  wasModified = false;

  exec();

  return wasModified;
}

void RepresentationConfigDialog::changeColor(QObject *gui)
{
  QPushButton *colorButton = qobject_cast<QPushButton *>(gui);
  QPalette palette = colorButton->palette();

  QColor newColor = QColorDialog::getColor ( palette.color(QPalette::Button), this);
  if (newColor.isValid())
    {
      palette.setColor(QPalette::Button, newColor);      
      colorButton->setPalette(palette);  
    }
}

void RepresentationConfigDialog::changeTransferFunction(QObject *gui)
{
  TransferRepresentation *tr = qobject_cast<TransferRepresentation *>(gui);
  QDialog *dlg = new QDialog(this);
  QVBoxLayout *vbox = new QVBoxLayout;
  
  Qtfe *fe = new Qtfe;
  fe->addCanals(4);
  fe->addOutputs(1);
  fe->bindCanaltoOutputR(0, 0);
	fe->bindCanaltoOutputG(1, 0);
	fe->bindCanaltoOutputB(2, 0);
	fe->bindCanaltoOutputA(3, 0);

  {
    const TransferFunction& tf = tr->transfer();

    for (int i = 0; i < 4; i++)
      {
        const QVector<TransferPoint>& tp = tf.channel[i];
        int N = tp.size();
        QtfeCanal *canal = fe->getCanal(i);
        
        canal->setFirstPoint(tp[0].color);

        for (int j = 1; j < N-1; j++)
          {
            canal->insertPoint(QPointF(tp[j].x, tp[j].color));
          }
          
        canal->setLastPoint(tp[N-1].color);
      }
  }
  vbox->addWidget(fe);
  
  QHBoxLayout *hbox = new QHBoxLayout;
  QPushButton *pb_ok = new QPushButton(QObject::tr("Ok"));
  QPushButton *pb_cancel = new QPushButton(QObject::tr("Cancel"));
  
  connect(pb_ok, SIGNAL(clicked()), dlg, SLOT(accept()));
  connect(pb_cancel, SIGNAL(clicked()), dlg, SLOT(reject()));
  
  hbox->addWidget(pb_ok);
  hbox->addWidget(pb_cancel);
  vbox->addLayout(hbox);
  
  dlg->setLayout(vbox);
  
  if (dlg->exec())
    {
      TransferFunction tf;
      TransferPoint p;

      for (int i = 0; i < 4; i++)
        {
          QtfeCanal *canal = fe->getCanal(i);
          const QList<QPointF*>& pnts = canal->getPoints();
          tf.channel[i].clear();

          p.x = 0;
          p.color = fe->getCanal(i)->getFirstPoint();
          tf.channel[i].push_back(p);
  
          for (QList<QPointF*>::const_iterator j = pnts.begin()+1;
               j+1 != pnts.end();
               ++j)
            {
              p.x = (*j)->x();
              p.color = (*j)->y();
              tf.channel[i].push_back(p);
            }

          p.x = 1;
          p.color = fe->getCanal(i)->getLastPoint();
          tf.channel[i].push_back(p);
        }

      tr->updateTransferFunction(tf);
    }
}

void  RepresentationConfigDialog::applyModifications()
{
  const QMetaObject *meta = representation->metaObject();

  int numProps = table->rowCount();

  for (int i = 0; i < numProps; i++)    
    {      
      QTableWidgetItem *item = table->item(i, 0);
      QString propName = item->text();
      QByteArray propStrArray = propName.toAscii();
      char *propStr= propStrArray.data();

      std::cout  << "property = " << propStr << std::endl;

      QMetaProperty metaprop = meta->property(meta->indexOfProperty(propStr));
      QWidget *widget = table->cellWidget(i, 1);

      switch (metaprop.type())
      {
      case QVariant::String:
        {
          QLineEdit *line = qobject_cast<QLineEdit *>(widget);
          representation->setProperty(propStr, line->text());
          break;
        }
      case QVariant::Color:
        {
          QPushButton *colorButton = qobject_cast<QPushButton *>(widget);
          QPalette palette = colorButton->palette();
          QColor color = palette.color(QPalette::Button);
          
          representation->setProperty(propStr, color);
          break;
        }
      case QVariant::Double:
        {
          QDoubleSpinBox *spinbox = qobject_cast<QDoubleSpinBox *>(widget);
          
          cout << "set " << propStr << " to " << spinbox->value() << endl;
          representation->setProperty(propStr, spinbox->value());
          break;
        }
      case QVariant::Int:
        {
          QSpinBox *spinbox = qobject_cast<QSpinBox *>(widget);
          representation->setProperty(propStr, spinbox->value());
          break;
        }
      case QVariant::Bool:
        {
          QCheckBox *box = qobject_cast<QCheckBox *>(widget);
          
          representation->setProperty(propStr, box->isChecked());
          break;
        }
      case QVariant::UserType:
        {
          if (QString(metaprop.typeName()) == "GalaxExplorer::TransferFunction")
            {
              TransferRepresentation *tr = qobject_cast<TransferRepresentation *>(widget);
              
              representation->setProperty(propStr, QVariant::fromValue<TransferFunction>(tr->transfer()));
            }
          break;
        }
      default:
        break;
      }
    }

  emit representationModified();
}

SelectorPropertyTracking *RepresentationConfigDialog::addTracking(QObject *gui, int id, const char *signalName)
{
  SelectorPropertyTracking *tracking = new SelectorPropertyTracking(gui, id);

  tracking->setParent(this);
  connect(gui, signalName, tracking, SLOT(modified()));
  connect(tracking, SIGNAL(propertyModified(QObject*,int)), this, SLOT(propertyModified()));

  return tracking;
}

void RepresentationConfigDialog::propertyModified()
{
  wasModified = true;
}

