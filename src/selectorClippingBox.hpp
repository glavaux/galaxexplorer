/*+
This is GalaxExplorer (./src/selectorClippingBox.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef _SELECT_CLIPPING_HPP
#define _SELECT_CLIPPING_HPP

#include <QObject>
#include "datasets.hpp"
#include "dataSelectors.hpp"

namespace GalaxExplorer
{

  class DataSelectionClippingBox: public DataSelectorDescriptor
  {
    Q_OBJECT
  public:
    DataSelectionClippingBox();
    virtual ~DataSelectionClippingBox();


    virtual DataSetSelector *newSelector();
  };

  class SelectClippingBox: public DataSetSelector
  {
    Q_OBJECT
    
    Q_PROPERTY(double xmin READ xmin WRITE setXMin)
    Q_PROPERTY(double xmax READ xmax WRITE setXMax)
    Q_PROPERTY(double ymin READ ymin WRITE setYMin)
    Q_PROPERTY(double ymax READ ymax WRITE setYMax)
    Q_PROPERTY(double zmin READ zmin WRITE setZMin)
    Q_PROPERTY(double zmax READ zmax WRITE setZMax)
  public:
    SelectClippingBox(DataSelectionClippingBox *s);
    ~SelectClippingBox();;

    void setXMin(double x);
    double xmin();
    void setXMax(double x);
    double xmax();

    void setYMin(double x);
    double ymin();
    void setYMax(double x);
    double ymax();

    void setZMin(double x);
    double zmin();
    void setZMax(double x);
    double zmax();

    virtual void loadState(QDataStream& s);
    virtual void saveState(QDataStream& s);

    virtual uint32_t getFirst(const DataSet& d, uint32_t cur) const;
  private:
    double minClip[3];
    double maxClip[3];
  };

};


#endif
