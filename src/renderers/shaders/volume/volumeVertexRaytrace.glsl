#version 120
attribute vec3 cube_coord;
varying vec3 enteringCoord;

void ShadeVertex()
{
  gl_FrontColor = gl_Color;
  enteringCoord = cube_coord;
}
