varying vec3 exitingCoord;

void main(void)
{
  gl_FragColor = vec4(exitingCoord.xyz,1.0);
}

