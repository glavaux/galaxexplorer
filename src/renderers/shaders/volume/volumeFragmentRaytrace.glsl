#version 120
#extension GL_ARB_texture_rectangle : enable

#include "/volume/mod_sampler.frag"

varying vec3 enteringCoord;
uniform sampler2DRect exitingCoord;
uniform sampler3D cube;
uniform int Nsteps;
uniform sampler1D transferFunction;
uniform VolumeStruct volstruct;

vec4 getTransferValue(VolumeStruct volstruct, float value)
{
//  return texture1D(volstruct.transfer, value);
  return vec4(value, value, value, exp(10*(value-1)));
}


vec4 ShadeFragment()
{
  vec3 exit_point = texture2DRect(exitingCoord, gl_FragCoord.xy).xyz;
  vec3 entry_point = enteringCoord;
  int step;
  vec4 fColor = vec4(0,0,0,1);
  float dL = length(entry_point-exit_point) / Nsteps;

  for (step = 0; step < Nsteps; step++)
    {
      float t = float(step) / Nsteps;
      vec3 pos = entry_point*t + (1-t)*exit_point;
      vec4 c = computeLocalLighting(volstruct, cube, pos.xyz);
      
      fColor.rgb = c.rgb * (1-c.a) + c.a*fColor.rgb;
      fColor.a = (1-c.a)*fColor.a;
    }

  fColor.a = 0.8;//-fColor.a;
  
  return fColor;
}
