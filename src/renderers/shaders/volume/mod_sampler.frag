struct VolumeStruct {
  vec3 volume_step;
  vec3 light_pos;
};

vec4 getTransferValue(VolumeStruct,float);

vec4 textureLookup3D(sampler3D t, vec3 x)
{
  return texture3D(t,  x);
}

float getValue3D_R(sampler3D cube, vec3 position)
{
  return textureLookup3D(cube, position).r; 
}

vec3 computeGradientR(VolumeStruct volstruct, sampler3D cube, vec3 position)
{
  vec3 delta = volstruct.volume_step;
  float GX = 
   (getValue3D_R(cube, position+vec3(delta.x,0,0)) -
   getValue3D_R(cube, position-vec3(delta.x,0,0)))/(2*delta.x);
  float GY = 
   (getValue3D_R(cube, position+vec3(0,delta.y,0)) -
   getValue3D_R(cube, position-vec3(0,delta.y,0)))/(2*delta.y);
  float GZ = 
   (getValue3D_R(cube, position+vec3(0,0,delta.z)) -
   getValue3D_R(cube, position-vec3(0,0,delta.z)))/(2*delta.z);
   
  return vec3(GX,GY,GZ);
}

vec4 computeLocalLighting(VolumeStruct volstruct, sampler3D cube, vec3 position)
{
  vec3 gradient = computeGradientR(volstruct, cube, position);
  float value = getValue3D_R(cube, position);
  vec4 transfer = getTransferValue(volstruct, value);
  
  vec3 lp = normalize(volstruct.light_pos);
  float alpha = dot(normalize(gradient), normalize(volstruct.light_pos));
  vec3 color = vec3(0,0,0);
 
  if (alpha > 0)
    color = alpha * transfer.rgb;
  
  color += transfer.rgb*0.01;
  
  return vec4(color, transfer.a);
}
