attribute vec3 cube_coord;
varying vec3 exitingCoord;


void main(void)
{
  gl_Position = ftransform();
  gl_FrontColor = gl_Color;
  exitingCoord = cube_coord;
}
