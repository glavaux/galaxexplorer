#version 120
uniform int distanceCorrection;
uniform float pointSize;
uniform sampler1D auxFieldTable;

attribute float auxField;

void ShadeVertex()
{
  vec4 r = gl_ModelViewMatrix * gl_Vertex;
  vec4 cc = texture1D(auxFieldTable, auxField);
  
  if (distanceCorrection != 0)
    gl_PointSize = pointSize / length(r.xyz);
  else
    gl_PointSize = pointSize;
    
  
  gl_FrontColor = cc;
}
