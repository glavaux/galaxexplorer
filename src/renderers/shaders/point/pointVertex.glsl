uniform int distanceCorrection;
uniform float pointSize;

void ShadeVertex()
{
  vec4 r = gl_ModelViewMatrix * gl_Vertex;
  
  if (distanceCorrection != 0)
    gl_PointSize = pointSize / length(r.xyz);
  else
    gl_PointSize = pointSize;
    
  gl_FrontColor = gl_Color;
}
