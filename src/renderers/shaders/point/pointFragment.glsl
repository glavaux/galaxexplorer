#version 120
uniform float haloConcentration;

vec4 ShadeFragment()
{
  vec2 xx = vec2(gl_PointCoord.x - 0.5, gl_PointCoord.y - 0.5);
  float l2 = length(xx);
  
  return gl_Color/(1.0+l2*haloConcentration);
}
