/*+
This is GalaxExplorer (./src/renderers/shaders/wavg/wavg_final_fragment.glsl) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#version 120
#extension GL_ARB_texture_rectangle : enable
uniform sampler2DRect ColorTex0, ColorTex1;
uniform vec3 BackgroundColor;
uniform float interactiveScale;

vec4 getScaledTex(in sampler2DRect t, in vec2 fxy)
{
  float s = interactiveScale;
  vec4 c;

  c = 20.0f*texture2DRect(t, s*fxy.xy);
  c += texture2DRect(t, vec2(s*fxy.x+1.0f,s*fxy.y));
  c += texture2DRect(t, vec2(s*fxy.x-1.0f,s*fxy.y));
  c += texture2DRect(t, vec2(s*fxy.x,s*fxy.y-1.0f));
  c += texture2DRect(t, vec2(s*fxy.x,s*fxy.y+1.0f));
  c /= 24.0f;
  return c;
}

void main(void)
{
  vec2 fxy = gl_FragCoord.xy;
	float n = getScaledTex(ColorTex1, fxy).r;
	vec4 totalColor = getScaledTex(ColorTex0, fxy);

	if (n == 0.0f) {
	   gl_FragColor.rgb = BackgroundColor;
	   return;
	}

	vec3 avgColor = totalColor.rgb / totalColor.a;
	float avgAlpha = totalColor.a / n;
	float T;

	if (avgAlpha > 0.99)
	  T = 0.0f;
	else	
	  T = pow(1.0f-avgAlpha, n);

	gl_FragColor.rgb = avgColor*(1.0f-T) + BackgroundColor * T;
}
