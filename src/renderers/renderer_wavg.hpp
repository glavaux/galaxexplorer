/*+
This is GalaxExplorer (./src/renderers/renderer_wavg.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __GLREND_WEIGHED_AVG_HPP
#define __GLREND_WEIGHED_AVG_HPP

#include <QtCore>
#include "includeGL.hpp"
#include "renderer.hpp"
#include "GLSLProgramObject.h"
#include "renderTarget.hpp"

namespace GalaxExplorer
{
  class UniverseWidget;
  class GLConnector;
  
  class GLRendererWeighedAvg: public GLRenderer {
    Q_OBJECT

  public:
    GLRendererWeighedAvg(UniverseWidget& w)
      throw(RendererInitFailure);
    virtual ~GLRendererWeighedAvg();

    virtual bool usingShaders();
    virtual void assembleProgramShader(GLConnector& c, GLSLProgramObject& o, int id);
    virtual void assembleProgramShader(MultiShader& ms, GLSLProgramObject& o);
    virtual void useShaderInConnector(GLConnector& c, int id);
    virtual void useShader(MultiShader& c);
    virtual void useDefaultShader();
    virtual GLSLProgramObject& getShader(GLConnector& c, int id);
    virtual GLSLProgramObject& getShader(MultiShader& c);

    virtual void paint();    
    virtual void notifySize(int w, int h);    

    virtual void beginSnapshot(int w, int h);
    virtual void endSnapshot();

    virtual void setInteractiveMode(bool interactive);
    virtual bool getInteractiveMode() const { return interactiveMode;  }
    virtual RenderTarget *createTempRenderTarget();

  private:
    GLSLProgramObject shaderInit, shaderFinal, defaultInit;
    float backgroundColor[3];
    int pass, prevId;
    bool inDefaultMode;
    int oldw, oldh;
    GLuint universeFBO, universeRenderBuffer;
    GLuint accumulationTexId[2], accumulationFboId;
    bool interactiveMode;
    int effectiveWidth, effectiveHeight;
    RenderTarget *mainTarget, *screenTarget;

    void initShaders()
      throw(RendererInitFailure);
    void drawQuad();
    void initFrameBuffer()
      throw(RendererInitFailure);
    void deleteFrameBuffer();

    void initOutputFrameBuffer();
    void deleteOutputFrameBuffer();

    void activateTarget1();
    void deActivateTarget1();
   };

};

#endif
