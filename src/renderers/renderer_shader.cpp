/*+
This is GalaxExplorer (./src/renderers/renderer_shader.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include "includeGL.hpp"
#include "renderer_shader.hpp"
#include <iostream>
#include <QtCore>
#include <QtGui>
#include "glwidget.hpp"
#include "renderer.hpp"

using namespace GalaxExplorer;
using namespace std;

#define CHECK_GL_ERRORS \
{ \
    GLenum err = glGetError(); \
    if (err) \
      { \
        QString s; \
        cout << "Error " << err << " at line " << __LINE__ << endl; \
      } \
}


/* -----------------------------
 * Rendering with weighed avg
 * -----------------------------
 */
GLRendererShader::GLRendererShader(UniverseWidget& w)
  throw (RendererInitFailure)
  : GLRenderer(w)
{
  fbWidth = 100;
  fbHeight = 100;
  outputFramebuffer = 0;
  initShaders();
  inDefaultMode = false;

  backgroundColor[0] = 0;
  backgroundColor[1] = 0;
  backgroundColor[2] = 0;
  current_state->setViewport(0, 0, fbWidth,  fbHeight);
}

GLRendererShader::~GLRendererShader()
{
}

void GLRendererShader::notifySize(int w, int h)
{
  fbWidth = w;
  fbHeight = h;
  current_state->setViewport(0, 0, fbWidth,  fbHeight);
}

void GLRendererShader::initShaders()
  throw(RendererInitFailure)
{
  try
    {
      GLSLShaderObject init("passthrough/shader_passthrough_fragment.glsl", GL_FRAGMENT_SHADER);
      GLSLShaderObject initVertex("passthrough/shader_passthrough_vertex.glsl", GL_VERTEX_SHADER);

      shaderBase.attachShader(initVertex);
      shaderBase.attachShader(init);
      
      GLSLShaderObject vertex_default("defaultVertex.glsl", GL_VERTEX_SHADER);
      GLSLShaderObject frag_default("defaultFragment.glsl", GL_FRAGMENT_SHADER);
      GLSLProgramObject defaultShader;
      defaultShader.attachShader(vertex_default);
      defaultShader.attachShader(frag_default);
      
      defaultShader.pushShaders(defaultShader);
      defaultShader.pushShaders(shaderBase);
      defaultShader.link();     
    }
  catch (const ShaderCompileError& e)
    {
      QErrorMessage::qtHandler()->showMessage(tr("The shader %1 failed to compile").arg(e.getPath()));
      throw RendererInitFailure();
    }
}

static void checkFBO(int id)
{
  GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
  if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
	cout << "error in " << id << " status is " << status << endl;
}

void GLRendererShader::beginSnapshot(int w, int h)
{
  oldw = fbWidth;
  oldh = fbHeight;

  cout << "Resizing buffer to (" << w << "," << h << ")" << endl;
  notifySize(w, h);

  initOutputFrameBuffer();

  outputFramebuffer = universeFBO;
  cout << "Drawing..." << endl;
  universe.refreshGL();
  cout << "Pointing read buffer to framebuffer" << endl;
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, universeFBO);
  glReadBuffer(GL_FRAMEBUFFER_EXT);
}

void GLRendererShader::endSnapshot()
{
  cout << "Setting back read buffer to standard position" << endl;
  glReadBuffer(GL_NONE);
  outputFramebuffer = 0;
  cout << "Resetting size" << endl;
  notifySize(oldw, oldh);

  deleteOutputFrameBuffer();
}

bool GLRendererShader::usingShaders()
{
  return true;
}

void GLRendererShader::assembleProgramShader(GLConnector& c, GLSLProgramObject& o, int id)
{
  c.getShader(id).destroy();
  c.getShader(id).pushShaders(shaderBase);
  c.getShader(id).pushShaders(o);
  c.getShader(id).setAllocator(new GLSLSimpleAllocator(0));
  c.getShader(id).link();
}

void GLRendererShader::assembleProgramShader(MultiShader& ms, GLSLProgramObject& o)
{
  ms.resize(1);
  ms[0].destroy();
  ms[0].pushShaders(shaderBase);
  ms[0].pushShaders(o);
  ms[0].setAllocator(new GLSLSimpleAllocator(0));
  ms[0].link();
  //  ms[1].setTextureUnit("DepthTex", 0);
}

void GLRendererShader::useShaderInConnector(GLConnector& c, int id)
{
  c.getShader(id).bind();
  inDefaultMode = false;
}

void GLRendererShader::useShader(MultiShader& c)
{
  c[0].bind();
  inDefaultMode = false;
}


void GLRendererShader::useDefaultShader()
{
  if (inDefaultMode)
    return;

  defaultShader.bind();
  inDefaultMode = true;
}


GLSLProgramObject& GLRendererShader::getShader(GLConnector& c, int id)
{
  return c.getShader(id);
}

GLSLProgramObject& GLRendererShader::getShader(MultiShader& shader)
{
  return shader[0];
}

void GLRendererShader::paint()
{
  QColor bgColor = universe.getState()->getBackgroundColor();
  GLfloat backgroundColor[3] = {
    bgColor.redF(), bgColor.greenF(), bgColor.blueF()
  };

  glViewport(0, 0, fbWidth, fbHeight);

  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, outputFramebuffer);
  glClearColor(backgroundColor[0], backgroundColor[1], backgroundColor[2], 0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);


  if (outputFramebuffer != 0 || true)
    glDrawBuffer(GL_FRAMEBUFFER_EXT);
  else
    glDrawBuffer(GL_BACK);
  
  inDefaultMode = true;
  defaultShader.bind();
  drawScene();
  defaultShader.unbind();
  glDisable(GL_DEPTH_TEST);
}  

void GLRendererShader::initOutputFrameBuffer()
{
  glGenFramebuffersEXT(1, &universeFBO);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, universeFBO);
  
  // Create the render buffer for depth

  glGenRenderbuffersEXT(1, &universeRenderBuffer);
  glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, universeRenderBuffer);
  glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, fbWidth, fbHeight);

  glGenTextures(1, &universe_tex);
  
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, universe_tex);
     glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA8, fbWidth, fbHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE_ARB, universe_tex, 0);
  
  glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, universeRenderBuffer);

  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  
  GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
  if(status != GL_FRAMEBUFFER_COMPLETE_EXT)
    throw RendererInitFailure();
}


void GLRendererShader::deleteOutputFrameBuffer()
{
  glDeleteFramebuffersEXT(1, &universeFBO);
  glDeleteRenderbuffersEXT(1, &universeRenderBuffer);
  glDeleteTextures(1, &universe_tex);
}

RenderTarget *GLRendererShader::createTempRenderTarget()
{
  RenderTarget *t = new_Basic_FBO_Target(this, fbWidth, fbHeight);
  
  tempTargets.insert(t);
  return t;
}
