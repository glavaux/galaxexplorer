/*+
This is GalaxExplorer (./src/renderers/renderer.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include "includeGL.hpp"
#include <iostream>
#include <QtCore>
#include <QtGui>
#include "glwidget.hpp"
#include "renderer.hpp"
#include "render_utils.hpp"
#include "glerror.hpp"
#include "renderTarget_screen.hpp"

using namespace GalaxExplorer;
using namespace std;

static QString Galax_glErrorMessage = QObject::tr("Error %x at line %d\n");
//        s= (Galax_glErrorMessage.arg(err).arg(__LINE__));
//        QErrorMessage::qtHandler()->showMessage(Galax_glErrorMessage.arg(err).arg(__LINE__)); 
GLRenderer::GLRenderer(UniverseWidget& w)
  throw (RendererInitFailure)
  : universe(w)
{
  QSettings settings;

  fbWidth = 100;
  fbHeight = 100;
  lightPosition.x = 0;
  lightPosition.y = 0;
  lightPosition.z = 20;
  interactiveResolution = settings.value("interactiveResolution", 4).toInt();
  current_state = new RenderViewState();
  current_state->setViewport(0, 0, fbWidth,  fbHeight);
}

GLRenderer::~GLRenderer()
{
  delete current_state;
}

RenderTarget *GLRenderer::createTempRenderTarget()
{
  return 0;
}

void GLRenderer::destroyRenderTarget(RenderTarget* tgt)
{
  set<RenderTarget *>::iterator iter = tempTargets.find(tgt);

  if (iter == tempTargets.end()) 
    throw InvalidRenderTarget();

  tempTargets.erase(iter); 
  delete tgt;
}

void GLRenderer::pushMesh(boost::shared_ptr<GeneralMesh> m)
{
}

void GLRenderer::beginSnapshot(int w, int h)
{
}

void GLRenderer::endSnapshot()
{
}

bool GLRenderer::usingShaders()
{
  return false;
}

void GLRenderer::assembleProgramShader(GLConnector& c, GLSLProgramObject& o, int id)
{
  cout << "Renderer " << metaObject()->className() << " does not support shaders." << endl;
  abort();
}

void GLRenderer::assembleProgramShader(MultiShader& c, GLSLProgramObject& o)
{
  cout << "Renderer " << metaObject()->className() << " does not support shaders." << endl;
  abort();
}

void GLRenderer::useShaderInConnector(GLConnector& c, int id)
{
  cout << "Renderer " << metaObject()->className() << " does not support shaders." << endl;
  abort();
}

void GLRenderer::useShader(MultiShader& c)
{
  cout << "Renderer " << metaObject()->className() << " does not support shaders." << endl;
  abort();
}


void GLRenderer::useDefaultShader()
{
  cout << "Renderer " << metaObject()->className() << " does not support shaders." << endl;
  abort();
}

GLSLProgramObject& GLRenderer::getShader(GLConnector& c, int id)
{
  cout << "Renderer " << metaObject()->className() << " does not support shaders." << endl;
  abort();
  return dummy;
}

GLSLProgramObject& GLRenderer::getShader(MultiShader& c)
{
  cout << "Renderer " << metaObject()->className() << " does not support shaders." << endl;
  abort();
  return dummy;
}

void GLRenderer::reloadViewState()
{
  float Mat[16];
  
  glMatrixMode(GL_PROJECTION);
  current_state->getGLProjectionMatrix(Mat);
  glLoadMatrixf(Mat);
  glMatrixMode(GL_MODELVIEW);
  current_state->getGLModelViewMatrix(Mat);
  glLoadMatrixf(Mat);
}

void GLRenderer::drawScene()
{
  reloadViewState();  
  universe.drawScene();
}

void GLRenderer::notifySize(int w, int h)
{
  fbWidth = w;
  fbHeight = h;
  current_state->setViewport(0, 0, fbWidth,  fbHeight);
}

GLBasicRenderer::GLBasicRenderer(UniverseWidget& w)
  throw(RendererInitFailure)
  : GLRenderer(w)
{
  mainTarget = new RenderScreenTarget(this, fbWidth, fbHeight);
}

GLBasicRenderer::~GLBasicRenderer()
{
  delete mainTarget;
}

void GLBasicRenderer::notifySize(int w, int h)
{
  GLRenderer::notifySize(w, h);
  delete mainTarget;
  mainTarget = new RenderScreenTarget(this, fbWidth, fbHeight);
}

void GLBasicRenderer::paint()
{
  QColor bgColor = universe.getState()->getBackgroundColor();

  glViewport(0, 0, fbWidth, fbHeight);
  glShadeModel(GL_FLAT);
  glClearColor(bgColor.redF(), bgColor.greenF(), bgColor.blueF(), 1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  drawScene();
  glDisable(GL_DEPTH_TEST);
}

/* -----------------------------
 * Rendering through framebuffer
 * -----------------------------
 */
GLBufferedRenderer::GLBufferedRenderer(UniverseWidget& w)
  throw (RendererInitFailure)
  : GLRenderer(w)
{
  outputFramebuffer = 0;
  initFrameBuffer();
}

GLBufferedRenderer::~GLBufferedRenderer()
{
  deleteFrameBuffer();
}

void GLBufferedRenderer::notifySize(int w, int h)
{
  fbWidth = w;
  fbHeight = h;

  for (set<RenderTarget *>::iterator iter = tempTargets.begin();
       iter != tempTargets.end();
       ++iter)
    {
      delete (*iter);
    }

  deleteFrameBuffer();
  initFrameBuffer();
}

void GLBufferedRenderer::beginSnapshot(int w, int h)
{
  oldw = fbWidth;
  oldh = fbHeight;

  cout << "Resizing buffer to (" << w << "," << h << ")" << endl;
  notifySize(w, h);
  cout << "Drawing..." << endl;
  universe.refreshGL();
  cout << "Pointing read buffer to framebuffer" << endl;
  mainTarget->activate();
  glReadBuffer(GL_FRAMEBUFFER_EXT);
}

void GLBufferedRenderer::endSnapshot()
{
  cout << "Setting back read buffer to standard position" << endl;
  glReadBuffer(GL_NONE);
  cout << "Resetting size" << endl;
  notifySize(oldw, oldh);
}

RenderTarget *GLBufferedRenderer::createTempRenderTarget()
{
  return new_Basic_FBO_Target(this, fbWidth, fbHeight);
}

void GLBufferedRenderer::initFrameBuffer()
{
  mainTarget = new_Basic_FBO_Target(this, fbWidth, fbHeight);
  if (mainTarget == 0)
    throw RendererInitFailure();
}

void GLBufferedRenderer::deleteFrameBuffer()
{
  if (mainTarget != 0)
    delete mainTarget;
}

void GLBufferedRenderer::paint()
{
  QColor bgColor = universe.getState()->getBackgroundColor();

  mainTarget->activate();
  glEnable(GL_DEPTH_TEST);
  glClearColor(bgColor.redF(), bgColor.greenF(), bgColor.blueF(), 1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
 
  drawScene();

  glDisable(GL_DEPTH_TEST);

  // Now draw the framebuffer.
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  if (!outputFramebuffer) {
    mainTarget->drawQuad();
  }
}

