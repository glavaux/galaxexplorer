/*+
This is GalaxExplorer (./src/renderers/renderTarget.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include "includeGL.hpp"
#include <QtCore>
#include "renderers/renderTarget.hpp"
#include "renderers/renderer.hpp"
#include "glerror.hpp"

using namespace GalaxExplorer;

RenderTarget::RenderTarget(GLRenderer *r, GLuint fbo, GLuint renderbuf, 
			   int w, int h)
  : renderer(r)
{
  this->fbo = fbo;
  this->renderbuf = renderbuf;
  this->fbWidth = w;
  this->fbHeight = h;
  this->autoDestroy = true;
}

RenderTarget::~RenderTarget()
{
  emit targetInvalidated();
  if (autoDestroy)
    {
      glDeleteFramebuffersEXT(1, &fbo);
      if (renderbuf != 0)
        glDeleteRenderbuffersEXT(1, &renderbuf);
    }
}

void RenderTarget::addPostDeactivation(boost::function0<void> f)
{
  activation_cb.push_back(f);
}

void RenderTarget::addPostActivation(boost::function0<void> f)
{
  deactivation_cb.push_back(f);
}

void RenderTarget::activate()
{
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  CHECK_GL_ERRORS;
  glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, renderbuf);
  CHECK_GL_ERRORS;
  if (fbo != 0)
    glDrawBuffer(GL_COLOR_ATTACHMENT0);
  else
    glDrawBuffer(GL_BACK);
  CHECK_GL_ERRORS;
  glViewport(0, 0, fbWidth, fbHeight);

  for (List_CB::iterator i = activation_cb.begin();
       i != activation_cb.end();
       ++i)
     (*i)();
}

void RenderTarget::deactivate()
{
  RenderTarget *t = renderer->getMainTarget();

  for (List_CB::iterator i = deactivation_cb.begin();
       i != deactivation_cb.end();
       ++i)
     (*i)();
  
  if (t != 0)
    renderer->getMainTarget()->activate();
  else
    {
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
      glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);
    }
}


