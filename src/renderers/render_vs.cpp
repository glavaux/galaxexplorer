#include <iostream>
#include <cmath>
#include <Eigen/Core>
#include <Eigen/LU>
#include <Eigen/Geometry>
#include "render_vs.hpp"

using std::tan;
using std::cout;
using std::endl;
using namespace GalaxExplorer;

RenderViewState::RenderViewState()
{
  resetModelView();
  resetProjection();
}

RenderViewState::RenderViewState(const RenderViewState& s)
  : projection_matrix(s.projection_matrix), modelview_matrix(s.modelview_matrix)
{
  memcpy(view_xy, s.view_xy, sizeof(view_xy));
  memcpy(view_len, s.view_len, sizeof(view_len));
}

RenderViewState::~RenderViewState()
{
}

void RenderViewState::setFOV(float fov, float aspect, float near, float far)
{
  float f = 1/tan(0.5*M_PI/180*fov);
  
  projection_matrix = VSMatrix4::Zero();
  
  projection_matrix(0,0) = f/aspect;
  projection_matrix(1,1) = f;
  projection_matrix(2,2) = (near+far)/(near-far);
  projection_matrix(3,2) = -1;
  projection_matrix(2,3) = 2*near*far/(near-far);
}

void RenderViewState::setFrustum(float l, float r, float b, float t, float near, float far)
{
  projection_matrix = VSMatrix4::Zero();
  
  projection_matrix(0,0) = 2*near/(r-l);
  projection_matrix(1,1) = 2*near/(t-b);
  projection_matrix(0,2) = (r+l)/(r-l);
  projection_matrix(1,2) = (t+b)/(t-b);
  projection_matrix(2,2) = (near+far)/(near-far);
  projection_matrix(3,2) = -1;
  projection_matrix(2,3) = 2*near*far/(near-far);
}

void RenderViewState::setOrtho(float l, float r, float b, float t, float n, float f)
{
  projection_matrix = VSMatrix4::Zero();
  
  projection_matrix(0,0) = 2/(r-l);
  projection_matrix(1,1) = 2/(t-b);
  projection_matrix(2,2) = 2/(n-f);
  projection_matrix(0,3) = (r+l)/(l-r);
  projection_matrix(1,3) = (t+b)/(b-t);  
  projection_matrix(2,3) = (f+n)/(n-f);  
  projection_matrix(3,3) = 1;
}
   
void RenderViewState::lookAt(const VSVector3& eye, const VSVector3& center, 
                             const VSVector3& up_direction)
{
  VSVector3 F = (center-eye).normalized();
  VSVector3 up = up_direction.normalized();
  VSVector3 s = F.cross(up);
  VSVector3 u = s.normalized().cross(F);

  modelview_matrix.block<1,3>(0,0) = s.transpose();
  modelview_matrix.block<1,3>(1,0) = u.transpose();
  modelview_matrix.block<1,3>(2,0) = -F.transpose();
  modelview_matrix.block<1,3>(3,0) = Eigen::Matrix<float,1,3>::Zero();
  modelview_matrix.block<3,1>(0,3) = Eigen::Matrix<float,3,1>::Zero();
  modelview_matrix(3,3) = 1;
  
  translate(-eye);  
}

void RenderViewState::translate(const VSVector3& v)
{
  VSMatrix4 T = VSMatrix4::Identity();
  
  T.block<3,1>(0,3) = v;
  modelview_matrix = modelview_matrix*T;
}

void RenderViewState::translate(float x, float y, float z)
{
  VSVector3 v;
  v  << x,y,z;
  translate(v);
}

void RenderViewState::preScale(float x, float y, float z)
{
  VSMatrix4 T = VSMatrix4::Identity();
  
  T.diagonal().block<1,3>(0,0) << x,y,z;
  
  modelview_matrix = T*modelview_matrix;
}


void RenderViewState::scale(float x, float y, float z)
{
  VSMatrix4 T = VSMatrix4::Identity();
  
  T.diagonal().block<1,3>(0,0) << x,y,z;
  
  modelview_matrix = modelview_matrix*T;
}


void RenderViewState::rotate(float theta, const VSVector3& v)
{
  VSMatrix4 R = VSMatrix4::Zero();
  
  VSMatrix3 Q = v*v.transpose();
  VSMatrix3 S = VSMatrix3::Zero();
  
  theta *= M_PI/180;
  
  S(1,0) = v(2);
  S(0,1) = -v(2);

  S(2,0) = -v(1);
  S(0,2) = v(1);

  S(2,1) = v(0);
  S(1,2) = -v(0);
  
  R.block<3,3>(0,0).noalias() = Q + cos(theta)*(VSMatrix3::Identity() - Q) + sin(theta)*S;
  R(3,3) = 1;
  
  modelview_matrix = modelview_matrix*R;
}

void RenderViewState::resetModelView()
{
  modelview_matrix = VSMatrix4::Identity();
}

void RenderViewState::resetProjection()
{
  projection_matrix = VSMatrix4::Identity();
}


void RenderViewState::getGLProjectionMatrix(float *M)
{
  for (int i = 0 ; i < 16; i++)
    M[i] = *(projection_matrix.transpose().data()+i);
}

void RenderViewState::getGLModelViewMatrix(float *M)
{
  for (int i = 0 ; i < 16; i++)
    M[i] = *(modelview_matrix.transpose().data()+i);
}

RenderViewState::VSVector3 RenderViewState::fromWindow(const VSVector3& p) const
{
  VSVector4 q,c;

  q.block<3,1>(0,0) = p;
  q(3) = 1;
  c = (projection_matrix*modelview_matrix).lu().solve(q);

  return c.block<3,1>(0,0);
}

RenderViewState::VSVector3 RenderViewState::fromWindow4(const VSVector4& p) const
{
  VSVector4 q,c;

  q = p;
  q.block<3,1>(0,0).array() *= q(3);
  c = (projection_matrix*modelview_matrix).lu().solve(q);

  return c.block<3,1>(0,0);
}

RenderViewState::VSVector4 RenderViewState::fromPosition4(const VSVector3& p) const
{
  VSVector4 q,c;
  
  q.block<3,1>(0,0) = p;
  q(3) = 1;
  c = (projection_matrix*modelview_matrix)*q;
  
  c.block<3,1>(0,0).array() /= c(3);
  return c;
}

RenderViewState::VSVector3 RenderViewState::fromPosition(const VSVector3& p) const
{
  VSVector4 q,c;
  
  q.block<3,1>(0,0) = p;
  q(3) = 1;
  c = (projection_matrix*modelview_matrix)*q;
  
  c.array() /= c(3);
  return c.block<3,1>(0,0);
}

RenderViewState::VSVector3 RenderViewState::screenToPosition(const VSVector3& v) const
{
  VSVector3 p;
  
  p(0) = (v(0)-view_xy[0])*2/view_len[0] - 1;
  p(1) = (v(1)-view_xy[1])*2/view_len[1] - 1;
  p(2) = v(2)*2 - 1;
  
  return fromWindow(p);
}

RenderViewState::VSVector3 RenderViewState::positionToScreen(const VSVector3& p) const
{
  VSVector3 v = fromPosition(p);
  VSVector3 r;
  
  r(0) = view_xy[0] + (v(0)+1)/2*view_len[0];
  r(1) = view_xy[1] + (v(1)+1)/2*view_len[1];
  r(2) = (v(2)+1)/2;
  
  return r;
}

RenderViewState::VSVector3 RenderViewState::screen4ToPosition(const VSVector4& v) const
{
  VSVector4 p;
  
  p(0) = (v(0)-view_xy[0])*2/view_len[0] - 1;
  p(1) = (v(1)-view_xy[1])*2/view_len[1] - 1;
  p(2) = v(2)*2 - 1;
  p(3) = v(3);
  
  return fromWindow4(p);
}

RenderViewState::VSVector4 RenderViewState::positionToScreen4(const VSVector3& p) const
{
  VSVector4 v = fromPosition4(p);
  VSVector4 r;
  
  //cout << "Relative screen position: " << v.transpose() <<endl;
  
  r(0) = view_xy[0] + (v(0)+1)/2*view_len[0];
  r(1) = view_xy[1] + (v(1)+1)/2*view_len[1];
  r(2) = (v(2)+1)/2;
  r(3) = v(3);
  
  //cout << "Screen position:" << r.transpose() << endl;
  
  return r;
}


void RenderViewState::setViewport(int x0, int y0, int w, int h)
{
  view_xy[0] = x0;
  view_xy[1] = y0;
  view_len[0] = w;
  view_len[1] = h;
}

RenderViewState::VSVector3 RenderViewState::getCameraPosition() const
{
  VSVector4 b;
  
  b << 0,0,0,1;
  return modelview_matrix.lu().solve(b).block<3,1>(0,0);
}

RenderViewState::VSMatrix3 RenderViewState::getRotationMatrix() const
{
  return modelview_matrix.block<3,3>(0,0).inverse();
}
