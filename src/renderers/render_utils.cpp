/*+
This is GalaxExplorer (./src/renderers/render_utils.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <iostream>
#include "includeGL.hpp"
#include "GLSLProgramObject.h"
#include "renderers/render_utils.hpp"
#include "renderers/renderer.hpp"
#include "glerror.hpp"
#include "gltools.hpp"

using namespace GalaxExplorer;
using std::cout;
using std::endl;

RenderTargetTextured::RenderTargetTextured(GLRenderer* r, GLuint fbo, GLuint renderbuf, GLuint texture, int w, int h)
  : RenderTarget(r, fbo, renderbuf, w, h), textureId(texture)
{
}

RenderTargetTextured::~RenderTargetTextured()
{
  glDeleteTextures(1, &textureId);
}

RenderTargetTextured *GalaxExplorer::new_Basic_FBO_Target(GLRenderer *r, int fbWidth, int fbHeight)
{
  GLuint fbo, renderbuf, textureId;

  glGenFramebuffersEXT(1, &fbo);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  
  // Create the render buffer for depth

  glGenRenderbuffersEXT(1, &renderbuf);
  glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, renderbuf);
  glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, fbWidth, fbHeight);

  glGenTextures(1, &textureId);
  
  CHECK_GL_ERRORS;
  
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, textureId);
     glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA8, fbWidth, fbHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE_ARB, textureId, 0);
  
  glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, renderbuf);

  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  
  GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
  if(status != GL_FRAMEBUFFER_COMPLETE_EXT)
    {
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
      return 0;
    }

  CHECK_GL_ERRORS;

  return new RenderTargetTextured(r, fbo, renderbuf, textureId, fbWidth, fbHeight);
}


void RenderTargetTextured::drawQuad()
{
  glEnable(GL_TEXTURE_RECTANGLE_ARB);
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, textureId);  
  glDisable(GL_DEPTH_TEST);
  GalaxExplorer::drawQuad(fbWidth, fbHeight);
  glDisable(GL_TEXTURE_RECTANGLE_ARB);

}
