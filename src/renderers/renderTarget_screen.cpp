#include "renderTarget_screen.hpp"

using namespace GalaxExplorer;

RenderScreenTarget::RenderScreenTarget(GLRenderer *r, int w, int h)
  : RenderTarget(r, 0, 0, w, h)
{
  autoDestroy = false;
}

void RenderScreenTarget::deactivate()
{
  for (List_CB::iterator i = deactivation_cb.begin();
       i != deactivation_cb.end();
       ++i)
     (*i)();
}


