#ifndef __RENDER_VIEWSTATE_HPP
#define __RENDER_VIEWSTATE_HPP

#include <Eigen/Core>

namespace GalaxExplorer
{

  class RenderViewState {
  public:
    typedef Eigen::Matrix<float,4,4,Eigen::ColMajor> VSMatrix4;
    typedef Eigen::Matrix<float,4,1,Eigen::ColMajor> VSVector4;
    typedef Eigen::Matrix<float,3,3,Eigen::ColMajor> VSMatrix3;
    typedef Eigen::Matrix<float,3,1,Eigen::ColMajor> VSVector3;

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW 

    RenderViewState();
    RenderViewState(const RenderViewState& s);
    ~RenderViewState();

    // This sets the projection style   
    void setViewport(int x0, int y0, int w, int h);
    void setFOV(float fov, float aspect, float near, float far);
    void setFrustum(float l, float r, float b, float t, float n, float f);
    void setOrtho(float l, float r, float b, float t, float n, float f);
    
    void lookAt(const VSVector3& eye, const VSVector3& center, const VSVector3& up_direction);

    void translate(const VSVector3& v);
    void translate(float x, float y, float z);

    void scale(float x, float y, float z);
    void preScale(float x, float y, float z);
    void rotate(float theta, const VSVector3& v);
    
    void resetModelView();
    void resetProjection();
    
    void getGLProjectionMatrix(float *M); //  M is 16 floats
    void getGLModelViewMatrix(float *M); //  M is 16 floats
    
    const VSMatrix3& getProjectionMatrix() const;
    const VSMatrix3& getModelViewMatrix() const;
    
    VSVector3 fromWindow(const VSVector3& p) const;
    VSVector3 fromPosition(const VSVector3& p) const;

    VSVector3 fromWindow4(const VSVector4& p) const;
    VSVector4 fromPosition4(const VSVector3& p) const;
    
    VSVector3 screenToPosition(const VSVector3& p) const;
    VSVector3 positionToScreen(const VSVector3& p) const;

    VSVector3 screen4ToPosition(const VSVector4& p) const;
    VSVector4 positionToScreen4(const VSVector3& p) const;
    
    VSVector3 getCameraPosition() const;
    VSMatrix3 getRotationMatrix() const; 
    
  private:
    VSMatrix4 modelview_matrix;
    VSMatrix4 projection_matrix;
    float view_xy[2];
    float view_len[2];
  };

};

#endif
