/*+
This is GalaxExplorer (./src/renderers/renderer_wavg.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include "includeGL.hpp"
#include "renderer_wavg.hpp"
#include <boost/bind.hpp>
#include <iostream>
#include <QtCore>
#include <QtGui>
#include "glwidget.hpp"
#include "renderer.hpp"
#include "glerror.hpp"
#include "render_utils.hpp"
#include "renderTarget_screen.hpp"
#include "gltools.hpp"

using namespace GalaxExplorer;
using namespace std;

/* -----------------------------
 * Rendering with weighed avg
 * -----------------------------
 */
GLRendererWeighedAvg::GLRendererWeighedAvg(UniverseWidget& w)
  throw (RendererInitFailure)
  : GLRenderer(w)
{
  cout << "Attempting to build GLRendererWeighedAvg" << endl;
  fbWidth = 100;
  fbHeight = 100;
  interactiveMode = false;
  effectiveWidth = fbWidth;
  effectiveHeight = fbHeight;
  mainTarget = screenTarget = new RenderScreenTarget(this, fbWidth, fbHeight);
  initFrameBuffer();
  initShaders();

  backgroundColor[0] = 0;
  backgroundColor[1] = 0;
  backgroundColor[2] = 0;
}

GLRendererWeighedAvg::~GLRendererWeighedAvg()
{
  deleteFrameBuffer();
  delete screenTarget;
}

void GLRendererWeighedAvg::notifySize(int w, int h)
{
  bool mainTargetIsScreen = mainTarget == screenTarget;

  delete screenTarget;
  deleteFrameBuffer();  
  screenTarget = new RenderScreenTarget(this, w, h);
  if (mainTargetIsScreen)
    mainTarget = screenTarget;

  fbWidth = w;
  fbHeight = h;
  effectiveWidth = fbWidth;
  effectiveHeight = fbHeight;
  if (interactiveMode) {
    effectiveWidth /= interactiveResolution;
    effectiveHeight /= interactiveResolution;
  }
  current_state->setViewport(0, 0, fbWidth,  fbHeight);
  initFrameBuffer();
}

void GLRendererWeighedAvg::setInteractiveMode(bool interactive)
{
  interactiveMode = interactive;
  notifySize(fbWidth, fbHeight);
}

void GLRendererWeighedAvg::initShaders()
  throw(RendererInitFailure)
{
  try
    {
      GLSLShaderObject init("wavg/wavg_init_fragment.glsl", GL_FRAGMENT_SHADER);
      GLSLShaderObject initVertex("noVertex.glsl", GL_VERTEX_SHADER);

      shaderInit.attachShader(initVertex);
      shaderInit.attachShader(init);
      
      GLSLShaderObject vertex_final("wavg/wavg_final_vertex.glsl", GL_VERTEX_SHADER);
      GLSLShaderObject fragment_final("wavg/wavg_final_fragment.glsl", GL_FRAGMENT_SHADER);

      shaderFinal.attachShader(vertex_final);
      shaderFinal.attachShader(fragment_final);
      shaderFinal.link();

      GLSLShaderObject vertex_default("defaultVertex.glsl", GL_VERTEX_SHADER);
      GLSLShaderObject frag_default("defaultFragment.glsl", GL_FRAGMENT_SHADER);
      GLSLProgramObject defaultShader;
      defaultShader.attachShader(vertex_default);
      defaultShader.attachShader(frag_default);
      
      defaultInit.pushShaders(defaultShader);
      defaultInit.pushShaders(shaderInit);
      defaultInit.link();     
    }
  catch (const ShaderCompileError& e)
    {
      QErrorMessage::qtHandler()->showMessage(tr("The shader %1 failed to compile").arg(e.getPath()));
      throw RendererInitFailure();
    }
}

static void checkFBO(int id)
{
  GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
  if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
	cout << "error in " << id << " status is " << status << endl;
}

void GLRendererWeighedAvg::beginSnapshot(int w, int h)
{
  oldw = fbWidth;
  oldh = fbHeight;

  cout << "Resizing buffer to (" << w << "," << h << ")" << endl;
  notifySize(w, h);

  mainTarget = new_Basic_FBO_Target(this, w, h);
  
  universe.refreshGL();
  
  mainTarget->activate();
  glReadBuffer(GL_FRAMEBUFFER_EXT);
}

void GLRendererWeighedAvg::endSnapshot()
{
  glReadBuffer(GL_NONE);
  delete mainTarget;
  mainTarget = screenTarget;
  notifySize(oldw, oldh);
}

void GLRendererWeighedAvg::initFrameBuffer()
  throw (RendererInitFailure)
{
  GLint maxbuffers;
  glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &maxbuffers);
  cout << "Max color attachment = " << maxbuffers << endl;
  glGetIntegerv(GL_MAX_DRAW_BUFFERS, &maxbuffers);
  cout << "Max draw buffers = " << maxbuffers << endl;

  CHECK_GL_ERRORS;

  glGenTextures(2, accumulationTexId);
  glGenFramebuffersEXT(1, &accumulationFboId);

  CHECK_GL_ERRORS;
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, accumulationTexId[0]);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  CHECK_GL_ERRORS;
  glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA, //16F_ARB,
	       effectiveWidth, effectiveHeight, 0, GL_RGBA, GL_FLOAT, 0);
  CHECK_GL_ERRORS;

  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, accumulationTexId[1]);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  CHECK_GL_ERRORS;
  glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA, //16F_ARB,
	       effectiveWidth, effectiveHeight, 0, GL_RGBA, GL_FLOAT, 0);
  CHECK_GL_ERRORS;

  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, accumulationFboId);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
			    GL_TEXTURE_RECTANGLE_ARB, accumulationTexId[0], 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT1_EXT,
			    GL_TEXTURE_RECTANGLE_ARB, accumulationTexId[1], 0);
  CHECK_GL_ERRORS;
  
  GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
  if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
    {
      QErrorMessage::qtHandler()->showMessage(tr("It was not possible to create all requisited framebuffers"));
      cout << "status=" << status << endl;
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
      throw RendererInitFailure();
    }
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

}

void GLRendererWeighedAvg::deleteFrameBuffer()
{
  glDeleteFramebuffersEXT(1, &accumulationFboId);
  glDeleteTextures(2, accumulationTexId);
}

void GLRendererWeighedAvg::drawQuad()
{
  GalaxExplorer::drawQuad(effectiveWidth, effectiveHeight);  
  reloadViewState();
}

bool GLRendererWeighedAvg::usingShaders()
{
  return true;
}

void GLRendererWeighedAvg::assembleProgramShader(GLConnector& c, GLSLProgramObject& o, int id)
{
  c.getShader(id).destroy();
  c.getShader(id).pushShaders(shaderInit);
  c.getShader(id).pushShaders(o);
  c.getShader(id).setAllocator(new GLSLSimpleAllocator(0));
  c.getShader(id).link();
}

void GLRendererWeighedAvg::assembleProgramShader(MultiShader& ms, GLSLProgramObject& o)
{
  ms.resize(1);
  ms[0].destroy();
  ms[0].pushShaders(shaderInit);
  ms[0].pushShaders(o);
  ms[0].setAllocator(new GLSLSimpleAllocator(0));
  ms[0].link();
  //  ms[1].setTextureUnit("DepthTex", 0);
}

void GLRendererWeighedAvg::useShaderInConnector(GLConnector& c, int id)
{
  c.getShader(id).bind();
  inDefaultMode = false;
}

void GLRendererWeighedAvg::useShader(MultiShader& c)
{
  c[0].bind();
  inDefaultMode = false;
}


void GLRendererWeighedAvg::useDefaultShader()
{
  if (inDefaultMode)
    return;

  defaultInit.bind();
  inDefaultMode = true;
}


GLSLProgramObject& GLRendererWeighedAvg::getShader(GLConnector& c, int id)
{
  return c.getShader(id);
}

GLSLProgramObject& GLRendererWeighedAvg::getShader(MultiShader& shader)
{
  return shader[0];
}

void GLRendererWeighedAvg::activateTarget1()
{
  GLuint outBuffers[2] = {
    GL_COLOR_ATTACHMENT0_EXT,
    GL_COLOR_ATTACHMENT1_EXT
  };
  glDrawBuffers(2, outBuffers);
}

void GLRendererWeighedAvg::deActivateTarget1()
{
  glDrawBuffer(GL_BACK);
}

void GLRendererWeighedAvg::paint()
{
  QColor bgColor = universe.getState()->getBackgroundColor();
  GLuint outBuffers[2] = {
    GL_COLOR_ATTACHMENT0_EXT,
    GL_COLOR_ATTACHMENT1_EXT
  };
  GLfloat backgroundColor[3] = {
    bgColor.redF(), bgColor.greenF(), bgColor.blueF()
  };
  
  glViewport(0, 0, effectiveWidth, effectiveHeight);


  mainTarget->addPostDeactivation(boost::bind(&GLRendererWeighedAvg::deActivateTarget1, this));
  mainTarget->addPostActivation(boost::bind(&GLRendererWeighedAvg::activateTarget1, this));
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, accumulationFboId);
  activateTarget1();

  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT);

  glDisable(GL_DEPTH_TEST);
  glBlendEquationEXT(GL_FUNC_ADD);
  glBlendFunc(GL_ONE, GL_ONE);
  glEnable(GL_BLEND);

  inDefaultMode = true;
  defaultInit.bind();
  drawScene();
  defaultInit.unbind();
 
  glDisable(GL_BLEND);

  mainTarget->activate();
  CHECK_GL_ERRORS;
  if (mainTarget != screenTarget)
    glDrawBuffer(GL_COLOR_ATTACHMENT0);
  else
    glDrawBuffer(GL_BACK);
  CHECK_GL_ERRORS;

  shaderFinal.bind();
  GLfloat iScale = (!interactiveMode) ? 1.0 : 1.0/interactiveResolution;
  shaderFinal.setUniform("BackgroundColor", backgroundColor, 3);
  CHECK_GL_ERRORS;
  shaderFinal.setUniform("interactiveScale", &iScale, 1);
  CHECK_GL_ERRORS;
  shaderFinal.bindTextureRECT("ColorTex0", accumulationTexId[0]);
  shaderFinal.bindTextureRECT("ColorTex1", accumulationTexId[1]);
  drawQuad();
  shaderFinal.unbind();
} 

RenderTarget *GLRendererWeighedAvg::createTempRenderTarget()
{
  return new_Basic_FBO_Target(this, fbWidth, fbHeight);
}
