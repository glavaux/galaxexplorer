#ifndef __RENDER_TARGET_SCREEN_HPP
#define __RENDER_TARGET_SCREEN_HPP

#include "includeGL.hpp"
#include "renderTarget.hpp"
#include <QtCore>

namespace GalaxExplorer
{
  class RenderScreenTarget: public RenderTarget
  {
    Q_OBJECT
  
  public:
    RenderScreenTarget(GLRenderer *r, int w, int h);

    virtual void deactivate();
  };
};

#endif
