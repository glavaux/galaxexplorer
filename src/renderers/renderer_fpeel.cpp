/*+
This is GalaxExplorer (./src/renderers/renderer_fpeel.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <iostream>
#include "includeGL.hpp"
#include "glwidget.hpp"
#include "GLSLProgramObject.h"
#include "glerror.hpp"
#include "renderer.hpp"
#include "renderer_fpeel.hpp"
#include "gltools.hpp"

using namespace GalaxExplorer;
using namespace std;


/* -----------------------------
 * Rendering with front-peeling
 * -----------------------------
 */
GLFrontPeeling::GLFrontPeeling(UniverseWidget& w)
  throw (RendererInitFailure)
  : GLRenderer(w)
{
  cout << "Attempted to build GLFrontPeeling" << endl;
  fbWidth = 100;
  fbHeight = 100;
  outputFramebuffer = 0;
  interactiveMode = false;
  effectiveWidth = fbWidth;
  effectiveHeight = fbHeight;
  initFrameBuffer();
  initShaders();
  numLayers = 20;

  glGenQueries(1, &queryID);

  backgroundColor[0] = 0;
  backgroundColor[1] = 0;
  backgroundColor[2] = 0;
}

GLFrontPeeling::~GLFrontPeeling()
{
  deleteFrameBuffer();
}

void GLFrontPeeling::setInteractiveMode(bool mode)
{
  interactiveMode = mode;
  notifySize(fbWidth, fbHeight);
}

void GLFrontPeeling::notifySize(int w, int h)
{
  effectiveWidth = fbWidth = w;
  effectiveHeight = fbHeight = h;
  if (interactiveMode) {
    effectiveWidth /= interactiveResolution;
    effectiveHeight /= interactiveResolution;
  }

  deleteFrameBuffer();
  initFrameBuffer();
}

void GLFrontPeeling::initShaders()
  throw(RendererInitFailure)
{
  try
    {
      GLSLShaderObject init("front_peeling/front_peeling_init_fragment.glsl", GL_FRAGMENT_SHADER);
      GLSLShaderObject peel("front_peeling/front_peeling_peel_fragment.glsl", GL_FRAGMENT_SHADER);
      GLSLShaderObject initVertex("noVertex.glsl", GL_VERTEX_SHADER);

      shaderFrontInit.attachShader(initVertex);
      shaderFrontInit.attachShader(init);
      shaderFrontPeel.attachShader(initVertex);
      shaderFrontPeel.attachShader(peel);
      
      GLSLShaderObject vertex_blend("front_peeling/front_peeling_blend_vertex.glsl", GL_VERTEX_SHADER);
      GLSLShaderObject fragment_blend("front_peeling/front_peeling_blend_fragment.glsl", GL_FRAGMENT_SHADER);
      shaderFrontBlend.attachShader(vertex_blend);
      shaderFrontBlend.attachShader(fragment_blend);
      shaderFrontBlend.link();
      shaderFrontBlend.setTextureUnit("TempTex", 0);
      
      GLSLShaderObject vertex_final("front_peeling/front_peeling_final_vertex.glsl", GL_VERTEX_SHADER);
      GLSLShaderObject fragment_final("front_peeling/front_peeling_final_fragment.glsl", GL_FRAGMENT_SHADER);
      shaderFrontFinal.attachShader(vertex_final);
      shaderFrontFinal.attachShader(fragment_final);
      shaderFrontFinal.link();
      shaderFrontFinal.setTextureUnit("FrontTex", 0);

      GLSLShaderObject vertex_default("defaultVertex.glsl", GL_VERTEX_SHADER);
      GLSLShaderObject frag_default("defaultFragment.glsl", GL_FRAGMENT_SHADER);
      GLSLProgramObject defaultShader;
      defaultShader.attachShader(vertex_default);
      defaultShader.attachShader(frag_default);
      
      defaultFrontInit.pushShaders(defaultShader);
      defaultFrontInit.pushShaders(shaderFrontInit);
      defaultFrontInit.link();
      
      defaultFrontPeel.pushShaders(defaultShader);
      defaultFrontPeel.pushShaders(shaderFrontPeel);
      defaultFrontPeel.link();
      //      defaultFrontPeel.setTextureUnit("DepthTex", 0);
    }
  catch (const ShaderCompileError& e)
    {
      QErrorMessage::qtHandler()->showMessage(tr("The shader %1 failed to compile").arg(e.getPath()));
      throw RendererInitFailure();
    }
}

void checkFBO(int id)
{
  GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
  if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
	cout << "error in " << id << " status is " << status << endl;
}

void GLFrontPeeling::beginSnapshot(int w, int h)
{
  oldw = fbWidth;
  oldh = fbHeight;
  bool saveMode = interactiveMode;

  interactiveMode = false;

  cout << "Resizing buffer to (" << w << "," << h << ")" << endl;
  notifySize(w, h);

  initOutputFrameBuffer();

  outputFramebuffer = universeFBO;
  cout << "Drawing..." << endl;
  universe.refreshGL();
  cout << "Pointing read buffer to framebuffer" << endl;

  interactiveMode = saveMode;

  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, universeFBO);
  glReadBuffer(GL_FRAMEBUFFER_EXT);
}

void GLFrontPeeling::endSnapshot()
{
  cout << "Setting back read buffer to standard position" << endl;
  glReadBuffer(GL_NONE);
  outputFramebuffer = 0;

  cout << "Resetting size" << endl;
  notifySize(oldw, oldh);

  deleteOutputFrameBuffer();
}

void GLFrontPeeling::initFrameBuffer()
  throw (RendererInitFailure)
{
  glGenTextures(2, frontDepthTexId);
  glGenTextures(2, frontColorTexId);
  glGenFramebuffersEXT(2, frontFboId);
  checkFBO(-1);
  
  for (int i = 0; i < 2; i++)
    {
      glBindTexture(GL_TEXTURE_RECTANGLE_ARB, frontDepthTexId[i]);
      glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP);
      glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_DEPTH_COMPONENT32F,
		   effectiveWidth, effectiveHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
      CHECK_GL_ERRORS;
      
      glBindTexture(GL_TEXTURE_RECTANGLE_ARB, frontColorTexId[i]);
      glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP);
      glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA, effectiveWidth, effectiveHeight,
		   0, GL_RGBA, GL_FLOAT, 0);
      CHECK_GL_ERRORS;
      
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frontFboId[i]);
      glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT,
				GL_TEXTURE_RECTANGLE_ARB, frontDepthTexId[i], 0);
      glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
				GL_TEXTURE_RECTANGLE_ARB, frontColorTexId[i], 0);
    }
  
  glGenTextures(1, &frontColorBlenderTexId);
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, frontColorBlenderTexId);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA, effectiveWidth, effectiveHeight,
	       0, GL_RGBA, GL_FLOAT, 0);
  
  glGenTextures(1, &blenderDepthTex);
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, blenderDepthTex);
      glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP);
      glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_DEPTH_COMPONENT32F,
		   effectiveWidth, effectiveHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);   

  glGenFramebuffersEXT(1, &frontColorBlenderFboId);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frontColorBlenderFboId);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT,
			    GL_TEXTURE_RECTANGLE_ARB, frontDepthTexId[0], 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
			    GL_TEXTURE_RECTANGLE_ARB, frontColorBlenderTexId, 0);  
  
  GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
  if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
    {
      //QErrorMessage::qtHandler()->showMessage(tr("It was not possible to create all requisited framebuffers"));
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
      throw RendererInitFailure();
    }
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);


}

void GLFrontPeeling::deleteFrameBuffer()
{
  glDeleteFramebuffersEXT(2, frontFboId);
  glDeleteFramebuffersEXT(1, &frontColorBlenderFboId);
  glDeleteTextures(2, frontDepthTexId);
  glDeleteTextures(2, frontColorTexId);
  glDeleteTextures(1, &frontColorBlenderTexId);
  glDeleteTextures(1, &blenderDepthTex);
}

void GLFrontPeeling::drawQuad()
{
  GalaxExplorer::drawQuad(effectiveWidth, effectiveHeight);
  reloadViewState();
}

bool GLFrontPeeling::usingShaders()
{
  return true;
}

void GLFrontPeeling::assembleProgramShader(GLConnector& c, GLSLProgramObject& o, int id)
{
  GLSLProgramObject *s[2] = {
    &shaderFrontInit,
    &shaderFrontPeel
  };

  for (int k = 0; k < 2; k++)
    {
      c.getShader(k+2*id).destroy();

      c.getShader(k+2*id).pushShaders(*s[k]);
      c.getShader(k+2*id).pushShaders(o);
      // We reserve texture unit 0 for optimizing the pipeline.
      c.getShader(k+2*id).setAllocator(new GLSLSimpleAllocator(1));
      c.getShader(k+2*id).link();
    }
  
  c.getShader(1+2*id).setTextureUnit("DepthTex", 0);
}

void GLFrontPeeling::assembleProgramShader(MultiShader& ms, GLSLProgramObject& o)
{
  GLSLProgramObject *s[2] = {
    &shaderFrontInit,
    &shaderFrontPeel
  };

  ms.resize(2);

  for (int k = 0; k < 2; k++)
    {
      ms[k].destroy();

      ms[k].pushShaders(*s[k]);
      ms[k].pushShaders(o);
      ms[k].setAllocator(new GLSLSimpleAllocator(1));
      ms[k].link();
    }
  
  ms[1].setTextureUnit("DepthTex", 0);
}

void GLFrontPeeling::useShaderInConnector(GLConnector& c, int id)
{
  if (pass == 0)
    {
      c.getShader(0+2*id).bind();
    }
  else
    {
      c.getShader(1+2*id).bind();
    }
  inDefaultMode = false;
}

void GLFrontPeeling::useShader(MultiShader& c)
{
  if (pass == 0)
    {
      c[0].bind();
    }
  else
    {
      c[1].bind();
    }
  inDefaultMode = false;
}


void GLFrontPeeling::useDefaultShader()
{
  if (inDefaultMode)
    return;

  if (pass == 0)
    {
      defaultFrontInit.bind();
    }
  else
    {
      defaultFrontPeel.bind();
    }
  inDefaultMode = true;
}


GLSLProgramObject& GLFrontPeeling::getShader(GLConnector& c, int id)
{
  if (pass == 0)
    return c.getShader(2*id+0);
  else
    return c.getShader(2*id+1);
}

GLSLProgramObject& GLFrontPeeling::getShader(MultiShader& shader)
{
  if (pass == 0)
    return shader[0];
  else
    return shader[1];
}

void GLFrontPeeling::paint()
{
  QColor bgColor = universe.getState()->getBackgroundColor();

  glViewport(0, 0, effectiveWidth, effectiveHeight);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frontColorBlenderFboId);
  glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT);
  glClearColor(0,0,0,1);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glEnable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);
  glDepthFunc(GL_LEQUAL);

  pass = 0;
  inDefaultMode = true;

  //  cout << "Initial scene drawing" << endl;
  defaultFrontInit.bind();
  drawScene();
  defaultFrontInit.unbind();
  //  cout << "Done" << endl;

  pass = 1;
  for (int layer = 1; ;layer++)
    {
      int currId = layer % 2;      
      
      prevId = 1-currId;

      //      cout << "Doing layer " << layer << endl;

      // Peel the scene. Draw into currId FBO. 
      // Use threshold in DepthTex[prevId];
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frontFboId[currId]);
      glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT);
      
      glClearColor(0,0,0,0);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      
      glDisable(GL_BLEND);
      glEnable(GL_DEPTH_TEST);      
      glDepthFunc(GL_LEQUAL);

      inDefaultMode = true;

      // Detect if we reach convergence
      glBeginQuery(GL_SAMPLES_PASSED_ARB, queryID);

      defaultFrontPeel.bind();
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_RECTANGLE_ARB, frontDepthTexId[prevId]);
      reloadViewState();
      drawScene();      
      defaultFrontPeel.unbind();

      glEndQuery(GL_SAMPLES_PASSED_ARB);

      // Blend the new scene with the old one.
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frontColorBlenderFboId);
      glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT);      
      glDisable(GL_DEPTH_TEST);
      glEnable(GL_BLEND);
      
      glBlendEquation(GL_FUNC_ADD);
      glBlendFuncSeparate(GL_DST_ALPHA, GL_ONE,
      			  GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);
      
      shaderFrontBlend.bind();      
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_RECTANGLE_ARB, frontColorTexId[currId]);
      drawQuad();
      shaderFrontBlend.unbind();
      
      glDisable(GL_BLEND);   

      GLuint sample_count;
      glGetQueryObjectuiv(queryID, GL_QUERY_RESULT_ARB, &sample_count);

      //cout << "Layer = " << layer << " sample_count = " << sample_count << endl;

      if (sample_count == 0)
	{
	  break;
	}
      if (layer == numLayers)
	{
	  QErrorMessage::qtHandler()->showMessage(tr("Number of transparency has overflown (number was %1)").arg(layer));
	  break;
	}
     }

  glViewport(0, 0, fbWidth, fbHeight);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, outputFramebuffer);
  if (outputFramebuffer != 0)
    glDrawBuffer(GL_FRAMEBUFFER_EXT);
  else
    glDrawBuffer(GL_BACK);
  glDisable(GL_DEPTH_TEST);
  
  backgroundColor[0] = bgColor.redF();
  backgroundColor[1] = bgColor.greenF();
  backgroundColor[2] = bgColor.blueF();

  glClearColor(bgColor.redF(), bgColor.greenF(), bgColor.blueF(),0.0);
  shaderFrontFinal.bind();
  GLfloat iScale = interactiveMode ? (1.0/interactiveResolution) : 1.0;
  shaderFrontFinal.setUniform("interactiveScale", &iScale, 1);
  shaderFrontFinal.setUniform("BackgroundColor", backgroundColor, 3);
  shaderFrontFinal.bindTextureRECT("ColorTex", frontColorBlenderTexId);
  drawQuad();
  shaderFrontFinal.unbind();
}  

void GLFrontPeeling::initOutputFrameBuffer()
{
  glGenFramebuffersEXT(1, &universeFBO);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, universeFBO);
  
  // Create the render buffer for depth

  glGenRenderbuffersEXT(1, &universeRenderBuffer);
  glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, universeRenderBuffer);
  glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, effectiveWidth, effectiveHeight);

  glGenTextures(1, &universe_tex);
  
  CHECK_GL_ERRORS;
  
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, universe_tex);
     glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA8, effectiveWidth, effectiveHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE_ARB, universe_tex, 0);
  
  glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, universeRenderBuffer);

  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  
  GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
  if(status != GL_FRAMEBUFFER_COMPLETE_EXT)
    exit(1);
  
  CHECK_GL_ERRORS;
}


void GLFrontPeeling::deleteOutputFrameBuffer()
{
  glDeleteFramebuffersEXT(1, &universeFBO);
  glDeleteRenderbuffersEXT(1, &universeRenderBuffer);
  glDeleteTextures(1, &universe_tex);
}

