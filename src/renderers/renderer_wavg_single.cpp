/*+
This is GalaxExplorer (./src/renderers/renderer_wavg_single.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include "includeGL.hpp"
#include "renderer_wavg.hpp"
#include <iostream>
#include <QtCore>
#include <QtGui>
#include "glwidget.hpp"
#include "inits.hpp"
#include "renderer_wavg_single.hpp"

using namespace GalaxExplorer;
using namespace std;

#define CHECK_GL_ERRORS \
{ \
    GLenum err = glGetError(); \
    if (err) \
      { \
        QString s; \
        cout << "Error " << err << " at line " << __LINE__ << endl; \
      } \
}


/* -----------------------------
 * Rendering with weighed avg
 * -----------------------------
 */
GLRendererWeighedAvgSingleDraw::GLRendererWeighedAvgSingleDraw(UniverseWidget& w)
  throw (RendererInitFailure)
  : GLRenderer(w)
{
  QSettings settings;

  cout << "Attempting to build GLRendererWeighedAvgSingleDraw" << endl;
  effectiveWidth = fbWidth = 100;
  effectiveHeight = fbHeight = 100;
  outputFramebuffer = 0;
  interactiveMode = false;

  backgroundColor[0] = 0;
  backgroundColor[1] = 0;
  backgroundColor[2] = 0;
  pass_id = 0;

  if (g_gl_extensions.find("GL_ARB_texture_float")==g_gl_extensions.end())
    {
      cout << "Float texture not supported" << endl;
      internal_format = GL_RGBA;
      pixel_format = GL_INT;
    }
  else
    {
      cout << "Float texture supported ! " << endl;
      internal_format = GL_RGBA16F_ARB;
      pixel_format = GL_FLOAT;
    }
  
  initFrameBuffer();
  initShaders();
}

GLRendererWeighedAvgSingleDraw::~GLRendererWeighedAvgSingleDraw()
{
  deleteFrameBuffer();
}

void GLRendererWeighedAvgSingleDraw::setInteractiveMode(bool on)
{
  interactiveMode = on;
  notifySize(fbWidth, fbHeight);
}

void GLRendererWeighedAvgSingleDraw::notifySize(int w, int h)
{
  effectiveWidth = fbWidth = w;
  effectiveHeight = fbHeight = h;
  if (interactiveMode)
    {
      effectiveWidth /= interactiveResolution; 
      effectiveHeight /= interactiveResolution; 
    }
  deleteFrameBuffer();
  initFrameBuffer();
}

void GLRendererWeighedAvgSingleDraw::initShaders()
  throw(RendererInitFailure)
{
  try
    {
      GLSLShaderObject init0("wavg/wavg_init_fragment_single_buffer0.glsl", GL_FRAGMENT_SHADER);
      GLSLShaderObject init1("wavg/wavg_init_fragment_single_buffer1.glsl", GL_FRAGMENT_SHADER);
      GLSLShaderObject initVertex("noVertex.glsl", GL_VERTEX_SHADER);

      shaderInit[0].attachShader(initVertex);
      shaderInit[0].attachShader(init0);
      shaderInit[1].attachShader(initVertex);
      shaderInit[1].attachShader(init1);
      
      GLSLShaderObject vertex_final("wavg/wavg_final_vertex.glsl", GL_VERTEX_SHADER);
      GLSLShaderObject fragment_final("wavg/wavg_final_fragment.glsl", GL_FRAGMENT_SHADER);

      shaderFinal.attachShader(vertex_final);
      shaderFinal.attachShader(fragment_final);
      shaderFinal.link();

      GLSLShaderObject vertex_default("defaultVertex.glsl", GL_VERTEX_SHADER);
      GLSLShaderObject frag_default("defaultFragment.glsl", GL_FRAGMENT_SHADER);
      GLSLProgramObject defaultShader;
      defaultShader.attachShader(vertex_default);
      defaultShader.attachShader(frag_default);

      for (int q = 0; q < 2; q++) {
        defaultInit[q].pushShaders(defaultShader);
        defaultInit[q].pushShaders(shaderInit[q]);
        defaultInit[q].link();     
      }
    }
  catch (const ShaderCompileError& e)
    {
      QErrorMessage::qtHandler()->showMessage(tr("The shader %1 failed to compile").arg(e.getPath()));
      throw RendererInitFailure();
    }
}

static void checkFBO(int id)
{
  GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
  if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
	cout << "error in " << id << " status is " << status << endl;
}

void GLRendererWeighedAvgSingleDraw::beginSnapshot(int w, int h)
{
  oldw = fbWidth;
  oldh = fbHeight;

  cout << "Resizing buffer to (" << w << "," << h << ")" << endl;
  notifySize(w, h);

  initOutputFrameBuffer();

  outputFramebuffer = universeFBO;
  cout << "Drawing..." << endl;
  universe.refreshGL();
  cout << "Pointing read buffer to framebuffer" << endl;
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, universeFBO);
  glReadBuffer(GL_FRAMEBUFFER_EXT);
}

void GLRendererWeighedAvgSingleDraw::endSnapshot()
{
  cout << "Setting back read buffer to standard position" << endl;
  glReadBuffer(GL_NONE);
  outputFramebuffer = 0;
  cout << "Resetting size" << endl;
  notifySize(oldw, oldh);

  deleteOutputFrameBuffer();
}

void GLRendererWeighedAvgSingleDraw::initFrameBuffer()
  throw (RendererInitFailure)
{
  GLint maxbuffers;
  glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &maxbuffers);

  cout << "Max color attachment = " << maxbuffers << endl;
  glGetIntegerv(GL_MAX_DRAW_BUFFERS, &maxbuffers);
  cout << "Max draw buffers = " << maxbuffers << endl;

  CHECK_GL_ERRORS;

  glGenTextures(2, accumulationTexId);
  glGenFramebuffersEXT(1, &accumulationFboId);

  CHECK_GL_ERRORS;
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, accumulationTexId[0]);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  CHECK_GL_ERRORS;
  glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, internal_format,
	       effectiveWidth, effectiveHeight, 0, GL_RGBA, pixel_format, 0);
  CHECK_GL_ERRORS;

  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, accumulationTexId[1]);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  CHECK_GL_ERRORS;
  glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, internal_format,
	       effectiveWidth, effectiveHeight, 0, GL_RGBA, pixel_format, 0);
  CHECK_GL_ERRORS;


  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, accumulationFboId);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
			    GL_TEXTURE_RECTANGLE_ARB, accumulationTexId[0], 0);
  CHECK_GL_ERRORS;
  
  GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
  if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
    {
      QErrorMessage::qtHandler()->showMessage(tr("It was not possible to create all requisited framebuffers"));
      cout << "status=" << status << endl;
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
      throw RendererInitFailure();
    }
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

}

void GLRendererWeighedAvgSingleDraw::deleteFrameBuffer()
{
  glDeleteFramebuffersEXT(1, &accumulationFboId);
  glDeleteTextures(2, accumulationTexId);
}

void GLRendererWeighedAvgSingleDraw::drawQuad()
{
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();
  gluOrtho2D(0.0, 1.0, 0.0, 1.0);

  glColor3f(1.0,1.0,1.0);
  glBegin(GL_QUADS);
  {
    glTexCoord2f(0, 0); 
    glVertex2f(0.0, 0.0);

    glTexCoord2f(effectiveWidth, 0); 
    glVertex2f(1.0, 0.0);

    glTexCoord2f(effectiveWidth, effectiveHeight); 
    glVertex2f(1.0, 1.0);

    glTexCoord2f(0, effectiveHeight); 
    glVertex2f(0.0, 1.0);
  }
  glEnd();
  
  glPopMatrix();

  glEndList();
}

bool GLRendererWeighedAvgSingleDraw::usingShaders()
{
  return true;
}

void GLRendererWeighedAvgSingleDraw::assembleProgramShader(GLConnector& c, GLSLProgramObject& o, int id)
{
  for (int q = 0; q < 2; q++) {
  c.getShader(2*id+q).destroy();
  c.getShader(2*id+q).pushShaders(shaderInit[q]);
  c.getShader(2*id+q).pushShaders(o);
  c.getShader(2*id+q).setAllocator(new GLSLSimpleAllocator(0));
  c.getShader(2*id+q).link();
  }
}

void GLRendererWeighedAvgSingleDraw::assembleProgramShader(MultiShader& ms, GLSLProgramObject& o)
{
  abort();
}

void GLRendererWeighedAvgSingleDraw::useShaderInConnector(GLConnector& c, int id)
{
  c.getShader(2*id+pass_id).bind();
  inDefaultMode = false;
}

void GLRendererWeighedAvgSingleDraw::useShader(MultiShader& c)
{
  abort();
}


void GLRendererWeighedAvgSingleDraw::useDefaultShader()
{
  if (inDefaultMode)
    return;

  defaultInit[pass_id].bind();
  inDefaultMode = true;
}


GLSLProgramObject& GLRendererWeighedAvgSingleDraw::getShader(GLConnector& c, int id)
{
  return c.getShader(2*id+pass_id);
}

GLSLProgramObject& GLRendererWeighedAvgSingleDraw::getShader(MultiShader& shader)
{
  abort();
}

void GLRendererWeighedAvgSingleDraw::paint()
{
  QColor bgColor = universe.getState()->getBackgroundColor();
  GLuint outBuffers[2] = {
    GL_COLOR_ATTACHMENT0_EXT,
    GL_COLOR_ATTACHMENT1_EXT
  };
  GLfloat backgroundColor[3] = {
    bgColor.redF(), bgColor.greenF(), bgColor.blueF()
  };

  glViewport(0, 0, effectiveWidth, effectiveHeight);

  glDisable(GL_DEPTH_TEST);

  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, accumulationFboId);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
			    GL_TEXTURE_RECTANGLE_ARB, accumulationTexId[0], 0);
//  glDrawBuffers(2, outBuffers);

  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT);

  glBlendEquationEXT(GL_FUNC_ADD);
  glBlendFunc(GL_ONE, GL_ONE);
  glEnable(GL_BLEND);

  inDefaultMode = true;
  pass_id = 0;
  defaultInit[0].bind();
  drawScene();
  defaultInit[0].unbind();

  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
			    GL_TEXTURE_RECTANGLE_ARB, accumulationTexId[1], 0);

  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT);
  pass_id = 1;
  defaultInit[1].bind();
  drawScene();
  defaultInit[1].unbind();
 
  glDisable(GL_BLEND);

  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, outputFramebuffer);
  if (outputFramebuffer != 0)
    glDrawBuffer(GL_FRAMEBUFFER_EXT);
  else
    glDrawBuffer(GL_BACK);
  glViewport(0, 0, fbWidth, fbHeight);
  
  shaderFinal.bind();
  GLfloat iScale = (interactiveMode ? (1.0/interactiveResolution) : 1.0);
  shaderFinal.setUniform("BackgroundColor", backgroundColor, 3);
  shaderFinal.setUniform("interactiveScale", &iScale, 1);
  shaderFinal.bindTextureRECT("ColorTex0", accumulationTexId[0]);
  shaderFinal.bindTextureRECT("ColorTex1", accumulationTexId[1]);
  drawQuad();
  shaderFinal.unbind();
}  

void GLRendererWeighedAvgSingleDraw::initOutputFrameBuffer()
{
  glGenFramebuffersEXT(1, &universeFBO);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, universeFBO);
  
  // Create the render buffer for depth

  glGenRenderbuffersEXT(1, &universeRenderBuffer);
  glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, universeRenderBuffer);
  glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, fbWidth, fbHeight);

  glGenTextures(1, &universe_tex);
  
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, universe_tex);
     glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA8, fbWidth, fbHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
     glTexParameterf(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE_ARB, universe_tex, 0);
  
  glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, universeRenderBuffer);

  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  
  GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
  if(status != GL_FRAMEBUFFER_COMPLETE_EXT)
    throw RendererInitFailure();
}


void GLRendererWeighedAvgSingleDraw::deleteOutputFrameBuffer()
{
  glDeleteFramebuffersEXT(1, &universeFBO);
  glDeleteRenderbuffersEXT(1, &universeRenderBuffer);
  glDeleteTextures(1, &universe_tex);
}

