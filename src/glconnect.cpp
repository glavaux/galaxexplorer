/*+
This is GalaxExplorer (./src/glconnect.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <string>
#include <map>
#include "includeGL.hpp"
#include "glconnect.hpp"
#include "inits.hpp"
#include "vboObject.hpp"

using namespace std;
using namespace GalaxExplorer;

Connector::Connector()
{
  show = false;
  initialized = false;
  connector = 0;
}

Connector::Connector(const DataConnector& c)
{
  connector = c.copyConnector();
  show = true;
  initialized = true;
}

Connector::Connector(const Connector& c)
{
  if (c.connector != 0)
    connector = c.connector->copyConnector();
  else
    connector = 0;
  myName = c.myName;
  initialized = c.initialized;
  show = c.show;
}

Connector& Connector::operator=(const Connector& c)
{
  if (connector != 0)
    delete connector;

  if (c.connector != 0)
    connector = c.connector->copyConnector();
  else
    connector = 0;

  myName = c.myName;
  initialized = c.initialized;
  show = c.show;
  return *this;
}

Connector::~Connector()
{
}

GLConnector::GLConnector()
  : Connector()
{
  myList = glGenLists(1);
  m_prepared = false;
  allShaders.resize(10);
}

GLConnector::GLConnector(const SetConnector& c)
  : Connector(c)
{
  myList = glGenLists(1);
  m_prepared = false;
  allShaders.resize(10);
}

GLConnector::GLConnector(const Connector& c)
  : Connector(c)
{
  myList = glGenLists(1);
  allShaders.resize(10);
  m_prepared = false;
}

GLConnector::GLConnector(const GLConnector& c)
  : Connector(c)
{
  myList = glGenLists(1);
  allShaders.resize(10);
  m_prepared = false;
}

GLConnector::~GLConnector()
{
  glDeleteLists(myList, 1);
  for (int i = 0; i < allLists.size(); i++)
    glDeleteLists(allLists[i], 1);

  for (int i = 0; i < allVBOs.size(); i++)
    delete allVBOs[i];
}

GLuint GLConnector::allocateList()
{
  GLuint l = glGenLists(1);
  allLists.push_back(l);
  
  return l;
}

int GLConnector::allocateVBO(const VBO_Descriptor& desc)
{
  VBO_Object *object = new VBO_Object(desc);

  allVBOs.push_back(object);

  return allVBOs.size()-1;
}

GLConnector& GLConnector::operator=(const GLConnector& c)
{
  abort();
}

GLSLProgramObject& GLConnector::getShader(int id)
{
  assert (id < allShaders.size());

  return allShaders[id];
}

void GLConnector::unprepare()
{
//  glDeleteLists(myList, 1);
  for (int i = 0; i < allLists.size(); i++)
    glDeleteLists(allLists[i], 1);

  for (int i = 0; i < allVBOs.size(); i++)
    delete allVBOs[i];
  allVBOs.clear();

  for (int i = 0; i < allShaders.size();  i++)
    allShaders[i].destroy();

  m_prepared = false;
}

Connector GalaxExplorer::buildConnector(const QString& repname, const QString& name,
					DataSet *data, DataSetSelector *sel)
{
  assert(g_allRepresentations[repname] != 0);  
  
  Connector c(SetConnector(data, g_allRepresentations[repname], sel));
  c.setName(name);
  return c;
}

Connector GalaxExplorer::buildConnector(const QString& repname, const QString& name,
					DataGrid *data)
{
  assert(g_allRepresentations[repname] != 0);  
  
  Connector c(GridConnector(data, g_allRepresentations[repname]));
  c.setName(name);
  return c;
}

Connector GalaxExplorer::buildConnector(const QString& repname, const QString& name,
					DataMesh *mesh, DataSet *data)
{
  assert(g_allRepresentations[repname] != 0);  
  
  Connector c(MeshConnector(mesh, data, g_allRepresentations[repname]));
  c.setName(name);
  return c; 
}


