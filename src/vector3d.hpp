/*+
This is GalaxExplorer (./src/vector3d.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __VECTOR_3D_HPP
#define __VECTOR_3D_HPP

namespace GalaxExplorer
{


  template<typename T>
  class Vector3D
  {
  public:
    union {
      struct  {
	T x;
	T y;
	T z;
      } vec;
      T vecdata[3];
    };

    Vector3D() 
    {
      vec.x = vec.y = vec.z = 0;
    }

    Vector3D(const Vector3D<T>& v) 
      : vec(v.vec)
    {

    }

    Vector3D(T x_, T y_, T z_) 
    {
      vec.x = x_; 
      vec.y = y_;
      vec.z = z_;
    }
    ~Vector3D() {}

    Vector3D<T> operator+(const Vector3D<T>& v2) const
    {
      Vector3D<T> tmp_v;

      tmp_v.vec.x = vec.x + v2.vec.x;
      tmp_v.vec.y = vec.y + v2.vec.y;
      tmp_v.vec.z = vec.z + v2.vec.z;
      return tmp_v;
    }

    const Vector3D<T>& operator+=(const Vector3D<T>& v2)
    {
      vec.x += v2.vec.x;
      vec.y += v2.vec.y;
      vec.z += v2.vec.z;

      return *this;
    }

    const Vector3D<T>& operator*=(const T& alpha)
    {
      vec.x *= alpha;
      vec.y *= alpha;
      vec.z *= alpha;
      return *this;
    }

    Vector3D<T> operator*(const T& alpha) const
    {
      Vector3D<T> tmp_v;

      tmp_v.vec.x = alpha*vec.x;
      tmp_v.vec.y = alpha*vec.y;
      tmp_v.vec.z = alpha*vec.z;
      return tmp_v;
    }

    Vector3D<T> operator-(const Vector3D<T>& v2) const
    {
      Vector3D<T> tmp_v;

      tmp_v.vec.x = vec.x - v2.vec.x;
      tmp_v.vec.y = vec.y - v2.vec.y;
      tmp_v.vec.z = vec.z - v2.vec.z;
      return tmp_v;
    }

    const Vector3D<T>& operator=(T alpha) 
    {
      vec.x = vec.y = vec.z = alpha;
      return *this;
    }

    T& operator()(int k)
    {
      return vecdata[k];
    }

    const T& operator()(int k) const
    {
      return vecdata[k];
    }

    double norm() const
    {
      return sqrt(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z);
    }

    void normalize()
    {
      double n = norm();

      if (n != 0)
	for (int i = 0; i < 3; i++)
	  vecdata[i] /= n;
    }

    template<typename T2>
    void scale(T2 scales[3])
    {
      for (int i = 0; i < 3; i++)
	vecdata[i] *= scales[i];
    }
    
  };

  template<typename T>
  Vector3D<T> operator*(double alpha, const Vector3D<T>& V)
  {
    return Vector3D<T>(alpha*V.vec.x, alpha*V.vec.y, alpha*V.vec.z);
  }

  template<typename T>
  QDataStream& operator<<(QDataStream& out, const Vector3D<T>& V)
  {
    return (out << V.vec.x << V.vec.y << V.vec.z);
  }

  template<typename T>
  QDataStream& operator>>(QDataStream& in, Vector3D<T>& V)
  {
    return (in >> V.vec.x >> V.vec.y >> V.vec.z);
  }

};

#endif
