/*+
This is GalaxExplorer (./src/datagrid.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <QObject>
#include "datagrid.hpp"
#include "array3d.hpp"

using namespace GalaxExplorer;

DataGrid::DataGrid()
  : DataSource("DataGrid")
{
  for (int i = 0; i < 6; i++)
    boundaries[i] = 0;
}

DataGrid::DataGrid(const Array3D<double>& array, 
		   double boundaries[6])
  : DataSource("DataGrid")
{
  memcpy(this->boundaries, boundaries, sizeof(this->boundaries));
  data = array;
}

DataGrid::DataGrid(const Array3D<double>& array, 
		   const Array3D<Vector3D<double> >& displacement,
		   double boundaries[6])
  : DataSource("DataGrid")
{
  memcpy(this->boundaries, boundaries, sizeof(this->boundaries));
  this->data = array;
  this->displacement = displacement;
}

DataGrid::~DataGrid()
{
}

void DataGrid::saveData(QDataStream& outdata)
{
  // Write the header first.
  for (int i = 0; i < 6; i++)
    outdata << boundaries[i];
  outdata << getDistanceMultiplier();
  // Then the data itself
  outdata << data;
  outdata << displacement;
}

void DataGrid::loadData(QDataStream& indata)
{
  qint8 version;

  for (int i = 0; i < 6; i++)
    indata >> boundaries[i];
  double mul;
  indata >> mul;
  setDistanceMultiplier(mul);
  indata >> data;
  indata >> displacement;
}
