/*+
This is GalaxExplorer (./src/window.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <iostream>
#include <QtGui>
#include <functional>
#include <QtDebug>
#include <QErrorMessage>
#include <QSettings>

#include "renderers/renderer.hpp"
#include "glwidget.hpp"
#include "window.hpp"
#include "dataSelectors.hpp"
#include "ui_dialogCreateConnector.h"
#include "ui_newSelector.h"
#include "inits.hpp"
#include "connectorDialog.hpp"
#include "save_load.hpp"
#include "io/load_data.hpp"
#include "representationDialog.hpp"
#include "createEditRepresentation.hpp"
#include "visuScript.hpp"
#include "io/load_trajectory.hpp"
#include "io/load_mesh.hpp"
#include "setup.hpp"
#include "io/load_gadget.hpp"
#include "dbus/dbus_if.hpp"

using namespace std;

using namespace GalaxExplorer;

class GraphicsView : public QGraphicsView
{
public:
    GraphicsView(QWidget *parent)
      : QGraphicsView(parent)
    {
    }

protected:
    void resizeEvent(QResizeEvent *event) {
        if (scene())
            scene()->setSceneRect(QRect(QPoint(0, 0), event->size()));
        QGraphicsView::resizeEvent(event);
    }
};

QDockWidget *Window::createPositionSubWindow()
{
  QDockWidget *sliders_dock = new QDockWidget(tr("Positions"), this);
  QWidget *sliders = new QWidget(sliders_dock);
  QGridLayout *sliderLayout = new QGridLayout;

  xSlider = createSlider();
  ySlider = createSlider();
  zSlider = createSlider();
  deepSlider = createDeepSlider();
  velBoost = createVelocitySlider();

  connect(xSlider, SIGNAL(valueChanged(int)), this, SLOT(setXRotation(int)));
  connect(xSlider, SIGNAL(sliderPressed()), this, SLOT(enableInteraction()));
  connect(xSlider, SIGNAL(sliderReleased()), this, SLOT(disableInteraction()));
  connect(world->universeWidget, SIGNAL(xRotationChanged(float)), this, SLOT(setXSliderValue(float)));
  connect(ySlider, SIGNAL(valueChanged(int)), this, SLOT(setYRotation(int)));
  connect(ySlider, SIGNAL(sliderPressed()), this, SLOT(enableInteraction()));
  connect(ySlider, SIGNAL(sliderReleased()), this, SLOT(disableInteraction()));
  connect(world->universeWidget, SIGNAL(yRotationChanged(float)), this, SLOT(setYSliderValue(float)));
  connect(zSlider, SIGNAL(valueChanged(int)), this, SLOT(setZRotation(int)));
  connect(zSlider, SIGNAL(sliderPressed()), this, SLOT(enableInteraction()));
  connect(zSlider, SIGNAL(sliderReleased()), this, SLOT(disableInteraction()));
  connect(world->universeWidget, SIGNAL(zRotationChanged(float)), this, SLOT(setZSliderValue(float)));
  connect(deepSlider, SIGNAL(valueChanged(int)), world->universeWidget, SLOT(setTravel(int)));
  connect(deepSlider, SIGNAL(sliderPressed()), this, SLOT(enableInteraction()));
  connect(deepSlider, SIGNAL(sliderReleased()), this, SLOT(disableInteraction()));
  connect(world->universeWidget->getState(), SIGNAL(stateUpdated()), this, SLOT(autoUpdatedState()));

  connect(world, SIGNAL(postLoadState()), this, SLOT(postLoadState()));

  sliderLayout->addWidget(new QLabel(tr("Alpha")), 0, 0);
  sliderLayout->addWidget(new QLabel(tr("Beta")), 0, 1);
  sliderLayout->addWidget(new QLabel(tr("Gamma")), 0, 2);
  sliderLayout->addWidget(new QLabel(tr("Depth")), 0, 3);

  sliderLayout->addWidget(xSlider, 1, 0);
  sliderLayout->addWidget(ySlider, 1, 1);
  sliderLayout->addWidget(zSlider, 1, 2);
  sliderLayout->addWidget(deepSlider, 1, 3);
 
  sliders->setLayout(sliderLayout);
  sliders_dock->setWidget(sliders);

  return sliders_dock;
}

void Window::setXSliderValue(float angle)
{
  xSlider->setValue(angle * 16);
}

void Window::setYSliderValue(float angle)
{
  ySlider->setValue(angle * 16);
}

void Window::setZSliderValue(float angle)
{
  zSlider->setValue(angle * 16);
}

void Window::setXRotation(int angle)
{
  float a = angle / 16.0;
  world->universeWidget->updateLight();
  world->universeWidget->setXRotation(a);
}

void Window::setYRotation(int angle)
{
  float a = angle / 16.0;
  world->universeWidget->updateLight();
  world->universeWidget->setYRotation(a);
}

void Window::setZRotation(int angle)
{
  float a = angle / 16.0;
  world->universeWidget->updateLight();
  world->universeWidget->setZRotation(a);
}

void Window::autoUpdatedState()
{
  ViewState *v = world->universeWidget->getState();

  world->universeWidget->updateLight();
  xSlider->setValue(16 * v->getRotationX());
  ySlider->setValue(16 * v->getRotationY());
  zSlider->setValue(16 * v->getRotationZ());
  deepSlider->setValue(int(v->getDistance()*5));
}

void Window::showConnectorContextMenu(const QPoint& p)
{
  QListWidgetItem *i = connectorsList->itemAt(p);
  if (i==0 && connectorsList->currentItem() == 0)
    {
      menuConnectionAlternate->exec(connectorsList->mapToGlobal(p));
      return;
    }

  connectorsList->setCurrentItem(i); 
  menuConnection->exec(connectorsList->mapToGlobal(p)); 
}

QDockWidget *Window::createConnectorHandling()
{
  QDockWidget *connectors_dock = new QDockWidget(tr("Connectors"), this);
  QWidget *connectors = new QWidget(connectors_dock);

  QVBoxLayout *cVbox = new QVBoxLayout;
  QHBoxLayout *buttons2Layout = new QHBoxLayout;

  connectorsList = new QListWidget;
  connectorsList->setSelectionMode (QListWidget::MultiSelection);

  connectorsList->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(connectorsList, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(showConnectorContextMenu(const QPoint&)));

  QPushButton *upConnector = new QPushButton(tr("Up"));
  QPushButton *downConnector = new QPushButton(tr("Down"));

  menuConnection = new QMenu;

  menuConnection->addAction(tr("Create"), this, SLOT(createConnectorDialog()));
  menuConnection->addAction(tr("Edit"));
  menuConnection->addAction(tr("Destroy"), this, SLOT(destroyConnector()));
  menuConnection->addAction(tr("Be master"), this, SLOT(setMasterConnector()));
  menuConnection->addAction(tr("Edit representations"), this, SLOT(editRepresentations()));
  multipleSelectionAction = new QAction(tr("Multiple selection"), this);
  multipleSelectionAction->setCheckable(true);
  multipleSelectionAction->setChecked(true);
  menuConnection->addAction(multipleSelectionAction);
  connect(multipleSelectionAction, SIGNAL(toggled(bool)), this, SLOT(toggleMultipleSelection(bool)));

  menuConnectionAlternate = new QMenu;
  menuConnectionAlternate->addAction(tr("Create"), this, SLOT(createConnectorDialog()));
  menuConnectionAlternate->addAction(multipleSelectionAction);

  QCheckBox *rotationButton = new QCheckBox(tr("Enable rotation"));

  QPushButton *runScript = new QPushButton(tr("Run..."));

  showHideAction = menuConnection->addAction(tr("Show"));
  showHideAction->setCheckable(true);

  cVbox->addWidget(connectorsList);
  buttons2Layout->addWidget(upConnector);
  buttons2Layout->addWidget(downConnector);
  buttons2Layout->addWidget(runScript);
  //  buttons2Layout->addWidget(rotationButton);
  cVbox->addLayout(buttons2Layout);

  connect(upConnector, SIGNAL(clicked(bool)), this, SLOT(moveConnectorUp()));
  connect(downConnector, SIGNAL(clicked(bool)), this, SLOT(moveConnectorDown()));
  connect(showHideAction, SIGNAL(triggered(bool)), this, SLOT(showHideConnector(bool)));
  connect(connectorsList, SIGNAL(currentRowChanged(int)), this, SLOT(setCurrentConnector(int)));
  connect(rotationButton, SIGNAL(toggled(bool)), this, SLOT(enableRotation(bool)));
  connect(runScript, SIGNAL(clicked(bool)), this, SLOT(runScript()));

  connectors->setLayout(cVbox);
  connectors_dock->setWidget(connectors);

  return connectors_dock;
}

QMenu *Window::createDataMenu()
{
  QMenu *menuData = new QMenu(tr("Data"));

  menuData->addAction(tr("Save state"), this, SLOT(saveConnectors()));
  menuData->addAction(tr("Load state"), this, SLOT(loadConnectors()));
  menuData->addAction(tr("Load text data"), this, SLOT(loadData()));
#ifdef HDF5_PRESENT
  menuData->addAction(tr("Load HDFCAT data"), this, SLOT(loadHDFCatData()));
#endif
#ifdef NETCDF_PRESENT
  menuData->addAction(tr("Load trajectory"), this, SLOT(loadTrajectoryData()));
  menuData->addAction(tr("Load mesh"), this, SLOT(loadMeshData()));
#endif
#ifdef COSMOTOOL_PRESENT
  menuData->addAction(tr("Load Gadget snapshot"), this, SLOT(loadGadgetData()));
#endif
  menuData->addAction(tr("Load grid data"), this, SLOT(loadGridData()));
  menuData->addAction(tr("Load particles"), this, SLOT(loadParticleData()));
  
  return menuData;
}

void Window::createConnectorDialog()
{
  ConnectorDialog dlg(this, &world->selectors);

  connect(&dlg, SIGNAL(selectorModified(DataSetSelector*)), this, SLOT(recompileSelector(DataSetSelector*)));

  Connector *new_connector = dlg.fullExecution();    
  if (new_connector == 0)
    return;

  world->addNewConnector(new_connector);
  delete new_connector;
}

void Window::addConnectorToList(const QString& name, int id)
{
  connectorsList->addItem(name);
  connectorsList->scrollToItem(connectorsList->item(id));  
  connectorsList->setCurrentRow(id);
}

void Window::recompileSelector(DataSetSelector* s)
{
  int j = 0;
  for (ConnectorVector::const_iterator i = world->allConnectors.begin(); i != world->allConnectors.end(); ++i,++j)
    {
      if (const SetConnector *c = qobject_cast<const SetConnector *>(i->getConnector()))
	{
	  if (c->getSelector() == s)
	    {
	      world->universeWidget->recompileConnection(j);
	    }
	}
    } 
}

void Window::setMasterConnector()
{
  if (connectorsList->currentItem() == 0)
    return;

  int row = connectorsList->currentRow();
  DataConnector *c = world->allConnectors[row].getConnector();
  world->universeWidget->getState()->setMultiplier(c->getDataSource()->getDistanceMultiplier());
  world->universeWidget->recompileAll();
}

void Window::destroyConnector()
{
  if (connectorsList->currentItem() == 0)
    return;
  
  int row = connectorsList->currentRow();

  world->removeConnector(world->allConnectors[row].getName());
}

void Window::showHideConnector(bool toggled)
{
  QList<QListWidgetItem *> selection = connectorsList->selectedItems();

  if (selection.size() == 0)
    return;

  for (QList<QListWidgetItem *>::iterator iter = selection.begin();
       iter != selection.end();
       ++iter)
    {
      int row = connectorsList->row(*iter);
      Connector& c = world->allConnectors[row];
      
      c.setShow(toggled);
      if (c.isShown())
	world->universeWidget->showConnector(c.getName());
      else
	world->universeWidget->hideConnector(c.getName());
      
    }
  world->universeWidget->refresh();  
}

void Window::setCurrentConnector(int row)
{
  if (row < 0)
    return;

  bool shown = world->allConnectors[row].isShown();

  showHideAction->setChecked(shown);
}

void Window::moveConnectorUp()
{
  if (connectorsList->currentItem() == 0)
    return;

  int tomove = connectorsList->currentRow();

  if (tomove == 0)
    return;

  swap(world->allConnectors[tomove], world->allConnectors[tomove-1]);  
  recreateConnectorList();
  connectorsList->scrollToItem(connectorsList->item(tomove-1));  
  connectorsList->setCurrentRow(tomove-1);
}

void Window::moveConnectorDown()
{
  if (connectorsList->currentItem() == 0)
    return;

  int tomove = connectorsList->currentRow();

  if (tomove == world->allConnectors.size()-1)
    return;

  swap(world->allConnectors[tomove], world->allConnectors[tomove+1]);  
  recreateConnectorList();
  connectorsList->scrollToItem(connectorsList->item(tomove+1));  
  connectorsList->setCurrentRow(tomove+1);
}

void Window::recreateConnectorList()
{
  connectorsList->clear();
  
  for (ConnectorVector::const_iterator i = world->allConnectors.begin(); i != world->allConnectors.end(); ++i)
    connectorsList->addItem(i->getName());  
}

void Window::installConnectors(const ConnectorVector& c)
{
  world->installConnectors(c);
  recreateConnectorList();
}

void Window::gl_ready()
{
  register_World(this);
}

QWidget *Window::createScene()
{
  QGLFormat glformat;

  glformat.setVersion(3,0);
  glformat.setProfile(QGLFormat::CoreProfile);

  GraphicsView *view = new GraphicsView(this);
  QGLWidget *glw = new QGLWidget(glformat);
  view->setViewport(glw);
  view->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

  world->universeWidget = new UniverseWidget(this, glw, view);
  view->setScene(world->universeWidget);

  resize(1200, 600);

  connect(world->universeWidget, SIGNAL(gl_ready()), this, SLOT(gl_ready()));

  return view;
}

void Window::createInterface() 
{
  QWidget *view = createScene();

  velocityToggle = new QCheckBox(tr("Enable velocities"));
  extraToggle = new QCheckBox(tr("Show extra data"));
  
  QDockWidget *positions = createPositionSubWindow();
  QDockWidget *connectors = createConnectorHandling();
  positions->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
  connectors->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
  addDockWidget(Qt::RightDockWidgetArea, positions);
  addDockWidget(Qt::RightDockWidgetArea, connectors);
  
  xSlider->setValue(270 * 16);
  ySlider->setValue(0 * 16);
  zSlider->setValue(270 * 16);     
  deepSlider->setValue(500);
  

  menuGLOptions = new QMenu;
    
  menuGLOptions->addAction(positions->toggleViewAction());
  menuGLOptions->addAction(connectors->toggleViewAction());
  menuGLOptions->addAction(tr("Setup"), this, SLOT(mainSetup()));

  QAction *lightAction = new QAction(tr("Visible light"), this);
  lightAction->setCheckable(true);
  lightAction->setChecked(true);
  connect(lightAction, SIGNAL(toggled(bool)), world->universeWidget, SLOT(setLightSourceVisible(bool)));

  menuGLOptions->addAction(lightAction);

  QMenu *dataMenu = createDataMenu();
  menuGLOptions->addMenu(dataMenu);

  world->universeWidget->setOptionMenu(menuGLOptions);

  setCentralWidget(view);
  setWindowTitle(tr("GalaxExplorer"));
}

Window::Window()
{
  world = new World(this);

  r_factory = 0;

  createInterface();  

  DataSetSelector *baseSelector;
  world->selectors[tr("No selection")] = baseSelector = g_allDataSelectors[tr("No selection")]->newSelector();
  baseSelector->setName(tr("No selection"));

  setup = new SetupDialog(*this);

  world->finish_initialization();
}

QSlider *Window::createSlider()
{
  QSlider *slider = new QSlider(Qt::Vertical);
  slider->setRange(0, 360 * 16);
  slider->setSingleStep(16);
  slider->setPageStep(15 * 16);
  slider->setTickInterval(15 * 16);
  slider->setTickPosition(QSlider::TicksRight);
  return slider;
}

QSlider *Window::createDeepSlider()
{
  QSlider *slider = new QSlider(Qt::Vertical);
  slider->setRange(0, 500);
  slider->setSingleStep(10);
  slider->setPageStep(20);
  slider->setTickInterval(20);
  slider->setTickPosition(QSlider::TicksRight);
  return slider;
}

QSlider *Window::createVelocitySlider()
{
  QSlider *slider = new QSlider(Qt::Vertical);
  slider->setRange(-50,50);
  slider->setSingleStep(1);
  slider->setPageStep(10);
  slider->setTickInterval(10);
  slider->setTickPosition(QSlider::TicksRight);

  return slider;
}

void Window::keyPressEvent(QKeyEvent *event)
{
  if (event->key() == Qt::Key_S)
    {
      int w, h;

      world->universeWidget->getRenderer()->getCurrentSize(w, h);
      cout << "w=" << w << " h=" << h << endl;
      world->universeWidget->capture("snapshot.png", w, h);
    }
}

void Window::saveConnectors()
{
  QString fname = QFileDialog::getSaveFileName(this, tr("Save current state to file"));

  if (fname == "")
    return;

  world->saveState(fname);
}

void Window::loadConnectors()
{
  QString fname = QFileDialog::getOpenFileName(this, tr("Load current state from file"));  

  if (fname == "")
    return;

  world->loadState(fname);
}

void Window::postLoadState()
{
  recreateConnectorList();
}

void Window::loadGridData()
{
  QString fname = QFileDialog::getOpenFileName(this, tr("Load grid data from file"));  

  if (fname == "")
    return; 

  GalaxExplorer::loadGridData(fname);
}

void Window::loadData()
{
  QString fname = QFileDialog::getOpenFileName(this, tr("Load current state from file"));  

  if (fname == "")
    return; 

  loadDataFromFile(fname);
}

void Window::loadGadgetData()
{
#ifdef COSMOTOOL_PRESENT
  QString fname = QFileDialog::getOpenFileName(this, tr("Load gadget snapshot from file"));
  
  if (fname == "")
    return;

  loadGadget(fname);
#endif
}

void Window::loadHDFCatData()
{
#ifdef HDF5_PRESENT
  QString fname = QFileDialog::getOpenFileName(this, tr("Load hdf catalog from file"));  

  if (fname == "")
    return; 

  loadHDFCatalog(fname);
#endif
}

void Window::loadTrajectoryData()
{
#ifdef NETCDF_PRESENT
  QString fname = QFileDialog::getOpenFileName(this, tr("Load trajectory data from file"));  

  if (fname == "")
    return; 

  loadTrajectoryDataFromFile(fname);  
#endif
}

void Window::loadMeshData()
{
#ifdef NETCDF_PRESENT
  QString fname = QFileDialog::getOpenFileName(this, tr("Load mesh data from file"));  

  if (fname == "")
    return; 

  loadMeshDataFromFile(fname);  
#endif
}

void Window::loadParticleData()
{
  QString fname = QFileDialog::getOpenFileName(this, tr("Load particle data from file"));  

  if (fname == "")
    return; 

  GalaxExplorer::loadParticles(fname);    
}

void Window::editRepresentations()
{
//  EditRepresentationDialog dlg(this);


//  dlg.run();
  
  int row = connectorsList->currentRow();
  DataRepresentation *r = world->allConnectors[row].getConnector()->getRepresentation();
  RepresentationConfigDialog dlg(r, this);
  
  currentRepresentationName = r->getName();
  connect(&dlg, SIGNAL(representationModified()), this, SLOT(ackRepresentationFixed()));
  dlg.exec();
}

void Window::ackRepresentationFixed()
{
  ackRepresentation(currentRepresentationName);
}

void Window::ackRepresentation(QString s)
{
  int j = 0;
  for (ConnectorVector::const_iterator i = world->allConnectors.begin(); 
       i != world->allConnectors.end(); ++i,++j)
    {
      DataRepresentation *r = i->getConnector()->getRepresentation();

      if (r->getName() == s && r->rebuildNeeded())
	{
	  world->universeWidget->recompileConnection(j);
	}
    } 

  world->universeWidget->refresh();
}

void Window::timerEvent(QTimerEvent *event)
{
  if (event->timerId() != myRotationEvent)
    {
      QWidget::timerEvent(event);
      return;
    }

  int angle = zSlider->value() + 15;
  world->universeWidget->setZRotation(angle);  
}

void Window::enableRotation(bool rotation)
{
  if (!rotation)
    killTimer(myRotationEvent);
  else
    myRotationEvent = startTimer(70);
}


void Window::runScript()
{
  QString fname = QFileDialog::getOpenFileName(this, tr("Run script from file"));  

  if (fname == "")
    return; 

  VisuScript script(fname, world);
  
  script.execute();
}

void Window::mainSetup()
{
  setup->setup();
}

void Window::changeRenderer(GLRendererFactory *f)
{
  GLRendererFactory *old_fac = r_factory;
  QSettings settings;

  r_factory = f;
  if (!getUniverse()->restartRenderer())
    r_factory = old_fac;

  settings.setValue("renderer", r_factory->getRendererName());
}


void Window::toggleMultipleSelection(bool c)
{
  connectorsList->setSelectionMode(c ? QListWidget::MultiSelection : QListWidget::SingleSelection);
}

void Window::enableInteraction()
{
  world->universeWidget->getRenderer()->setInteractiveMode(true);
}

void Window::disableInteraction()
{
  world->universeWidget->getRenderer()->setInteractiveMode(false);
  world->universeWidget->refresh();
}
