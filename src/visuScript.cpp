/*+
This is GalaxExplorer (./src/visuScript.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <cmath>
#include <QApplication>
#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QErrorMessage>
#include "glwidget.hpp"
#include "glconnect.hpp"
#include "visuScript.hpp"
#include "base.hpp"
#include "inits.hpp"

using namespace GalaxExplorer;

#define SCRIPT_DIR_VARNAME "SCRIPT_DIR" 

Q_SCRIPT_DECLARE_QMETAOBJECT(ViewState, QObject *)

#define MATH_WRAPPER(fun) \
QScriptValue mathWrapper_##fun(QScriptContext *ctx, QScriptEngine *eng) \
{ \
  if (!ctx->argument(0).isValid()) \
     return ctx->throwError("Function " #fun " needs one argument"); \
\
  QVariant arg0 = ctx->argument(0).toVariant(); \
  if (!arg0.canConvert<double>())					\
     return ctx->throwError("Function " #fun " takes a double argument"); \
  double val = arg0.toDouble();	\
  return eng->newVariant(fun(val));	    \
}


#define REGISTER_MATH_WRAPPER(engine,fun) \
{ \
  QScriptValue myFunction = (engine).newFunction(mathWrapper_##fun);	\
  (engine).globalObject().setProperty(#fun, myFunction);		\
}

MATH_WRAPPER(cos)
MATH_WRAPPER(sin)
MATH_WRAPPER(log)
MATH_WRAPPER(log10)

QScriptValue includeOtherScript(QScriptContext *context,
				QScriptEngine *engine)
{
  if (context->argumentCount() != 1)
      return context->throwError("include takes exactly one argument");

  QString fname = context->argument(0).toVariant().toString();

  QString cwdDir = engine->globalObject().property(SCRIPT_DIR_VARNAME).toVariant().toString();

  QDir d(cwdDir);

  QString fPath = d.filePath(fname);
  QFile f(fPath);
  if (!f.open(QFile::ReadOnly))
    {
      return context->throwError(QString("Cannot open ") + fPath);
    }  
  engine->
    globalObject().setProperty(SCRIPT_DIR_VARNAME,
			       engine->newVariant(QFileInfo(f).absolutePath()));

  QTextStream ts(&f);
  QString program = ts.readAll();
  QScriptValue v = engine->evaluate(program, fname);

  engine->globalObject().setProperty(SCRIPT_DIR_VARNAME,
				     engine->newVariant(cwdDir));
  return v;
}

QScriptValue sleepFunction(QScriptContext *context,
			   QScriptEngine *engine)
{
  bool ok;
  double milliseconds = context->argument(0).toVariant().toDouble(&ok);

  if (!ok)
    return context->throwError("Argument not convertible to double");

  qApp->processEvents(QEventLoop::AllEvents, milliseconds);

  return engine->nullValue();
}

QScriptValue buildColorFromRgbFunction(QScriptContext *context,
				       QScriptEngine *engine)
{
  if (context->argumentCount() < 3)
    return context->throwError("Not enough arguments to buildColorFromRgb");

  QScriptValue a0 = context->argument(0);
  QScriptValue a1 = context->argument(1);
  QScriptValue a2 = context->argument(2);

  if (!(a0.isNumber() && a1.isNumber() && a2.isNumber()))
    return context->throwError("buildColorFromRgb only takes numbers in argument");

  int r = a0.toNumber();
  int g = a1.toNumber();
  int b = a2.toNumber();

  return engine->newVariant(QColor::fromRgb(r,g,b));
}


VisuScript::VisuScript(const QString& programFile, 
		       World *w,
		       QObject *parent)
  throw (NoSuchFileException)
  : QObject(parent)
{
  existingConnections = &w->allConnectors;
  connectionPool = new QObject(this);
  world = w;

  // Build a connection map
  for (int i = 0; i < existingConnections->size(); i++)
    {
      ConnectionScripted *cs = new ConnectionScripted(w->universeWidget,
						      &(*existingConnections)[i],
						      i, connectionPool);
      connectionMap[(*existingConnections)[i].getName()] = cs;
    }

  // Load program
  QFile f(programFile);
  if (!f.open(QFile::ReadOnly))
    {
      throw NoSuchFileException();
    }

  QTextStream ts(&f);
  program = ts.readAll();
  this->programFile = programFile;

  // Setup engine
  engine.globalObject().setProperty("world", engine.newQObject(this));
  engine.globalObject().setProperty("include", engine.newFunction(includeOtherScript));
  engine.globalObject().setProperty("sleep", engine.newFunction(sleepFunction));
  engine.globalObject().setProperty("pi", engine.newVariant(M_PI));
  engine.globalObject().setProperty(SCRIPT_DIR_VARNAME, 
				    engine.newVariant(
						      QFileInfo(programFile).absolutePath()
						      )
				    );
  engine.globalObject().setProperty("universe", engine.newQObject(w->universeWidget));
  engine.globalObject().setProperty("buildColorFromRgb", engine.newFunction(buildColorFromRgbFunction));

  engine.globalObject().setProperty("ViewState", qScriptValueFromQMetaObject<ViewState>(&engine));

  // Math functions
  REGISTER_MATH_WRAPPER(engine, cos);
  REGISTER_MATH_WRAPPER(engine, sin);
  REGISTER_MATH_WRAPPER(engine, log);
  REGISTER_MATH_WRAPPER(engine, log10);
}


VisuScript::~VisuScript()
{  
}

void VisuScript::execute()
{
  engine.pushContext();

  QScriptValue ret = engine.evaluate(program, programFile);

  engine.popContext();

  if (ret.isError())
    {
      //      QErrorMessage::qtHandler()->showMessage(tr("An error occurred during script execution:") + ret.toString());
      qWarning() << "An error occured: "  << ret.toString()  << endl;
    }
  world->universeWidget->setScreenRefreshing(false);
}

QObject *VisuScript::getConnection(const QString& connectionName)
{
  return connectionMap[connectionName];
}

void VisuScript::loadState(QString f)
{
  world->loadState(f);

  delete connectionPool;
  connectionPool = new QObject(this);

  connectionMap.clear();
  for (int i = 0; i < existingConnections->size(); i++)
    {
      ConnectionScripted *cs = new ConnectionScripted(world->universeWidget,
						      &(*existingConnections)[i],
						      i, connectionPool);
      connectionMap[(*existingConnections)[i].getName()] = cs;
    }
}

void VisuScript::hideAllConnectors()
{
  for (int i = 0; i < existingConnections->size(); i++)
    {
      (*existingConnections)[i].setShow(false);
      world->universeWidget->hideConnector((*existingConnections)[i].getName());
    }
}

void VisuScript::setDistanceMultiplier(float v)
{
  world->universeWidget->getState()->setMultiplier(v);
  world->universeWidget->recompileAll();
}

ConnectionScripted::ConnectionScripted(UniverseWidget *w, 
				       Connector *c, int id, QObject *parent)
  : QObject(parent)
{
  this->universe = w;
  this->connector = c;
  this->connectionId = id;
}

ConnectionScripted::~ConnectionScripted()
{
}

void ConnectionScripted::show(bool val)
{
  connector->setShow(val);
  if (connector->isShown())
    universe->showConnector(connector->getName());
  else
    universe->hideConnector(connector->getName());
}

void ConnectionScripted::setMaster()
{
  universe->getState()->setMultiplier( 
    connector->getConnector()->getDataSource()->getDistanceMultiplier()
  );
  universe->recompileAll();
}

void ConnectionScripted::recompile()
{
  universe->recompileConnection(connectionId);
}

QObject *ConnectionScripted::getDataSet()
{
  if (SetConnector *c = qobject_cast<SetConnector *>(connector->getConnector()))
    {
      return c->getDataset();
    }
}

QObject *ConnectionScripted::getDataSetSelector()
{
  if (SetConnector *c = qobject_cast<SetConnector *>(connector->getConnector()))
    {
       return c->getSelector();
    }
}

QObject *ConnectionScripted::getRepresentation()
{
  return connector->getConnector()->getRepresentation();
}
