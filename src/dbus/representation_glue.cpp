/*+
This is GalaxExplorer (./src/dbus/representation_glue.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <iostream>
#include <QtCore>
#include "representations/dataRepresentation.hpp"
#include "inits.hpp"
#include "representation_glue.hpp"

using namespace GalaxExplorer;
using namespace std;

RepresentationHandlerGlue::RepresentationHandlerGlue(QObject *parent)
  : QObject(parent)
{
}

RepresentationHandlerGlue::~RepresentationHandlerGlue()
{
}

bool RepresentationHandlerGlue::duplicateRepresentation(const QString& dst, const QString& src)
{
  DataRepresentationMap::iterator i = g_allRepresentations.find(src);
  DataRepresentationMap::iterator j = g_allRepresentations.find(dst);
  DataRepresentation *representation;

  if (i == g_allRepresentations.end())
    return false;

  if (j != g_allRepresentations.end())
    return false;

  representation = (*i)->duplicate();
  representation->setName(dst);

  registerDataRepresentation(representation);
  return true;
}

QStringList RepresentationHandlerGlue::getRepresentationList()
{
  return g_allRepresentations.keys();
}




RepresentationGlue::RepresentationGlue(const QString& repname, QObject *parent)
  : QObject(parent), name(repname)
{
}

RepresentationGlue::~RepresentationGlue()
{
}

QString RepresentationGlue::getType()
{
  return g_allRepresentations[name]->getRepresentationName();
}

ParameterResult RepresentationGlue::getParameter(const QString& param, QDBusVariant& value)
{
  DataRepresentation *representation = g_allRepresentations[name];
  const QMetaObject *meta = representation->metaObject();
  QVariant v;
  
  int idx_prop = meta->indexOfProperty(qPrintable(param));
  QMetaProperty metaprop;

  if (idx_prop < 0)
    return PARAM_ERROR_NO_PROPERTY;

  metaprop = meta->property(idx_prop);
  v = metaprop.read(representation);

  value.setVariant(v);

  return PARAM_SUCCESS;
}

ParameterResult RepresentationGlue::setParameter(const QString& param, const QDBusVariant& value)
{
  DataRepresentation *representation = g_allRepresentations[name];
  const QMetaObject *meta = representation->metaObject();
  QVariant v = value.variant();
  
  int idx_prop = meta->indexOfProperty(qPrintable(param));
  QMetaProperty metaprop;

  if (idx_prop < 0)
    return PARAM_ERROR_NO_PROPERTY;

  metaprop = meta->property(idx_prop);
  if (v.canConvert(metaprop.type()))
    {
      cout << "Writing '" << qPrintable(v.toString()) << "' to " << qPrintable(param) << endl;
      metaprop.write(representation, v);
    }
  else
    {
      return PARAM_ERROR_INVALID_TYPE;
    }

  
  
  return PARAM_SUCCESS;
}

QStringList RepresentationGlue::getParameterList()
{
  const QMetaObject *meta = g_allRepresentations[name]->metaObject();
  QStringList l;

  for (int i = 0; i < meta->propertyCount(); i++)    
    {
      QMetaProperty metaprop = meta->property(i);
      QString propName = metaprop.name();

      if (propName == QString("objectName") || propName == QString("name"))
	continue;

      l.push_back(propName);
    }

  return l;
}

QString RepresentationGlue::getParameterType(const QString& n, ParameterResult& result)
{
  DataRepresentation *representation = g_allRepresentations[name];
  const QMetaObject *meta = representation->metaObject();
  int idx_prop = meta->indexOfProperty(qPrintable(n));

  QMetaProperty metaprop;

  if (idx_prop < 0)
    {
      result = PARAM_ERROR_NO_PROPERTY;
      return "";
    }

  result = PARAM_SUCCESS;
  metaprop = meta->property(idx_prop);
  switch (metaprop.type())
    {
    case QVariant::Bool:
      return "bool";
    case QVariant::Int:      
      return "int";
    case QVariant::Color:
      return "color";
    case QVariant::String:
      return "str";
    case QVariant::Double:
      return "double";
    default:
      result = PARAM_ERROR_INVALID_TYPE;
      return "";
    }
}
