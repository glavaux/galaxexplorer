/*+
This is GalaxExplorer (./src/dbus/dbus_if.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <QtDBus/QtDBus>
#include "dbus_if.hpp"
#include "dbus_glue.hpp"
#include "viewstateadaptor.h"
#include "dataadaptor.h"
#include "data_handler.hpp"
#include "window.hpp"
#include "connectoradaptor.h"
#include "connector_glue.hpp"
#include "skybox_glue.hpp"
#include "skyboxadaptor.h"
#include "dbus_enum.hpp"
#include "representation_glue.hpp"
#include "representationadaptor.h"
#include "representation_handleradaptor.h"
#include <iostream>

using namespace GalaxExplorer;
using namespace std;

void GalaxExplorer::initializeDBusService()
{
  qDBusRegisterMetaType<GalaxExplorer::PositionStructure>();
  qDBusRegisterMetaType<GalaxExplorer::ParameterResult>();
  qDBusRegisterMetaType<GalaxExplorer::SkyBoxColor>();
  qDBusRegisterMetaType<GalaxExplorer::SkyBox_SingleValuePalette>();
  qDBusRegisterMetaType<GalaxExplorer::SkyBoxValuePalette>();

  QDBusConnection connection = QDBusConnection::sessionBus();
  connection.registerService("org.GalaxExplorer");

  DataHandler *h = new DataHandler();

  new DataHandlerAdaptor(h);

  connection.registerObject("/Data", h);
  
  //  connection.registerObject("/Connectors", c);
}

void GalaxExplorer::registerDBusRepresentation(DataRepresentation *r)
{
  QDBusConnection connection = QDBusConnection::sessionBus();
  RepresentationGlue *rg = new RepresentationGlue(r->getName(), r);

  new RepresentationAdaptor(rg);

  QString path = QString("/world/representations/") + r->getName();
  connection.registerObject(path, rg);

  cout << "Registered representation at " << qPrintable(path) << endl;
}

void GalaxExplorer::register_World(Window *main_window)
{
  QDBusConnection connection = QDBusConnection::sessionBus();
  ConnectorGlue *cg = new ConnectorGlue(main_window->getWorld(), main_window);
  ViewStateGlue *g  = new ViewStateGlue(main_window->getWorld()->universeWidget);
  SkyBoxGlue *sg = new SkyBoxGlue(main_window->getWorld()->universeWidget);

  RepresentationHandlerGlue *rh = new RepresentationHandlerGlue(main_window);

  new ConnectorHandlerAdaptor(cg);
  new ViewStateAdaptor(g);
  new RepresentationHandlerAdaptor(rh);
  new SkyBoxAdaptor(sg);

  connection.unregisterObject("/world/connectors");
  connection.registerObject("/world/connectors",  cg);
  
  connection.unregisterObject("/world/ViewState");
  connection.registerObject("/world/ViewState", g);

  connection.unregisterObject("/world/RepresentationHandler");
  connection.registerObject("/world/RepresentationHandler", rh);

  connection.unregisterObject("/world/SkyBox");
  connection.registerObject("/world/SkyBox", sg);
}
