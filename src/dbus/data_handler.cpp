/*+
This is GalaxExplorer (./src/dbus/data_handler.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <iostream>
#include <QtCore>
#include <QFrame>
#include <QTcpServer>
#include <QTcpSocket>
#include <utility>
#include "data_handler.hpp"
#include "datastorage.hpp"
#include "datasets.hpp"
#include "ui_incoming_data.h"
#include "io/load_data.hpp"
#include "inits.hpp"
#include "datamesh.hpp"
#include "virtualDataSetTrajectory.hpp"
#include "io/load_gadget.hpp"
#include "io/load_ramses.hpp"
#include <boost/shared_ptr.hpp>
#include "datagrid.hpp"
#include "data_workers.hpp"

#define MAX_PACKETS 1024

#define ALL_FLAGS (0)

using namespace GalaxExplorer;
using namespace std;

DataHandler::DataHandler(QObject *parent)
  : server(new QTcpServer(this))
{
  server->setMaxPendingConnections(1);

  activateServer = false;

  currentCookie = 1;
  points = 0;
  numPoints = 0;
  currentSocket = 0;
  all_extra_attributes.clear();

  info_win_data = new Ui::IncomingDataFrame();
  //  info_win = new QFrame();
  //  info_win_data->setupUi(info_win);

  //  connect(info_win_data->cancel, SIGNAL(clicked(bool)), this, SLOT(forceDisconnection()));
  created = false;
}

DataHandler::~DataHandler()
{
  abortConnection();

  delete info_win;
  delete info_win_data;

  delete server;
}

void DataHandler::abortConnection()
{
  // First we cleanup the previous allocation
  cout << "Abort connection." << endl;
  if (points != 0)
    {
      delete[] points;
      for (int i = 0; i < all_extra_attributes.size(); i++)
	delete all_extra_attributes[i].second;
      points = 0;
      this->numPoints = 0;
      all_extra_attributes.clear();
    }
}

void DataHandler::replyWaiterOk()
{
  cout << "Reply to waiters that it is ok" << endl;
  replyAllWaiter(1);
}

void DataHandler::replyWaiterFailure()
{
  replyAllWaiter(-1);
}

bool DataHandler::dropData(const QString& n, bool force_destroy)
{
  return dropDataSource(n, force_destroy);
}

int DataHandler::createPointDataSet(const QString& data_name, int numPoints, int flags,
				    const QStringList& attributes, double multiplier, int& cookie)
{
  if (currentSocket != 0)
    return -1;

  if (activateServer)
    {
      abortConnection();
      linkDisconnected();
      server->close();
      currentCookie++;
    }
  
  if (!server->listen(QHostAddress::LocalHost))
    return -1;

  created = false;
  activateServer = true;

  points = new BarePoint[numPoints];
  if (points == 0)
    {
      server->close();
      activateServer = false;
      return -1;
    }

  assert(all_extra_attributes.size() == 0);

  QStringList::const_iterator iter = attributes.begin();
  for (int i = 0; i < attributes.size(); i++)
    {
      all_extra_attributes.push_back
	(pair<QString,DataStorage*>(*iter, createStorage<float>(numPoints)));
      ++iter;
    }

  this->data_name = data_name;
  this->numPoints = numPoints;

  cookie = currentCookie;

  currentMultiplier = multiplier;
  
  disconnect(server, SIGNAL(newConnection()), this, 0);
  connect(server, SIGNAL(newConnection()), this, SLOT(newDataConnection_points()));

  return server->serverPort();
}

int DataHandler::createDataGrid(const QString& data_name, int Nx, int Ny, int Nz, int& cookie)
{
  if (currentSocket != 0)
    return -1;

  if (activateServer)
    {
      abortConnection();
      linkDisconnected();
      server->close();
      currentCookie++;
    }
  
  if (!server->listen(QHostAddress::LocalHost))
    return -1;

  BoundaryVector boundaries(6);

  boundaries[0] = -1;
  boundaries[1] = 1;
  boundaries[2] = -1;
  boundaries[3] = 1;
  boundaries[4] = -1;
  boundaries[5] = 1;
  if (boundaries.size() != 6)
    return -2;

  created = false;
  activateServer = true;

  numPoints = Nx*Ny*Nz;

  try
    {
      datagrid.alloc(Nx,Ny,Nz);
    }
  catch (CannotAllocateArray& e)
    {
      server->close();
      activateServer = false;
      return -1;
    }

  this->data_name = data_name;
  for (int i = 0; i < 6; i++)
    this->boundaries[i] = boundaries[i];
  pointsRead = 0;

  cookie = currentCookie;

  disconnect(server, SIGNAL(newConnection()), this, 0);
  connect(server, SIGNAL(newConnection()), this, SLOT(newDataConnection_grid()));

  return server->serverPort();
}

int DataHandler::createDataMesh(const QString& data_name, int numTriangles, int flags,
				int& cookie)
{
  if (currentSocket != 0)
    return -1;

  if (activateServer)
    {
      abortConnection();
      linkDisconnected();
      server->close();
      currentCookie++;
    }
  
  if (!server->listen(QHostAddress::LocalHost))
    return -1;

  created = false;
  activateServer = true;

  trilist = new int[3*numTriangles];
  if (trilist == 0)
    {
      server->close();
      return -1;
    }

  this->data_name = data_name;
  this->numPoints = numTriangles;

  cookie = currentCookie;

  disconnect(server, SIGNAL(newConnection()), this, 0);
  connect(server, SIGNAL(newConnection()), this, SLOT(newDataConnection_mesh()));

  return server->serverPort();
}



bool DataHandler::checkVolatileServer()
{
  cout << "Incoming connection " << endl;

  QTcpSocket *socket = server->nextPendingConnection();

  if (socket == 0)
    return false;

  cout << "Activate value = " << activateServer << endl;
  if (!activateServer)
    {
      delete socket;
      return false;
    }

  QByteArray array;
  
  // Check the cookie
  if (!socket->waitForReadyRead(1000))
    {
      delete socket;
      return false;
    }
  
  array = socket->read(sizeof(int));
  cout << "Size read = " << array.size() << endl;
  if (array.size() != sizeof(int))
    {
      delete socket;
      return false;
    }

  cout << "Got some cookie data" << endl;
  cout << "Incoming value is " << *(const int *)array.data() << endl;
  if (*(const int *)array.data() != currentCookie)
    {
      delete socket;
      return false;
    }

  cout << "Got the good cookie." << endl;

  // We got the right cookie.
  activateServer = false;
  
  // Close server
  server->close();

  pointsRead = 0;

  currentSocket = socket;
  cout << "Create info window." << endl;
  createInfoWindow();

  return true;
}

void DataHandler::newDataConnection_points()
{
  if (!checkVolatileServer())
    return;

  connect(currentSocket, SIGNAL(disconnected()), this, SLOT(linkDisconnected()));
  connect(currentSocket, SIGNAL(readyRead()), this, SLOT(newIncomingData_points()));

  if (currentSocket->bytesAvailable() != 0)
    newIncomingData_points();
}

void DataHandler::newDataConnection_mesh()
{
  if (!checkVolatileServer())
    return;

  connect(currentSocket, SIGNAL(disconnected()), this, SLOT(linkDisconnected()));
  connect(currentSocket, SIGNAL(readyRead()), this, SLOT(newIncomingData_mesh()));

  if (currentSocket->bytesAvailable() != 0)
    newIncomingData_mesh();
}

void DataHandler::newDataConnection_grid()
{
  if (!checkVolatileServer())
    return;
  
  connect(currentSocket, SIGNAL(disconnected()), this, SLOT(linkDisconnected()));
  connect(currentSocket, SIGNAL(readyRead()), this, SLOT(newIncomingData_grid()));

  if (currentSocket->bytesAvailable() != 0)
    newIncomingData_grid();
}

void DataHandler::newIncomingData_grid()
{
  cout << "Got some data." << endl;
  if (currentSocket == 0)
    return;
  
  int packetSize = sizeof(float);
  while (currentSocket != 0)
    {
      QByteArray allNewData = currentSocket->read(packetSize*MAX_PACKETS);
      int numPackets;

      if (allNewData.size() == 0)
        return;

      current_buffer.append(allNewData);
 
      numPackets = current_buffer.size()/packetSize;

      if (numPackets+pointsRead > numPoints)
        {
          cout << "Overflow" << endl;
          linkDisconnected();
          return;
        }
            
      for (int i = 0 ; i < numPackets; i++,pointsRead++)
        {
          float *base = (float*)&current_buffer.data()[i*packetSize];
          
          datagrid[pointsRead] = *base;
        }
      
      current_buffer = current_buffer.right(current_buffer.size() - numPackets*packetSize);

      updateInfoWindow();
      
      if (pointsRead == numPoints)
        {
          cout << "Done obtaining the points" << endl;
          buildDataGrid();
          linkDisconnected();
          return;
        }
    }
}

void DataHandler::newIncomingData_mesh()
{
  cout << "Got some data." << endl;
  if (currentSocket == 0)
    return;
  
  int packetSize = 3*sizeof(int);
  while (currentSocket != 0)
    {
      QByteArray allNewData = currentSocket->read(packetSize*MAX_PACKETS);
      int numPackets;
      
      if (allNewData.size() == 0)
	return;
      
      current_buffer.append(allNewData);

      cout << "Received some data (s=" << allNewData.size() << ")" << endl;
      
      numPackets = current_buffer.size()/packetSize;
      
      cout << "NumPackets+pointsRead=" << numPackets + pointsRead << endl;
      cout << "numPoints= " << numPoints << endl;
      if (numPackets+pointsRead > numPoints)
        {
          cout << "Too much data" << endl;
          linkDisconnected();
          return;
        }
      
      for (int i = 0 ; i < numPackets; i++,pointsRead++)
        {
          int *base = (int*)&current_buffer.data()[i*packetSize];
          
          trilist[3*pointsRead + 0] = base[0];
          trilist[3*pointsRead + 1] = base[1];
          trilist[3*pointsRead + 2] = base[2];	  
        }
      
      current_buffer = current_buffer.right(current_buffer.size() - numPackets*packetSize);

      updateInfoWindow();
      
      cout << "Points read = "<< pointsRead << endl;
      if (pointsRead == numPoints)
        {
          buildDataMesh();
          linkDisconnected();
          return;
        }
    }
}

void DataHandler::newIncomingData_points()
{
  cout << "Got some data." << endl;
  if (currentSocket == 0)
    return;
  
  int packetSize = (3+all_extra_attributes.size())*sizeof(float);
  while (currentSocket != 0)
    {
      QByteArray allNewData = currentSocket->read(packetSize*MAX_PACKETS);
      int numPackets;
      
      if (allNewData.size() == 0)
        return;
      
      current_buffer.append(allNewData);

      cout << "Received some data (s=" << allNewData.size() << ")" << endl;
      
      numPackets = current_buffer.size()/packetSize;
      
      cout << "NumPackets+pointsRead=" << numPackets + pointsRead << endl;
      cout << "numPoints= " << numPoints << endl;
      if (numPackets+pointsRead > numPoints)
        {
          cout << "Too much data" << endl;
          linkDisconnected();
          return;
        }
      
      for (int i = 0 ; i < numPackets; i++,pointsRead++)
        {
          float *base = (float*)&current_buffer.data()[i*packetSize];
          
          points[pointsRead].x = base[0];
          points[pointsRead].y = base[1];
          points[pointsRead].z = base[2];
          
          for (int j = 0; j < all_extra_attributes.size(); j++)
            all_extra_attributes[j].second->getValue<float>(pointsRead) = base[3+j];
        }
      
      current_buffer = current_buffer.right(current_buffer.size() - numPackets*packetSize);

      updateInfoWindow();
      
      cout << "Points read = "<< pointsRead << endl;
      if (pointsRead == numPoints)
        {
          buildDataset();
          linkDisconnected();
          return;
        }
    }
}

void DataHandler::linkDisconnected()
{
  QTcpSocket *sock = currentSocket;
    
  if (currentSocket == 0)
    return;
  currentSocket = 0;

  abortConnection();

  sock->deleteLater();
  destroyInfoWindow();

  currentCookie++;
  activateServer = false;
  server->close();

  replyAllWaiter(-2);
}

void DataHandler::replyAllWaiter(int status)
{
  QList<QDBusMessage>::iterator iter_message = replyMessageList.begin();
  QList<QDBusConnection>::iterator iter_conn = replyConnectionList.begin();

  for (int i = 0; i < replyMessageList.size(); i++)
    {
      QDBusMessage reply = iter_message->createReply(QVariant(1));
      iter_conn->send(reply);

      iter_message++;
      iter_conn++;
    }

  replyMessageList.clear();
  replyConnectionList.clear();
}

void DataHandler::buildDataset()
{
  DataSet *data;

  cout << "Building dataset" << endl;
  data = new DataSet(points, numPoints);

  for (int j = 0; j < all_extra_attributes.size(); j++)
    {
      cout << "Adding attribute " << qPrintable(all_extra_attributes[j].first) << " ..." << endl;
      data->addAttribute(all_extra_attributes[j].first, all_extra_attributes[j].second);
    }

  all_extra_attributes.clear();

  data->setDistanceMultiplier(currentMultiplier);

  cout << "Add dat source" << endl;
  GalaxExplorer::addDataSource(data_name, data);

  created = true;
  points = 0;
  numPoints = 0;

  replyAllWaiter(1);
}

void DataHandler::buildDataMesh()
{
  DataMesh *data;

  data = new DataMesh(trilist, numPoints);

  GalaxExplorer::addDataSource(data_name, data);

  created = true;
  trilist = 0;
  numPoints = 0;

  replyAllWaiter(1);
}

void DataHandler::buildDataGrid()
{
  DataGrid *data;

  data = new DataGrid(datagrid, boundaries);

  GalaxExplorer::addDataSource(data_name, data);

  created = true;
  numPoints = 0;
  datagrid.free();

  replyAllWaiter(1);
}

void DataHandler::createInfoWindow()
{
  QByteArray a = data_name.toAscii();

  info_win = new QFrame();
  info_win_data->setupUi(info_win);
  connect(info_win_data->cancel, SIGNAL(clicked(bool)), this, SLOT(forceDisconnection()));

  cout << "Showing window. (name=" << a.data() << ")" << endl;
  info_win_data->data_set_name->setText(data_name);

  info_win_data->progress->setRange(0, numPoints);
  info_win_data->progress->setValue(0);

  info_win->show();
}


void DataHandler::updateInfoWindow()
{
  info_win_data->progress->setValue(pointsRead);
}


void DataHandler::destroyInfoWindow()
{
  delete info_win;
  //info_win->hide();
}


void DataHandler::forceDisconnection()
{
  linkDisconnected();
}

int DataHandler::waitData(const QString& s)
{
  if (g_allDataSources.find(s) != g_allDataSources.end())
    return 1;

  if (data_name != s)
    return -1;

  replyMessageList.push_back(message());
  replyConnectionList.push_back(connection());
  setDelayedReply(true);

  return 0;
}

int DataHandler::createMultiRamses(const QString& name,
				   const QString& s, int N,
                                   double shift_x, double shift_y, double shift_z)
{
  QThread *thread = new QThread;
  LoadingMultiRamsesDataThread *load_run = new LoadingMultiRamsesDataThread();
  
  load_run->moveToThread(thread);

  load_run->name = name;
  load_run->s = s;
  load_run->sx = shift_x;
  load_run->sy = shift_y;
  load_run->sz = shift_z;
  load_run->N = N;
  connect(load_run, SIGNAL(finished()), this, SLOT(replyWaiterOk()));
  connect(load_run, SIGNAL(error()), this, SLOT(replyWaiterFailure()));
  connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
  connect(thread, SIGNAL(started()), load_run, SLOT(run()));
  connect(load_run, SIGNAL(finished()), thread, SLOT(quit()));


  replyMessageList.push_back(message());
  replyConnectionList.push_back(connection());
  setDelayedReply(true);

  thread->start();
    
  return 0;
}

int DataHandler::createMultiGadget(const QString& name,
				   const QString& s, int N,
                                   double shift_x, double shift_y, double shift_z)
{
  try
    {
      cerr << "Create loader" << endl;
      boost::shared_ptr<EvolvingDataSetLoader> loader(new EvolvingGadgetLoader(s, N));
      cerr << "Trajectory binding" << endl;
      VirtualDataSetTrajectory *traj = new VirtualDataSetTrajectory(loader, shift_x, shift_y, shift_z);
      cerr << "Link trajectory to core" << endl;
      GalaxExplorer::addDataSource(name, traj);

      replyAllWaiter(1);
      return 1;
    }
  catch (const VirtualDataSetException& e)
    {
      cerr << "Trajectory exception: " << e.what() << endl;
      replyAllWaiter(-1);
      return -1;
    }

  return 0;
}

int DataHandler::loadRamses(const QString& name, const QString& basepath, int id)
{
  LoadingRamsesDataThread *load_run = new LoadingRamsesDataThread();
  QThread *thread = new QThread;

  load_run->moveToThread(thread);
  load_run->name = name;
  load_run->basepath = basepath;
  load_run->id = id;
  connect(load_run, SIGNAL(finished()), this, SLOT(replyWaiterOk()));
  connect(load_run, SIGNAL(error()), this, SLOT(replyWaiterFailure()));
  connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
  connect(thread, SIGNAL(started()), load_run, SLOT(run()));
  connect(load_run, SIGNAL(finished()), thread, SLOT(quit()));

  replyMessageList.push_back(message());
  replyConnectionList.push_back(connection());

  thread->start();

  return 0;
}

int DataHandler::setTrajectoryTime(const QString& name,
				   float t)
{
  DataSourceMap::iterator p = g_allDataSources.find(name);

  if (p == g_allDataSources.end())
    return -3;

  VirtualDataSetTrajectory *traj = qobject_cast<VirtualDataSetTrajectory *>(*p);

  if (traj == 0)
    return -2;

  try
    {
      traj->setTimePosition(t);
    }
  catch (VirtualDataSetException& e)
    {
      cerr << "Trajectory exception: " << e.what() << endl;
      return -1;
    }

  return 1;
}


int DataHandler::loadGridFromFile(const QString& name,
				  const QString &filename)
{
  DataGrid *g = GalaxExplorer::loadGridDataBase(filename);

  if (g==0)
    return -1;

  GalaxExplorer::addDataSource(name, g);
  return 1;
}
