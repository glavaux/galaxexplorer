/*+
This is GalaxExplorer (./src/dbus/connector_glue.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <iostream>
#include <QtCore>
#include "glconnect.hpp"
#include "inits.hpp"
#include "glwidget.hpp"
#include "connector_glue.hpp"
#include "datasource.hpp"
#include "datasets.hpp"
#include "datamesh.hpp"
#include "world.hpp"

using namespace std;
using namespace GalaxExplorer;

ConnectorGlue::ConnectorGlue(World *W, QObject *parent)
  : QObject(parent), world(W)
{
}

ConnectorGlue::~ConnectorGlue()
{
}

bool ConnectorGlue::destroyConnection(const QString& connector_name)
{
  return world->removeConnector(connector_name);
}

void ConnectorGlue::setVisibility(const QString& name, bool visible)
{
  world->setShowConnector(name, visible);
}

QStringList ConnectorGlue::getConnectorList()
{
  return world->getConnectorList();
}

bool ConnectorGlue::createMeshConnection(const QString& connector_name,
					 const QString& mesh_name,
					 const QString& data_name,
					 const QString& representation_name)
{
  Connector c;
  DataSourceMap::iterator i_source = g_allDataSources.find(data_name);
  DataSourceMap::iterator i_mesh_source = g_allDataSources.find(mesh_name);
  DataRepresentationMap::iterator i_repr = g_allRepresentations.find(representation_name);

  // Sanity check...
  if (i_source == g_allDataSources.end() || i_mesh_source == g_allDataSources.end())
    return false;

  if (i_repr == g_allRepresentations.end())
    return false;
  
  ConnectorVector& cv = world->getConnectors();

  // Check the existence of a connection
  for (ConnectorVector::iterator i_conn = cv.begin();
       i_conn != cv.end();
       ++i_conn)
    {
      if (i_conn->getName() == connector_name)
	return false;
    }

  DataSet *src_set = qobject_cast<DataSet *>(*i_source);
  DataMesh *src_mesh = qobject_cast<DataMesh *>(*i_mesh_source);
  
  if (src_set == 0 || src_mesh == 0)
    return false;

  c = buildConnector(representation_name, connector_name, 
		     src_mesh, src_set);

  world->addNewConnector(&c);

  return true; 
}

bool ConnectorGlue::createConnection(const QString& connector_name,
				     const QString& data_name,
				     const QString& selector_name,
				     const QString& representation_name)
{
  Connector *c = 0;
  DataSourceMap::iterator i_source = g_allDataSources.find(data_name);
  DataRepresentationMap::iterator i_repr = g_allRepresentations.find(representation_name);
  QString sel_name = selector_name;

  if (i_source == g_allDataSources.end())
    return false;

  if (i_repr == g_allRepresentations.end())
    return false;
    
  if (sel_name == "")
    sel_name = "No selection";
  
  ConnectorVector& cv = world->getConnectors();

  for (ConnectorVector::iterator i_conn = cv.begin();
       i_conn != cv.end();
       ++i_conn)
    {
      if (i_conn->getName() == connector_name)
        return false;
    }

  DataSource *src = *i_source;
  
  if (DataSet *ds = qobject_cast<DataSet *>(g_allDataSources[data_name]))
    {
      AllDataSelector::iterator i_sel = world->selectors.find(sel_name);

      if (i_sel == world->selectors.end())
        return false;
      
      c = new Connector(buildConnector(representation_name, connector_name, ds,
				       *i_sel));
    }
  else if (DataGrid *dg = qobject_cast<DataGrid *>(g_allDataSources[data_name]))
    {
      c = new Connector(buildConnector(representation_name, connector_name, dg));
    }
  else
    return false;

  world->addNewConnector(c);
  delete c;

  return true;
}


bool ConnectorGlue::setAuxiliaryData(const QString& connector_name,
                                     int aux,
                                     const QString& auxdata_name)
{
  ConnectorVector& wv = world->getConnectors();
  DataSourceMap::iterator i_aux = g_allDataSources.find(auxdata_name);  
  
  if (i_aux == g_allDataSources.end())
    return false;
  
  for (ConnectorVector::iterator i = wv.begin(); i != wv.end(); ++i)
    {
      if (i->getName() == connector_name)
        {
          if (*i_aux)
            {
              cout << "Setup auxiliary data aux_id=" << aux << " with " << qPrintable(auxdata_name) << endl;
              i->getConnector()->setAdditionalData(aux, *i_aux);
            }
        }
    }
  world->refreshConnectors();  
  return true;
}

