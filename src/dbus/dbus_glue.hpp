/*+
This is GalaxExplorer (./src/dbus/dbus_glue.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef _GALAX_DBUS_GLUE_HPP
#define _GALAX_DBUS_GLUE_HPP

#include <QtCore>
#include <QtDBus>
#include "glwidget.hpp"

namespace GalaxExplorer
{
  struct PositionStructure
  {
    double x, y, z;
  };
  
  static
  inline
  QDBusArgument &operator<<(QDBusArgument &argument, const PositionStructure& s)
  {
    argument.beginStructure();
    argument << s.x << s.y << s.z;
    argument.endStructure();
    return argument;
  }

  static
  inline
  const QDBusArgument &operator>>(const QDBusArgument &argument, PositionStructure& s)
  {
    argument.beginStructure();
    argument >> s.x >> s.y >> s.z;
    argument.endStructure();
    return argument;
  }

  class ViewStateGlue: public QObject
  {
    Q_OBJECT
  public:
    ViewStateGlue(UniverseWidget *w);
    ~ViewStateGlue();
    
    PositionStructure getRotation();
    void setRotation(const PositionStructure& p);    
    PositionStructure getCenter();
    void setCenter(const PositionStructure& p);

    void moveCube(const PositionStructure& p);

    void setBackgroundColor(double r, double g, double b);
    void setShowAxis(bool s);

    void changeScaling(double multiplier);

    void capture(const QString& filename, int width, int height);

    void refresh();

    void setFOV(double fov);

    void recompileAll();

    void setZDistance(double d);

  private:
    ViewState *m_state;
    UniverseWidget *m_widget;
  };

};
 
Q_DECLARE_METATYPE(GalaxExplorer::PositionStructure)

#endif
