/*+
This is GalaxExplorer (./src/dbus/skybox_glue.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __SKYBOX_GLUE_HPP
#define __SKYBOX_GLUE_HPP

#include <QtCore>
#include <QtDBus>
#include "glwidget.hpp"
#include "dbus_enum.hpp"

namespace GalaxExplorer
{

  struct SkyBoxColor
  {
    double r, g, b, a;
  };

  struct SkyBox_SingleValuePalette
  {
    double value;
    SkyBoxColor rgba;
  };

  typedef QList<SkyBox_SingleValuePalette> SkyBoxValuePalette;

  static
  inline
  QDBusArgument &operator<<(QDBusArgument &argument, const SkyBoxColor& s)
  {
    argument.beginStructure();
    argument << s.r << s.g << s.b << s.a;
    argument.endStructure();
    return argument;
  }

  static
  inline
  const QDBusArgument &operator>>(const QDBusArgument &argument, SkyBoxColor& s)
  {
    argument.beginStructure();
    argument >> s.r >> s.g >> s.b >> s.a;
    argument.endStructure();
    return argument;
  }

  static
  inline
  QDBusArgument &operator<<(QDBusArgument &argument, const SkyBox_SingleValuePalette& s)
  {
    argument.beginStructure();
    argument << s.value << s.rgba;
    argument.endStructure();
    return argument;
  }

  static
  inline
  const QDBusArgument &operator>>(const QDBusArgument &argument, SkyBox_SingleValuePalette& s)
  {
    argument.beginStructure();
    argument >> s.value >> s.rgba;
    argument.endStructure();
    return argument;
  }

  class SkyBoxGlue: public QObject
  {
    Q_OBJECT
  public:
    SkyBoxGlue(UniverseWidget *universe);
    virtual ~SkyBoxGlue();

    GalaxDbusResult add(const QString& name, const QString& fname);
    QList<QString> getList();
    GalaxDbusResult reorder(const QList<QString>& list_box);
    GalaxDbusResult remove(const QString& name);
    
    GalaxDbusResult setColorValue(const QString& s, const SkyBoxColor& minimum_color,
				  const SkyBoxColor& maximum_color, const SkyBoxValuePalette& palette);

    GalaxDbusResult setMinMax(const QString& s, double vmin, double vmax);
    
  private:
    QMap<QString, SkyBox *> all_skies;
    UniverseWidget *universe;
  };

};

Q_DECLARE_METATYPE(GalaxExplorer::SkyBox_SingleValuePalette)
Q_DECLARE_METATYPE(GalaxExplorer::SkyBoxValuePalette)
Q_DECLARE_METATYPE(GalaxExplorer::SkyBoxColor)


#endif

