/*+
This is GalaxExplorer (./src/dbus/representation_glue.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __GALAXEXPLORER_REPRESENTATION_GLUE_HPP
#define __GALAXEXPLORER_REPRESENTATION_GLUE_HPP

#include <QtDBus>
#include "dbus_enum.hpp"

namespace GalaxExplorer
{
  enum ParameterResult
    {
      PARAM_SUCCESS = 0,
      PARAM_ERROR_NO_PROPERTY = 1,
      PARAM_ERROR_INVALID_TYPE = 2,
      PARAM_ERROR_INVALID_VALUE = 3
    };

  class RepresentationHandlerGlue : public QObject
  {
    Q_OBJECT
  public:
    RepresentationHandlerGlue(QObject *parent = 0);
    virtual ~RepresentationHandlerGlue();

    bool duplicateRepresentation(const QString& dst, const QString& src);
    QStringList getRepresentationList();
  };


  class RepresentationGlue: public QObject
  {
    Q_OBJECT
  public:
    RepresentationGlue(const QString& representation, QObject *parent = 0);
    virtual ~RepresentationGlue();

    QString getType();
    ParameterResult setParameter(const QString& paramName, const QDBusVariant& value);
    ParameterResult getParameter(const QString& paramName, QDBusVariant& value);
    
    QStringList getParameterList();
    QString getParameterType(const QString& param, ParameterResult& result);
  private:
    QString name;
  };

};

Q_DECLARE_METATYPE(GalaxExplorer::ParameterResult)

#endif
