/*+
This is GalaxExplorer (./src/dbus/data_handler.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef _GALAX_DATA_HANDLER_HPP
#define _GALAX_DATA_HANDLER_HPP

#include <QtCore>
#include <QTcpServer>
#include <vector>
#include <algorithm>
#include <QFrame>
#include "datastorage.hpp"
#include <QtDBus>
#include "array3d.hpp"

namespace Ui
{
  class IncomingDataFrame;
};

namespace GalaxExplorer
{

  typedef QVector<double> BoundaryVector;
  class BarePoint;
  
  class DataHandler: public QObject, protected QDBusContext
  {
    Q_OBJECT
  public:
    DataHandler(QObject *parent = 0);
    ~DataHandler();

    int createPointDataSet(const QString& s, int numPoints, int flags,
			   const QStringList& attributes,
			   double multiplier,
			   int& cookie);

    int createDataMesh(const QString& s, int numTriangles, int flags,
		       int& cookie);

    int createDataGrid(const QString& s, int Nx, int Ny, int Nz, 
		       int &cookie);

    int createMultiGadget(const QString& name,
			  const QString& pattern, int numFiles, double sx, double sy, double sz);

    int createMultiRamses(const QString& name,
			  const QString& base, int numFiles, double sx, double sy, double sz);

    int loadRamses(const QString& name, const QString& basepath, int id);

    bool dropData(const QString& s, bool force_destroy);
    
    int waitData(const QString& s);

    int setTrajectoryTime(const QString& name, float t);
 
    int loadGridFromFile(const QString& name, const QString& filename);
    
  private:

    void abortConnection();
    void createInfoWindow();
    void updateInfoWindow();
    void destroyInfoWindow();
    void buildDataset();
    void buildDataMesh();
    void buildDataGrid();

    void replyAllWaiter(int status);

    bool checkVolatileServer();

  private slots:
    void newDataConnection_points();
    void newDataConnection_mesh();
    void newDataConnection_grid();
    void newIncomingData_points();
    void newIncomingData_mesh();
    void newIncomingData_grid();
    void linkDisconnected();
    void forceDisconnection();
    void replyWaiterOk();
    void replyWaiterFailure();

  private:
    QTcpServer *server;
    QTcpSocket *currentSocket;
    int currentCookie;
    bool activateServer;

    QList<QDBusConnection> replyConnectionList;
    QList<QDBusMessage> replyMessageList;

    QString data_name;
    BarePoint *points;
    int *trilist;
    int numPoints;
    int pointsRead;
    std::vector<std::pair<QString, DataStorage *> > all_extra_attributes;
    int radius_id, normal_id;
    
    Array3D<double> datagrid;
    double boundaries[6];

    Ui::IncomingDataFrame *info_win_data;
    QFrame *info_win;
    QByteArray current_buffer;

    double currentMultiplier;
    bool created;

  };

};

#endif
