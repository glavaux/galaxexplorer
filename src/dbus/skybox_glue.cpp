/*+
This is GalaxExplorer (./src/dbus/skybox_glue.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifdef HEALPIX_PRESENT
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#endif
#include "skybox_glue.hpp"


using namespace GalaxExplorer;


SkyBoxGlue::SkyBoxGlue(UniverseWidget *w)
  : QObject(w), universe(w)
{
}


SkyBoxGlue::~SkyBoxGlue()
{
}

GalaxDbusResult SkyBoxGlue::add(const QString& name, const QString& fname)
{
  SkyBox *sky;

  if (all_skies.find(name) != all_skies.end())
    return GALAX_EXISTS;

#ifdef HEALPIX_PRESENT
  Healpix_Map<float> map;
  try
    {
      read_Healpix_map_from_fits(qPrintable(fname), map);
    }
  catch(const PlanckError& e)
    {
      return GALAX_INV_FILE;
    }

  sky = new SkyBox(map);
  sky->setObjectName(name);
  all_skies[name] = sky;
  
  universe->getSkyBoxList().push_back(sky);

  universe->refresh();

  return GALAX_SUCCESS;
#else
  return GALAX_INV_FILE;
#endif
}

QList<QString> SkyBoxGlue::getList()
{
  QList<QString> outlist;
  QList<SkyBox *>& actives = universe->getSkyBoxList();

  for (QList<SkyBox *>::iterator i = actives.begin();
       i != actives.end();
       ++i)
    outlist.push_back((*i)->objectName());
  
  return outlist;
}

GalaxDbusResult SkyBoxGlue::reorder(const QList<QString>& list_box)
{
  // First validate all elements of the list.
  QList<QString>::const_iterator i = list_box.begin();
  QMap<QString,int> newmap;
  int j = 0;
  QList<SkyBox *>& skylist = universe->getSkyBoxList();
  QVector<SkyBox *> tovec;
  QMap<QString,int>::iterator newmap_iter;

  while (i != list_box.end())
    {
      if (all_skies.find(*i) == all_skies.end())
	return GALAX_INVAL;

      if (newmap.find(*i) != newmap.end())
	return GALAX_INVAL;

      newmap[*i] = j++;
    }

  if (all_skies.size() != newmap.size())
    return GALAX_INVAL;

  // All elements exist and the reordering list has the
  // same size as the active elements.

  // Clear.
  skylist.clear();

  tovec.resize(j);
  newmap_iter = newmap.begin();
  while (newmap_iter != newmap.end())
    {
      tovec[newmap_iter.value()] = all_skies[newmap_iter.key()];
      ++newmap_iter;
    }
  
  skylist = tovec.toList();

  universe->refresh();

  return GALAX_SUCCESS;
}

GalaxDbusResult SkyBoxGlue::remove(const QString& name)
{
  QMap<QString,SkyBox *>::iterator i = all_skies.find(name);

  if (i == all_skies.end())
    return GALAX_INVAL;

  SkyBox* this_sky = *i;

  QList<SkyBox *>& skylist = universe->getSkyBoxList();
  QList<SkyBox *>::iterator j = skylist.begin();

  while (j != skylist.end())
    {
      if (*j == this_sky)
	j = skylist.erase(j);
      else
	++j;
    }

  all_skies.erase(i);

  universe->refresh();

  return GALAX_SUCCESS;
}
    



GalaxDbusResult SkyBoxGlue::setColorValue(const QString& s, const SkyBoxColor& min_value,
					  const SkyBoxColor& max_value,
					  const SkyBoxValuePalette& palette)
{
  SkyBox *skybox;
  GPalette p;
  GColor color;
  double previous_value = 0;

  if (all_skies.find(s) == all_skies.end())
    return GALAX_INVAL;

  skybox = all_skies[s];

  color.rgba[0] = min_value.r;
  color.rgba[1] = min_value.g;
  color.rgba[2] = min_value.b;
  color.rgba[3] = min_value.a;
  p.setMinimum(color);

  color.rgba[0] = max_value.r;
  color.rgba[1] = max_value.g;
  color.rgba[2] = max_value.b;
  color.rgba[3] = max_value.a;
  p.setMaximum(color);


  for (SkyBoxValuePalette::const_iterator i = palette.begin();
       i != palette.end();
       ++i)
    {
      if (i->value < previous_value)
	return GALAX_INVAL;

      color.rgba[0] = i->rgba.r;
      color.rgba[1] = i->rgba.g;
      color.rgba[2] = i->rgba.b;
      color.rgba[3] = i->rgba.a;
      p.addPoint(i->value, color);
    }

  if (!skybox->setPalette(p))
    {
      return GALAX_INVAL;
    }

  universe->refresh();

  return GALAX_SUCCESS;
}

GalaxDbusResult SkyBoxGlue::setMinMax(const QString& s, double vmin, double vmax)
{
  if (all_skies.find(s) == all_skies.end())
    return GALAX_INVAL;

  all_skies[s]->setMinMax(vmin, vmax);
  return GALAX_SUCCESS;
}
