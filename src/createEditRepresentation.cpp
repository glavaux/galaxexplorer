/*+
This is GalaxExplorer (./src/createEditRepresentation.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <cmath>
#include <QtGui>
#include "representationDialog.hpp"
#include "connectorDialog.hpp"
#include <iostream>
#include "inits.hpp"
#include "createEditRepresentation.hpp"

using namespace std;
using namespace GalaxExplorer;

CreateRepresentationDialog::CreateRepresentationDialog(QWidget *parent)
  : QDialog(parent)
{
  ui.setupUi(this);

  ui.representationList->addItems(g_allRepresentations.keys());
}


CreateRepresentationDialog::~CreateRepresentationDialog()
{
}

void CreateRepresentationDialog::run()
{
  bool ok = false;
  while (!ok)
    {
      // User canceled ?
      if (!exec())
	return;
      
      // No, let's create !
      
      // Sanity check: if the name already exists print an error restart the dialog
      QString repName = ui.representationName->text();
      if (repName == "" || g_allRepresentations[repName] != 0)
	{
	  QErrorMessage::qtHandler()->showMessage(tr("The given representation name is empty or already exists"));
	  continue;
	}

      QListWidgetItem *item = ui.representationList->currentItem();
      if (item == 0)
	{
	  QErrorMessage::qtHandler()->showMessage(tr("You have not selected a parent representation"));
	  continue;
	}

      QString baseName = item->text();

      DataRepresentation *representation = g_allRepresentations[baseName]->duplicate();
      representation->setName(repName);
      
      g_allRepresentations[repName] = representation;
      ok = true;
    }      
}

/* EDITION ............ */


EditRepresentationDialog::EditRepresentationDialog(QWidget *parent)
  : QDialog(parent)
{
  ui.setupUi(this);

  ui.representationList->addItems(g_allRepresentations.keys());
}

EditRepresentationDialog::~EditRepresentationDialog()
{
}

void EditRepresentationDialog::doModifyRepresentation()
{
  QListWidgetItem *item = ui.representationList->currentItem();
  QString repName = item->text();

  emit representationModified(repName);
}

void EditRepresentationDialog::run()
{
  if (!exec())
    return;

  QListWidgetItem *item = ui.representationList->currentItem();

  if (item == 0)
    return;

  QString repName = item->text();

  {
    RepresentationConfigDialog editDlg(g_allRepresentations[repName], this);
    
    connect(&editDlg, SIGNAL(representationModified()), this, SLOT(doModifyRepresentation()));
    editDlg.runConfig();
  }
}


