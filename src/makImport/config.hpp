/*+
This is GalaxExplorer (./src/makImport/config.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __MAK_CONFIG_HPP
#define __MAK_CONFIG_HPP

#include <stdint.h>
#include <exception>
#include <cstring>

namespace MAK
{

#define NUMDIMS 3
#define NUMCUBES 8

  /**
   * Base type to specity at what precision we
   * must achieve computations.
   */
  typedef double ComputePrecision;
  /**
   * Coordinate type (should be a 3-array).
   */
  typedef double Coordinates[NUMDIMS];
  
  /**
   * This function is used whenever one needs a general
   * conversion between mass and luminosity (or the opposite).
   * It should take a "mass" (or luminosity) in input, a unit is
   * given to convert this mass into solar units. The output should
   * be the "luminosity" (or mass), in solar units.
   */
  typedef double (*BiasFunction)(double mass, double unit);
  
  /**
   * Function to copy the coordinates "a" into "b".
   */
  inline void copyCoordinates(const Coordinates& a, Coordinates& b)
  {
    memcpy(b, a, sizeof(a));
  }

  /**
   * Base exception class for all exceptions handled by
   * this library.
   */
  class MAKException : public std::exception
  {
  public:
    MAKException(const char *mess = 0)
      : msg(mess) {}

    const char *getMessage() const { return msg; };
    virtual const char *what() const throw () { return msg; };

  private:
    const char *msg;
  };

  /**
   * Exception raised when an invalid argument has been
   * passed to a function of the library.
   */
  class InvalidArgumentException : public MAKException
  {
  public:
    InvalidArgumentException(const char *mess = 0)
      : MAKException(mess) {}
  };

  /**
   */
  class InvalidRangeException : public MAKException
  {
  public:
    InvalidRangeException(const char *mess = 0)
      : MAKException(mess) {}
  };
  
  /**
   */
  class NoSuchFileException : public MAKException
  {
  public:
    NoSuchFileException(const char *mess = 0)
      : MAKException(mess) {}
  };

  /**
   */
  class InvalidFileFormatException : public MAKException
  {
  public:
    InvalidFileFormatException(const char *mess = 0)
      : MAKException(mess) {}
  };
};

#endif
