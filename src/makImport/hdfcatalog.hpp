/*+
This is GalaxExplorer (./src/makImport/hdfcatalog.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
 #ifndef __HDF_CATALOG_HPP
#define __HDF_CATALOG_HPP

#include "config.hpp"
#include <inttypes.h>
#include <string>
#include <vector>
#include <utility>

namespace MAK
{  
  static const double noDM = -1;
  static const int noHType = 255;
  static const int32_t noGroup = -1;
  static const int32_t isGroup = -2;

  /**
   * This structure holds the definition
   * of an object of the HDF catalog of galaxies.
   * Some fields are not mandatory but they must be
   * marked accordingly.
   */
  typedef struct
  {
    /**
     * This entry holds the identifier of the group
     * to which belong this object. The recursive definition
     * of a group is not yet handled by MAK::HDFCatalog.
     * If the object does not belong to a group, this field must be set to
     * MAK::noGroup.
     */
    int32_t groupId;

    /**
     * This field holds the identifier of this object. It must be unique
     * among all single objects, or for all grouped objects. But a group object
     * may have the same identifier as a single object.
     */ 
    int32_t canonicalId;

    /**
     * This is the galactic latitude of the object, expressed in radian.
     */
    double GalLat;

    /**
     * This is the galactic longitude of the object, expressed in radian.
     */
    double GalLongit;
    
    /**
     * This is the redshift of the object. There is no restriction concerning
     * the rest frame in which it is expressed. However, expressing it in the CMB
     * rest frame is generally the custom.
     * 
     */
    double Redshift; 

    /**
     * This field holds an estimated distance of the object, which have been used
     * for the computation of the MAK::HDFCatalogEntry::Luminosity field.
     */
    double EstimatedDistance;

    /**
     * This field holds the estimated total {\bf absolute} luminosity of the 
     * given object. It is not specified in which band this luminosity has been
     * computed. 
     */
    double Luminosity;

    /**
     * This is the estimated mass (in solar luminosities) of the considered 
     * object. The mass may include some account for incompleteness.
     */
    double Mass;

    /**
     * This is the original mass of the object, which should not include
     * additional mass due to incompleteness effects in a remote part of the
     * catalog.
     */
    double OriginalMass;
    
    /**
     * This is the measured distance modulus for the object. To obtain the 
     * distance \f$d\f$ in MegaParsec, from the distance modulus \f$\mu\f$,
     * use the following formula: \f[
     *  d = 10^{\mu/5 - 3} \times \frac{H}{100\mathrm{ km/s/Mpc}}
     * \f]
     * where \f$H\f$ is the hubble constant.
     */
    double DistanceModulus;

    /**
     * This is the error on the distance modulus.
     * It can be converted into an error on the distance using the
     * following formula: \f[
     *   \sigma_d = d \times \mathrm{errDistanceModulus} \times \frac{5}{\mathrm{log}(10)}
     * \f]
     */
    double errDistanceModulus;

    /**
     * This is the estimated radius, in km/s, of the object. This gives
     * an indication to MAK::HDFCatalog on how to build the MAK reconstruction
     * mesh.
     */
    double radius;    

    /**
     * This is the galaxy hubble type. Not used.
     */
    int HubbleType;

    /**
     * This field is set to 1, if the object is fake and has been introduced
     * to fill up the Zone Of Avoidance. Otherwise, it is 0.
     */
    int ZOA_object;

    /**
     * This field is set to 1, if the object is fake.
     */
    int AdditionalTracer;

    /**
     * This field holds the boost ratio which have been used to correct
     * the total luminosity of this object for unobserved galaxies.
     */
    double BoostRatio;

    /**
     * This is the name of the object.
     */
    char Name[21];

    /**
     * This is the UGC identifier.
     */
    uint32_t UGC_id;
  } HDFCatalogEntry;

  /**
   * This vector-type samples a curve describing incompleteness.
   */
  typedef std::vector< std::pair<double,double> > IncompletenessCurveType;

  /**
   * This is a structure to have a raw access to data stored in
   * a HDF catalog. This is the advised approach when accounting for
   * observational issues before building the final corrected catalog.
   */
  typedef struct
  {
    /**
     * \f$\Omega_\mathrm{m}\f$ assumed when building this catalog. It may
     * be 0, if no incompleteness correction has yet been applied.
     */
    double Omega_m;
    /**
     * This is the identifier for dummy particles that the MAK mesh builder
     * may have to introduce prior to MAK reconstruction.
     */
    uint32_t dummyId;
    /**
     * This field holds all entries describing a single object (by opposition
     * to a group object as in MAK::FullHDFCatalog::groupEntries).
     */
    HDFCatalogEntry* entries;
    /**
     * The number of elements in MAK::FullHDFCatalog::entries.
     */
    uint32_t numEntries;
    /**
     * This field holds all entries describing a group object.
     * @see MAK::FullHDFCatalog::entries.
     */
    HDFCatalogEntry* groupEntries;
    /**
     * The number of elements in MAK::FullHDFCatalog::groupEntries.
     */
    uint32_t numGroupEntries;
    /**
     * This is a string to describe the finger-of-god compression
     * algorithm that has been used.
     */
    std::string fogParameters;
    /**
     * This is the incompleteness correction that should be used
     * if available.
     */
    IncompletenessCurveType incompletenessCurve;
    /**
     * This vector of string holds all extra comments that a user
     * may think of.
     */
    std::vector<std::string> comments;
  } FullHDFCatalog;

  /**
   */
  void readGenHDFCatalog(const char *fname, FullHDFCatalog& catalog)
    throw (NoSuchFileException);

  void writeGenHDFCatalog(const char *fname, const FullHDFCatalog& catalog);

  void addCommentHDFCatalog(const char *fname, const std::string& comment) throw (NoSuchFileException,InvalidFileFormatException);
  
  void readAllCommentsInHDFCat(const char *fname, std::vector<std::string>& comments);

};

#endif
