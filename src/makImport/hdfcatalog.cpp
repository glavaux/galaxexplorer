/*+
This is GalaxExplorer (./src/makImport/hdfcatalog.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <cstdlib>
#include <assert.h>
#include <time.h>
#include <string>
#include <unistd.h>
#include <cmath>
#include <iostream>
#include <iomanip>
#include "hdfcatalog.hpp"
#include "hdf5.h"

using namespace MAK;
using namespace std;

static hid_t createNameType()
{
  hid_t nameid = H5Tcopy(H5T_C_S1);

  H5Tset_size(nameid, 21);
  H5Tset_strpad(nameid, H5T_STR_NULLTERM);
  
  return nameid;
}

static hid_t createGroupHDFType(hid_t NameType)
{
  hid_t groupid = H5Tcreate(H5T_COMPOUND, sizeof(HDFCatalogEntry));

  H5Tinsert(groupid, "GroupID", offsetof(HDFCatalogEntry, groupId), H5T_NATIVE_INT );
  H5Tinsert(groupid, "ID", offsetof(HDFCatalogEntry, canonicalId), H5T_NATIVE_INT);
  H5Tinsert(groupid, "GalacticLatitude", offsetof(HDFCatalogEntry, GalLat), H5T_NATIVE_DOUBLE);
  H5Tinsert(groupid, "GalacticLongitude", offsetof(HDFCatalogEntry, GalLongit), H5T_NATIVE_DOUBLE); 
  H5Tinsert(groupid, "Redshift", offsetof(HDFCatalogEntry, Redshift), H5T_NATIVE_DOUBLE);
  H5Tinsert(groupid, "Luminosity", offsetof(HDFCatalogEntry, Luminosity), H5T_NATIVE_DOUBLE);
  H5Tinsert(groupid, "Mass", offsetof(HDFCatalogEntry, Mass), H5T_NATIVE_DOUBLE);
  H5Tinsert(groupid, "OriginalMass", offsetof(HDFCatalogEntry, OriginalMass), H5T_NATIVE_DOUBLE);
  H5Tinsert(groupid, "DistanceModulus", offsetof(HDFCatalogEntry, DistanceModulus), H5T_NATIVE_DOUBLE);
  H5Tinsert(groupid, "errDistanceModulus", offsetof(HDFCatalogEntry, errDistanceModulus), H5T_NATIVE_DOUBLE);
  H5Tinsert(groupid, "IsZOA", offsetof(HDFCatalogEntry, ZOA_object), H5T_NATIVE_INT);
  H5Tinsert(groupid, "IsVirtualTracer", offsetof(HDFCatalogEntry, AdditionalTracer), H5T_NATIVE_INT);
  H5Tinsert(groupid, "Radius", offsetof(HDFCatalogEntry, radius), H5T_NATIVE_DOUBLE);
  H5Tinsert(groupid, "HubbleType", offsetof(HDFCatalogEntry, HubbleType), H5T_NATIVE_INT);
  H5Tinsert(groupid, "Boost", offsetof(HDFCatalogEntry, BoostRatio), H5T_NATIVE_DOUBLE);
  H5Tinsert(groupid, "Name", offsetof(HDFCatalogEntry, Name), NameType);
  H5Tinsert(groupid, "EstimatedDistance", offsetof(HDFCatalogEntry, EstimatedDistance), H5T_NATIVE_DOUBLE);
  H5Tinsert(groupid, "UGC", offsetof(HDFCatalogEntry, UGC_id), H5T_NATIVE_INT);

  return groupid;
}

static void writeSimpleDouble(hid_t file_id, const char *objname, double value)
{
  hsize_t dim[] = { 1 };
  hid_t space = H5Screate_simple(1, dim, NULL);
  hid_t dataset_id = H5Dcreate(file_id, objname, H5T_NATIVE_DOUBLE, space, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &value);
  H5Dclose(dataset_id);
  H5Sclose(space);
}

static double readSimpleDouble(hid_t file_id, const char *objname)
{
  hid_t dataset_id = H5Dopen(file_id, objname, H5P_DEFAULT);
  double value;

  H5Dread(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &value);
  H5Dclose(dataset_id);

  return value;
}


static void writeSimpleInt(hid_t file_id, const char *objname, int value)
{
  hsize_t dim[] = { 1 };
  hid_t space = H5Screate_simple(1, dim, NULL);
  hid_t dataset_id = H5Dcreate(file_id, objname, H5T_NATIVE_INT, space, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  if (dataset_id > 0)
    H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &value);  
  H5Dclose(dataset_id);
  H5Sclose(space);
}

static int readSimpleInt(hid_t file_id, const char *objname)
{
  int value;
  hid_t dataset_id = H5Dopen(file_id, objname, H5P_DEFAULT);

  if (dataset_id > 0)
    H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &value);  
  H5Dclose(dataset_id);

  return value;
}

static bool readSimpleDouble2DArray(hid_t file_id, const char *objname,
				    double *xis, double *yis)
{
  hid_t dataset_id = H5Dopen(file_id, objname, H5P_DEFAULT);

  if (dataset_id < 0)
    return false;

  hid_t spaceFile_id = H5Dget_space(dataset_id);
  if (spaceFile_id < 0)
    {
      H5Dclose(dataset_id);
      return false;
    }

  if (H5Sget_simple_extent_ndims(spaceFile_id) != 2)
    {
      H5Sclose(spaceFile_id);
      H5Dclose(dataset_id);
      return false;
    }

  hsize_t dims[2], maxdims[2];
  //  if (H5Sget_simple_extent_dims(fileSpace_id, dims, maxdims) < 0)
    {
      H5Sclose(spaceFile_id);
      H5Dclose(dataset_id);
      return false;
    }

  //  hid_t mySpace_id = H5Screate_simple(2, const hsize_t * dims, const hsize_t * maxdims )

  //  herr_t H5Dread(dataset_id, H5T_NATIVE_DOUBLE, mySpaceID, hid_t file_space_id,H5P_DEFAULT, memData)

  //  herr_t H5Sselect_hyperslab(hid_t space_id, H5S_seloper_t op, const hsize_t *start, const hsize_t *stride, const hsize_t *count, const hsize_t *block  )
  
  //  hid_t 
  
}

static bool readSimpleString(hid_t file_id, const char *objname, string& s)
{
  // Dangerous !!! ///
  char buffer[1024];
  hid_t dataset_id = H5Dopen(file_id, objname, H5P_DEFAULT);
  hid_t type_id;

  if (dataset_id < 0)
    return false;

  if ((type_id = H5Dget_type(dataset_id)) < 0)
    goto out;

  if (H5Dread(dataset_id, type_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer) < 0)
    goto out;
  
  s = string(buffer);
  
  if (H5Dclose(dataset_id))
    goto out;
  if (H5Tclose(type_id))
    return false;
  
  return true;
 out:
  H5Dclose(dataset_id);
  H5Tclose(type_id);

  return false;
}

static bool writeSimpleString(hid_t file_id, const char *objname, const string& s)
{
  const char *buf = s.c_str();
  hid_t   did=-1;
  hid_t   sid=-1;
  hid_t   tid;
  size_t  size;
  
  /* create a string data type */
  if ( (tid = H5Tcopy( H5T_C_S1 )) < 0 )
    goto out;
  
  size = s.length() + 1; /* extra null term */
  
  if ( H5Tset_size(tid,size) < 0 )
    goto out;
  
  if ( H5Tset_strpad(tid,H5T_STR_NULLTERM ) < 0 )
    goto out;
  
  /* Create the data space for the dataset. */
  if ( (sid = H5Screate( H5S_SCALAR )) < 0 )
    goto out;

  /* Create the dataset. */
  if ( (did = H5Dcreate(file_id, objname, tid, sid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)) < 0 )
    goto out;
  
  /* Write the dataset only if there is data to write */
  
  if (buf)
    {
      if ( H5Dwrite(did, tid, H5S_ALL, H5S_ALL, H5P_DEFAULT, buf) < 0 )
	goto out;
    }
  
  /* close*/
  if ( H5Dclose(did) < 0 )
    return false;
  if ( H5Sclose(sid) < 0 )
    return false;
  if ( H5Tclose(tid) < 0 )
    goto out;
  
  return true;
  
 out:
  H5Dclose(did);
  H5Tclose(tid);
  H5Sclose(sid);
  return false;
}

void MAK::readGenHDFCatalog(const char *fname, FullHDFCatalog& catalog)
  throw (NoSuchFileException)
{
  /* Save old error handler */
  H5E_auto_t old_func;
  void *old_client_data;
  H5Eget_auto(H5E_DEFAULT, &old_func, &old_client_data);
  
  /* Turn off error handling */
  H5Eset_auto(H5E_DEFAULT, NULL, NULL);
  
  hid_t fileid = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
  
  if (fileid <= 0)
    {
      H5Eset_auto(H5E_DEFAULT, old_func, old_client_data);
      throw NoSuchFileException(fname);
    }

  hid_t nameType = createNameType();
  hid_t groupType = createGroupHDFType(nameType);
  hid_t datasetid;
  hid_t space_id;  
  int status;
  double& Omega_m = catalog.Omega_m;
  uint32_t& dummyId = catalog.dummyId;
  HDFCatalogEntry*& entries = catalog.entries;
  uint32_t& numEntries = catalog.numEntries;
  HDFCatalogEntry*& groupEntries = catalog.groupEntries;
  uint32_t& numGroupEntries = catalog.numGroupEntries;

  Omega_m = readSimpleDouble(fileid, "/Omega");
  dummyId = readSimpleInt(fileid, "/DummyID");

  string dateS;
  if (readSimpleString(fileid, "/Date", dateS))
    {
      cerr << "HDF Catalog date: " << dateS << endl;
    }

  if (!readSimpleString(fileid, "/FOGParams", catalog.fogParameters))
    {
      catalog.fogParameters = "";
    }
  
  // ========================================================================
  // ATOM OBJECTS
  // ============
  datasetid = H5Dopen(fileid, "/AtomObjects", H5P_DEFAULT);
  space_id =  H5Dget_space(datasetid);
  numEntries = H5Sget_simple_extent_npoints(space_id);

  entries = new HDFCatalogEntry[numEntries];

  status = H5Dread(datasetid, groupType, H5S_ALL, H5S_ALL, H5P_DEFAULT, entries);
  if (status < 0)
    {
      H5Eprint(H5E_DEFAULT, stderr);
      cerr << "Error while reading HDF catalog (AtomObjects). Aborting..." << endl;
      abort();
    }
  H5Dclose(datasetid);
  
  // ========================================================================
  // GROUP OBJECTS
  // =============
  datasetid = H5Dopen(fileid, "/GroupObjects", H5P_DEFAULT);
  if (datasetid >= 0) {
  space_id =  H5Dget_space(datasetid);
  numGroupEntries = H5Sget_simple_extent_npoints(space_id);

  groupEntries = new HDFCatalogEntry[numGroupEntries];

  status = H5Dread(datasetid, groupType, H5S_ALL, H5S_ALL, H5P_DEFAULT, groupEntries);
  if (status < 0)
    {
      H5Eprint(H5E_DEFAULT, stderr);
      cerr << "Error while reading HDF catalog (GroupObjects). Aborting..." << endl;
      abort();
    }

  H5Dclose(datasetid);
  } else {
    numGroupEntries = 0;
    groupEntries = new HDFCatalogEntry[1];
  }

  H5Tclose(nameType);
  H5Tclose(groupType);

  try
    {

      hid_t incompid = H5Dopen(fileid, "/Incompleteness", H5P_DEFAULT);
      if (incompid > 0)
	{
	  hid_t filespace = H5Dget_space(incompid);
	  hsize_t dims[2], maxdims[2];
	  
	  try
	    {
	      
	      if (H5Sget_simple_extent_ndims(filespace) != 2)
		throw InvalidFileFormatException("Incompleteness space not valid");
	      if (H5Sget_simple_extent_dims(filespace, dims, maxdims) < 0)
		throw InvalidFileFormatException("Cannot retrieve incompleteness space");
	      if (dims[0] != 2)
		throw InvalidFileFormatException("Invalid dimensions of incompleteness space");
	    }
	  catch (const exception& e)
	    {
	      H5Sclose(filespace);
	      H5Dclose(incompid);
	      throw;
	    }
	  
	  hid_t memspace = H5Screate_simple(1, &dims[1],NULL);
	  
	  hsize_t offsetMem[] = { 0, 0};
	  hsize_t countMem[] = { 1, dims[1] };
	  
	  int status;
	  
	  hid_t filespace2 = H5Scopy(filespace);
	  
	  status = H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offsetMem, NULL,
				       countMem, NULL);
	  offsetMem[0] = 1;
	  status = H5Sselect_hyperslab(filespace2, H5S_SELECT_SET, offsetMem, NULL,
				       countMem, NULL);
	  
	  double *pair1;
	  double *pair2;
	  try
	    {
	      pair1 = new double[dims[1]];
	      pair2 = new double[dims[1]];
	      
	      H5Dread(incompid, H5T_NATIVE_DOUBLE, memspace, filespace, H5P_DEFAULT, pair1);
	      H5Dread(incompid, H5T_NATIVE_DOUBLE, memspace, filespace2, H5P_DEFAULT, pair2);
	    }
	  catch (const exception& e)
	    {
	      H5Sclose(memspace);
	      H5Sclose(filespace);
	      H5Sclose(filespace2);
	      H5Dclose(incompid);
	      throw;
	    }
	  
	  H5Sclose(memspace);
	  H5Sclose(filespace);
	  H5Sclose(filespace2);
	  
	  catalog.incompletenessCurve.resize(dims[1]);
	  for (uint32_t i = 0; i < dims[1]; i++)
	    {
	      catalog.incompletenessCurve[i].first = pair1[i];
	      catalog.incompletenessCurve[i].second = pair2[i];
	    }
	  
	  delete[] pair1;
	  delete[] pair2;
	  H5Dclose(incompid);
	}
    }
  catch (const exception& e)
    {
      H5Fclose(fileid);
      H5Eset_auto(H5E_DEFAULT, old_func, old_client_data);
      throw;
    }
  H5Fclose(fileid);
  H5Eset_auto(H5E_DEFAULT, old_func, old_client_data);
}

void MAK::addCommentHDFCatalog(const char *fname, const string& comment) throw (NoSuchFileException,InvalidFileFormatException)
{
  hid_t dcom = 0;
  hid_t space_id = 0;
  hid_t sType = 0;
  hid_t memSpace = 0;
  hid_t fileSpace = 0;

  try
    {
      hid_t fileid = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);
  
      if (fileid < 0)
	throw NoSuchFileException(fname);
      
      std::vector<string> commentArray;
      
      if (comment.size() >= 256)
	{
	  string commentSplit = comment;
	  
	  while (commentSplit.size() >= 256)
	    {
	      commentArray.push_back(commentSplit.substr(0, 255));
	      commentSplit = commentSplit.substr(256);
	    }
	}
      else
	commentArray.push_back(comment);
      
      hsize_t olddim;
      dcom = H5Dopen(fileid, "/CatalogComments", H5P_DEFAULT);
      
      if (dcom >= 0)
	{
	  hsize_t dims[1];
	  hsize_t maxdims[1];
	  
	  sType = H5Dget_type(dcom);
	  space_id = H5Dget_space(dcom);
	  if (H5Sget_simple_extent_ndims(space_id) != 1)
	    throw InvalidFileFormatException("Comment space not valid");
	  if (H5Sget_simple_extent_dims(space_id, dims, maxdims) < 0)
	    throw InvalidFileFormatException("Cannot retrieve comment space");
	  
	  olddim = dims[0];
	  dims[0] += commentArray.size();
	  if (H5Dextend(dcom, dims) < 0)
	    throw InvalidFileFormatException("Cannot extend data space");	 

	  H5Sclose(space_id);
	}
      else
	{
	  hsize_t dims[1] = { commentArray.size() };
	  hsize_t maxdims[1] = { H5S_UNLIMITED };
	  
	  if ((space_id = H5Screate_simple(1, dims, maxdims)) < 0)
	    throw InvalidFileFormatException("Cannot create dataspace for comments");
	  
	  sType = H5Tcopy(H5T_C_S1);
	  H5Tset_size(sType, 256);
	  H5Tset_strpad(sType, H5T_STR_NULLTERM );
	  
	  dcom = H5Dcreate(fileid, "/CatalogComments", sType, space_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	  olddim = 0;
	  
	  H5Sclose(space_id);
	}
      
      fileSpace = H5Dget_space(dcom);
      
      hsize_t offset[1];
      hsize_t size[1];
      hsize_t dims[1] = {  };
      hsize_t maxdims[1] = { H5S_UNLIMITED };
     
      offset[0] = olddim;
      size[0] = 1;
      memSpace = H5Screate_simple(1, dims, maxdims);
      
      H5Sselect_hyperslab (fileSpace, H5S_SELECT_SET, offset, NULL, size, NULL);
      
      if ( H5Dwrite(dcom, sType, memSpace, fileSpace,H5P_DEFAULT, comment.c_str()) < 0 )
	throw InvalidFileFormatException("Cannot write");

      H5Sclose(fileSpace);
      H5Sclose(memSpace);
      H5Dclose(dcom);
      H5Tclose(sType);
      H5Fclose(fileid);
      
    } catch (const MAKException& e) {
      if (dcom)
	H5Dclose(dcom);
      if (space_id)
	H5Sclose(space_id);
      if (memSpace)
	H5Sclose(memSpace);
      if (fileSpace)
	H5Sclose(fileSpace);
      if (sType)
	H5Tclose(sType);
      throw;
    }
}

void readAllCommentsInHDFCat(const char *fname, vector<std::string>& comments);

void MAK::writeGenHDFCatalog(const char *fname, const FullHDFCatalog& catalog)

{
  hid_t fileid =  H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  hid_t nameType = createNameType();
  hid_t groupType = createGroupHDFType(nameType);
  hid_t datasetid;
  hid_t space_id;  
  int status;
  
  double Omega_m = catalog.Omega_m;
  uint32_t dummyId = catalog.dummyId;
  const HDFCatalogEntry* entries = catalog.entries;
  uint32_t numEntries = catalog.numEntries;
  const HDFCatalogEntry* groupEntries = catalog.groupEntries;
  uint32_t numGroupEntries = catalog.numGroupEntries;

  hsize_t dim_objects[] = { numEntries };
  hsize_t dim_groups[] = { numGroupEntries };

  time_t t = time(0);
  string versionId;
  char hname[256];

  gethostname(hname, 256);
  versionId += hname;
  versionId += " - "; 
  versionId += (ctime(&t));
  versionId.erase(versionId.end()-1);

  
  writeSimpleString(fileid, "/FOGParams", catalog.fogParameters.c_str());
  writeSimpleString(fileid, "/Date", versionId.c_str());
  writeSimpleDouble(fileid, "/Omega", Omega_m);
  writeSimpleInt(fileid, "/DummyID", dummyId);

  // =======================================================================
  // ATOM OBJECTS
  // ============
  space_id = H5Screate_simple(1, dim_objects, NULL);
  datasetid = H5Dcreate(fileid, "/AtomObjects", groupType, space_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  status = H5Dwrite(datasetid, groupType, H5S_ALL, H5S_ALL, H5P_DEFAULT, entries);
  if (status < 0)
    {
      cerr << "Error while writing HDF catalog (AtomObjects). Aborting..." << endl;
      abort();
    }
  H5Dclose(datasetid);
  H5Sclose(space_id);

  // =======================================================================
  // GROUP OBJECTS
  // =============
  if (numGroupEntries != 0) {
    space_id = H5Screate_simple(1, dim_groups, NULL);
    datasetid = H5Dcreate(fileid, "/GroupObjects", groupType, space_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    status = H5Dwrite(datasetid, groupType, H5S_ALL, H5S_ALL, H5P_DEFAULT, groupEntries);
    if (status < 0)
      {
        cerr << "Error while writing HDF catalog (GroupObjects). Aborting..." << endl;
        abort();
      }

    H5Dclose(datasetid);
    H5Sclose(space_id);
  }  

  H5Tclose(groupType);

  // =====================
  // INCOMPLETENESS TYPE
  // ===================

  if (catalog.incompletenessCurve.size() != 0)
    {
      hsize_t dim_curve[] = { 2, catalog.incompletenessCurve.size() };
      hid_t space_id = H5Screate_simple(2, dim_curve, NULL);      
      hid_t incompid = H5Dcreate(fileid, "/Incompleteness", H5T_NATIVE_DOUBLE, 
				 space_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
      
      hid_t dataspace = H5Screate_simple(1,&dim_curve[1],NULL);
      hid_t filespace = H5Dget_space(incompid);
      hsize_t offsetMem[] = { 0, 0};
      hsize_t countMem[] = { 1, dim_curve[1] };

      int status = H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offsetMem, NULL,
				       countMem, NULL);

      double *pair1 = new double[dim_curve[1]];
      double *p = pair1;
      for (IncompletenessCurveType::const_iterator i = catalog.incompletenessCurve.begin();
	   i != catalog.incompletenessCurve.end();
	   i++)
	{
	  *(p++) = (*i).first;
	}

      H5Dwrite(incompid, H5T_NATIVE_DOUBLE, dataspace, filespace, H5P_DEFAULT, pair1);

      H5Sclose(filespace);
      filespace = H5Dget_space(incompid);
      
      p = pair1;
      for (IncompletenessCurveType::const_iterator i = catalog.incompletenessCurve.begin();
	   i != catalog.incompletenessCurve.end();
	   i++)
	{
	  *(p++) = (*i).second;
	}
      
      offsetMem[0] = 1;      
      status = H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offsetMem, NULL,
				   countMem, NULL);
     
      H5Dwrite(incompid, H5T_NATIVE_DOUBLE, dataspace, filespace, H5P_DEFAULT, pair1);

      delete[] pair1;

      H5Sclose(space_id);
      H5Sclose(filespace);
      H5Sclose(dataspace);
      H5Dclose(incompid);
    }
  H5Fclose(fileid);
}

