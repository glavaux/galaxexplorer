/*+
This is GalaxExplorer (./src/representations/galaxyRepresentation.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __GALAXY_REPRESENTATION_HPP
#define __GALAXY_REPRESENTATION_HPP

#include <QtOpenGL>
#include "dataRepresentation.hpp"
#include "glwidget.hpp"
#include "GLSLProgramObject.h"

namespace GalaxExplorer
{

  class GalaxyRepresentation: public DataSetCapableRepresentation
  {
    Q_OBJECT
    
    Q_PROPERTY(double galaxySize READ getGalaxySize WRITE setGalaxySize)  
    Q_PROPERTY(bool billboard READ billboardMode WRITE setBillboardMode)
    Q_PROPERTY(QString galaxyTexture READ getGalaxyTexture WRITE setGalaxyTexture)
    Q_PROPERTY(double alphaPower READ getAlphaPower WRITE setAlphaPower)
    Q_PROPERTY(bool autoTransparency READ getAutoTransparency WRITE setAutoTransparency)
  public:
    GalaxyRepresentation();
    virtual ~GalaxyRepresentation();
  
    double getGalaxySize() const { return galaxySize; }
    void setGalaxySize(double s) { galaxySize = s; }

    double getAlphaPower() const { return alphaPower; }
    void setAlphaPower(double p) { alphaPower = p; }

    bool getAutoTransparency() const { return autoTransparency; }
    void setAutoTransparency(bool a) { autoTransparency = a; }

    // Billboard is applied at realDrawData time
    // Random orientation must be disabled
    bool billboardMode() const { return billboard; }
    void setBillboardMode(bool b) { billboard = b; }

    QString getGalaxyTexture() const { return galaxyTextureFilename; }
    void setGalaxyTexture(QString& texfile) { 
      std::cout << "Changing texture" << endl;
      QImage img_tmp(QString("textures:") + texfile);

      if (img_tmp.isNull()) {
        std::cout << "Could not load texture file ";
	return;
      }

      img = img_tmp;

      galaxyTextureFilename = texfile;
      need_rebind = true;
    }

    virtual void loadState(const QMap<QString,QVariant>& d);
    virtual void saveState(QMap<QString,QVariant>& d) const;

    virtual void prepareDrawData(UniverseWidget& universe,
				 GLConnector& connector,
				 DataSetConstIterator begin,
				 DataSetConstIterator end);
    virtual void realDrawData(UniverseWidget& universe,
			      GLConnector& connector);

    virtual DataRepresentation *duplicate() const;

    virtual QString getRepresentationName() const;

    void delayedInit(UniverseWidget& universe);
  protected:
    GLuint texture;
    bool has_init, need_rebind;
    GLSLProgramObject myShader, myBillboardShader;
    QImage img;
    QString galaxyTextureFilename;
    float galaxySize;
    float alphaPower;
    bool billboard, autoTransparency;
  };

};

#endif
