/*+
This is GalaxExplorer (./src/representations/vectorRepresentation.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __VECTOR_REPRESENTATION_HPP
#define __VECTOR_REPRESENTATION_HPP

#include <QObject>
#include <QColor>
#include "representations/dataRepresentation.hpp"

namespace GalaxExplorer
{
  
  class VectorRepresentation: public DataSetCapableRepresentation
  {
    Q_OBJECT

    Q_PROPERTY(double vectorScaling READ getVectorScaling WRITE setVectorScaling)
    Q_PROPERTY(QColor lineColor READ getLineColor WRITE setLineColor)
    Q_PROPERTY(QColor headColor READ getHeadColor WRITE setHeadColor)
    Q_PROPERTY(double headSize READ getHeadSize WRITE setHeadSize)
    Q_PROPERTY(double headRadius READ getHeadRadius WRITE setHeadRadius)
  public:
    VectorRepresentation();
    virtual ~VectorRepresentation();

    virtual void saveState(QDataStream& d) const;
    virtual void loadState(QDataStream& d);
    
    virtual void drawData(UniverseWidget& universe,
			  DataSetConstIterator begin,
			  DataSetConstIterator end);
    

    void setVectorBoost(double fraction) { fractionVeloc = fraction; }
    QString getRepresentationName() const;

    virtual DataRepresentation *duplicate() const;

    double getVectorScaling() const { return fractionVeloc; }
    void setVectorScaling(double s) { fractionVeloc = s; }

    double getHeadSize() const { return headSize; }
    void setHeadSize(double s) { headSize = s; }

    double getHeadRadius() const { return headRadius; }
    void setHeadRadius(double s) { headRadius = s; }

    QColor getLineColor() const { return myColor; }
    void setLineColor(const QColor& l) { myColor = l; }

    QColor getHeadColor() const { return coneColor; }
    void setHeadColor(const QColor& c)  { coneColor = c; }

  protected:
    QColor myColor;
    QColor coneColor;
    double fractionVeloc;
    double headSize;
    double headRadius;
  };
};

#endif
