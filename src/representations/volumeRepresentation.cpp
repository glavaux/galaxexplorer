#include "includeGL.hpp"
#include <QObject>
#include <QColor>
#include <vector>
#include <utility>
#include <QMap>
#include "points.hpp"
#include "datasets.hpp"
#include "triplet.hpp"
#include <iostream>
#include "GLSLProgramObject.h"
#include "object_helper.hpp"
#include "volumeRepresentation.hpp"
#include "glerror.hpp"
#include "renderers/render_utils.hpp"

using namespace GalaxExplorer;

VolumeRepresentation::VolumeRepresentation()
  : has_init(false)
{
}

VolumeRepresentation::~VolumeRepresentation()
{
}

        
void VolumeRepresentation::loadState(const QMap<QString,QVariant>& d)
{
}

void VolumeRepresentation::saveState(QMap<QString,QVariant>& d) const
{
}

void VolumeRepresentation::prepareDrawGrid(UniverseWidget& universe,
                                           GLConnector& c)
{
  GLRenderer *r = universe.getRenderer();


  if (!r->usingShaders())
    return;

  delayedInit(universe);

  if (!c.prepared())
    {
      r->assembleProgramShader(c, shaderRaytrace, 0);
    }

  c.setPrepared(true);

  int vboId;
  
  if (c.numberOfVBO() == 0)
    {
      VBO_Descriptor desc;

      desc.tex3d = false;
      desc.numAttribs = 1;
      desc.attribIndex = new GLuint[1];
      desc.attribSizes = new int[1];
      desc.attribIndex[0] = 0;
      desc.attribSizes[0] = 3;
      vboId = c.allocateVBO(desc);
    }
  else
    vboId = 0;

  VBO_Object& vbo = c.getVBO(vboId);
  // We need to draw cube faces
  
  CubeDrawer drawer;
  
  drawer.setColor(0, 1., 0.2);
  drawer.set3DAttribute(0);
  drawer.draw(vbo, 0, 0, 0, 10.);
  
  // Upload data into the texture cube
  glGenTextures(1, &cubeTex);
  glGenTextures(1, &transferTex);

  DataGrid *data = qobject_cast<GridConnector *>(c.getConnector())->getDataGrid();
  int Nx = data->getData().Nx();
  int Ny = data->getData().Ny();
  int Nz = data->getData().Nz();
  const double *boundaries = data->getBoundaries();

  assert(Nx==Ny && Nx==Nz);
  
  Array3D<float> *texdata = data->getData().astype<float>();

  glBindTexture(GL_TEXTURE_3D, cubeTex);
    glTexImage3D(GL_TEXTURE_3D, 0, GL_R32F, Nx, Nx, Nx, 0, GL_RED, GL_FLOAT, &(*texdata)[0]);
  CHECK_GL_ERRORS;
    glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glBindTexture(GL_TEXTURE_3D, 0);
  
  delete texdata;
  
  unsigned char transferData[4*4] = {
    0, 0, 0, 255,
    255, 0, 0, 100,
    0, 255, 0, 150,
    0, 0, 255, 200
  };
  
  glBindTexture(GL_TEXTURE_1D, transferTex);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA8, 4, 0, GL_RGB, GL_UNSIGNED_BYTE, transferData);
  CHECK_GL_ERRORS;
    glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glBindTexture(GL_TEXTURE_1D, 0);
}

void VolumeRepresentation::realDrawGrid(UniverseWidget& universe,
			      GLConnector& c)
{
  GLRenderer *r = universe.getRenderer();

  if (!r->usingShaders()) 
    return;
    

  RenderTarget *tgt = r->createTempRenderTarget();
  assert(tgt != 0);
  RenderTargetTextured *ttgt = dynamic_cast<RenderTargetTextured *>(tgt);

  assert(ttgt != 0);

  ttgt->activate();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  shaderOutPosition.bind();

  c.getVBO(0).
    updateAttributeLocation(0, shaderOutPosition.getAttribLocation("cube_coord"));

  glFrontFace(GL_CW);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glCullFace(GL_FRONT);
 
  c.getVBO(0).draw(GL_TRIANGLE_FAN);

  ttgt->deactivate();
  
  c.getVBO(0).
    updateAttributeLocation(0,
                            r->getShader(c, 0).
                               getAttribLocation("cube_coord"));

  r->resetGLState();
  r->useShaderInConnector(c, 0);

  GLSLProgramObject& progRay = r->getShader(c, 0);
  DataGrid *data = qobject_cast<GridConnector *>(c.getConnector())->getDataGrid();
  int Nx = data->getData().Nx();
  int nSteps = 2*Nx;  
  Point3d  plight = r->getLightSource();
  GLfloat light_pos[3] = {plight.x, plight.y, plight.z };
  GLfloat vol_step[3] = {1.0/Nx, 1.0/Nx, 1.0/Nx };
  
  progRay.bindTextureRECT("exitingCoord", ttgt->outtex());
  progRay.bindTexture3D("cube", cubeTex);
  progRay.bindTexture(GL_TEXTURE_1D, "transferFunction", transferTex);
  progRay.setIntUniform("Nsteps", &nSteps, 1);
  progRay.setUniform("volstruct.light_pos", light_pos, 3);
  progRay.setUniform("volstruct.volume_step", vol_step, 3);

  CHECK_GL_ERRORS;

  glDepthFunc(GL_LESS);
  glCullFace(GL_BACK);

  CHECK_GL_ERRORS;

  c.getVBO(0).draw(GL_TRIANGLE_FAN);

  CHECK_GL_ERRORS;
  
  glDisable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
  glFrontFace(GL_CCW);

  r->destroyRenderTarget(ttgt);
}
    
DataRepresentation *VolumeRepresentation::duplicate() const
{
  return new VolumeRepresentation();
}

QString VolumeRepresentation::getRepresentationName() const
{
  return "VolumeRepresentation";
}

void VolumeRepresentation::delayedInit(UniverseWidget& universe)
{
  if (has_init)
    return;

  has_init = true;

  if (universe.getRenderer()->usingShaders())
    {
      try {
        GLSLShaderObject surfF("/volume/volumeFragment.glsl", GL_FRAGMENT_SHADER);
        GLSLShaderObject surfV("/volume/volumeVertex.glsl", GL_VERTEX_SHADER);
        GLSLShaderObject rayF("/volume/volumeFragmentRaytrace.glsl", GL_FRAGMENT_SHADER);
        GLSLShaderObject rayV("/volume/volumeVertexRaytrace.glsl", GL_VERTEX_SHADER);

        shaderOutPosition.attachShader(surfF);
        shaderOutPosition.attachShader(surfV);
        shaderRaytrace.attachShader(rayF);
        shaderRaytrace.attachShader(rayV);
        shaderOutPosition.link();
      } catch (const ShaderCompileError& e) {
        QErrorMessage::qtHandler()->showMessage(tr("The shader %1 failed to compile").arg(e.getPath()));
        throw RendererInitFailure();
      }
    }
    
}
 
