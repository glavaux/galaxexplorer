/*+
This is GalaxExplorer (./src/representations/dataRepresentation.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __DATA_REPRESENTATION_HPP
#define __DATA_REPRESENTATION_HPP

#include <QObject>
#include <QColor>
#include <vector>
#include <utility>
#include <QMap>
#include "points.hpp"
#include "datasets.hpp"
#include "triplet.hpp"
#include "transferFunction.hpp"
#include <iostream>
#include "GLSLProgramObject.h"

namespace GalaxExplorer
{
  class UniverseWidget;  
  class GLConnector;


  class DataRepresentation: public QObject
  {
    Q_OBJECT

    Q_PROPERTY(QString name READ getName WRITE setName)
  public:
    DataRepresentation();
    virtual ~DataRepresentation();

    void setDistanceMultiplier(double mul) { distanceMultiplier = mul; }

    const QString& getName() const { return myName; }
    void setName(const QString& name) { myName = name; }

    bool rebuildNeeded() const { return needRebuild; }
    
    virtual void loadState(QDataStream& d);
    virtual void saveState(QDataStream& d) const;

    virtual void loadState(const QMap<QString,QVariant>& d);
    virtual void saveState(QMap<QString,QVariant>& d) const;

    virtual DataRepresentation *duplicate() const = 0;

    virtual QString getRepresentationName() const = 0;
  protected:
    double distanceMultiplier;
    bool needRebuild;
  private:
    QString myName;
  };

  class DataSetCapableRepresentation: public DataRepresentation
  {
    Q_OBJECT
  public:
    DataSetCapableRepresentation();
    virtual ~DataSetCapableRepresentation();

    virtual void drawData(UniverseWidget& universe,
			  DataSetConstIterator begin,
			  DataSetConstIterator end);
    virtual void prepareDrawData(UniverseWidget& universe,
				 GLConnector& c,
				 DataSetConstIterator begin,
				 DataSetConstIterator end);
    virtual void realDrawData(UniverseWidget& universe,
			      GLConnector& c);
  };

  class DataGridCapableRepresentation: public DataRepresentation
  {
    Q_OBJECT
  public:
    DataGridCapableRepresentation();
    virtual ~DataGridCapableRepresentation();

    virtual void drawGrid(UniverseWidget& universe,
			  GLConnector& c);
    virtual void prepareDrawGrid(UniverseWidget& universe,
				 GLConnector& c);
    virtual void realDrawGrid(UniverseWidget& universe,
			      GLConnector& c);
  };

  class DataMeshCapableRepresentation: public DataRepresentation
  {
    Q_OBJECT
  public:
    DataMeshCapableRepresentation();
    virtual ~DataMeshCapableRepresentation();

    virtual void prepareDrawMesh(UniverseWidget& universe,
				 GLConnector& c);
    virtual void realDrawMesh(UniverseWidget& universe,
			      GLConnector& c);
  };


  typedef DataRepresentation *(*DataRepresentationFactory)();

  typedef QMap<QString, DataRepresentation *> DataRepresentationMap;

  void executeConnection(UniverseWidget& universe, GLConnector& connector);
  void drawConnection(UniverseWidget& universe, GLConnector& connector);
  
};

#endif
