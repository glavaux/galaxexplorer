#ifndef __GALAX_COLORPOINT_HPP
#define __GALAX_COLORPOINT_HPP

#include <QObject>
#include <QColor>
#include <vector>
#include <utility>
#include <QMap>
#include "points.hpp"
#include "datasets.hpp"
#include <iostream>
#include "GLSLProgramObject.h"
#include "dataRepresentation.hpp"

namespace GalaxExplorer
{

  class ColorPointRepresentation: public DataSetCapableRepresentation
  {
    Q_OBJECT
    Q_PROPERTY(QColor normalColor READ getNormalColor WRITE setNormalColor STORED true)
    Q_PROPERTY(QColor selectedColor READ getSelectedColor WRITE setSelectedColor)  
    Q_PROPERTY(double pointSize READ getPointSize WRITE setPointSize)
    Q_PROPERTY(bool distanceCorrection READ distanceCorrected WRITE setDistanceCorrection)
    Q_PROPERTY(double haloConcentration READ getHaloConcentration WRITE setHaloConcentration)
    Q_PROPERTY(QString auxFieldRed READ getAuxFieldRed WRITE setAuxFieldRed)
    Q_PROPERTY(QString auxFieldGreen READ getAuxFieldGreen WRITE setAuxFieldGreen)
    Q_PROPERTY(QString auxFieldBlue READ getAuxFieldBlue WRITE setAuxFieldBlue)
    Q_PROPERTY(QString auxFieldAlpha READ getAuxFieldAlpha WRITE setAuxFieldAlpha)
        
#define AUX_FIELD(N) \
protected: \
    QString auxField##N; \
public: \
    QString getAuxField##N () const { return auxField##N; } \
    void setAuxField##N (const QString& s) { auxField##N = s; }
    
    
    AUX_FIELD(Red)
    AUX_FIELD(Green)
    AUX_FIELD(Blue)
    AUX_FIELD(Alpha)
  public:
    ColorPointRepresentation();
    virtual ~ColorPointRepresentation();
        
    void setNormalColor(const QColor& normal);
    QColor getNormalColor() const;
    void setSelectedColor(const QColor& selected);
    QColor getSelectedColor() const;
    double getPointSize() const { return pointSize; }
    void setPointSize(double p) { pointSize = p; } 
    bool distanceCorrected() const { return distanceCorrection; }
    void setDistanceCorrection(bool b) { distanceCorrection = b; }
    double getHaloConcentration() const { return haloConcentration; }
    void setHaloConcentration(double h) { haloConcentration = h; }

/*    QString getAuxFieldRed() const { return auxFieldRed; }
    void setAuxFieldRed(const QString& s) { auxFieldRed = s; }
    QString getAuxFieldGreen() const { return auxFieldRed; }
    void setAuxFieldGreen(const QString& s) { auxFieldRed = s; }
    QString getAuxFieldBlue() const { return auxFieldRed; }
    void setAuxFieldBlue(const QString& s) { auxFieldRed = s; }*/
 
    virtual void loadState(const QMap<QString,QVariant>& d);
    virtual void saveState(QMap<QString,QVariant>& d) const;

    virtual void prepareDrawData(UniverseWidget& universe,
				 GLConnector& c,
				 DataSetConstIterator begin,
				 DataSetConstIterator end);
    virtual void realDrawData(UniverseWidget& universe,
			      GLConnector& c);
    
    virtual DataRepresentation *duplicate() const;

    virtual QString getRepresentationName() const;
  protected:
    QColor normalColor, selectedColor;
    double pointSize;
    bool has_init;
    GLfloat haloConcentration;
    bool distanceCorrection;
    GLSLProgramObject myShader;

    void delayedInit(UniverseWidget& w);
  };

};

#endif
