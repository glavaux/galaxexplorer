/*+
This is GalaxExplorer (./src/representations/dataRepresentation.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include "includeGL.hpp"
#include <iostream>
#include <cmath>
#include <QtGui>
#include <QtOpenGL>
#include <QColor>
#include <QString>
#include <QMap>
#include <QVariant>
#include <vector>
#include "glwidget.hpp"
#include "representations/dataRepresentation.hpp"
#include "representations/stdRepresentations.hpp"
#include "inits.hpp"
#include "glconnect.hpp"
#include "renderers/renderer.hpp"
#include "object_helper.hpp"

using namespace GalaxExplorer;

using namespace std;

#define SPHERE_RESOLUTION 5
 

void GalaxExplorer::executeConnection(UniverseWidget& universe, GLConnector& connector)
{
  DataConnector& dc = *connector.getConnector();

  dc.getRepresentation()->setDistanceMultiplier(universe.getState()->getMultiplier());

  if (DataSetCapableRepresentation *dscr = qobject_cast<DataSetCapableRepresentation *>(dc.getRepresentation()))
    {  
      SetConnector& sc = *qobject_cast<SetConnector *>(&dc);

      if (sc.getSelector() != 0)
	dscr->prepareDrawData(universe, connector, sc.getDataset()->beginSelected(*sc.getSelector()), sc.getDataset()->end());
      else
	dscr->prepareDrawData(universe, connector, sc.getDataset()->begin(), sc.getDataset()->end());
    }
  else if (DataGridCapableRepresentation *dgcr = qobject_cast<DataGridCapableRepresentation *>(dc.getRepresentation()))
    {
      dgcr->prepareDrawGrid(universe, connector);	
    }
  else if (DataMeshCapableRepresentation *dmcr = qobject_cast<DataMeshCapableRepresentation *>(dc.getRepresentation()))
    {
      dmcr->prepareDrawMesh(universe, connector);
    }
}

void GalaxExplorer::drawConnection(UniverseWidget& universe, GLConnector& connector)
{  
  if (SetConnector *c = qobject_cast<SetConnector *>(connector.getConnector()))
    {
      DataSetCapableRepresentation *dscr = 
	qobject_cast<DataSetCapableRepresentation *>(c->getRepresentation());

      if (dscr != 0)
	dscr->realDrawData(universe, connector);
      else
	qFatal("Invalid data representation type");
      
    }
  else if (GridConnector *c = qobject_cast<GridConnector *>(connector.getConnector()))
    {
      DataGridCapableRepresentation *dgcr =
	qobject_cast<DataGridCapableRepresentation *>(c->getRepresentation());
      
      if (dgcr != 0)
	dgcr->realDrawGrid(universe, connector);
      else
	qFatal("Invalid data representation type");
    }
  else if (MeshConnector *c = qobject_cast<MeshConnector *>(connector.getConnector()))
    {
      DataMeshCapableRepresentation *dmcr =
	qobject_cast<DataMeshCapableRepresentation *>(c->getRepresentation());
      if (dmcr != 0)
	dmcr->realDrawMesh(universe, connector);
      else
	qFatal("Invalid data representation type");
    }
}

DataRepresentation::DataRepresentation()
{
  distanceMultiplier = -1.0;
  needRebuild = false;
}

DataRepresentation::~DataRepresentation()
{
}

DataSetCapableRepresentation::DataSetCapableRepresentation()
{
}

DataSetCapableRepresentation::~DataSetCapableRepresentation()
{
}

void DataSetCapableRepresentation::drawData(UniverseWidget& universe,
				  DataSetConstIterator begin,
				  DataSetConstIterator end)
{
  cerr << "drawData not defined" << endl;
  abort();
}

void DataSetCapableRepresentation::prepareDrawData(UniverseWidget& universe,
					 GLConnector& c,
					 DataSetConstIterator begin,
					 DataSetConstIterator end)
{
  cerr << "prepareDrawData not defined" << endl;
  abort();
}

void DataSetCapableRepresentation::realDrawData(UniverseWidget& universe,
						GLConnector& c)
{
  cerr << "realDrawData not defined" << endl;
  abort();
}

void DataRepresentation::saveState(QDataStream& data) const
{
  QMap<QString,QVariant> map;

  saveState(map);
  data << map;
}

void DataRepresentation::loadState(QDataStream& data)
{
  QMap<QString,QVariant> map;

  data >> map;
  loadState(map);
}

void DataRepresentation::saveState(QMap<QString,QVariant>& data) const
{  
}

void DataRepresentation::loadState(const QMap<QString,QVariant>& data)
{
}

// Basic representations: b&w point and sphere

BWPointRepresentation::BWPointRepresentation()
{
  normalColor = QColor::fromRgbF(1.0, 1.0, 1.0);
  selectedColor = QColor::fromRgbF(1.0, 0.0, 0.0);
  alpha = 1.0;
  blend = false;
  pointSize = 1.0;
  has_init = false;
  useTransfer = false;
  haloConcentration = 0.5;
  distanceCorrection = false;
  textureCreated = false;
  textureNeedUpdate = true;
}


BWPointRepresentation::~BWPointRepresentation()
{
  if (textureCreated)
    glDeleteTextures(1, &transferFunctionTexture);
}

 
QString BWPointRepresentation::getRepresentationName() const
{
  return "BWPointRepresentation";
}

DataRepresentation *BWPointRepresentation::duplicate() const
{
  BWPointRepresentation *rep = new BWPointRepresentation();

  rep->setName(getName());
  rep->setNormalColor(normalColor);
  rep->setSelectedColor(selectedColor);
  rep->alpha = alpha;
  rep->blend = blend;
  rep->pointSize = pointSize;
  rep->distanceCorrection = distanceCorrection;
  rep->haloConcentration = haloConcentration;
  rep->useTransfer = useTransfer;
  rep->transferFunction = transferFunction;

  return rep;
}

void BWPointRepresentation::setNormalColor(const QColor& normal)
{
  normalColor = normal;
}

QColor BWPointRepresentation::getNormalColor() const
{
  return normalColor;
}

void BWPointRepresentation::setSelectedColor(const QColor& selected)
{
  selectedColor = selected;
}

QColor BWPointRepresentation::getSelectedColor() const
{
  return selectedColor;
}

void BWPointRepresentation::setAlpha(double f)
{
  cout << "[POINT]  alpha=" << f << endl;
  alpha = f;
}

double BWPointRepresentation::getAlpha() const
{
  return alpha;
}

bool BWPointRepresentation::isBlending() const
{
  return blend;
}

void BWPointRepresentation::setBlending(bool b)
{
  cout << "[BLEND=" << b << "]" << endl;
  blend = b;
}

void BWPointRepresentation::saveState(QMap<QString,QVariant>& data) const
{
  data["normalColor"] = normalColor;
  data["selectedColor"] = selectedColor;
  data["alpha"] = alpha;
  data["blend"] = blend;
  data["pointSize"] = pointSize;
  data["distanceCorrection"] = distanceCorrection;
  data["haloConcentration"] = haloConcentration;
}

void BWPointRepresentation::loadState(const QMap<QString,QVariant>& data) 
{
  normalColor = data["normalColor"].value<QColor>();
  selectedColor = data["selectedColor"].value<QColor>();
  alpha = data["alpha"].toDouble();
  blend = data["blend"].toBool();
  pointSize = data["pointSize"].toDouble();
  distanceCorrection = data["distanceCorrection"].toBool();
  haloConcentration = data["haloConcentration"].toDouble();
  cout << alpha << " " << blend << " " << pointSize << endl;
}

void BWPointRepresentation::delayedInit(UniverseWidget& universe)
{
  if (has_init)
    return;

  has_init = true;

  if (universe.getRenderer()->usingShaders())
    {
      GLSLShaderObject surfF("/point/pointFragment.glsl", GL_FRAGMENT_SHADER);
      GLSLShaderObject surfV("/point/pointVertex.glsl", GL_VERTEX_SHADER);
      GLSLShaderObject tf_surfV("/point/pointVertex_tf.glsl", GL_VERTEX_SHADER);

      myShader.attachShader(surfF);
      myShader.attachShader(surfV);
      
      myShader_tf.attachShader(surfF);
      myShader_tf.attachShader(tf_surfV);
    }
}

void BWPointRepresentation::prepareDrawData(UniverseWidget& universe,
					    GLConnector& c,
					    DataSetConstIterator begin,
					    DataSetConstIterator end)
{
  GLRenderer *r = universe.getRenderer();

  glColor3f(normalColor.redF(), normalColor.greenF(), normalColor.blueF());
  
  delayedInit(universe);

  if (r->usingShaders() && !c.prepared()) {
    r->assembleProgramShader(c, myShader, 0);
    r->assembleProgramShader(c, myShader_tf, 1);
  }
  
  c.setPrepared(true);

  int vboId;
  int attr_id = !auxFieldName.isEmpty() ? begin.getAttributeID(auxFieldName) : -1;
  VBO_Descriptor desc;

  if (attr_id >= 0) {
    desc.numAttribs = 1;
    desc.attribSizes = new int[1];
    desc.attribSizes[0] = 1;
    desc.attribIndex = new GLuint[1];
    desc.attribIndex[0] = 0;
  } 

  if (c.numberOfVBO() == 0)
    {
      vboId = c.allocateVBO(desc);
    }
  else
    {
      VBO_Object o(desc);
      c.getVBO(0).transfer(o);
      vboId = 0;
    }
    
  VBO_Object& vbo = c.getVBO(vboId);


  vbo.begin();
  
  DataSet::const_iterator iter = begin;
  bool colorIsSelected = false;
  while (iter != end)
    {
      const BarePoint& p = *iter;
      
      vbo.addPoint(distanceMultiplier*p.x, distanceMultiplier*p.y, distanceMultiplier*p.z);
      if (attr_id >= 0) {
        GLfloat f = iter.getAttribute<float>(attr_id);
        vbo.setAttrib(0,  &f );
      }

      ++iter;
    }
  vbo.end();
}


void BWPointRepresentation::realDrawData(UniverseWidget& universe,
					 GLConnector& c)
{
  GLRenderer *r = universe.getRenderer();
  GLSLProgramObject& s = r->getShader(c, useTransfer ? 1 : 0);
  int intDcor = distanceCorrection ? 1 : 0;
  GLfloat ps = pointSize;
  
  if (r->getInteractiveMode())
    ps /= r->getInteractiveResolution();

  if (useTransfer)
    {
      if (!textureCreated)
        {
          glGenTextures(1, &transferFunctionTexture);
          textureCreated = true;
        }
        
      if (textureNeedUpdate)
        {
          transferFunction.rebuildTransferTexture(transferFunctionTexture);
          textureNeedUpdate = false;
        }
      r->useShaderInConnector(c, 1);
      s.bindTexture1D("auxFieldTable", transferFunctionTexture);
      if (c.getVBO(0).getAccessor().getNumAttrib() > 0)
        {
          cout << "Activate auxField..." << endl;
          c.getVBO(0).updateAttributeLocation(0,  s.getAttribLocation("auxField"));
        }
    }
  else
    {
      r->useShaderInConnector(c, 0);
    }
  glEnable( GL_VERTEX_PROGRAM_POINT_SIZE );
  s.setUniform("pointSize", &ps, 1);
  s.setIntUniform("distanceCorrection", &intDcor, 1);
  s.setUniform("haloConcentration", &haloConcentration, 1);
  
  glColor4f(normalColor.redF(), normalColor.greenF(), normalColor.blueF(), alpha);
  c.getVBO(0).draw(GL_POINTS);

}


BWSphereRepresentation::BWSphereRepresentation()
{
  normalColor = QColor::fromRgbF(1.0, 0.0, 1.0);
  selectedColor = QColor::fromRgbF(0.3, 1.0, 0.3);
  radius = 0.1;
}

BWSphereRepresentation::~BWSphereRepresentation()
{
}

void BWSphereRepresentation::saveState(QDataStream& data) const
{
  data << normalColor << selectedColor << radius;
}

void BWSphereRepresentation::loadState(QDataStream& data) 
{
  data >> normalColor >> selectedColor >> radius;
}

QString BWSphereRepresentation::getRepresentationName() const
{
  return "BWSphereRepresentation";
}

void BWSphereRepresentation::setNormalColor(const QColor& normal)
{
  normalColor = normal;
}

QColor BWSphereRepresentation::getNormalColor() const
{
  return normalColor;
}

void BWSphereRepresentation::setSelectedColor(const QColor& selected)
{
  selectedColor = selected;
}

QColor BWSphereRepresentation::getSelectedColor() const
{
  return selectedColor;
}

DataRepresentation *BWSphereRepresentation::duplicate() const
{
  BWSphereRepresentation *rep = new BWSphereRepresentation();

  rep->setName(getName());
  rep->setNormalColor(normalColor);
  rep->setSelectedColor(selectedColor);
  rep->setRadius(radius);

  return rep;
}


void BWSphereRepresentation::realDrawData(UniverseWidget& universe, GLConnector& c)
{
  GLRenderer *r = universe.getRenderer();

  if (r->usingShaders())
    r->useDefaultShader();

  glColor3f(normalColor.redF(), normalColor.greenF(), normalColor.blueF());
  c.getVBO(0).draw(GL_TRIANGLE_STRIP);
}

void BWSphereRepresentation::prepareDrawData(UniverseWidget& universe,
					     GLConnector& c,
					     DataSetConstIterator begin,
					     DataSetConstIterator end)
{
  DataSet::const_iterator iter = begin;
  bool colorIsSelected = false;

  c.setPrepared(true);

  int vboId;

  if (c.numberOfVBO() == 0)
    {
      VBO_Descriptor desc;
      vboId = c.allocateVBO(desc);
    }
  else
    vboId = 0;

  VBO_Object& vbo = c.getVBO(vboId);
  SphereDrawer sphere(SPHERE_RESOLUTION);

  vbo.begin();

  while (iter != end)
    {
      const BarePoint& p = *iter;

#if 0
      if (p.selected != colorIsSelected)
	{
	  if (p.selected)
	    universe.qglColor(selectedColor);
	  else
	    universe.qglColor(normalColor);
	  colorIsSelected = !colorIsSelected;
	}
#endif

      vbo.newSequence();
      sphere.draw(vbo, distanceMultiplier*p.x, distanceMultiplier*p.y, distanceMultiplier*p.z, radius);
      ++iter;
    }

  vbo.end();
}

ColorSphereRepresentation::ColorSphereRepresentation()
{
  normalColor = QColor::fromRgbF(1.0, 0.0, 1.0);
  selectedColor = QColor::fromRgbF(0.3, 1.0, 0.3);
  radius = 1.0;
}

ColorSphereRepresentation::~ColorSphereRepresentation()
{
}

void ColorSphereRepresentation::loadState(QDataStream& d)
{
  d >> radius;
}

void ColorSphereRepresentation::saveState(QDataStream& d) const
{
  d << radius;
}

DataRepresentation *ColorSphereRepresentation::duplicate() const
{
  ColorSphereRepresentation *rep = new ColorSphereRepresentation();

  rep->setName(getName());
  rep->setRadius(radius);

  return rep;
}

QString ColorSphereRepresentation::getRepresentationName() const
{
  return "ColorSphereRepresentation";
}

void ColorSphereRepresentation::realDrawData(UniverseWidget& universe,
					     GLConnector& c)
{
  GLRenderer *r = universe.getRenderer();

  if (r->usingShaders())
    r->useDefaultShader();

  c.getVBO(0).draw(GL_TRIANGLE_STRIP);  
}

void ColorSphereRepresentation::prepareDrawData(UniverseWidget& universe,
						GLConnector& c,
						DataSetConstIterator begin,
						DataSetConstIterator end)
{
  c.setPrepared(true);

  int vboId;

  if (c.numberOfVBO() == 0)
    {
      VBO_Descriptor desc;

      desc.numcomponents = 3;
      desc.subcolors = true;
      vboId = c.allocateVBO(desc);
    }
  else
    vboId = 0;

  VBO_Object& vbo = c.getVBO(vboId);
  SphereDrawer sphere(SPHERE_RESOLUTION);

  vbo.begin();

  DataSet::const_iterator iter = begin;
 
  try {
    while (iter != end)
      {
	const BarePoint& p = *iter;

	float colorR = iter.getAttribute<float>("colorR");
	float colorG = iter.getAttribute<float>("colorG");
	float colorB = iter.getAttribute<float>("colorB");
	float p_radius = iter.getAttribute<float>("radius");

	vbo.newSequence();
	sphere.setColor(colorR, colorG, colorB);
	sphere.draw(vbo, distanceMultiplier*p.x, distanceMultiplier*p.y, distanceMultiplier*p.z, radius*p_radius*distanceMultiplier);
	
	++iter;
      }
  } catch (NoSuchAttributeError& e) {
    cerr << "Lazy catching of an unknown attribute in ColorSphere"<< endl;
  }

  vbo.end();
}


LumSphereRepresentation::LumSphereRepresentation()
{
  radius = 0.04;
}

LumSphereRepresentation::~LumSphereRepresentation()
{
}

void LumSphereRepresentation::loadState(QDataStream& d)
{
  d >> radius;
}

void LumSphereRepresentation::saveState(QDataStream& d) const
{
  d << radius;
}

DataRepresentation *LumSphereRepresentation::duplicate() const
{
  LumSphereRepresentation *rep = new LumSphereRepresentation();

  rep->setName(getName());
  rep->setRadius(radius);

  return rep;
}

QString LumSphereRepresentation::getRepresentationName() const
{
  return "LumSphereRepresentation";
}

void LumSphereRepresentation::realDrawData(UniverseWidget& universe,
					   GLConnector& c)
{
  GLRenderer *r = universe.getRenderer();

  if (r->usingShaders())
    r->useDefaultShader();

  c.getVBO(0).draw(GL_TRIANGLE_STRIP);  
}

void LumSphereRepresentation::prepareDrawData(UniverseWidget& universe,
					      GLConnector& c,
					      DataSetConstIterator begin,
					      DataSetConstIterator end)
{
  c.setPrepared(true);

  int vboId;

  if (c.numberOfVBO() == 0)
    {
      VBO_Descriptor desc;

      desc.numcomponents = 3;
      desc.subcolors = true;
      vboId = c.allocateVBO(desc);
    }
  else
    vboId = 0;

  VBO_Object& vbo = c.getVBO(vboId);
  SphereDrawer sphere(SPHERE_RESOLUTION);

  vbo.begin();

  DataSet::const_iterator iter = begin;

  while (iter != end)
    {
      const BarePoint& p = *iter;
      double lum = iter.getAttribute<float>("lum");

      double lumH = (log10(lum) - 8) / 4;
      if (lumH > 1.0)
        lumH = 1.0;
      if (lumH < 0.0)
        lumH = 0.0;

      int ilumH = (int)(359*lumH);
      QColor c = QColor::fromHsv(ilumH,255,100+(int)(155*lumH));

      vbo.newSequence();
      sphere.setColor(c.redF(), c.greenF(), c.blueF());
      sphere.draw(vbo, distanceMultiplier*p.x, distanceMultiplier*p.y, distanceMultiplier*p.z, radius);
      ++iter;
    }
  vbo.end();
}

ScalingLumSphereRepresentation::ScalingLumSphereRepresentation()
{
  radius = 1.0;
  logColScale = true;
  minColLum = 8;
  maxColLum = 12;
}

ScalingLumSphereRepresentation::~ScalingLumSphereRepresentation()
{
}

void ScalingLumSphereRepresentation::loadState(const QMap<QString,QVariant>& data)
{
  radius = data["radius"].toDouble();
  minColLum = data["minColLum"].toDouble();
  maxColLum = data["maxColLum"].toDouble();
}

void ScalingLumSphereRepresentation::saveState(QMap<QString,QVariant>& data) const
{
  data["radius"] = radius;
  data["minColLum"] = minColLum;
  data["maxColLum"] = maxColLum;
}

QString ScalingLumSphereRepresentation::getRepresentationName() const
{
  return "ScalingLumSphereRepresentation";
}

DataRepresentation *ScalingLumSphereRepresentation::duplicate() const
{
  ScalingLumSphereRepresentation *rep = new ScalingLumSphereRepresentation();

  rep->setName(getName());
  rep->setRadius(radius);

  return rep;
}

void ScalingLumSphereRepresentation::realDrawData(UniverseWidget& universe,
						  GLConnector& c)
{
  GLRenderer *r = universe.getRenderer();

  if (r->usingShaders())
    r->useDefaultShader();

  c.getVBO(0).draw(GL_TRIANGLE_STRIP);  
}

void ScalingLumSphereRepresentation::prepareDrawData(UniverseWidget& universe,
						     GLConnector& c,
						     DataSetConstIterator begin,
						     DataSetConstIterator end)
{
  c.setPrepared(true);

  int vboId;

  if (c.numberOfVBO() == 0)
    {
      VBO_Descriptor desc;

      desc.numcomponents = 3;
      vboId = c.allocateVBO(desc);
    }
  else
    vboId = 0;

  VBO_Object& vbo = c.getVBO(vboId);
  SphereDrawer sphere(SPHERE_RESOLUTION);

  DataSet::const_iterator iter = begin;
  
  vbo.begin();
  try{

  while (iter != end)
    {
      const BarePoint& p = *iter;
      
      double lumH;
      double l = iter.getAttribute<float>("lum");
      float p_radius = iter.getAttribute<float>("radius");

      if (logColScale)
	l = log10(l);
      
      l -= minColLum;
      if (l < 0)
	l = 0;

      lumH = l / (maxColLum - minColLum);
      if (isnan(lumH) || isinf(lumH))
	lumH = 1.0;

      if (lumH > 1.0)
        lumH = 1.0;
      if (lumH < 0.0)
        lumH = 0.0;

      int ilumH = (int)(359*lumH);

      QColor color = QColor::fromHsv(ilumH,255,255);

      vbo.newSequence();
      sphere.setColor(color.redF(), color.greenF(), color.blueF());
      sphere.draw(vbo, distanceMultiplier*p.x, distanceMultiplier*p.y, distanceMultiplier*p.z, radius*distanceMultiplier*p_radius);

      ++iter;
    }
  } catch (const NoSuchAttributeError& err) {}
  vbo.end();

}



DataGridCapableRepresentation::DataGridCapableRepresentation()
{
}
 
DataGridCapableRepresentation::~DataGridCapableRepresentation()
{
}

void DataGridCapableRepresentation::drawGrid(UniverseWidget& universe,
					     GLConnector& c)
{
  cerr << "drawGrid not defined" << endl;
  abort();
}

void DataGridCapableRepresentation::prepareDrawGrid(UniverseWidget& universe,
						    GLConnector& c)
{
  cerr << "prepareDrawGrid not defined" << endl;
  abort();
}

void DataGridCapableRepresentation::realDrawGrid(UniverseWidget& universe,
						 GLConnector& c)
{
  cerr << "realDrawGrid not defined" << endl;
  abort();
}

DataMeshCapableRepresentation::DataMeshCapableRepresentation()
{
}


DataMeshCapableRepresentation::~DataMeshCapableRepresentation()
{
}


void DataMeshCapableRepresentation::prepareDrawMesh(UniverseWidget& universe,
						    GLConnector& c)
{
  cerr << "prepareDrawMesh not defined" << endl;
  abort();
}

void DataMeshCapableRepresentation::realDrawMesh(UniverseWidget& universe,
						 GLConnector& c)
{
  cerr << "realDrawMesh not defined" << endl;
  abort();
}
