#include <iostream>
#include <cmath>
#include <QObject>
#include <QWidget>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QPainter>
#include <QMouseEvent>
#include "transferRepresentation.hpp"

using namespace GalaxExplorer;
using namespace std;

TransferRepresentation::TransferRepresentation(TransferFunction f, QWidget *parent)
  : QWidget(parent), tf(f), buttonPressed(false)
{
}

TransferRepresentation::~TransferRepresentation()
{
}

void TransferRepresentation::updateTransferFunction(TransferFunction f)
{
  tf = f;
  update();  
}

void TransferRepresentation::mousePressEvent(QMouseEvent *event)
{
  if (event->button() == Qt::LeftButton)
    buttonPressed = true;
    
 QWidget::mousePressEvent(event);
}


void TransferRepresentation::mouseReleaseEvent(QMouseEvent *event)
{
  if (event->button() == Qt::LeftButton && buttonPressed)
    {
      buttonPressed = false;
      if (rect().contains(event->pos()))
        emit clicked();
    }
}


void TransferRepresentation::paintEvent(QPaintEvent *event)
{
  cout << "repaint transfer function w=" << width() << " h=" << height() << endl;
  
  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing, true);

  QImage image(size(), QImage::Format_ARGB32);
  
  for(int i = 0; i < width() ; ++i)
    {
      double x = i / qreal(width());
      double f;
      
      QRgb color = tf.getColor(x).rgb();

      for (int j=0 ; j < height(); ++j)
        image.setPixel(i,j,color + 0xff000000);
    }

  painter.drawImage(0, 0, image);
  QWidget::paintEvent(event);
}
