#ifndef __GALAX_TRANSFER_REPRESENTATION_HPP
#define __GALAX_TRANSFER_REPRESENTATION_HPP

#include <QWidget>
#include "transferFunction.hpp"

namespace GalaxExplorer
{

  class TransferRepresentation: public QWidget
  {
    Q_OBJECT
  public:
    TransferRepresentation(TransferFunction f, QWidget *parent = 0);
    virtual ~TransferRepresentation();
    
    const TransferFunction& transfer() const { return tf; }
    void updateTransferFunction(TransferFunction f);
    
  protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    QWidget * background;
    TransferFunction tf;
    bool buttonPressed;
  signals:
    void clicked();

  };

};

#endif
