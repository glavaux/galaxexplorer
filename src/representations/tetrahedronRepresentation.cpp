/*+
This is GalaxExplorer (./src/representations/tetrahedronRepresentation.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <iostream>
#include "representations/dataRepresentation.hpp"
#include "glwidget.hpp"
#include "GLSLProgramObject.h"
#include "representations/tetrahedronRepresentation.hpp"
#include "dataConnector.hpp"
#include "surfaceCleaner.hpp"
#include "datamesh.hpp"

using namespace std;
using namespace GalaxExplorer;

TetrahedronRepresentation::TetrahedronRepresentation()
{
  surfaceColor = QColor::fromRgbF(1.0, 0.0, 0.0);
  specularColor = QColor::fromRgbF(1.0, 1.0, 1.0);  
  ambientColor = QColor::fromRgbF(0.3, 0.0, 0.0);
  ambientAlpha = 1.0;
  surfaceAlpha = 1.0;
  specularAlpha = 1.0;
  shininess = 8.0;
  smoothSurface = true;
  showRidges = true;
  showNormals = true;
  has_init = false;
}

TetrahedronRepresentation::~TetrahedronRepresentation()
{
}

void TetrahedronRepresentation::delayedInit(UniverseWidget& universe)
{
  if (has_init)
    return;

  has_init = true;

  if (universe.getRenderer()->usingShaders())
    {
      GLSLShaderObject surfF("tetrahedron/tetraFragment.glsl", GL_FRAGMENT_SHADER);
      GLSLShaderObject surfV("tetrahedron/tetraVertex.glsl", GL_VERTEX_SHADER);

      myShader.attachShader(surfF);
      myShader.attachShader(surfV);
    }
}
 
void TetrahedronRepresentation::prepareDrawMesh(UniverseWidget& universe,
						GLConnector& c)
{
  GLRenderer *r = universe.getRenderer();
  MeshConnector *mc = qobject_cast<MeshConnector *>(c.getConnector());
  DataMesh *data = mc->getDataMesh();
  DataSet *pset = mc->getDataParticles();

  delayedInit(universe);

  if (r->usingShaders() && !c.prepared())
    {
      r->assembleProgramShader(c, myShader, 0);
      c.setPrepared(true);
    }

  needRebuild = false;

  int vboId, vboId2, vboId3;
  
  if (c.numberOfVBO() == 0)
    {
      VBO_Descriptor desc, desc2, desc3;

      desc.subnormals = true;
      
      vboId = c.allocateVBO(desc);
      vboId2 = c.allocateVBO(desc2);
      vboId3 = c.allocateVBO(desc3);
    }
  else {
    vboId = 0; vboId2 = 1; vboId3 = 2;
  }

  VBO_Object& vbo = c.getVBO(vboId);
  VBO_Object& vbo2 = c.getVBO(vboId2);

  SurfaceCleaner cleaner(smoothSurface);

  vbo.begin();
  cleaner.begin(data->getNumTriangles(), pset->size());
  for (int t = 0; t < data->getNumTriangles(); t++)
    {
      const int *plist;
      
      data->getTriangle(t, plist);
      cleaner.addTriangle(*pset, t, plist[0], plist[1], plist[2]);	  
    }
  cleaner.end();
      
  for (int t = 0; t < data->getNumTriangles(); t++)
    {
      const int *plist;
      
      data->getTriangle(t, plist);

      for (int p = 2; p >= 0; p--)
	{
	  const BarePoint& pnt_data = (*pset)[plist[p]];
	  SurfaceCleaner::Point n;
	  
	  cleaner.getNormal(t, plist[p], n);
	  vbo.addPoint(distanceMultiplier*pnt_data.x, distanceMultiplier*pnt_data.y, distanceMultiplier*pnt_data.z);
	  vbo.setNormal(n.xyz[0], n.xyz[1], n.xyz[2]);
	}
    }
  vbo.end();

  if (showRidges)
    {
      vbo2.begin();
      for (int t = 0; t < data->getNumTriangles(); t++)
	{
	  const int *plist;
	  data->getTriangle(t, plist);
	  for (int p = 0; p < 3; p++)
	    {
	      const BarePoint& pnt_data = (*pset)[plist[p]];
	      
	      vbo2.addPoint(distanceMultiplier*pnt_data.x, distanceMultiplier*pnt_data.y, distanceMultiplier*pnt_data.z);
	    }
	  
	  const BarePoint& pnt_data = (*pset)[plist[0]];
	  vbo2.addPoint(distanceMultiplier*pnt_data.x, distanceMultiplier*pnt_data.y, distanceMultiplier*pnt_data.z);
	}
      vbo2.end();
    }

  if (showNormals && !smoothSurface)
    {
      VBO_Object& vbo3 = c.getVBO(2);
      SurfaceCleaner::Point n;
     
      vbo3.begin();
      for (int t = 0; t < data->getNumTriangles(); t++)
	{
	  const int *plist;
	  float pxyz[3] = {0,0,0};
	  double n2 = 0;

	  data->getTriangle(t, plist);
	  cleaner.getNormal(t, plist[0], n);
	  
	  for (int p = 2; p >= 0; p--)
	    {
	      const BarePoint& pnt_data = (*pset)[plist[p]];

	      pxyz[0] += distanceMultiplier*pnt_data.x/3;
	      pxyz[1] += distanceMultiplier*pnt_data.y/3;
	      pxyz[2] += distanceMultiplier*pnt_data.z/3;
	      n2 += n.xyz[p]*n.xyz[p];
	    }
	  
	  n2 = sqrt(n2);

	  vbo3.addPoint(pxyz[0], pxyz[1], pxyz[2]);
	  vbo3.addPoint(pxyz[0]+n.xyz[0]/n2, 
			pxyz[1]+n.xyz[1]/n2, 
			pxyz[2]+n.xyz[2]/n2);
	}
      vbo3.end();
    }
       
}

static void QColorToGL(const QColor& color, double alpha, GLfloat *glcolor)
{
  glcolor[0] = color.redF();
  glcolor[1] = color.greenF();
  glcolor[2] = color.blueF();
  glcolor[3] = alpha;
}

void TetrahedronRepresentation::realDrawMesh(UniverseWidget& universe,
					     GLConnector& c)
{

  GLRenderer *r = universe.getRenderer();

  glEnable(GL_NORMALIZE);
  glEnable(GL_AUTO_NORMAL);
  if (!r->usingShaders())
    {
      glEnable(GL_CULL_FACE);
      glEnable(GL_DEPTH_TEST);
      glDisable(GL_BLEND);
      glEnable(GL_LIGHTING);
      glEnable(GL_LIGHT0);
      glShadeModel(GL_SMOOTH);

      //      glBlendEquation(GL_FUNC_ADD);
      //      glBlendFuncSeparate(GL_ONE_MINUS_DST_ALPHA, GL_ONE_MINUS_SRC_ALPHA, //ZERO, //DST_ALPHA, GL_ONE,
      //			  GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);
      
      GLfloat std_colorf[4];
      GLfloat nocolor[4] = { 0,0,0,1.};
      GLfloat specularf[4];
      GLfloat shininessf[] = { shininess };     

      QColorToGL(surfaceColor, surfaceAlpha, std_colorf);
      QColorToGL(specularColor, specularAlpha, specularf);
 
      //glMaterialfv(GL_BACK,  GL_AMBIENT,   nocolor);
      glMaterialfv(GL_FRONT, GL_AMBIENT,   nocolor);
      //glMaterialfv(GL_BACK,  GL_DIFFUSE,   std_colorf);
      glMaterialfv(GL_FRONT, GL_DIFFUSE,   std_colorf);      
      glMaterialfv(GL_FRONT, GL_EMISSION,  nocolor);
      //glMaterialfv(GL_BACK, GL_EMISSION,  nocolor);

      glMaterialfv(GL_FRONT, GL_SPECULAR,  specularf);
      glMaterialfv(GL_FRONT, GL_SHININESS, shininessf);
    }
  else
    {
      GLfloat std_colorf[4];
      GLfloat camera_pos[3];
      GLfloat light_pos[3];
      GLfloat spec_color[4], amb_color[4];
      GLfloat shininessf = shininess;

      // Grab the current camera position 
      r->getScene().getState()->getCameraPosition(camera_pos);

      QColorToGL(surfaceColor, surfaceAlpha, std_colorf);
      QColorToGL(ambientColor, ambientAlpha, amb_color);
      QColorToGL(specularColor, specularAlpha, spec_color);

      Point3d ls = r->getLightSource();
      light_pos[0] = ls.x;
      light_pos[1] = ls.y;
      light_pos[2] = ls.z;
      
      glDisable(GL_CULL_FACE);
      r->useShaderInConnector(c, 0);


      GLfloat fmul = distanceMultiplier;
      GLSLProgramObject& p = r->getShader(c, 0);
      p.setUniform("surfaceColor", std_colorf, 4);
      p.setUniform("cameraPosition", camera_pos, 3);
      p.setUniform("lightPos", light_pos, 3);
      p.setUniform("shininess", &shininessf, 1);
      p.setUniform("specularColor", spec_color, 4);
      p.setUniform("ambientColor", amb_color, 4);
      p.setUniform("multiplier", &fmul, 1);
    }

  c.getVBO(0).draw(GL_TRIANGLES);

  GLdouble modelMatrix[16];
  RenderViewState old_rvs = r->viewState();
  if (!r->usingShaders())    
    {
      glDisable(GL_BLEND);
      glDisable(GL_LIGHTING);
      glDisable(GL_LIGHT0);
      r->viewState().preScale(distanceMultiplier, distanceMultiplier, distanceMultiplier);
      r->reloadViewState();
    }
  else
    {
      r->useDefaultShader();
    }

  if (showRidges)
    {
      glColor4f(surfaceColor.redF(),surfaceColor.greenF(),surfaceColor.blueF(),1.0);
      c.getVBO(1).draw(GL_LINES);
    }
 
  if (showNormals && !smoothSurface)
    {
      glColor4f(0, 1.0, 0.0, 1.0);
      c.getVBO(2).draw(GL_LINES);
    }

   if (!r->usingShaders())
    {
      glEnable(GL_DEPTH_TEST);
      r->viewState() = old_rvs;
      r->reloadViewState();
    }
   glDisable(GL_AUTO_NORMAL);
   glDisable(GL_NORMALIZE);
}



DataRepresentation *TetrahedronRepresentation::duplicate() const
{
  return new TetrahedronRepresentation();
}

QString TetrahedronRepresentation::getRepresentationName() const
{
  return "TetrahedronRepresentation";
}
