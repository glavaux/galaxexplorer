#ifndef __DATA_STDREPRESENTATION_HPP
#define __DATA_STDREPRESENTATION_HPP

#include <QObject>
#include <QColor>
#include <vector>
#include <utility>
#include <QMap>
#include "points.hpp"
#include "datasets.hpp"
#include "triplet.hpp"
#include "transferFunction.hpp"
#include <iostream>
#include "GLSLProgramObject.h"
#include "dataRepresentation.hpp"

namespace GalaxExplorer
{

  class BWPointRepresentation: public DataSetCapableRepresentation
  {
    Q_OBJECT
    Q_PROPERTY(QColor normalColor READ getNormalColor WRITE setNormalColor STORED true)
    Q_PROPERTY(QColor selectedColor READ getSelectedColor WRITE setSelectedColor)  
    Q_PROPERTY(double transparency READ getAlpha WRITE setAlpha)
    Q_PROPERTY(double pointSize READ getPointSize WRITE setPointSize)
    Q_PROPERTY(bool distanceCorrection READ distanceCorrected WRITE setDistanceCorrection)
    Q_PROPERTY(double haloConcentration READ getHaloConcentration WRITE setHaloConcentration)
    Q_PROPERTY(GalaxExplorer::TransferFunction transferFunction READ getColorTransfer WRITE setColorTransfer)
    Q_PROPERTY(bool useTransfer READ getUseTransfer WRITE setUseTransfer)
    Q_PROPERTY(QString auxFieldName READ getAuxFieldName WRITE setAuxFieldName)
  public:
    BWPointRepresentation();
    virtual ~BWPointRepresentation();
        
    void setNormalColor(const QColor& normal);
    QColor getNormalColor() const;
    void setSelectedColor(const QColor& selected);
    QColor getSelectedColor() const;
    double getAlpha() const;
    void setAlpha(double f);
    bool isBlending() const; 
    void setBlending(bool b);
    double getPointSize() const { return pointSize; }
    void setPointSize(double p) { pointSize = p; } 
    bool distanceCorrected() const { return distanceCorrection; }
    void setDistanceCorrection(bool b) { distanceCorrection = b; }
    double getHaloConcentration() const { return haloConcentration; }
    void setHaloConcentration(double h) { haloConcentration = h; }
    TransferFunction getColorTransfer() const { return transferFunction; }
    void setColorTransfer(const TransferFunction& f) { transferFunction = f; textureNeedUpdate = true; }
    bool getUseTransfer() const { return useTransfer; }
    void setUseTransfer(bool b) { useTransfer = b;  }
    QString getAuxFieldName() const { return auxFieldName; }
    void setAuxFieldName(const QString& s) { auxFieldName = s; }
 
    virtual void loadState(const QMap<QString,QVariant>& d);
    virtual void saveState(QMap<QString,QVariant>& d) const;

    virtual void prepareDrawData(UniverseWidget& universe,
				 GLConnector& c,
				 DataSetConstIterator begin,
				 DataSetConstIterator end);
    virtual void realDrawData(UniverseWidget& universe,
			      GLConnector& c);
    
    virtual DataRepresentation *duplicate() const;

    virtual QString getRepresentationName() const;
  protected:
    QColor normalColor, selectedColor;
    double alpha;
    bool blend;
    double pointSize;
    bool has_init;
    GLfloat haloConcentration;
    bool distanceCorrection;
    GLSLProgramObject myShader, myShader_tf;
    bool useTransfer;
    TransferFunction transferFunction;
    QString auxFieldName;
    bool textureCreated, textureNeedUpdate;
    GLuint transferFunctionTexture;

    void delayedInit(UniverseWidget& w);
  };

   class BWSphereRepresentation: public DataSetCapableRepresentation
  {
    Q_OBJECT

    Q_PROPERTY(QColor normalColor READ getNormalColor WRITE setNormalColor STORED true)
    Q_PROPERTY(QColor selectedColor READ getSelectedColor WRITE setSelectedColor)  
    Q_PROPERTY(double radius READ getRadius WRITE setRadius)
  public:
    BWSphereRepresentation();
    virtual ~BWSphereRepresentation();

    virtual QString getRepresentationName() const;
    virtual DataRepresentation *duplicate() const;
 
    virtual void loadState(QDataStream& d);
    virtual void saveState(QDataStream& d) const;

    QColor getNormalColor() const;
    void setNormalColor(const QColor& normal);   
    QColor getSelectedColor() const;
    void setSelectedColor(const QColor& selected);
    double getRadius() const { return radius; }
    void setRadius(double r) { radius = r; std::cout << "Radius set to " << r << std::endl; }

    virtual void prepareDrawData(UniverseWidget& universe, GLConnector& c,
			  DataSetConstIterator begin,
			  DataSetConstIterator end);
    virtual void realDrawData(UniverseWidget& universe, GLConnector& c);

  protected:
    QColor normalColor, selectedColor;
    double radius;
  };

 class ColorSphereRepresentation: public DataSetCapableRepresentation
  {
    Q_OBJECT
    
    Q_PROPERTY(double scalingRadius READ getRadius WRITE setRadius)
  public:
    ColorSphereRepresentation();
    virtual ~ColorSphereRepresentation();

    virtual QString getRepresentationName() const;
    virtual DataRepresentation *duplicate() const;

    virtual void loadState(QDataStream& d);
    virtual void saveState(QDataStream& d) const;
    
    double getRadius() const { return radius; }
    void setRadius(double r) { radius = r; }    

    void setColors(const QColor& normal, const QColor& selected);
    virtual void prepareDrawData(UniverseWidget& universe,
				 GLConnector& c,
				 DataSetConstIterator begin,
				 DataSetConstIterator end);
    virtual void realDrawData(UniverseWidget& universe,
			      GLConnector& c);
  protected:
    QColor normalColor, selectedColor;
    double radius;
  };

  class LumSphereRepresentation: public DataSetCapableRepresentation
  {
    Q_OBJECT

    Q_PROPERTY(double scalingRadius READ getRadius WRITE setRadius)
  public:
    LumSphereRepresentation();
    virtual ~LumSphereRepresentation();

    double getRadius() const { return radius; }
    void setRadius(double r) { radius = r; }    
 
    virtual void loadState(QDataStream& d);
    virtual void saveState(QDataStream& d) const;

    virtual DataRepresentation *duplicate() const;
    virtual QString getRepresentationName() const;
    virtual void prepareDrawData(UniverseWidget& universe,
				 GLConnector& c,
				 DataSetConstIterator begin,
				 DataSetConstIterator end);
    virtual void realDrawData(UniverseWidget& universe,
			      GLConnector& c);
  private:
    double radius;
  };

  class ScalingLumSphereRepresentation: public DataSetCapableRepresentation
  {
    Q_OBJECT

    Q_PROPERTY(double scalingRadius READ getRadius WRITE setRadius)
    Q_PROPERTY(double colorMinLum READ getColorMinLuminosity WRITE setColorMinLuminosity)
    Q_PROPERTY(double colorMaxLum READ getColorMaxLuminosity WRITE setColorMaxLuminosity)
    Q_PROPERTY(bool logColScale READ logColorScale WRITE setLogColorScale)
  public:
    ScalingLumSphereRepresentation();
    virtual ~ScalingLumSphereRepresentation();

    double getRadius() const { return radius; }
    void setRadius(double r) { radius = r; }    

    double getColorMinLuminosity() const { return minColLum; }
    void setColorMinLuminosity(double c) { minColLum = c; }

    double getColorMaxLuminosity() const { return maxColLum; }
    void setColorMaxLuminosity(double c) { maxColLum = c; }

    bool logColorScale() const { return logColScale; }
    void setLogColorScale(bool logCol) { logColScale = logCol; }

    virtual void loadState(const QMap<QString,QVariant>& d);
    virtual void saveState(QMap<QString,QVariant>& d) const;

    virtual DataRepresentation *duplicate() const;
    virtual QString getRepresentationName() const;
    virtual void realDrawData(UniverseWidget& universe,
			      GLConnector& c);			      
    virtual void prepareDrawData(UniverseWidget& universe,
				 GLConnector& c,
				 DataSetConstIterator begin,
				 DataSetConstIterator end);
  protected:
    double radius;
    double minColLum, maxColLum;
    bool logColScale;
  };
};

#endif
