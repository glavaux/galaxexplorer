/*+
This is GalaxExplorer (./src/representations/massiveRepresentation.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include "includeGL.hpp"
#include <iostream>
#include <CosmoTool/octTree.hpp>
#include "datasets.hpp"
#include "representations/massiveRepresentation.hpp"
#include <QtCore>
#include <QtGui>
#include "glwidget.hpp"
#include "renderers/renderer.hpp"

using namespace std;
using namespace CosmoTool;
using namespace GalaxExplorer;

#define DO_FACES

MassivePointRepresentation::MassivePointRepresentation()
{
  normalColor = QColor::fromRgbF(1.0, 1.0, 1.0);
  maxTreeDepth = 2;
  densityLimit = 0;
  Llimit = 0;
  densityMul = 1;
  densityLogScale = 1.;
  lenNorm = 1;
  particleNorm = 1;
}

MassivePointRepresentation::~MassivePointRepresentation()
{
}


void MassivePointRepresentation::saveState(QMap<QString,QVariant>& data) const
{
  data["maxTreeDepth"] = maxTreeDepth;
  data["densityLimit"] = densityLimit;
  data["Llimit"] = Llimit;
  data["densityMul"] = densityMul;
  data["normalColor"] = normalColor;
  data["dLogScale"] = densityLogScale;
  data["lenNorm"] = lenNorm;
  data["particleNorm"] = particleNorm;
}

void MassivePointRepresentation::loadState(const QMap<QString,QVariant>& data)
{
  densityMul = data["densityMul"].toDouble();
  Llimit = data["Llimit"].toDouble();
  densityLimit = data["densityLimit"].toDouble();
  maxTreeDepth = data["maxTreeDepth"].toInt();
  normalColor = data["normalColor"].value<QColor>();
  densityLogScale = data["dLogScale"].toDouble();
  lenNorm = data["lenNorm"].toDouble();
  particleNorm = data["particleNorm"].toDouble();
}

class TreeDrawer
{
private:
  float multiplier;
  float dMultiplier;
  double logScale;
  float rBase, gBase, bBase;
  UniverseWidget& widget;
public:
  TreeDrawer(UniverseWidget& w, float mul, float dMul, double lScale, float r, float g, float b)
    : widget(w), multiplier(mul), dMultiplier(dMul), logScale(lScale), rBase(r), gBase(g), bBase(b) {}

  ~TreeDrawer() {}

  void operator()(const FCoordinates& c,
		  uint32_t num,
		  float L,
		  bool meta,
		  bool leaf)
  {
    // We do not represent internal nodes.
    if (!meta && !leaf)
      return;

    const float coords[8][3] = {
      { -0.5, -0.5, -0.5 },
      { 0.5, -0.5, -0.5 },
      { 0.5, 0.5, -0.5 },
      { -0.5, 0.5, -0.5 },
      { -0.5, -0.5, 0.5 },
      { 0.5, -0.5, 0.5 },
      { 0.5, 0.5, 0.5 },
      { -0.5, 0.5, 0.5 }
    };

#ifdef DO_LINES    
    const int corners[6][4] = {
      { 0, 1, 2, 3 },
      { 0, 1, 5, 4 },
      { 0, 3, 7, 4 },
      { 2, 3, 7, 6 },
      { 1, 2, 6, 5 },
      { 4, 5, 6, 7 }
    };

    cout << c[0] << " " << c[1] << " " << c[2] << " l=" << L << endl;

    for (int i = 0; i < 6; i++)
      {
        glBegin(GL_TRIANGLE_STRIP);    
        for (int j = 0; j < 4; j++)
            glVertex3f(multiplier*(c[0]+L*coords[corners[i][j]][0]),
                 multiplier*(c[1]+L*coords[corners[i][j]][1]),
                 multiplier*(c[2]+L*coords[corners[i][j]][2]));
        glEnd();
  }
#endif
#ifdef DO_FACES
    const int tris[12][3] = {
      { 0, 1, 5},
      { 0, 5, 4}, 
      { 0, 4, 7},
      { 0, 7, 3},
      { 1, 2, 6}, 
      { 1, 6, 5},
      { 2, 7, 6},
      { 2, 3, 7},
      { 4, 5, 6},
      { 4, 6, 7},
      { 0, 2, 1},
      { 0, 3, 2}
    };

    float density = num/(L*L*L);
    float a=min(double(dMultiplier*log(density*logScale)),1.);
    if (a<=0.)
      return;

    glColor4f(a,a,a,a);
    for (int j = 0; j < 12; j++)
      {	
	for (int i = 0; i < 3; i++)
	  {
	    int p = tris[j][i];
	    glVertex3f(multiplier*(c[0]+L*coords[p][0]),
		       multiplier*(c[1]+L*coords[p][1]),
		       multiplier*(c[2]+L*coords[p][2]));
	  }
      }

#endif

  }
};
  
class TreeDrawerCondition
{
private:
  UniverseWidget& widget;
  TreeDrawer& drawer;
  float DensityLimit;
  float Llimit;
public:
  TreeDrawerCondition(UniverseWidget& w, TreeDrawer& d, float dLimit, float lLimit)
    : widget(w), drawer(d), DensityLimit(dLimit), Llimit(lLimit) {}
  ~TreeDrawerCondition() {}

  bool operator()(const FCoordinates& c, octPtr num, float L, bool meta)
  {
    float density = num/L/L/L;
    
    if (density < DensityLimit && L < Llimit)
      {
	if (!meta)
	  drawer(c, num, L, true, false);
	return false;
      }
    return true;
  }
  
};


void MassivePointRepresentation::prepareDrawData(UniverseWidget& universe,
                                                GLConnector& c,
                                                DataSetConstIterator begin,
                                                DataSetConstIterator end)
{
  GLRenderer *r = universe.getRenderer();
  DataSetConstIterator iter(begin);
  uint32_t numParticles = 0;
  uint32_t i;
  FCoordinates *particles;
  OctTree *tree;

  for (iter = begin; iter != end; ++iter)
    numParticles++;

  particles = new FCoordinates[numParticles];

  i = 0;
  BarePoint p = *begin;
  float minP[3] = {p.x,p.y,p.z}, 
    maxP[3]={p.x,p.y,p.z};
  for (iter = begin; iter != end; ++iter)
    {
      BarePoint p = *iter;
      particles[i][0] = p.x;
      particles[i][1] = p.y;
      particles[i][2] = p.z;

      i++;
    }

  // 100 maximum depth should be sufficient at the moment.
/*  tree = new OctTree(particles, numParticles, 1+log(numParticles)/log(8.),
        		     maxTreeDepth, 1);
  TreeDrawer drawer(universe, distanceMultiplier, densityMul, densityLogScale*pow(lenNorm/particleNorm,3), normalColor.redF(), normalColor.greenF(), normalColor.blueF());
  TreeDrawerCondition walkCondition(universe, drawer, densityLimit, Llimit);

  glColor3f(normalColor.redF(), normalColor.greenF(), normalColor.blueF());

  glBegin(GL_TRIANGLES);
  tree->walkTree(drawer, walkCondition);
  glEnd();
  */
  delete[] particles;

  delete tree;
}
  
void MassivePointRepresentation::realDrawData(UniverseWidget& universe,
                                              GLConnector& c)
{
}
  
DataRepresentation *MassivePointRepresentation::duplicate() const
{
  return new MassivePointRepresentation();
}
  
QString MassivePointRepresentation::getRepresentationName() const
{
  return QString("MassivePointRepresentation");
}
