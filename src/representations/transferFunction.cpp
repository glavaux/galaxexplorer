#include <iostream>
#include <cassert>
#include <QColor>
#include <QVector>
#include "transferFunction.hpp"
#include "glerror.hpp"

using std::cout;
using std::endl;
using namespace GalaxExplorer;

TransferFunction::TransferFunction()
{
    for (int j = 0; j < 4; j++) {
        channel[j].resize(2);
        
        channel[j][0].x = 0;
        channel[j][0].color = 0;
        channel[j][1].x = 1;
        channel[j][1].color = 1;
    }
}


QColor TransferFunction::getColor(qreal x) const
{
  qreal rgba[4];
  int N[4];
  
  for (int i = 0; i < 4; i++)
    N[i] = channel[i].size();
  
  if (x<=0)
    {
      return QColor::fromRgbF(channel[0][0].color, channel[1][0].color, channel[2][0].color, channel[3][0].color);
    }

  if (x>=1)
    {
      return QColor::fromRgbF(channel[0][N[0]-1].color, channel[1][N[1]-1].color, channel[2][N[2]-1].color, channel[3][N[3]-1].color);
    }

  for (int j = 0; j < 4; j++)
    {
      int dir = 1;
      int iref;
      qreal delta;
      QVector<TransferPoint> ch = channel[j];
      
      for (iref = 0; iref < ch.size(); iref++)
        {
          if (ch[iref].x >= x)
            {
              iref--;
              break;
            }
        }
      assert(iref < N[j]-1);
      delta = (x - ch[iref].x)/(ch[iref+1].x - ch[iref].x);
      
      rgba[j] = ch[iref].color * (1-delta) + ch[iref+1].color * delta;
    }

  return QColor::fromRgbF(rgba[0], rgba[1], rgba[2], rgba[3]);
}


void TransferFunction::rebuildTransferTexture(GLuint texture) const
{
  static const int Ntf = 256;
  
  glBindTexture(GL_TEXTURE_1D, texture);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  CHECK_GL_ERRORS;
  
  unsigned char *transferFunctionData = new unsigned char[4*Ntf];

  for(int i = 0; i < Ntf; ++i)
    {
      double x = i / qreal(Ntf);
      QColor c = getColor(x);

      cout << "R=" << c.red() << " G=" << c.green() << endl;
      transferFunctionData[4*i + 0] = c.red();
      transferFunctionData[4*i + 1] = c.green();
      transferFunctionData[4*i + 2] = c.blue();
      transferFunctionData[4*i + 3] = c.alpha();
    }

  glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA,
  	           Ntf, 0, GL_RGBA, GL_UNSIGNED_BYTE, transferFunctionData);

  delete[] transferFunctionData;

  CHECK_GL_ERRORS;
}

