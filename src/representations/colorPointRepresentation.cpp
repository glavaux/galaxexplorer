#include "includeGL.hpp"
#include <iostream>
#include <cmath>
#include <QtGui>
#include <QtOpenGL>
#include <QColor>
#include <QString>
#include <QMap>
#include <QVariant>
#include <vector>
#include "glwidget.hpp"
#include "representations/dataRepresentation.hpp"
#include "representations/colorPointRepresentation.hpp"
#include "inits.hpp"
#include "glconnect.hpp"
#include "renderers/renderer.hpp"
#include "object_helper.hpp"


using namespace GalaxExplorer;

using namespace std;


ColorPointRepresentation::ColorPointRepresentation()
{
  normalColor = QColor::fromRgbF(1.0, 1.0, 1.0);
  selectedColor = QColor::fromRgbF(1.0, 0.0, 0.0);
  pointSize = 1.0;
  has_init = false;
  haloConcentration = 0.5;
  distanceCorrection = false;
}


ColorPointRepresentation::~ColorPointRepresentation()
{
}

 
QString ColorPointRepresentation::getRepresentationName() const
{
  return "ColorPointRepresentation";
}

DataRepresentation *ColorPointRepresentation::duplicate() const
{
  ColorPointRepresentation *rep = new ColorPointRepresentation();

  rep->setName(getName());
  rep->setNormalColor(normalColor);
  rep->setSelectedColor(selectedColor);
  rep->pointSize = pointSize;
  rep->distanceCorrection = distanceCorrection;
  rep->haloConcentration = haloConcentration;

  return rep;
}

void ColorPointRepresentation::setNormalColor(const QColor& normal)
{
  normalColor = normal;
}

QColor ColorPointRepresentation::getNormalColor() const
{
  return normalColor;
}

void ColorPointRepresentation::setSelectedColor(const QColor& selected)
{
  selectedColor = selected;
}

QColor ColorPointRepresentation::getSelectedColor() const
{
  return selectedColor;
}

void ColorPointRepresentation::saveState(QMap<QString,QVariant>& data) const
{
  data["normalColor"] = normalColor;
  data["selectedColor"] = selectedColor;
  data["pointSize"] = pointSize;
  data["distanceCorrection"] = distanceCorrection;
  data["haloConcentration"] = haloConcentration;
}

void ColorPointRepresentation::loadState(const QMap<QString,QVariant>& data) 
{
  normalColor = data["normalColor"].value<QColor>();
  selectedColor = data["selectedColor"].value<QColor>();
  pointSize = data["pointSize"].toDouble();
  distanceCorrection = data["distanceCorrection"].toBool();
  haloConcentration = data["haloConcentration"].toDouble();
}

void ColorPointRepresentation::delayedInit(UniverseWidget& universe)
{
  if (has_init)
    return;

  has_init = true;

  if (universe.getRenderer()->usingShaders())
    {
      GLSLShaderObject surfF("/point/pointFragment.glsl", GL_FRAGMENT_SHADER);
      GLSLShaderObject surfV("/point/pointVertex.glsl", GL_VERTEX_SHADER);

      myShader.attachShader(surfF);
      myShader.attachShader(surfV);
    }
}

void ColorPointRepresentation::prepareDrawData(UniverseWidget& universe,
					    GLConnector& c,
					    DataSetConstIterator begin,
					    DataSetConstIterator end)
{
  GLRenderer *r = universe.getRenderer();

  glColor3f(normalColor.redF(), normalColor.greenF(), normalColor.blueF());
  
  delayedInit(universe);

  if (r->usingShaders() && !c.prepared()) {
    r->assembleProgramShader(c, myShader, 0);
  }
  
  c.setPrepared(true);

  int vboId;
  int attr_id_r = !auxFieldRed.isEmpty() ? begin.getAttributeID(auxFieldRed) : -1;
  int attr_id_g = !auxFieldGreen.isEmpty() ? begin.getAttributeID(auxFieldGreen) : -1;
  int attr_id_b = !auxFieldBlue.isEmpty() ? begin.getAttributeID(auxFieldBlue) : -1;
  int attr_id_a = !auxFieldAlpha.isEmpty() ? begin.getAttributeID(auxFieldAlpha) : -1;
  VBO_Descriptor desc;


  desc.subcolors = true;
  desc.numcomponents = 4;
  if (c.numberOfVBO() == 0)
    {
      vboId = c.allocateVBO(desc);
    }
  else
    {
      VBO_Object o(desc);
      c.getVBO(0).transfer(o);
      vboId = 0;
    }

  if (attr_id_r < 0 || attr_id_g < 0 || attr_id_b < 0 || attr_id_a < 0) {
    // Empty VBO
    QErrorMessage::qtHandler()->showMessage(tr("Missing color channels in datasets provided to ColorPointRepresentation"));
    return;
  }
    
  VBO_Object& vbo = c.getVBO(vboId);

  vbo.begin();
  
  DataSet::const_iterator iter = begin;
  bool colorIsSelected = false;
  while (iter != end)
    {
      const BarePoint& p = *iter;
      
      vbo.addPoint(distanceMultiplier*p.x, distanceMultiplier*p.y, distanceMultiplier*p.z);

      vbo.setColor(iter.getAttribute<float>(attr_id_r), 
                  iter.getAttribute<float>(attr_id_g), 
                  iter.getAttribute<float>(attr_id_b), 
                  iter.getAttribute<float>(attr_id_a));

      ++iter;
    }
  vbo.end();
}


void ColorPointRepresentation::realDrawData(UniverseWidget& universe,
					 GLConnector& c)
{
  GLRenderer *r = universe.getRenderer();
  GLSLProgramObject& s = r->getShader(c, 0);
  int intDcor = distanceCorrection ? 1 : 0;
  GLfloat ps = pointSize;
  
  if (r->getInteractiveMode())
    ps /= r->getInteractiveResolution();

  r->useShaderInConnector(c, 0);
  glEnable( GL_VERTEX_PROGRAM_POINT_SIZE );
  s.setUniform("pointSize", &ps, 1);
  s.setIntUniform("distanceCorrection", &intDcor, 1);
  s.setUniform("haloConcentration", &haloConcentration, 1);
  
  c.getVBO(0).draw(GL_POINTS);
}

