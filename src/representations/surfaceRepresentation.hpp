/*+
This is GalaxExplorer (./src/representations/surfaceRepresentation.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __SURFACE_REPRESENTATION_HPP
#define __SURFACE_REPRESENTATION_HPP

#include <QtOpenGL>
#include "GLSLProgramObject.h"
#include <QObject>
#include <QColor>
#include "transferFunction.hpp"
#include "representations/dataRepresentation.hpp"
#include "array3d.hpp"

namespace GalaxExplorer
{
  class DataGrid;
  class VBO_Object;
  class GLRenderer;

  class SurfaceRepresentation: public DataGridCapableRepresentation
  {
    Q_OBJECT

    Q_PROPERTY(double thresholdValue READ getThresholdValue WRITE setThresholdValue)
    Q_PROPERTY(double transparencyLevel READ getTransparency WRITE setTranparency)
    Q_PROPERTY(double showValueMin READ getShowMin WRITE setShowMin)
    Q_PROPERTY(double showValueMax READ getShowMax WRITE setShowMax)
    Q_PROPERTY(bool switchSurface READ getSurfaceSwitched WRITE switchSurface)
    Q_PROPERTY(bool enableClipping READ getClipping WRITE setClipping)
    Q_PROPERTY(double clippingMinDistance READ getClippingMin WRITE setClippingMin)
    Q_PROPERTY(double clippingMaxDistance READ getClippingMax WRITE setClippingMax)
    Q_PROPERTY(QColor baseSurfaceColor READ getBaseColor WRITE setBaseColor)
    Q_PROPERTY(bool displacementEnabled READ isDisplacementEnabled WRITE enableDisplacement)
    Q_PROPERTY(double specularValue READ getSpecularValue WRITE setSpecularValue)
    Q_PROPERTY(double displacementMultiplier READ getDisplacementMul WRITE setDisplacementMul)
    Q_PROPERTY(double shininess READ getShininess WRITE setShininess)
    Q_PROPERTY(double specularTransparency READ getSpecularAlpha WRITE setSpecularAlpha)
    Q_PROPERTY(GalaxExplorer::TransferFunction transferFunction READ getTransferFunction WRITE setTransferFunction)
    Q_PROPERTY(bool useAuxiliaryField READ getUseAuxField WRITE setUseAuxField)
  public:
    SurfaceRepresentation();
    virtual ~SurfaceRepresentation();
    
    virtual void prepareDrawGrid(UniverseWidget& universe,
				 GLConnector& c);
    virtual void realDrawGrid(UniverseWidget& universe,
			      GLConnector& c);

    virtual DataRepresentation *duplicate() const;
    virtual QString getRepresentationName() const;

    TransferFunction getTransferFunction()  const { return transferFunction; }
    void setTransferFunction(TransferFunction f);
    
    void setUseAuxField(bool b) { useAuxField = b; }
    bool getUseAuxField() const { return useAuxField; }

    float getShininess() const { return fShininess; }
    void setShininess(float val) { fShininess = val; }

    float getSpecularValue() const { return specularValue; }
    void setSpecularValue(float val) { specularValue = val; }

    bool isDisplacementEnabled() const { return displacementEnabled; }
    void enableDisplacement(bool e) { displacementEnabled = e; }

    QColor getBaseColor() const { return baseColor; }
    void setBaseColor(const QColor& col) { baseColor = col; }

    double getThresholdValue() const { return thresholdValue; }
    void setThresholdValue(double t) { thresholdValue = t; }

    double getTransparency() const { return transparencyLevel; }
    void setTranparency(double t) { transparencyLevel = t; }

    double getShowMin() const { return showMin; }
    double getShowMax() const { return showMax; }

    void setShowMin(double smin)  { showMin = smin; }
    void setShowMax(double smax)  { showMax = smax; }

    bool getSurfaceSwitched() const { return switchNormals; }
    void switchSurface(bool s) { switchNormals = s; }

    bool getClipping() const { return clippingEnabled; }
    void setClipping(bool clip) { clippingEnabled = clip; }
   
    double getClippingMin() const { return minClipDistance; }
    void setClippingMin(double mn) { minClipDistance = mn; }
    double getClippingMax() const { return maxClipDistance; }
    void setClippingMax(double mx) { maxClipDistance = mx; }

    double getDisplacementMul() const  { return mulDisplacement; }
    void setDisplacementMul(double mul)  { mulDisplacement = mul; }
   
    virtual void saveState(QMap<QString,QVariant>& data) const;
    virtual void loadState(const QMap<QString,QVariant>& data);

    double getSpecularAlpha() const { return specularTransparency; }
    void setSpecularAlpha(double d) { specularTransparency = d; }

  protected:
    template<bool clipping, bool switchFace> void marchCube(DataGrid *data, DataGrid *auxData, VBO_Object& vbo, int ix, int iy, int iz, float *step3d);
    template<bool clipping, bool switchFace> void marchVolume(DataGrid *data, DataGrid *auxData, VBO_Object& vbo, int Nx, int Ny, int Nz, float *step3d);
    void prepareLighting(GLRenderer *r, GLConnector& c, GLSLProgramObject& p);
    void delayedInit(UniverseWidget& w);

    void rebuildTransferTexture();

    double mulDisplacement;
    bool displacementEnabled;
    bool has_init;
    GLSLProgramObject myShader, myShaderAux;
    double thresholdValue;
    double transparencyLevel;
    double showMin, showMax;
    bool switchNormals;
    bool clippingEnabled;
    double minClipDistance, maxClipDistance;
    QColor baseColor;
    float specularValue;
    float fShininess;
    float specularTransparency;
    bool useAuxField;
    TransferFunction transferFunction;
    GLuint transferFunctionTexture;
    bool createdTexture, rebuildTransfer;
  };

};

#endif
