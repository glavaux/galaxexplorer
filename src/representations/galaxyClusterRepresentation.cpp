/*+
This is GalaxExplorer (./src/representations/galaxyClusterRepresentation.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <QtCore>
#include <QtGui>
#include "GLee.h"
#include <iostream>
#include <cmath>
#include <QColor>
#include <vector>
#include "representations/dataRepresentation.hpp"
#include "inits.hpp"
#include "representations/galaxyRepresentation.hpp"
#include "renderer.hpp"

using namespace GalaxExplorer;
using namespace std;

#define CHECK_GL_ERRORS \
{ \
    GLenum err = glGetError(); \
    if (err) \
      printf( "Error %x at line %s:%d\n", err, __FILE__, __LINE__); \
}


GalaxyClusterRepresentation::GalaxyClusterRepresentation()
{
  has_init = false;
  galaxySize = 100;
}

GalaxyClusterRepresentation::~GalaxyClusterRepresentation()
{
  if (has_init)
    glDeleteTextures(1, &texture);
}
  
void GalaxyClusterRepresentation::loadState(QDataStream& d)
{
  d >> galaxySize;
}

void GalaxyClusterRepresentation::saveState(QDataStream& d) const
{
  d << galaxySize;
}

void GalaxyClusterRepresentation::delayedInit(UniverseWidget& universe)
{
  if (has_init)
    return;

  has_init = true;

  img = QImage("textures:galaxy.png");

  glEnable(GL_TEXTURE_2D);
  texture = universe.bindTexture(img, GL_TEXTURE_2D, GL_RGBA);
  CHECK_GL_ERRORS;

  if (universe.getRenderer()->usingShaders())
    {
      GLSLShaderObject galaxyF("galaxyFragment.glsl", GL_FRAGMENT_SHADER);
      GLSLShaderObject galaxyV("galaxyVertex.glsl", GL_VERTEX_SHADER);

      myShader.attachShader(galaxyF);
      myShader.attachShader(galaxyV);

      CHECK_GL_ERRORS;
    }
  glUseProgram(0);
}

static float refCorners[][3] = {
  { -0.5, -0.5, 0 },
  { +0.5, -0.5, 0 },
  { +0.5, +0.5, 0 },
  { -0.5, +0.5, 0 }
};

static void fixedRotation(const DataPoint& p, float *x1, float *x2, float *x3, float *x4, float galaxySize)
{
  float r[3] = {p.x, p.y, p.z };
  for (int j = 0; j < 3; j++)
    {
      x1[j] = refCorners[0][j] + 
      x1[j] = x1[j] * galaxySize + r[j];
      x2[j] = refCorners[1][j];
      x2[j] = x2[j] * galaxySize + r[j];
      x3[j] =  refCorners[2][j];
      x3[j] = x3[j] * galaxySize + r[j];
      x4[j] = refCorners[3][j];
      x4[j] = x4[j] * galaxySize + r[j];
    }
}

void GalaxyClusterRepresentation::realDrawData(UniverseWidget& universe,
					       GLConnector& connector)
{
  GLRenderer *r = universe.getRenderer();
 
  glPushMatrix();
  glLoadIdentity();
  glTranslatef(0,0,-universe.getState()->getDistance());

  if (!r->usingShaders())
    {  
      glUseProgram(0);
      glEnable(GL_TEXTURE_2D);
      glEnable(GL_BLEND);
      glDisable(GL_DEPTH_TEST);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
      glBindTexture( GL_TEXTURE_2D, texture );
      CHECK_GL_ERRORS;

      glCallList(connector.getList());

      CHECK_GL_ERRORS;
      glDisable(GL_BLEND);
      glEnable(GL_DEPTH_TEST);
      glDisable(GL_TEXTURE_2D);

      glPopMatrix();
      return;
    }
  
  glEnable(GL_TEXTURE_2D);

  CHECK_GL_ERRORS;
  r->useShaderInConnector(connector, 0);

  CHECK_GL_ERRORS;
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, texture);
  CHECK_GL_ERRORS;

  glCallList(connector.getList());

  r->useDefaultShader();
  CHECK_GL_ERRORS;

  glPopMatrix();
}

static void applyCoordinateShader(float x, float y)
{
  glMultiTexCoord2f(GL_TEXTURE1, x, y);
}

static void applyCoordinateSimple(float x, float y)
{
  glTexCoord2f(x,y);
}

void GalaxyClusterRepresentation::prepareDrawData(UniverseWidget& universe,
						  GLConnector& connector,
						  DataSetConstIterator begin,
						  DataSetConstIterator end)
{
  GLRenderer *r = universe.getRenderer();
  void (*applyCoord)(float,float) = 0;

  delayedInit(universe);

  if (r->usingShaders() && !connector.prepared()) {
      r->assembleProgramShader(connector, myShader, 0);
      //      r->shaderBindTexture(GL_TEXTURE_2D, connector, 0, "Galaxy", 1);
      applyCoord = applyCoordinateShader;
      CHECK_GL_ERRORS;
  }
  else
    applyCoord = applyCoordinateSimple;

  connector.setPrepared(true);
 
  glNewList(connector.getList(), GL_COMPILE);

  glDisable(GL_CULL_FACE);

  glBegin(GL_QUADS);
  glColor4f(1.0,1.0,1.0,1.0);
  DataSet::const_iterator iter = begin;
  bool colorIsSelected = false;
  while (iter != end)
    {
      const DataPoint& p = *iter;
      float x1[3], x2[3], x3[3], x4[3];
      
      randomRotation(p, x1, x2, x3, x4, galaxySize);
      
      applyCoord(0, 0); 
      glVertex3f(distanceMultiplier*x1[0], distanceMultiplier*x1[1],
		 distanceMultiplier*x1[2]);
      applyCoord(1, 0);
      glVertex3f(distanceMultiplier*x2[0], distanceMultiplier*x2[1],
		 distanceMultiplier*x2[2]);
      applyCoord(1, 1);
      glVertex3f(distanceMultiplier*x3[0], distanceMultiplier*x3[1],
		 distanceMultiplier*x3[2]);
      applyCoord(0, 1); 
      glVertex3f(distanceMultiplier*x4[0], distanceMultiplier*x4[1],
		 distanceMultiplier*x4[2]);

      ++iter;
    }
  glEnd();

  glEnable(GL_CULL_FACE);

  CHECK_GL_ERRORS;

  glEndList();

}

DataRepresentation *GalaxyClusterRepresentation::duplicate() const
{
  GalaxyClusterRepresentation *g = new GalaxyClusterRepresentation();

  g->setName(getName());
  
  return g;
}

QString GalaxyClusterRepresentation::getRepresentationName() const
{
  return "GalaxyClusterRepresentation";
}
