#ifndef __GALAX_TRANSFER_FUNCTION_HPP
#define __GALAX_TRANSFER_FUNCTION_HPP

#include "includeGL.hpp"
#include <QtCore>
#include <QVector>
#include <QColor>

namespace GalaxExplorer
{
  struct TransferPoint {
    qreal x;
    qreal color;
  };
  
  struct TransferFunction {
    QVector<TransferPoint> channel[4];
    
    TransferFunction();
    
    QColor getColor(qreal x) const;
    void rebuildTransferTexture(GLuint texture) const;
  };

};

Q_DECLARE_METATYPE ( GalaxExplorer::TransferFunction )


#endif
