/*+
This is GalaxExplorer (./src/representations/vectorRepresentation.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include "includeGL.hpp"
#include <cmath>
#include <QColor>
#include <QGLWidget>
#include "representations/vectorRepresentation.hpp"
#include "glwidget.hpp"
#include "gltools.hpp"

#define NUM_FACETS 5

using namespace std;
using namespace GalaxExplorer;

VectorRepresentation::VectorRepresentation()
{
  myColor = QColor::fromRgbF(0.3, 0.7, 0.4);
  coneColor = QColor::fromRgbF(1.0,0.0,0.0);
  fractionVeloc = 10.0;
  headSize = 0.4;
  headRadius = 0.1;
}

void VectorRepresentation::saveState(QDataStream& d) const
{
  d << myColor << coneColor << fractionVeloc << headSize << headRadius;
}

void VectorRepresentation::loadState(QDataStream& d)
{
  d >> myColor >> coneColor >> fractionVeloc >> headSize >> headRadius;
}

VectorRepresentation::~VectorRepresentation()
{
}

void VectorRepresentation::drawData(UniverseWidget& universe,
				    DataSetConstIterator begin,
				    DataSetConstIterator end)
{
  double m = distanceMultiplier;
  int aVx = begin.getAttributeID("velX");
  int aVy = begin.getAttributeID("velY");
  int aVz = begin.getAttributeID("velZ");

  glColor3f(myColor.redF(), myColor.greenF(), myColor.blueF());
  glBegin(GL_LINES);

  DataSetConstIterator iter = begin;
  while (iter != end)
    {
      const BarePoint& p = *iter;
      float vx = iter.getAttribute<float>(aVx); 
      float vy = iter.getAttribute<float>(aVy); 
      float vz = iter.getAttribute<float>(aVz); 

      double deltax = vx*.01*fractionVeloc;
      double deltay = vy*.01*fractionVeloc;
      double deltaz = vz*.01*fractionVeloc;
 
      glVertex3f(p.x*m, p.y*m, p.z*m);
      glVertex3f((p.x+deltax)*m, (p.y+deltay)*m, (p.z+deltaz)*m);

      ++iter;
    }

  glEnd();

  glColor3f(coneColor.redF(), coneColor.greenF(), coneColor.blueF());
  iter = begin;
  while (iter != end)
    {
      const BarePoint& p = *iter;
      float vx = iter.getAttribute<float>(aVx); 
      float vy = iter.getAttribute<float>(aVy); 
      float vz = iter.getAttribute<float>(aVz); 

      double deltax = vx*.01*fractionVeloc;
      double deltay = vy*.01*fractionVeloc;
      double deltaz = vz*.01*fractionVeloc;
      double n = sqrt(deltax*deltax + deltay*deltay + deltaz*deltaz)*m;
      double ux = deltax/n;
      double uy = deltay/n;
      double uz = deltaz/n;

      drawCone(m*(p.x+deltax), m*(p.y+deltay), m*(p.z+deltaz), headSize*n, headRadius*n, ux, uy, uz, NUM_FACETS);

      ++iter;
    }
}  

QString VectorRepresentation::getRepresentationName() const 
{
  return "VectorRepresentation";
}

DataRepresentation *VectorRepresentation::duplicate() const
{
  VectorRepresentation *rep = new VectorRepresentation();

  rep->setName(getName());

  return rep;
}
