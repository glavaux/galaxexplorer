#ifndef __VOLUME_DATA_REPRESENTATION_HPP
#define __VOLUME_DATA_REPRESENTATION_HPP

#include <QObject>
#include <QColor>
#include <vector>
#include <utility>
#include <QMap>
#include "points.hpp"
#include "datasets.hpp"
#include "triplet.hpp"
#include <iostream>
#include "GLSLProgramObject.h"
#include "glwidget.hpp"
#include "representations/dataRepresentation.hpp"
#include "renderers/renderer.hpp"

namespace GalaxExplorer
{
  class UniverseWidget;  
  class GLConnector;

  class VolumeRepresentation: public DataGridCapableRepresentation
  {
    Q_OBJECT
  public:
    VolumeRepresentation();
    virtual ~VolumeRepresentation();
         
    virtual void loadState(const QMap<QString,QVariant>& d);
    virtual void saveState(QMap<QString,QVariant>& d) const;

    virtual void prepareDrawGrid(UniverseWidget& universe,
				 GLConnector& c);
    virtual void realDrawGrid(UniverseWidget& universe,
			      GLConnector& c);
    
    virtual DataRepresentation *duplicate() const;

    virtual QString getRepresentationName() const;
  protected:
    GLSLProgramObject shaderOutPosition, shaderRaytrace;
    GLuint transferTex, cubeTex;
    bool has_init;

    void delayedInit(UniverseWidget& w);
  };

};

#endif
