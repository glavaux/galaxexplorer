/*+
This is GalaxExplorer (./src/representations/galaxyRepresentation.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include "includeGL.hpp"
#include <QtCore>
#include <QtGui>
#include <iostream>
#include <cmath>
#include <QColor>
#include <vector>
#include "dataRepresentation.hpp"
#include "inits.hpp"
#include "galaxyRepresentation.hpp"
#include "renderers/renderer.hpp"
#include "vboObject.hpp"
#include "gltools.hpp"

using namespace GalaxExplorer;
using namespace std;

#define CHECK_GL_ERRORS \
{ \
    GLenum err = glGetError(); \
    if (err) \
      printf( "Error %x at line %s:%d\n", err, __FILE__, __LINE__); \
}


GalaxyRepresentation::GalaxyRepresentation()
{
  has_init = false;
  galaxySize = 100;
  billboard = true;
  galaxyTextureFilename = "galaxy.png";
  autoTransparency = false;
  alphaPower = 0.0;
}

GalaxyRepresentation::~GalaxyRepresentation()
{
  if (has_init)
    glDeleteTextures(1, &texture);
}
  
void GalaxyRepresentation::loadState(const QMap<QString,QVariant>& d)
{
  galaxySize = d["galaxySize"].toDouble();
  galaxyTextureFilename = d["galaxyTexture"].toString();
  autoTransparency = d["autoTransparency"].toBool();
  alphaPower = d["alphaPower"].toDouble();

  if (has_init)
    {
      img = QImage("textures:" + galaxyTextureFilename);
    }
}

void GalaxyRepresentation::saveState(QMap<QString,QVariant>& d) const
{
  d["galaxySize"] = galaxySize;
  d["galaxyTexture"] = galaxyTextureFilename;
  d["autoTransparency"] = autoTransparency;
  d["alphaPower"] = alphaPower;
}

void GalaxyRepresentation::delayedInit(UniverseWidget& universe)
{
  if (has_init)
    return;

  need_rebind = false;
  has_init = true;

  img = QImage(QString("textures:") + galaxyTextureFilename);

  glEnable(GL_TEXTURE_2D);
  texture = universe.glwidget()->bindTexture(img, GL_TEXTURE_2D, GL_RGBA);
  CHECK_GL_ERRORS;

  if (universe.getRenderer()->usingShaders())
    {
      GLSLShaderObject galaxyF("/galaxy/galaxyFragment.glsl", GL_FRAGMENT_SHADER);
      GLSLShaderObject galaxyV("/galaxy/galaxyVertex.glsl", GL_VERTEX_SHADER);
      GLSLShaderObject galaxyV_B("/galaxy/galaxyBillboardVertex.glsl", GL_VERTEX_SHADER);
      
      myShader.attachShader(galaxyF);
      myShader.attachShader(galaxyV);

      myBillboardShader.attachShader(galaxyF);
      myBillboardShader.attachShader(galaxyV_B);

      CHECK_GL_ERRORS;
    }
  glUseProgram(0);
}

static double refCorners[][3] = {
  { 0, -0.5, -0.5 },
  { 0, +0.5, -0.5 },
  { 0, +0.5, +0.5 },
  { 0, -0.5, +0.5 }
};

static void randomRotation(DataSetConstIterator& iter,
			   int nx_id, int ny_id, int nz_id,
			   float *x1, float *x2, float *x3, float *x4, float galaxySize)
{
  const BarePoint& p = *iter;
  float M[3][3];
  float alpha, beta;
  
  alpha = 2*M_PI*drand48();
  beta = 2*M_PI*drand48();

  double c_alpha = cos(alpha), s_alpha = sin(alpha);
  double c_beta = cos(beta), s_beta = sin(beta);

  M[0][0] = c_alpha;
  M[1][0] = s_alpha;
  M[2][0] = 0;

  M[0][1] = -s_alpha * c_beta;
  M[1][1] = c_alpha * c_beta;
  M[2][1] = s_beta;

  M[0][2] = s_beta * s_alpha;
  M[1][2] = -s_beta * c_alpha;
  M[2][2] = c_beta;

  float r[3] = {p.x, p.y, p.z };
  for (int j = 0; j < 3; j++)
    {
      x1[j] = 
	M[j][0] * refCorners[0][0] + 
	M[j][1] * refCorners[0][1] + 
	M[j][2] * refCorners[0][2];
      x1[j] = x1[j] * galaxySize + r[j];
      x2[j] = 
	M[j][0] * refCorners[1][0] + 
	M[j][1] * refCorners[1][1] + 
	M[j][2] * refCorners[1][2];
      x2[j] = x2[j] * galaxySize + r[j];
      x3[j] = 
	M[j][0] * refCorners[2][0] + 
	M[j][1] * refCorners[2][1] + 
	M[j][2] * refCorners[2][2];
      x3[j] = x3[j] * galaxySize + r[j];
      x4[j] =
	M[j][0] * refCorners[3][0] + 
	M[j][1] * refCorners[3][1] + 
	M[j][2] * refCorners[3][2];
      x4[j] = x4[j] * galaxySize + r[j];
    }
}

static void matrixMultiply(GLdouble* M, double* v, double* v2)
{
  for (int i = 0; i < 3; i++)
    {
      v2[i] = 0;
      for (int j = 0; j < 3; j++)
	{
	  v2[i] += M[i+4*j] * v[j];
	}
    }
}

static void applyRealRotation(DataSetConstIterator& iter,
			      int nx_id, int ny_id, int nz_id,
			      float *x1, float *x2, float *x3, float *x4, float galaxySize)
{
  const BarePoint& p = *iter;
  float nx = iter.getAttribute<float>(nx_id);
  float ny = iter.getAttribute<float>(ny_id);
  float nz = iter.getAttribute<float>(nz_id);
  GLdouble M[16];

  cout << "Do a real rotation (" << nx << "," << ny << "," << nz << ")" << endl;

  buildMatrixLookAt(M, nz, ny, nx);

  double r[3] = {p.x, p.y, p.z };
  double y[4][3];

  for (int j = 0; j < 4; j++)
    matrixMultiply(M, refCorners[j], y[j]);

  for (int j = 0; j < 3; j++)
    {
      x1[j] = y[0][j] * galaxySize + r[j];
      x2[j] = y[1][j] * galaxySize + r[j];
      x3[j] = y[2][j] * galaxySize + r[j];
      x4[j] = y[3][j] * galaxySize + r[j];
    }
}

void GalaxyRepresentation::realDrawData(UniverseWidget& universe,
					GLConnector& connector)
{
  GLRenderer *r = universe.getRenderer();

  glDisable(GL_CULL_FACE);

  if (!r->usingShaders())
    {  
      glUseProgram(0);
      glEnable(GL_TEXTURE_2D);
      glEnable(GL_BLEND);
      glDisable(GL_DEPTH_TEST);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
      glBindTexture( GL_TEXTURE_2D, texture );
      CHECK_GL_ERRORS;

    //  glCallList(connector.getList());

      connector.getVBO(0).draw(GL_QUADS);

      CHECK_GL_ERRORS;
      glDisable(GL_BLEND);
      glEnable(GL_DEPTH_TEST);
      glDisable(GL_TEXTURE_2D);
      return;
    }

  CHECK_GL_ERRORS;
  r->useShaderInConnector(connector, 0);

  if (autoTransparency && alphaPower > 0)
    r->getShader(connector, 0).setUniform("autoAlpha", &alphaPower, 1); 
  else {
    float p = -1.0;
    r->getShader(connector, 0).setUniform("autoAlpha", &p, 1); 
  }

  if (billboard)
    {
      GLfloat bMatrix[4][4];
      GLfloat scale = galaxySize * distanceMultiplier;
      
      universe.getState()->getRotationMatrix(bMatrix);
      //      r->getShader(connector, 0).setMatrixUniform("billboardRotation", &bMatrix[0][0], 4);
      r->getShader(connector, 0).setUniform("scale", &scale, 1);
    }

  CHECK_GL_ERRORS;
  r->getShader(connector, 0).bindTexture2D("Galaxy", texture);
  
  glColor4f(1.0,1.0,1.0,1.0);
  connector.getVBO(0).
    updateAttributeLocation(0,
			 r->getShader(connector, 0).
			    getAttribLocation("s_galaxy_coord"));

  connector.getVBO(0).draw(GL_QUADS);
  CHECK_GL_ERRORS;

  r->useDefaultShader();
  CHECK_GL_ERRORS;
}

void GalaxyRepresentation::prepareDrawData(UniverseWidget& universe,
					   GLConnector& connector,
					   DataSetConstIterator begin,
					   DataSetConstIterator end)
{
  GLRenderer *r = universe.getRenderer();
  void (*applyCoord)(VBO_Object& vbo, float,float) = 0;
  int galaxy_attribute;
  void (*computeRotation)(DataSetConstIterator& iter,
			  int nx_id, int ny_id, int nz_id,
			  float *x1, float *x2, float *x3, float *x4, float galaxySize);

  delayedInit(universe);
  cout << "Compiling galaxies..." << endl;

  if (!r->usingShaders())
    {
      abort();
    }

  if (billboard)
    {
      cout << "Using billboard shader." << endl;
      r->assembleProgramShader(connector, myBillboardShader, 0);
    }
  else
    r->assembleProgramShader(connector, myShader, 0);

  if (need_rebind) {
    glDeleteTextures(1, &texture);
    texture = universe.glwidget()->bindTexture(img, GL_TEXTURE_2D, GL_RGBA);
    need_rebind=false;
  }

  //r->shaderBindTexture(GL_TEXTURE_2D, connector, 0, "Galaxy");

  galaxy_attribute = r->getShader(connector, 0).getAttribLocation("s_galaxy_coord");
  assert(galaxy_attribute >= 0);
  connector.setPrepared(true); 
 
  int vboId;
  if (connector.numberOfVBO() == 0)
    {
      VBO_Descriptor desc;
      
      desc.numAttribs = 1;
      desc.attribIndex = new GLuint[1];
      desc.attribSizes = new int[1];
      desc.attribIndex[0] = galaxy_attribute;
      desc.attribSizes[0] = 2;

      vboId = connector.allocateVBO(desc);
    }
  else
    vboId = 0;

  VBO_Object& vbo = connector.getVBO(vboId);

  vbo.begin();

  DataSet::const_iterator iter = begin;
  bool colorIsSelected = false;
  int nx_id = iter.getAttributeID("normal_x");
  int ny_id = iter.getAttributeID("normal_y");
  int nz_id = iter.getAttributeID("normal_z");

  cout << nx_id << " " << ny_id << " " << nz_id << endl;

  if (nx_id >= 0 && ny_id >= 0 && nz_id >= 0)
    computeRotation = applyRealRotation;
  else
    computeRotation = randomRotation;

  while (iter != end)
    {
      const BarePoint& p = *iter;
      float x1[3], x2[3], x3[3], x4[3];

      if (!billboard)
	{	  
	  computeRotation(iter, nx_id, ny_id, nz_id, x1, x2, x3, x4, galaxySize);
	}
      else
	{
          float xyz[3] = {p.x,p.y,p.z};
	  for (int j = 0; j < 3; j++)
	    {
	      x1[j] = distanceMultiplier*xyz[j];
	      x2[j] = distanceMultiplier*xyz[j];
	      x3[j] = distanceMultiplier*xyz[j];
	      x4[j] = distanceMultiplier*xyz[j];
	    }
	}

      vbo.addPoint(x1[0], x1[1], x1[2]);
      {
	GLfloat xy[2] = {0,0};
	vbo.setAttrib(0, xy);
      }

      vbo.addPoint(x2[0], x2[1], x2[2]);
      {
	GLfloat xy[2] = {1,0};
	vbo.setAttrib(0, xy);
      }

      vbo.addPoint(x3[0], x3[1], x3[2]);
      {
	GLfloat xy[2] = {1,1};
	vbo.setAttrib(0, xy);
      }
      
      vbo.addPoint(x4[0], x4[1], x4[2]);
      {
	GLfloat xy[2] = {0,1};
	vbo.setAttrib(0, xy);
      }
      ++iter;
    }

  vbo.end();

  CHECK_GL_ERRORS;
}

DataRepresentation *GalaxyRepresentation::duplicate() const
{
  GalaxyRepresentation *g = new GalaxyRepresentation();

  g->setName(getName());
  
  return g;
}

QString GalaxyRepresentation::getRepresentationName() const
{
  return "GalaxyRepresentation";
}
