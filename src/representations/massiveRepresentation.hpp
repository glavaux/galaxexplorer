/*+
This is GalaxExplorer (./src/representations/massiveRepresentation.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __MASSIVE_REPRESENTATION_HPP
#define __MASSIVE_REPRESENTATION_HPP

#include <CosmoTool/octTree.hpp>
#include "representations/dataRepresentation.hpp"

namespace GalaxExplorer
{

class MassivePointRepresentation: public DataSetCapableRepresentation
{
  Q_OBJECT
  Q_PROPERTY(QColor color READ getColor WRITE setColor STORED true)
  Q_PROPERTY(int treeDepth READ getTreeDepth WRITE setTreeDepth)
  Q_PROPERTY(double densityLimit READ getDensityLimit WRITE setDensityLimit)
  Q_PROPERTY(double lengthLimit READ getLenLimit WRITE setLenLimit)
  Q_PROPERTY(double densityMul READ getDensityMul WRITE setDensityMul)
  Q_PROPERTY(double densityLogScale READ getLogScale WRITE setLogScale)
  Q_PROPERTY(double lenNorm READ getLength WRITE setLength)
  Q_PROPERTY(double particleNorm READ getParticleResolution WRITE setParticleResolution)
public:
  MassivePointRepresentation();
  virtual ~MassivePointRepresentation();
        

  virtual void saveState(QMap<QString,QVariant>& data) const;
  virtual void loadState(const QMap<QString,QVariant>& data);

  virtual void prepareDrawData(UniverseWidget& universe,
		     GLConnector& c,
		     DataSetConstIterator begin,
		     DataSetConstIterator end);
  virtual void realDrawData(UniverseWidget& universe,
	          GLConnector& c);
    
  virtual DataRepresentation *duplicate() const;
  
  virtual QString getRepresentationName() const;

  double getLogScale() const { return densityLogScale; }
  void setLogScale(double lScale) { densityLogScale = lScale; }  

  double getDensityMul() const { return densityMul; }
  void setDensityMul(double m) { densityMul = m; }

  double getDensityLimit() const { return densityLimit; }
  void setDensityLimit(double d) { densityLimit = d; }
  
  double getLenLimit() const { return Llimit; }
  void setLenLimit(double l) { Llimit = l; }

  void setTreeDepth(int d) { maxTreeDepth = d; }
  int getTreeDepth() const { return maxTreeDepth; }
  
  QColor getColor() const { return normalColor; }
  void setColor(QColor c) { normalColor = c; }

  double getLength() const { return lenNorm; }
  void setLength(double l) { lenNorm = l; }

  double getParticleResolution() const { return particleNorm; }
  void setParticleResolution(double n) { particleNorm = n; }

protected:
  CosmoTool::OctTree *tree;
  QColor normalColor;
  int maxTreeDepth;
  float densityLimit, densityMul;
  float Llimit;
  double densityLogScale;
  double lenNorm, particleNorm;
};

};

#endif
