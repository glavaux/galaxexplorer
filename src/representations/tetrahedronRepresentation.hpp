/*+
This is GalaxExplorer (./src/representations/tetrahedronRepresentation.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __TETRAHEDRON_REPRESENTATION_HPP
#define __TETRAHEDRON_REPRESENTATION_HPP

#include "dataRepresentation.hpp"
#include "glwidget.hpp"
#include "GLSLProgramObject.h"


namespace GalaxExplorer
{

  class TetrahedronRepresentation: public DataMeshCapableRepresentation
  {
    Q_OBJECT

    Q_PROPERTY(QColor surfaceColor READ getSurfaceColor WRITE setSurfaceColor)
    Q_PROPERTY(double surfaceAlpha READ getSurfaceAlpha WRITE setSurfaceAlpha)
    Q_PROPERTY(bool smoothSurface READ isSmoothSurface WRITE setSmoothSurface)
    Q_PROPERTY(QColor specularColor READ getSpecularColor WRITE setSpecularColor)
    Q_PROPERTY(double specularAlpha READ getSpecularAlpha WRITE setSpecularAlpha)
    Q_PROPERTY(QColor ambientColor READ getAmbientColor WRITE setAmbientColor)
    Q_PROPERTY(double ambientAlpha READ getAmbientAlpha WRITE setAmbientAlpha)
    Q_PROPERTY(double shininess READ getShininess WRITE setShininess)
    Q_PROPERTY(bool showRidges READ isShowingRidges WRITE setShowRidges)
    Q_PROPERTY(bool showNormals READ isShowingNormals WRITE setShowNormals)
  private:
    QColor surfaceColor, specularColor, ambientColor;
    double surfaceAlpha, specularAlpha, ambientAlpha, shininess;
    bool smoothSurface, showRidges, showNormals;
    bool has_init;
    
    GLSLProgramObject myShader;
  public:
    TetrahedronRepresentation();
    virtual ~TetrahedronRepresentation();

    double getSurfaceAlpha() const { return surfaceAlpha; }
    void setSurfaceAlpha(double alpha) { surfaceAlpha = alpha; }
    
    QColor getSurfaceColor() const { return surfaceColor; }
    void setSurfaceColor(QColor color) { surfaceColor = color; }

    QColor getSpecularColor() const { return specularColor; }
    void setSpecularColor(QColor color) { specularColor = color; }

    double getSpecularAlpha() const { return specularAlpha; }
    void setSpecularAlpha(double alpha) { specularAlpha = alpha; }

    double getAmbientAlpha() const { return ambientAlpha; }
    void setAmbientAlpha(double a) { ambientAlpha = a; }

    QColor getAmbientColor() const { return ambientColor; }
    void setAmbientColor(QColor c) { ambientColor = c; }

    bool isSmoothSurface() const { return smoothSurface; }
    void setSmoothSurface(bool s) { smoothSurface = s; needRebuild = true; }

    bool isShowingNormals() const { return showNormals; }
    void setShowNormals(bool s) { showNormals = s; needRebuild = true; }

    double getShininess() const { return shininess; }
    void setShininess(double s) { shininess = s; }

    bool isShowingRidges() const { return showRidges; }
    void setShowRidges(bool s) { showRidges = s; needRebuild = true; }

    virtual void prepareDrawMesh(UniverseWidget& universe,
				 GLConnector& c);
    virtual void realDrawMesh(UniverseWidget& universe,
			      GLConnector& c);

    virtual DataRepresentation *duplicate() const;
    virtual QString getRepresentationName() const;

    void delayedInit(UniverseWidget& w);

  };

};


#endif
