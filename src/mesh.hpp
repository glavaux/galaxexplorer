/*+
This is GalaxExplorer (./src/mesh.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __GALAX_MESH_HPP
#define __GALAX_MESH_HPP

#include <boost/shared_array.hpp>
#include <vector>

namespace GalaxExplorer
{
  class VBO_Object;

  class GeneralMesh
  {
  public:
    struct Point {
      float xyz[3];      
      float operator[](int i) const {
	return xyz[i];
      }
    };
    typedef Point Direction;
    struct Normal {
      Point direction;
      float accum;
    };
    struct Triangle {
      int v[3];
      int adjacent[3];
    };

  public:
    template<typename PointIterator, typename TriangleIterator>
    GeneralMesh(PointIterator pstart, PointIterator pend,
		TriangleIterator tstart, TriangleIterator tend);
    //    GeneralMesh(Point *points, int numPoints,
    //		int *triangles, int numTriangles);
    ~GeneralMesh();
    
    void buildVBO(VBO_Object& vbo, bool smooth, bool keep_only_ordering = true);

    void computeTriangleOrdering(Point& eye_position, Direction& eye_look,
				 boost::shared_array<int>& order);
    void reorderVBO(Point& eye_position, Direction& eye_look,
		    VBO_Object& vbo);
    //    void build_shadow_volumes();

  private:
    std::vector<Point> all_points;
    std::vector<Point> triangle_centers;
    std::vector<Triangle> all_triangles;
  };

  template<typename PointIterator, typename TriangleIterator>
  GeneralMesh::GeneralMesh(PointIterator pstart, PointIterator pend, 
			   TriangleIterator tstart, TriangleIterator tend)
  {
    for (;pstart != pend; ++pstart)
      {
	Point p;

	p.xyz[0] = (*pstart)[0];
	p.xyz[1] = (*pstart)[1];
	p.xyz[2] = (*pstart)[2];	
	all_points.push_back(p);
      }

    all_points.resize(all_points.size());

    for (;tstart != tend; ++tstart)
      {
	Triangle t;
	int v[3] = {
	  (*tstart)[0],
	  (*tstart)[1],
	  (*tstart)[2]
	};

	t.v[0] = v[0];
	t.v[1] = v[1];
	t.v[2] = v[2];
	
	all_triangles.push_back(t);

	Point tc;
	for (int j = 0; j < 3; j++)
	  {	  
	    float p = 0;
	    for (int k = 0; k < 3; k++)
	      p += all_points[k].xyz[j]; 
	    p /= 3;
	  
	    tc.xyz[j] = p;
	  }

	triangle_centers.push_back(tc);
      }
    all_triangles.resize(all_triangles.size());
    triangle_centers.resize(triangle_centers.size());
  }
};

#endif


