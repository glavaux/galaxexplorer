/*+
This is GalaxExplorer (./src/vboObject.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __VBO_OBJECT_HPP
#define __VBO_OBJECT_HPP

#include "includeGL.hpp"
#include <cstdlib>
#include <vector>
#include "includeGL.hpp"
#include <exception>
#include <algorithm>
#include <boost/shared_array.hpp>

namespace GalaxExplorer
{
  class VBO_OutOfMemory : public std::exception {};

  class VBO_InvalidParameter : public std::exception {};

  class VBO_Descriptor
  {
  public:
    int numcomponents, numMulti, numAttribs;
    bool subvertices, subcolors, subnormals, tex2d, tex3d;
    GLint *multiTexId;
    GLuint *attribIndex;
    int *attribSizes;
    bool dynamicDraw;
  public:
    VBO_Descriptor()
    {
      dynamicDraw = false;
      numcomponents = 0;
      numAttribs = 0;
      attribSizes = 0;
      subvertices = true; 
      subcolors = false;
      subnormals = false;
      tex2d = false; 
      tex3d = false;
      numMulti = 0;
      multiTexId = 0;
      attribIndex = 0;
    }

    ~VBO_Descriptor()
    {
      if (multiTexId)
	delete[] multiTexId;
      if (attribSizes)
	delete[] attribSizes;
      if (attribIndex)
	delete[] attribIndex;
    }
  };

  class FlexiblePointAccessor 
  {
  public:
    enum FlexibleType
      {
	VERTEX_X = 0,
	VERTEX_Y,
	VERTEX_Z,
	COLOR_R,
	COLOR_G,
	COLOR_B,
	COLOR_A,
	NORMAL_X,
	NORMAL_Y,
	NORMAL_Z,
	TEX2D_X,
	TEX2D_Y,
	TEX3D_X,
	TEX3D_Y,
	TEX3D_Z,
	NUM_TYPES
      };

  private:
    int m_num;
    bool m_colors, m_vertex, m_normals, m_tex2d, m_tex3d;
    long element_size, float_size;
    int numMultiTextures;
    long *multiTextureOffset;
    GLint *multiTexId;
    long offset_type[NUM_TYPES];
    GLuint *attribIndex;
    long *attribOffset;
    int *attribSize;
    int numAttribs;
  public:
    FlexiblePointAccessor(const VBO_Descriptor& d)
      throw (VBO_InvalidParameter)
        : m_num(d.numcomponents), m_colors(d.subcolors), 
        m_vertex(d.subvertices), m_normals(d.subnormals), 
        m_tex2d(d.tex2d), m_tex3d(d.tex3d), numMultiTextures(d.numMulti),
        numAttribs(d.numAttribs)
    {
      int numfloat;

      if (numMultiTextures > 0)
        {
          multiTextureOffset = new long[numMultiTextures];
          multiTexId = new GLint[numMultiTextures];
        }
      else
        {
          multiTextureOffset = 0;
          multiTexId = 0;
        }

      if (numAttribs > 0)
        {
          attribOffset = new long[numAttribs];
          attribSize = new int[numAttribs];
          attribIndex = new GLuint[numAttribs];
        }

      element_size = 0;
      numfloat = 0;

      if (m_vertex)
	{
	  offset_type[VERTEX_X] = numfloat+0;
	  offset_type[VERTEX_Y] = numfloat+1;
	  offset_type[VERTEX_Z] = numfloat+2;	  
	  numfloat += 3;
	}

      if (m_colors)
	{
	  if (m_num < 3 || m_num > 4)
	    throw VBO_InvalidParameter();

	  offset_type[COLOR_R] = numfloat + 0;
	  offset_type[COLOR_G] = numfloat + 1;
	  offset_type[COLOR_B] = numfloat + 2;
	  offset_type[COLOR_A] = numfloat + 3;
	  numfloat += m_num;
	}

      if (m_normals)
	{
	  offset_type[NORMAL_X] = numfloat + 0;
	  offset_type[NORMAL_Y] = numfloat + 1;
	  offset_type[NORMAL_Z] = numfloat + 2;
	  numfloat += 3;
	}

      if (m_tex2d)
	{
	  offset_type[TEX2D_X] = numfloat + 0;
	  offset_type[TEX2D_Y] = numfloat + 1;
	  numfloat += 2;
	}

      if (m_tex3d)
	{
	  offset_type[TEX3D_X] = numfloat + 0;
	  offset_type[TEX3D_Y] = numfloat + 1;
	  offset_type[TEX3D_Z] = numfloat + 2;
	  numfloat += 3;      
	}

      if (numMultiTextures > 0)
	{
	  for (int i = 0; i < numMultiTextures; i++)
	    {
	      multiTexId[i] = d.multiTexId[i];
	      multiTextureOffset[i] = numfloat;
	      numfloat += 2;
	    }
	}

      if (numAttribs > 0)
	{
	  for (int i = 0; i < numAttribs; i++)
	    {
	      attribIndex[i] = d.attribIndex[i];
	      attribSize[i] = d.attribSizes[i];
	      attribOffset[i] = numfloat;
	      numfloat += d.attribSizes[i];
	    }
	}

      float_size = numfloat;
      element_size = numfloat*sizeof(GLfloat);
    }    

    bool hasTexture2D() const    {    return m_tex2d;   }
    bool hasTexture3D() const    {    return m_tex3d;   }
    bool hasColors() const { return m_colors; }
    bool hasNormals() const { return m_normals; }
    int numColorComponents() const { return m_num; }
    int numMultiTex() const { return numMultiTextures; }

    GLint multiTexID(int i) const { return multiTexId[i]; }
    GLuint getAttribIndex(int i) const { return attribIndex[i]; }
    void setAttribIndex(int i, GLuint index) { attribIndex[i] = index; }
    long getMultiTexCoordOffset(int i) const { return multiTextureOffset[i]*sizeof(GLfloat); }
    long getAttribOffset(int i) const { return attribOffset[i]*sizeof(GLfloat); }
    int getNumAttrib() const { return numAttribs; }
    
    unsigned long getStride() const
    {
      return element_size;
    }

    unsigned long operator[](long i) const
    {
      return element_size*i;
    }

    unsigned long operator()(long i, FlexibleType t) const
    {
      return float_size*i + offset_type[t];
    }

    unsigned long getMultiTexFloat(int point, int i, int coord) const
    {
      return float_size*point + multiTextureOffset[i] + coord;
    }

    unsigned long getAttribFloat(int point, int i) const
    {
      return float_size*point + attribOffset[i];
    }

    int getAttribSize(int i) const
    {
      return attribSize[i];
    }

    int getOffset(FlexibleType t) const { return sizeof(GLfloat)*offset_type[t]; }
    
    unsigned long getSize(long numPoints) const
    {
      unsigned long s = numPoints*element_size;
      //unsigned long mask = ~(unsigned long)0x1F;

      //return (s+31)&mask;
      return s;
    }

    long getNumFloats(long numPoints) const
    {
      return float_size*numPoints;
    }
  };

  class VBO_Object
  {
  protected:
    GLuint vboId;
    FlexiblePointAccessor accessor;
    std::vector<GLfloat> data;
    std::vector<GLint> sequencesStart;
    std::vector<GLsizei> sequencesLen;
    int currentNumPoints, currentPosPoint;
    bool reordered;
    boost::shared_array<int> order_indexes;
    bool dynamicDraw;
  private:
    void allocVBO() throw (VBO_OutOfMemory);
  public:
    VBO_Object(const VBO_Object& o) : accessor(VBO_Descriptor()) {
	abort();
    }

    VBO_Object();
 
    VBO_Object(const VBO_Descriptor& desc, int numpoints = -1) throw(VBO_OutOfMemory);
    ~VBO_Object();

    void updateAttributeLocation(int id, GLuint index)
    {
      accessor.setAttribIndex(id, index);
    }

    void begin();
    void addPoint(GLfloat x, GLfloat y, GLfloat z);
    void setColor(GLfloat r, GLfloat g, GLfloat b); 
    void setColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a); 
    void setTexture2D(GLfloat x, GLfloat y);
    void setTexture3D(GLfloat x, GLfloat y, GLfloat z);
    void setNormal(GLfloat x, GLfloat y, GLfloat z);
    void setMultiTexture(int id, GLfloat x, GLfloat y);
    void setAttrib(int id, GLfloat *a);
    void end() throw (VBO_OutOfMemory);

    int numPoints() const { return currentNumPoints; }

    void setOrdering(boost::shared_array<int>& order);

    void newSequence();
    
    void bind();
    void unbind();
    void draw(GLint mode);

    void transfer(VBO_Object& vbo);

    const FlexiblePointAccessor& getAccessor() const { return accessor; }
  };

  
  class VBO_Chain
  {
  private:
    std::vector<std::pair<GLint, VBO_Object *> > chain;
  public:
    VBO_Chain();
    ~VBO_Chain();

    void reset();
    void push(GLint draw_type, VBO_Object& vbo);

    void draw();
  };
};

#endif
