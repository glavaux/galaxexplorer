/*+
This is GalaxExplorer (./src/setup.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <QLineEdit>
#include <QPushButton>
#include <QRadioButton>
#include <QDialog>
#include <QDir>
#include <iostream>
#include <QColorDialog>
#include "window.hpp"
#include "glwidget.hpp"
#include "back_grid.hpp"

#ifdef HEALPIX_PRESENT
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#endif
#include "skybox.hpp"

#include "setup.hpp"
#include "ui_setupDialog.h"
#include "inits.hpp"

using namespace GalaxExplorer;
using namespace std;

void changeColorButton(QWidget *w, QColor& newColor)
{
  QPalette palette = w->palette();

  palette.setColor(QPalette::Button, newColor);
  palette.setColor(QPalette::ButtonText,
                   QColor::fromRgbF(1-newColor.redF(),1-newColor.greenF(),1-newColor.blueF()));
  w->setAutoFillBackground (true);
  w->setPalette(palette);
}

SetupDialog::SetupDialog(Window& mainWindow)
  : QDialog(&mainWindow), mainWin(mainWindow)
{
  ui = new Ui::SetupDialog();

  ui->setupUi(this);

  ui->texturePath->setText(QDir::searchPaths("textures").join(":"));
  ui->shaderPath->setText(QDir::searchPaths("shaders").join(":"));
  ui->axisPresent->setChecked(mainWin.getUniverse()->getState()->axisShown());
  ui->projectionPerspective->toggle();

  ui->backgroundColor->setAutoFillBackground (true);

  for (int i = 0; i < g_allRenderers.size(); i++) {
    ui->rendererList->addItem(g_allRenderers[i]->getRendererName());
  }
  
  setup_builders();

  connect(ui->buttonBox->button(QDialogButtonBox::Apply), SIGNAL(clicked(bool)), this, SLOT(apply()));
  connect(ui->axisPresent, SIGNAL(stateChanged(int)), this, SLOT(showCube(int)));
  connect(ui->projectionOrtho, SIGNAL(toggled(bool)), this, SLOT(orthoProjection(bool)));
  connect(ui->projectionPerspective, SIGNAL(toggled(bool)), this, SLOT(perspProjection(bool)));
  connect(ui->texturePath, SIGNAL(returnPressed()), this, SLOT(validateTexturePath()));
  connect(ui->shaderPath, SIGNAL(returnPressed()), this, SLOT(validateShaderPath()));
  connect(ui->backgroundColor, SIGNAL(clicked()), this, SLOT(showBgColorSelection()));
  connect(ui->choosePath_sphere, SIGNAL(clicked()), this, SLOT(showPathSphereChooser()));
  connect(ui->rendererList, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)),
          this, SLOT(changeRenderer(QListWidgetItem*,QListWidgetItem*)));
  connect(ui->grid_type, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)),
	  this, SLOT(changeGrid(QListWidgetItem*,QListWidgetItem*)));

  rendererUpdated = false;
  backGridUpdated = false;
  projectionChanged = false;
}

static QListWidgetItem *add_builder(const QString& name, boost::shared_ptr<GridBuilder> builder, QVector<boost::shared_ptr<GridBuilder> >& vec, QListWidget& lw)
{
  QListWidgetItem *lwi = new QListWidgetItem;

  lwi->setText(name);
  lwi->setData(Qt::UserRole, vec.size());
  vec.push_back(builder);

  lw.addItem(lwi);
  return lwi;
}

void SetupDialog::setup_builders()
{
  QList<QString> key_list;

  buildGridMapper(builders);
  
  key_list = builders.keys();

  boost::shared_ptr<GridBuilder> current_builder = mainWin.getUniverse()->getGridBuilder();

  for (QList<QString>::iterator i = key_list.begin();
       i != key_list.end();
       ++i)
    {
      QListWidgetItem *lwi = add_builder(*i, builders[*i], grid_builders, *ui->grid_type);
      if (builders[*i]->equal(current_builder.get()))
        ui->grid_type->setCurrentItem(lwi);
    }
}

void SetupDialog::showBgColorSelection()
{
  QPalette palette = ui->backgroundColor->palette();

  QColor newColor = QColorDialog::getColor ( palette.color(QPalette::Button), this);
  if (newColor.isValid())
    {
      changeColorButton(ui->backgroundColor, newColor);
      mainWin.getUniverse()->getState()->setBackgroundColor(newColor);
    }
}

void SetupDialog::validateShaderPath()
{
  QDir::setSearchPaths("shaders", ui->shaderPath->text().split(":"));
}

void SetupDialog::validateTexturePath()
{
  QDir::setSearchPaths("textures", ui->texturePath->text().split(":"));
}

void SetupDialog::orthoProjection(bool c)
{
  newProjection = UniverseWidget::OrthographicProj;  
  projectionChanged = true;
}

void SetupDialog::perspProjection(bool c)
{
  newProjection = UniverseWidget::PerspectiveProj;
  projectionChanged = true;
}

void SetupDialog::changeRenderer(QListWidgetItem *c, QListWidgetItem *p)
{
  rendererUpdated = true;
}

void SetupDialog::changeGrid(QListWidgetItem* c, QListWidgetItem* p)
{
  newBackGrid = c->data(Qt::UserRole).toInt();
  backGridUpdated = true;
}

void SetupDialog::showCube(int s)
{
  mainWin.getUniverse()->getState()->setAxis(s == Qt::Checked);
}

void SetupDialog::showPathSphereChooser()
{
  QString fname = 
    QFileDialog::getOpenFileName(this, 
				 tr("Load current state from file"), 
				 QString(), 
				 tr("Healpix file (*.fits)"));  

  if (fname == "")
    return; 

  ui->pathSphere->setText(fname);
}

SetupDialog::~SetupDialog()
{
}

void SetupDialog::setup()
{
  // Update the values
  QColor bgColor = mainWin.getUniverse()->getState()->getBackgroundColor();
  changeColorButton(ui->backgroundColor, bgColor);
  ui->axisPresent->setChecked(mainWin.getUniverse()->getState()->axisShown());
  ui->interactiveSlider->setValue(mainWin.getUniverse()->getRenderer()->getInteractiveResolution());


  {
    QSettings settings;
    QString rname = settings.value("renderer").toString();
    int rselected;

    for (int i = 0; i < g_allRenderers.size(); i++) {
      if (g_allRenderers[i]->getRendererName() == rname)
        rselected = i;
    }
    ui->rendererList->setCurrentRow(rselected);
  }

  if (exec())
    apply();
}

void SetupDialog::apply()
{
  QSettings settings;

  cout << "Apply !"  << endl;

  if (projectionChanged)
   {
     mainWin.getUniverse()->setProjection(newProjection);
     projectionChanged = false;
   }
 
  if (backGridUpdated)
   {
     mainWin.getUniverse()->setGridBuilder(grid_builders[newBackGrid]);
     backGridUpdated = false;
   }

  int r_id = ui->rendererList->currentRow();

  if (rendererUpdated)
    {
      mainWin.changeRenderer(g_allRenderers[r_id]);
      rendererUpdated = false;
    }

  mainWin.getUniverse()->getRenderer()->setInteractiveResolution(ui->interactiveSlider->value());

#ifdef HEALPIX_PRESENT
  QString path = ui->pathSphere->text();

  if (path != "")
    {
      Healpix_Map<float> map;
      
      read_Healpix_map_from_fits(qPrintable(path), map);

      QList<SkyBox *>& l_skybox = mainWin.getUniverse()->getSkyBoxList();

      if (l_skybox.size() != 0)
	{
	  QList<SkyBox *>::iterator iter= l_skybox.begin();

	  while (iter != l_skybox.end())
	    {
	      delete (*iter);
	      ++iter;
	    }
	  l_skybox.clear();
	}

      mainWin.getUniverse()->getSkyBoxList().push_back(new SkyBox(map));
    }
#endif
}
