/*+
This is GalaxExplorer (./src/inits.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <QObject>
#include <QVector>
#include "dataSelectors.hpp"
#include "inits.hpp"
#include "glconnect.hpp"
#include "selector_Luminosity.hpp"
#include "selectorByDistance.hpp"
#include "selectorType.hpp"
#include "representations/vectorRepresentation.hpp"
#include "representations/galaxyRepresentation.hpp"
#include "representations/surfaceRepresentation.hpp"
#ifdef COSMOTOOL_PRESENT
#include "representations/massiveRepresentation.hpp"
#endif
#include "representations/tetrahedronRepresentation.hpp"
#include "representations/volumeRepresentation.hpp"
#include "representations/stdRepresentations.hpp"
#include "representations/colorPointRepresentation.hpp"
#include "dbus/dbus_if.hpp"
#include "window.hpp"
#include "renderers/renderer.hpp"
#include "renderers/renderer_wavg.hpp"
#include "renderers/renderer_wavg_single.hpp"
#include "renderers/renderer_shader.hpp"
#include "renderers/renderer_fpeel.hpp"


using namespace GalaxExplorer;

AllSelectors GalaxExplorer::g_allDataSelectors;
DataRepresentationMap GalaxExplorer::g_allRepresentations;
DataSourceMap GalaxExplorer::g_allDataSources;
ConnectorVector GalaxExplorer::g_allConnectors;  
QMutex GalaxExplorer::g_allDataSourcesMutex;

double GalaxExplorer::g_distanceMultiplier = -1.0;

Window *GalaxExplorer::main_window = 0;

QSet<QString> GalaxExplorer::g_gl_extensions;

void GalaxExplorer::clearUniverse()
{
  for (DataSourceMap::iterator iter = g_allDataSources.begin();
       iter != g_allDataSources.end();
       iter++)
    delete (*iter);

  g_allDataSources.clear();
}

void GalaxExplorer::initializeSelectors()
{
  g_allDataSelectors[QString("No selection")] = new DataSelector_NoSelection();
  g_allDataSelectors[QString("Select by luminosity")] = new DataSelectionByLuminosity();
  g_allDataSelectors[QString("Select by distance")] = new DataSelectionByDistance();
  g_allDataSelectors[QString("Select by type")] = new DataSelectionByType();
  
  for (AllSelectors::iterator iter = g_allDataSelectors.begin();
       iter != g_allDataSelectors.end();
       ++iter)
    iter.value()->setName(iter.key());
}

void GalaxExplorer::registerDataRepresentation(DataRepresentation *r)
{
  g_allRepresentations[r->getName()] = r;
  registerDBusRepresentation(r);
}

template<class T>
void addRepresentation()
{
   T *c = new T();

   c->setName(c->getRepresentationName());
   registerDataRepresentation(c);
   g_allRepresentations[c->getName()] = c;
}

void GalaxExplorer::initDataRepresentations() 
{
  addRepresentation<BWSphereRepresentation>();
  addRepresentation<BWPointRepresentation>();
  addRepresentation<LumSphereRepresentation>();
  addRepresentation<VectorRepresentation>();
  addRepresentation<ScalingLumSphereRepresentation>();
  addRepresentation<ColorSphereRepresentation>();
  addRepresentation<GalaxyRepresentation>();
  addRepresentation<SurfaceRepresentation>();
#ifdef COSMOTOOL_PRESENT
  addRepresentation<MassivePointRepresentation>();
#endif
  addRepresentation<TetrahedronRepresentation>();
  addRepresentation<VolumeRepresentation>();
  addRepresentation<ColorPointRepresentation>();
}

template<typename T>
GLRendererFactory *getAutoRendererFactory()
{
  static GLRendererAutoFactory<T> r_factory;

  return &r_factory;
}

static GLRendererAutoFactory<GLBasicRenderer> gs_basic_factory("Basic renderer");
static GLRendererAutoFactory<GLBufferedRenderer> gs_buffered_factory("Buffered renderer");
static GLRendererAutoFactory<GLRendererShader> gs_basicshader_factory("Basic renderer (shader support)");
static GLRendererAutoFactory<GLRendererWeighedAvg> gs_wavg_factory("Weighed average renderer");
static GLRendererAutoFactory<GLFrontPeeling> gs_fpeel_factory("Front peeling renderer");
static GLRendererAutoFactory<GLRendererWeighedAvgSingleDraw> gs_wavg_single_factory("Weighed avg single output");

QVector<GLRendererFactory *> GalaxExplorer::g_allRenderers;

void GalaxExplorer::initRendererList()
{
  g_allRenderers.push_back(&gs_basic_factory);
  g_allRenderers.push_back(&gs_buffered_factory);
  g_allRenderers.push_back(&gs_basicshader_factory);
  g_allRenderers.push_back(&gs_wavg_factory);
  g_allRenderers.push_back(&gs_wavg_single_factory);
  g_allRenderers.push_back(&gs_fpeel_factory);
}
