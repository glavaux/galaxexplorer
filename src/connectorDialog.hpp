/*+
This is GalaxExplorer (./src/connectorDialog.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __CONNECTOR_DIALOG_HPP
#define __CONNECTOR_DIALOG_HPP

#include <QDialog>
#include <QVector>
#include "window.hpp"
#include "ui_dialogCreateConnector.h"
#include "datasets.hpp"
#include "glconnect.hpp"
#include "datasets.hpp"

namespace GalaxExplorer {

  class SelectorPropertyEditing;
  class ConnectorDialog;

  class SelectorPropertyTracking: public QObject
  {
    Q_OBJECT
  public:
    SelectorPropertyTracking(QObject *gui, int i)
    {
      id = i;
      this->gui = gui;
    }

    virtual ~SelectorPropertyTracking();

  signals:
    void propertyModified(QObject *gui, int id);

  public slots:
    void modified();   
    
  public:
    int id;
    QObject *gui;
  };

  class SelectorPropertyEditing: public QObject
  {
    Q_OBJECT

  public:
    SelectorPropertyEditing(DataSetSelector *s);
    virtual ~SelectorPropertyEditing();

    void addTracking(SelectorPropertyTracking *tracking);

  public slots:
    void propertyModified(QObject *gui, int id);

  signals:
    void modified(DataSetSelector *s);

  protected:
    DataSetSelector *selector;
  };

  class ConnectorDialog: public QDialog
  {
    Q_OBJECT
  public:
    ConnectorDialog(Window *parent, AllDataSelector *selectors);
    ConnectorDialog(Window *parent, AllDataSelector *selectors, const Connector& oldConnector);
    virtual ~ConnectorDialog();
    
    Connector *fullExecution();
			      
  signals:
    void selectorModified(DataSetSelector *s);

  public slots:
    void createSelectorDialog();
    void createRepDialog();
    void editSelector();
  private:
    Ui::BuildConnectorDialog connectorUi;
    AllDataSelector *selectors;
    
    void setupBaseUI(const Connector *oldConnector = 0);
  };

};

#endif
