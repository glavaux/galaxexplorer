/*+
This is GalaxExplorer (./src/surfaceCleaner.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __GALAX_SURFACE_CLEANER_HPP
#define __GALAX_SURFACE_CLEANER_HPP

namespace GalaxExplorer
{

  class SurfaceCleaner
  {
  public:
    struct Point {
	    float xyz[3];
    };
    struct Normal {
      Point direction;
      float accum;
    };

    SurfaceCleaner(bool smooth);
    ~SurfaceCleaner();

    void begin(int numFaces, int numPoints);
    template<typename PointAccessor>
    void addTriangle(PointAccessor& a, int face, int p1, int p2, int p3);

    void end();
    void getNormal(int t, int p, Point& n);
    
  private:
    Normal *all_normals;
    int numPoints;
    int numFaces;
    bool smoothSurface;
  };

  template<typename PointAccessor>
  void SurfaceCleaner::addTriangle(PointAccessor& a, int face, int p1, int p2, int p3)
  {
    float u[3], v[3], n[3];
    
    for (int i = 0; i < 3; i++)
    {
      u[i] = a[p2][i] - a[p1][i];
      v[i] = a[p3][i] - a[p1][i];
    }

    n[0] = -u[1]*v[2] + u[2]*v[1];
    n[1] = -u[2]*v[0] + u[0]*v[2];
    n[2] = -u[0]*v[1] + u[1]*v[0];
    // We got the normal to the face. Send it to all_normals

    if (smoothSurface)
      {
	double s = sqrt(n[0]*n[0]+n[1]*n[1]+n[2]*n[2]);
	for (int i = 0; i < 3; i++)
	  {
	    all_normals[p1].direction.xyz[i] += n[i]*s;
	    all_normals[p2].direction.xyz[i] += n[i]*s;
	    all_normals[p3].direction.xyz[i] += n[i]*s;
	  }
	all_normals[p1].accum+=s;
	all_normals[p2].accum+=s;
	all_normals[p3].accum+=s;
      }
    else
      {
	for (int i = 0; i < 3; i++)
	  {
	    all_normals[face].direction.xyz[i] = n[i];
	    all_normals[face].accum = 1;
	  }
      }

  }


};


#endif
