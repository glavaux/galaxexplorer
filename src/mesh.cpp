/*+
This is GalaxExplorer (./src/mesh.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <cmath>
#include "surfaceCleaner.hpp"
#include "mesh.hpp"
#include "vboObject.hpp"
#include <boost/bind.hpp>

using namespace std;
using namespace GalaxExplorer;

void GeneralMesh::buildVBO(VBO_Object& vbo, bool smooth,
			   bool keep_only_ordering)
{
  SurfaceCleaner cleaner(smooth);

  vbo.begin();
  cleaner.begin(all_triangles.size(), all_points.size());
  for (int t = 0; t < all_triangles.size(); t++)
    {
      cleaner.addTriangle(all_points, t, 
			  all_triangles[t].v[0],
			  all_triangles[t].v[1],
			  all_triangles[t].v[2]);
    }
  cleaner.end();

  for (int t = 0; t < all_triangles.size(); t++)
    {
      for (int p = 2; p >= 0; p--)
	{
	  SurfaceCleaner::Point n;
	  int v = all_triangles[t].v[p];

	  cleaner.getNormal(t, v, n);
	  vbo.addPoint(all_points[v].xyz[0], all_points[p].xyz[1], all_points[p].xyz[2]);
	  vbo.setNormal(n.xyz[0], n.xyz[1], n.xyz[2]);
	}
    }
  vbo.end();
  
  if (keep_only_ordering)
    {
      all_triangles.clear();
      all_points.clear();
    }
}

static 
bool compareTriangles(const float *all_projections, int i1, int i2)
{
  return all_projections[i1] < all_projections[i2];
}

void GeneralMesh::computeTriangleOrdering(Point& eye_position, Direction& eye_dir, boost::shared_array<int>& order)
{
  float *projected = new float[triangle_centers.size()];

  for (int i = 0; i < triangle_centers.size(); i++)
    {
      order[i] = i;
      projected[i] = 0;
      for (int j = 0; j < 3; j++)
	projected[i] += (triangle_centers[i].xyz[j]-eye_position[j]) * eye_dir[j];
    }  
  
  sort(&order[0], &order[triangle_centers.size()], boost::bind(compareTriangles, projected, _1, _2));
}

void GeneralMesh::reorderVBO(Point& eye_position, Direction& eye_dir, VBO_Object& vbo)
{
  boost::shared_array<int> order(new int[triangle_centers.size()]);

  computeTriangleOrdering(eye_position, eye_dir, order);

  vbo.setOrdering(order);
}
