/*+
This is GalaxExplorer (./src/dataConnector.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __DATA_CONNECTOR_HPP
#define __DATA_CONNECTOR_HPP

#include <QtGlobal>
#include <QObject>
#include <QVector>

namespace GalaxExplorer
{
  class DataGrid;
  class DataSet;
  class DataRepresentation;
  class DataSetSelector;
  class DataSource;
  class DataMesh;

  class DataConnector: public QObject
  {
    Q_OBJECT
  public:
    virtual DataConnector *copyConnector() const = 0;    
    
    DataSource *getDataSource() const { return source; }
    DataRepresentation *getRepresentation() const { return representation; }
    DataSource *getAdditionalData(int i) const {
      if (i >= additional_data.size())
          return 0;
      return additional_data[i];
    }
    
    void setAdditionalData(int i, DataSource *s) {
      if (i >= additional_data.size())
        additional_data.resize(i+1);
      additional_data[i] = s;
    }
    
  protected:
    DataRepresentation *representation;
    DataSource *source;
    QVector<DataSource *> additional_data;
  };

  class SetConnector: public DataConnector
  {
    Q_OBJECT
  public:
    SetConnector();
    SetConnector(const SetConnector& s);
    SetConnector(DataSet *dataset, DataRepresentation *representation,
		 DataSetSelector *selector);
    virtual ~SetConnector();

    virtual SetConnector *copyConnector() const;
    
    DataSet *getDataset() const;
    const DataSetSelector *getSelector() const { return selector; }
    DataSetSelector *getSelector() { return selector; }

  private:
    DataSetSelector *selector;    
  };

  class GridConnector: public DataConnector
  {
    Q_OBJECT
  public:
    GridConnector();
    GridConnector(const GridConnector& s);
    GridConnector(DataGrid *datasrc, DataRepresentation *representation);
    virtual ~GridConnector();

    virtual DataConnector *copyConnector() const;
    
    DataGrid *getDataGrid() const;
  };

  class MeshConnector: public DataConnector
  {
    Q_OBJECT
  public:
    MeshConnector();
    MeshConnector(const MeshConnector& s);
    MeshConnector(DataMesh *datasrc, DataSet* partsrc, DataRepresentation *representation);
    virtual ~MeshConnector();
    
    virtual DataConnector *copyConnector() const;

    DataMesh *getDataMesh() const;
    DataSet *getDataParticles() const;
  private:
    DataSet *particles_source;
  };
  
  

};

#endif
