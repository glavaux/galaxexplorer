/*+
This is GalaxExplorer (./src/glconnect.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __GALAXEXPLORER_CONNECT_HPP
#define __GALAXEXPLORER_CONNECT_HPP

#include "includeGL.hpp"
#include <cassert>
#include <map>
#include <string>
#include <QString>
#include <QVector>
#include <boost/shared_ptr.hpp>
#include "datasets.hpp"
#include "datagrid.hpp"
#include "representations/dataRepresentation.hpp"
#include "GLSLProgramObject.h"
#include "dataConnector.hpp"
#include "mesh.hpp"
#include "attributeManager.hpp"

namespace GalaxExplorer {  

  class VBO_Object;
  class VBO_Descriptor;

  class Connector: public AttributeManager
  {
  public:
    Connector();
    Connector(const DataConnector& c);
    Connector(const Connector& c);
    virtual ~Connector();

    DataConnector *getConnector() { return connector; }
    const DataConnector *getConnector() const { return connector; }
    const QString& getName() const { return myName; }
    void setName(const QString& s) { myName = s; }

    Connector &operator=(const Connector& c);

    void setShow(bool show) { this->show = show; }
    bool isShown() const { return show; }
  private:
    DataConnector *connector;
    QString myName;
    bool initialized;
    bool show;
  };

  class GLConnector: public Connector
  {
  public:
    GLConnector();
    GLConnector(const SetConnector& c);
    GLConnector(const Connector& c);
    GLConnector(const GLConnector& c);
    virtual ~GLConnector();

    GLuint allocateList();
    int allocateVBO(const VBO_Descriptor& vbo);

    template<typename PointIterator, typename TriangleIterator>
    GeneralMesh *createMesh(PointIterator pstart, PointIterator pend,
			    TriangleIterator tstart, TriangleIterator tend)
    {
      boost::shared_ptr<GeneralMesh> m(new GeneralMesh(pstart, pend, tstart, tend));
      allMeshes.push_back(m);
      return m.get();
    }

    GeneralMesh *getMesh(int id) { 
      assert(id < allMeshes.size());
      return allMeshes[id].get();
    }

    GLuint getList() const { return myList; }
    VBO_Object& getVBO(int id) { assert(id <= allVBOs.size()); return *allVBOs[id]; }
    int numberOfVBO() const { return allVBOs.size(); }
    GLConnector &operator=(const GLConnector& c);

    bool prepared() const { return m_prepared; }
    void setPrepared(bool p) { m_prepared = p; }
    void unprepare();

    GLSLProgramObject& getShader(int id);
  private:
    GLuint myList;
    bool m_prepared;
    std::vector<GLuint> allLists;
    std::vector<VBO_Object *> allVBOs;
    std::vector<boost::shared_ptr<GeneralMesh> > allMeshes;
    std::vector<GLSLProgramObject> allShaders;
  };

  
  Connector buildConnector(const QString& repname, const QString& name, 
			   DataSet *data, DataSetSelector *sel = 0);
  Connector buildConnector(const QString& repname, const QString& name, 
			   DataGrid *data);
  Connector buildConnector(const QString& repname, const QString& name, 
			   DataMesh *mesh, DataSet *data);


  typedef QVector<Connector> ConnectorVector;
  typedef QVector<GLConnector> GLConnectorVector;
  
};

#endif
