#ifndef __GALAX_TREECONNECTOR_HPP
#define __GALAX_TREECONNECTOR_HPP

#include "dataConnector.hpp"

namespace GalaxExplorer
{

  class TreeConnector: public DataConnector
  {
    Q_OBJECT
  public:
    TreeConnector();
    TreeConnector(const TreeConnector& t);
    TreeConnector(DataSet *datasrc, DataRepresentation *representation);
    virtual ~TreeConnector();
    
    virtual DataConnector *copyConnector() const;
    
    DataSet *getDataParticles() const;
    const OctTree *getTree() const;
  private:
    typedef CosmoTool::OctTree OctTree;
    
    OctTree *tree;
    DataSet *particle_sources;
  };
  
};

#endif
