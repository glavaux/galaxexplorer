/*+
This is GalaxExplorer (./src/gpalette.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <QtGui>
#include <QtCore>
#include <gsl/gsl_interp.h>
#include "gpalette.hpp"
#include <gsl/gsl_errno.h>

using namespace GalaxExplorer;


GPalette::GPalette()
{
  for (int i = 0; i < 3; i++)
    {
      min_color.rgba[i] = 0;
      max_color.rgba[i] = 1.0;
    }
  min_color.rgba[3] = max_color.rgba[3] = 1.0;
  for (int i = 0; i < 4; i++)
    {
      interp[i] = 0;
      ya[i] = 0;
    }
  accel = 0;
  xa = 0;
}


GPalette::GPalette(const GPalette& p)
{
   for (int i = 0; i < 3; i++)
    {
      min_color.rgba[i] = 0;
      max_color.rgba[i] = 1.0;
    }
  min_color.rgba[3] = max_color.rgba[3] = 1.0;
  for (int i = 0; i < 4; i++)
    {
      interp[i] = 0;
      ya[i] = 0;
    }
  accel = 0;
  xa = 0;

  operator=(p);
}

GPalette::~GPalette()
{
  free_interp();
}

void GPalette::clear()
{
  values.clear();
  colors.clear();
  free_interp();
}

void GPalette::free_interp()
{
  for (int i =  0; i < 4; i++) {
    if (interp[i])
      gsl_interp_free(interp[i]);
    interp[i] = 0;
    if (ya[i])
      delete[] ya[i];
    ya[i] = 0;
  }

  if (xa)
    delete[] xa;
  xa = 0;

  if (accel)
    gsl_interp_accel_free(accel);

  accel = 0;
}

template<int i>
float extractComponent(const GColor& c)
{
  return c.rgba[i];
}

typedef float (*extractType)(const GColor& c);

bool GPalette::rebuild()
{
  gsl_error_handler_t *old_handler =  gsl_set_error_handler_off ();

  free_interp();

  {
    int n = 0;

      extractType extractColor[4] = {
	extractComponent<0>, extractComponent<1>,
	extractComponent<2>, extractComponent<3>
      };
      QList<double>::iterator iter = values.begin();

      xa = new double[values.size()+2];
      for (int i = 0; i < 4; i++)
        ya[i] = new double[values.size()+2];

      xa[0] = 0;

      n=1;
      while (iter != values.end())
	{
	  xa[n] = *iter;
	  ++iter;
	  ++n;
	}
      xa[n] = 1.0;

      for (int i = 0; i < 4; i++)
	{
	  QList<GColor>::iterator iterc = colors.begin();

	  ya[i][0] = min_color.rgba[i];
	  n = 1;
	  while (iterc != colors.end())
	    {
	      ya[i][n] = extractColor[i](*iterc);
	      ++iterc;
	      ++n;
	    }
	  ya[i][n] = max_color.rgba[i];

	  interp[i] = gsl_interp_alloc(gsl_interp_linear, values.size()+2);

	  if (gsl_interp_init(interp[i], xa, ya[i], values.size()+2) != GSL_SUCCESS)
	    {
	      gsl_set_error_handler(old_handler);
	      free_interp();
	      return false;
	    }
	}

      accel = gsl_interp_accel_alloc();
    }

  gsl_set_error_handler(old_handler);
  return true;
}

void GPalette::setMinimum(GColor color )
{
  min_color = color;
}

void GPalette::setMaximum(GColor color)
{
  max_color = color;
}

void GPalette::addPoint(float value, GColor color)
{
  QList<double>::iterator iter = values.begin();
  QList<GColor>::iterator iterc = colors.begin();

  while (iter != values.end() && (*iter) < value)
    {
      ++iter;
      ++iterc;
    }
  values.insert(iter, value);
  colors.insert(iterc, color);  
}

void GPalette::getColor(float value, GColor& color)
{
  if (value < 0)
    {
      color = min_color;
      return;
    }
  if (value > 1)
    {
      color = max_color;
      return;
    }

  if (values.size() == 0)
    {
      for (int j = 0; j < 4; j++)
        color.rgba[j] = min_color.rgba[j] + value*(max_color.rgba[j]-min_color.rgba[j]);
      return;
    }

  for (int j = 0; j < 4; j++)
    color.rgba[j] = gsl_interp_eval(interp[j],xa,ya[j],value,accel);
}


const GPalette& GPalette::operator=(const GPalette& p)
{
  free_interp();

  values = p.values;
  colors = p.colors;
  min_color = p.min_color;
  max_color = p.max_color;

  return *this;
}
