/*+
This is GalaxExplorer (./src/window.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QCheckBox>
#include <QListWidget>
#include <QPushButton>
#include <list>
#include "points.hpp"
#include "glconnect.hpp"
#include "world.hpp"
#ifdef HEALPIX_PRESENT
#include <healpix_map.h>
#endif

class QSlider;

namespace GalaxExplorer
{
  class GLRendererFactory;
  class UniverseWidget;
  class SetupDialog;
  class Window : public QMainWindow
  {
    Q_OBJECT

  public slots:
    void createConnectorDialog();
    void installConnectors(const ConnectorVector& c);
    void moveConnectorUp();
    void moveConnectorDown();
    void recreateConnectorList();
    void showHideConnector(bool toggled);
    void setCurrentConnector(int current);
    void loadConnectors();
    void saveConnectors();
    void loadData();
    void loadGridData();
    void loadHDFCatData();
    void loadParticleData();
    void loadGadgetData();
    void destroyConnector();
    void recompileSelector(DataSetSelector *s);
    void setMasterConnector();
    void editRepresentations();
    void enableRotation(bool);
    void runScript();
    void postLoadState();
    void loadTrajectoryData();
    void loadMeshData();
    void mainSetup();
    void setXRotation(int angle);
    void setYRotation(int angle);
    void setZRotation(int angle);
    void setXSliderValue(float angle);
    void setYSliderValue(float angle);
    void setZSliderValue(float angle);
    void autoUpdatedState();
    void ackRepresentation(QString representation);
    void ackRepresentationFixed();
    void toggleMultipleSelection(bool toggled);

  private slots:
    void showConnectorContextMenu(const QPoint& p);
    void gl_ready();
    void enableInteraction();
    void disableInteraction();

  public:
    Window();
    
    UniverseWidget *getUniverse()
    {
      return world->universeWidget;
    }

    GLRendererFactory *getRendererFactory() const
    {
      return r_factory;
    }
    
    void changeRenderer(GLRendererFactory *r); 

    World *getWorld() { return world; }

  protected:
    virtual void keyPressEvent(QKeyEvent * event);
    virtual void timerEvent(QTimerEvent * event);
  private:
    QSlider *createSlider();
    QSlider *createDeepSlider();
    QSlider *createVelocitySlider();
    QWidget *createScene();
    QDockWidget *createPositionSubWindow();
    QDockWidget *createConnectorHandling();
    QMenu *createDataMenu();

    void addConnectorToList(const QString& name, int id);
    friend class World;
    
    QCheckBox *velocityToggle;
    QCheckBox *extraToggle;
    QSlider *xSlider;
    QSlider *ySlider;
    QSlider *zSlider;
    QSlider *deepSlider;
    QSlider *velBoost;
    QListWidget *connectorsList;
    QDialog *currentConnectorDialog;
    QAction *showHideAction;
    QMenu *menuConnection, *menuConnectionAlternate, *menuGLOptions;

    QString currentRepresentationName;
    bool showCube;
    int myRotationEvent;

    World *world;
    SetupDialog *setup;

    GLRendererFactory *r_factory;
    QAction *multipleSelectionAction;

    void createInterface();    
  };
};

#endif
