/*+
This is GalaxExplorer (./src/main.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <cmath>
#include <QGLContext>
#include <QApplication>
#include <QErrorMessage>
#include <QDir>
#include <fstream>
#include <list>
#include <iostream>
#include "window.hpp"
#include "points.hpp"
#include "glwidget.hpp"
#include "datasets.hpp"
#include "simpleDataFilter.hpp"
#include "inits.hpp"
#include "datagrid.hpp"
#include "dbus/dbus_if.hpp"
#include "representations/transferFunction.hpp"

using namespace std;
using namespace GalaxExplorer;

class SelectLuminosity
{
public:
  SelectLuminosity() {}
  ~SelectLuminosity() {}

  bool operator() (const DataPoint& p) const { return (p.lum > 1e9); }
};

inline double sq(double x)
{
  return x*x;
}

int parseOpenGLVersion(const char *sver)
{
  char *endptr;
  int version;

  version = strtol(sver, &endptr, 10)*100;

  if (*endptr != '.')
    return -1;

  version += strtol(endptr+1, 0, 10);

  return version;
}

bool checkOpenGL()
{
  char *sver = (char *)glGetString(GL_VERSION);
  int iver;
  static const char *required_ext[] = {
    "GL_EXT_framebuffer_object",
    "GL_ARB_vertex_buffer_object"
  };
  static const int numExt = sizeof(required_ext)/sizeof(required_ext[0]);

  // Dummy context to check OpenGL capabilities
  
  cout << "Got OpenGL version: " << sver << endl;
  iver = parseOpenGLVersion(sver);
  cout << "iver = " << iver << endl;
  if (iver < 201)
    {
      cout << "OpenGL version is less than 2.1. Stopping" << endl;
      return false;
    }

 return true;
  char *all_extensions = strdup((char *)glGetString(GL_EXTENSIONS));
  char *base_ptr = all_extensions;
  char *running_ptr = base_ptr;

//  cout << all_extensions << endl;
  while (*all_extensions != 0) {
    running_ptr = strchr(all_extensions, ' ');
    *running_ptr = 0; 
    g_gl_extensions.insert(all_extensions);
    all_extensions=running_ptr+1;
  }

  for (int i = 0; i < numExt; i++)
    {
      if (g_gl_extensions.find(required_ext[i]) == g_gl_extensions.end())
      {
	cout << "The extension " << required_ext[i] << " is required for GalaxExplorer to run." << endl;
        return false;
     }
  }
  
  return true;
}

typedef GLRendererFactory * (*FactoryConstructorType)();

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  QErrorMessage::qtHandler();

  QString texturePath, shaderPath;
  QDir appPath(QApplication::applicationDirPath());

  QCoreApplication::setOrganizationName("GalaxExplorer");
  QCoreApplication::setApplicationName("MainState");
  QSettings settings;

  Q_INIT_RESOURCE(galax);
  Q_INIT_RESOURCE(front_peeling);
  Q_INIT_RESOURCE(wavg_shader);
  Q_INIT_RESOURCE(base_shader);
  Q_INIT_RESOURCE(skybox_shader);
  Q_INIT_RESOURCE(galaxy_shaders);
  Q_INIT_RESOURCE(surface_shader);
  Q_INIT_RESOURCE(tetrahedron_shader);
  Q_INIT_RESOURCE(passthrough_shader);
  qRegisterMetaType<GalaxExplorer::TransferFunction>();
  
  appPath.cdUp();
  texturePath = appPath.absolutePath() + "/textures";
  shaderPath = appPath.absolutePath() + "/shaders";

  QDir::setSearchPaths("textures", QStringList() << (QDir::homePath() + "/.GalaxExplorer/textures") << texturePath);
  QDir::setSearchPaths("shaders", QStringList() << (QDir::homePath() + "/.GalaxExplorer/shaders") << shaderPath);

  initializeSelectors();
  initDataRepresentations();
  initializeDBusService();
  initRendererList();

  Window window;

  window.getUniverse()->glwidget()->makeCurrent();

  if (!checkOpenGL())
    return 1;

  window.show();

  main_window = &window;

  return app.exec();
}
