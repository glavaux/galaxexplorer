/*+
This is GalaxExplorer (./src/object_helper.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __THREED_OBJECT_HELPER_HPP
#define __THREED_OBJECT_HELPER_HPP

#include <cmath>
#include "vboObject.hpp"

namespace GalaxExplorer
{

  class ConeDrawer
  {
  private:
    float *sinTable, *cosTable;
    int nFace;
    bool colorSet;
    int colornum;
    float r, g, b, a;
  public:
    ConeDrawer(int n)
      : colorSet(false), nFace(n), sinTable(new float[n+1]), cosTable(new float[n+1])
    {
      for (int i = 0; i <= n; i++)
	{
	  sinTable[i] = std::sin((2*i*M_PI)/n);
	  cosTable[i] = std::cos((2*i*M_PI)/n);
	}      
    }
    
    ~ConeDrawer()
    {
      delete[] sinTable;
      delete[] cosTable;
    }

    void setColor(GLfloat r, GLfloat g, GLfloat b) { 
      this->r = r; this->g = g; this->b = b; 
      this->colornum = 3;
      this->colorSet = true;
    }

    void setColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a = 1.0) { 
      this->r = r; this->g = g; this->b = b; this->a = a;
      this->colornum = 4;
      this->colorSet = true;
    }

    void unsetColor() {
      this->colorSet = false;
    }

    void draw(VBO_Object& vbo,
	      GLfloat ux, GLfloat uy, GLfloat uz,
	      GLfloat x, GLfloat y, GLfloat z, GLfloat R, GLfloat h);
  };

  class SphereDrawer
  {
  private:
    float *sinTable, *cosTable;
    int nFace;
    GLfloat r, g, b, a;
    int colornum;
    bool colorSet;
  public:
    SphereDrawer(int n)
      : colorSet(false), nFace(n), sinTable(new float[2*n+1]), cosTable(new float[2*n+1])
    {
      for (int i = 0; i <= 2*n; i++)
	{
	  sinTable[i] = std::sin((i*M_PI)/n);
	  cosTable[i] = std::cos((i*M_PI)/n);
	}      
    }
    
    ~SphereDrawer()
    {
      delete[] sinTable;
      delete[] cosTable;
    }

    void setColor(GLfloat r, GLfloat g, GLfloat b) { 
      this->r = r; this->g = g; this->b = b; 
      this->colornum = 3;
      this->colorSet = true;
    }
    void setColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a) { 
      this->r = r; this->g = g; this->b = b; this->a = a;
      this->colornum = 4;
      this->colorSet = true;
    }

    void unsetColor() {
      this->colorSet = false;
    }

    void draw(VBO_Object& vbo, GLfloat x, GLfloat y, GLfloat z, GLfloat R);
  };

  class  CubeDrawer
  {
    GLfloat r, g, b, a;
    int colornum;
    bool colorSet;
    int tex3dattr;
    bool tex3dset;
  public:
    CubeDrawer() : colorSet(false), tex3dset(false) {}
    
    void setColor(GLfloat r, GLfloat g, GLfloat b) { 
      this->r = r; this->g = g; this->b = b; 
      this->colornum = 3;
      this->colorSet = true;
    }
    void setColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a) { 
      this->r = r; this->g = g; this->b = b; this->a = a;
      this->colornum = 4;
      this->colorSet = true;
    }

    void unsetColor() {
      this->colorSet = false;
    }

    void set3DAttribute(int attrid) { 
      this->tex3dattr = attrid; 
      this->tex3dset = true;
    }
    
    void unset3DAttribute() {
      this->tex3dset = false;
    }

    void draw(VBO_Object& vbo, GLfloat x, GLfloat y, GLfloat z, GLfloat L);
  };

};

#endif
