/*+
This is GalaxExplorer (./src/datasets.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <inttypes.h>
#include <vector>
#include "datasets.hpp"
#include "inits.hpp"

//#define INPUT_IS_DOUBLE

using namespace GalaxExplorer;
using namespace std;
  
DataSetSelector::DataSetSelector(DataSelectorDescriptor *d)
{
  descriptor = d;
}

DataSetSelector::~DataSetSelector()
{
}

void DataSetSelector::saveState(QDataStream& data)
{
}

void DataSetSelector::loadState(QDataStream& data)
{
}
    
uint32_t DataSetSelector::getNext(const DataSet& d, uint32_t cur) const
{
  if (cur == d.size())
    return cur;

  return getFirst(d, cur+1);
}

uint32_t DataSetSelector::getFirst(const DataSet& d, uint32_t cur) const
{
  return cur;
}


DataJoinSelector::DataJoinSelector()
{
}

DataJoinSelector::~DataJoinSelector()
{
}

uint32_t DataJoinSelector::getFirst(const DataSet& d, uint32_t cur) const
{
  uint32_t value = cur;
  uint32_t last_value = cur;
  do
    {
      SelectorArray::const_iterator iter = selectors.end();
      while (iter != selectors.begin())
	{
	  --iter;
	  value = (*iter)->getFirst(d, value);
	}
    }
  while (value != last_value);

  return value;
}

DataSet::const_iterator DataSet::begin() const
{
  const_iterator iterator(*this);

  iterator.current = 0;
  return iterator;
}

DataSet::iterator DataSet::begin()
{
  iterator iterator(*this);

  iterator.current = 0;
  return iterator;
}

DataSet::const_iterator DataSet::end() const
{
  const_iterator iterator(*this);

  iterator.current = size();
  return iterator;
}

DataSet::iterator DataSet::end()
{
  iterator iterator(*this, 0);

  iterator.current = size();
  return iterator;
}



DataSet::const_iterator DataSet::beginSelected(const DataSetSelector& selector) const
{
  const_iterator iterator(*this, &selector);

  iterator.current = selector.getFirst(*this, 0);
  return iterator;
}

DataSet::iterator DataSet::beginSelected(const DataSetSelector& selector)
{
  iterator iterator(*this, &selector);

  iterator.current = selector.getFirst(*this, 0);
  return iterator;
}




DataSet::DataSet(BarePoint *points, uint32_t numPoints)
  : DataSource("DataSet")
{
  numDataPoints = numPoints;
  dataPoints = points;
  allAttributes.clear();
}

DataSet::DataSet(const std::vector<DataPoint>& points)
  : DataSource("DataSet")
{
  bool needColor = false, needVelocity = false, needName = false;

  numDataPoints = points.size();
  dataPoints = new BarePoint[numDataPoints];
  for (long i = 0; i < points.size(); i++)
    {
      dataPoints[i].x = points[i].x;
      dataPoints[i].y = points[i].y;
      dataPoints[i].z = points[i].z;
      needColor = needColor || points[i].colPresent;
      needVelocity = needVelocity || points[i].velPresent;
      needName = needName || points[i].namePresent;
    }
  allAttributes.clear();
  createDefaultAttributes(points.begin(), points.end(), needColor, needVelocity, needName);
}

template<typename T>
void DataSet::createDefaultAttributes(T begin, T end,
				      bool needColor, bool needVelocity, bool needName)
{

  addAttribute("lum", createStorage<float>);
  addAttribute("radius", createStorage<float>);
  if (needColor) {
    addAttribute("colorR", createStorage<float>);
    addAttribute("colorG", createStorage<float>);
    addAttribute("colorB", createStorage<float>);
  }
  if (needVelocity) {
    addAttribute("velX", createStorage<float>);
    addAttribute("velY", createStorage<float>);
    addAttribute("velZ", createStorage<float>);
  }
  if (needName) { 
    addAttribute("name", createStorage<QString>);
  }
  DataStorage *attrLum = allAttributes[attributeMap["lum"]];
  DataStorage *attrRad = allAttributes[attributeMap["radius"]];
  DataStorage *attrColorR = (needColor) ? allAttributes[attributeMap["colorR"]] : 0;
  DataStorage *attrColorG = (needColor) ? allAttributes[attributeMap["colorG"]] : 0;
  DataStorage *attrColorB = (needColor) ? allAttributes[attributeMap["colorB"]] : 0;
  DataStorage *attrVelX = (needVelocity) ? allAttributes[attributeMap["velX"]] : 0;
  DataStorage *attrVelY = (needVelocity) ? allAttributes[attributeMap["velY"]] : 0;
  DataStorage *attrVelZ = (needVelocity) ? allAttributes[attributeMap["velZ"]] : 0;
  DataStorage *attrName = (needName) ? allAttributes[attributeMap["name"]] : 0;
  T p = begin;
  int i = 0;
  while (p != end)
   {
     attrLum->getValue<float>(i) = p->lum;
     attrRad->getValue<float>(i) = p->radius;
     if (attrColorR) attrColorR->getValue<float>(i) = p->color[0];
     if (attrColorG) attrColorG->getValue<float>(i) = p->color[1];
     if (attrColorB) attrColorB->getValue<float>(i) = p->color[2];
     if (attrVelX) attrVelX->getValue<float>(i) = p->vx;
     if (attrVelY) attrVelY->getValue<float>(i) = p->vy;
     if (attrVelZ) attrVelZ->getValue<float>(i) = p->vz;
     if (attrName) attrName->getValue<QString>(i) = p->name;
     ++p;
     ++i;
   }
}

DataSet::DataSet(const std::vector<BarePoint>& points)
  : DataSource("DataSet")
{
  numDataPoints = points.size();
  dataPoints = new BarePoint[numDataPoints];
  memcpy(dataPoints, &points[0], sizeof(BarePoint)*numDataPoints);
  allAttributes.clear();
}

DataSet::DataSet()
  : DataSource("DataSet")
{
  numDataPoints = 0;
  dataPoints = 0;
}

DataSet::DataSet(const std::list<DataPoint>& points)
  : DataSource("DataSet")
{
  bool needColor = false, needVelocity = false, needName = false;

  numDataPoints = points.size();
  dataPoints = new BarePoint[numDataPoints];
  
  std::list<DataPoint>::const_iterator iter = points.begin();
  uint32_t i = 0;
  while (iter != points.end())
    {
      dataPoints[i].x = iter->x;
      dataPoints[i].y = iter->y;
      dataPoints[i].z = iter->z;
      needColor = needColor || iter->colPresent;
      needVelocity = needVelocity || iter->velPresent;
      needName = needName || iter->namePresent;
      ++iter;
      ++i;
    }
  createDefaultAttributes(points.begin(), points.end(), needColor, needVelocity, needName);
}

DataSet::~DataSet()
{
  if (numDataPoints && dataPoints != 0)
    delete[] dataPoints;
}






void DataSet::addAttribute(const QString& s, StorageCreator f)
{
  attributeMap[s] = allAttributes.size();
  allAttributes.push_back(f(size()));
}

void DataSet::addAttribute(const QString& s, DataStorage *storage)
  throw (InvalidStorageError)
{
  if (storage->getNumElements() != size())
    throw InvalidStorageError();

  attributeMap[s] = allAttributes.size();
  allAttributes.push_back(storage);
}

void DataSet::saveData(QDataStream& data)
{  
  int numAttributes = allAttributes.size();

  data << numDataPoints << numAttributes;
  data << getDistanceMultiplier();
  data << attributeMap;
  for (uint32_t i = 0; i < numDataPoints; i++)
    {
      BarePoint& p = dataPoints[i];

      data
	<< p.x << p.y << p.z; 
    }

  for (uint32_t i = 0; i < allAttributes.size(); i++)
   {
     int32_t sType = allAttributes[i]->getStorageType();

     data << sType;
     allAttributes[i]->writeTo(data);
   }
}

void DataSet::loadData(QDataStream& data)
{
  if (numDataPoints)
    delete[] dataPoints;

  data >> numDataPoints;
  int numAttributes;
  data >> numAttributes;
  double mul;
  data >> mul;
  setDistanceMultiplier(mul);
  data >> attributeMap;

  dataPoints = new BarePoint[numDataPoints];
  for (uint32_t i = 0; i < numDataPoints; i++)
    {
      BarePoint& p = dataPoints[i];
      data >> p.x >> p.y >> p.z;
    }  
  allAttributes.clear();
  for (int i = 0; i < numAttributes; i++)
    {
       int32_t storageType;
       data >> storageType;
       allAttributes.push_back(dataStorageFromType(storageType, numDataPoints));
       allAttributes[i]->loadFrom(data);
    }
}


DataSetConstIterator& DataSetConstIterator::operator=(const DataSetConstIterator& i)
{
  selector = i.selector;
  current = i.current;
  
  return *this;
}


BarePoint& DataSet::virtualGet(uint32_t i)
{
  return dataPoints[i];
}

BarePoint DataSet::virtualGet(uint32_t i) const
{
  return dataPoints[i];
}

