/*+
This is GalaxExplorer (./src/selectorByDistance.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef _SELECT_BY_DISTANCE_HPP
#define _SELECT_BY_DISTANCE_HPP

#include <QObject>
#include "datasets.hpp"
#include "dataSelectors.hpp"

namespace GalaxExplorer
{
  
  class DataSelectionByDistance: public DataSelectorDescriptor
  {
    Q_OBJECT
  public:
    DataSelectionByDistance();
    virtual ~DataSelectionByDistance();


    virtual DataSetSelector *newSelector();
  };

  class SelectByDistance: public DataSetSelector
  {
    Q_OBJECT
    
    Q_PROPERTY(bool keepInsideRange READ keepInside WRITE setKeepInside)
    Q_PROPERTY(double minDistance READ getMinDistance WRITE setMinDistance)
    Q_PROPERTY(double maxDistance READ getMaxDistance WRITE setMaxDistance)
  public:
    SelectByDistance(DataSelectionByDistance *s);
    ~SelectByDistance();;

    void setKeepInside(bool keep);
    bool keepInside();
    void setMinDistance(double minDist);
    double getMinDistance();
    void setMaxDistance(double maxDist);
    double getMaxDistance();

    virtual void loadState(QDataStream& s);
    virtual void saveState(QDataStream& s);

    virtual uint32_t getFirst(const DataSet& d, uint32_t cur) const;
  private:
    bool keepInsideRange;
    double minDistance;
    double maxDistance;
  };

};


#endif
