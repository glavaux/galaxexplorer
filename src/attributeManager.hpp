#ifndef __GALAX_ATTRIBUTE_MGR_HPP
#define __GALAX_ATTRIBUTE_MGR_HPP

#include <exception>
#include <QString>
#include <QObject>
#include <QMap>

namespace GalaxExplorer
{

  class NoSuchAttribute: virtual std::exception
  {
  };
  
  class AttributeManager {
  public:
    AttributeManager();
    ~AttributeManager();
    
    void setObject(const QString& n, QObject* o);
    const QObject *get(const QString& n) const;
    QObject* get(const QString& n);

    const QObject* operator[](const QString& n) const
    {
      return get(n);
    }

    QObject* operator[](const QString& n)
    {
      return get(n);
    }

  protected:
    typedef QMap<QString,QObject *> AttribMap;
    AttribMap attribs;
  };

};

#endif
