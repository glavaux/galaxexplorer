#include "attributeManager.hpp"

using namespace GalaxExplorer;

AttributeManager::AttributeManager()
{
}

AttributeManager::~AttributeManager()
{
}

void AttributeManager::setObject(const QString& n, QObject *o)
{
  attribs[n] = o;
}

const QObject* AttributeManager::get(const QString& n) const
{
  AttribMap::const_iterator i = attribs.find(n);
  
  if (i == attribs.end())
    throw NoSuchAttribute();
    
  return *i;
}

QObject* AttributeManager::get(const QString& n)
{
  AttribMap::iterator i = attribs.find(n);
  
  if (i == attribs.end())
    throw NoSuchAttribute();
    
  return *i;
}
