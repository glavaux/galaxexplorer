/*+
This is GalaxExplorer (./src/datastorage.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __DATA_STORAGE_HPP
#define __DATA_STORAGE_HPP

#include <cassert>
#include <typeinfo>
#include <QtCore>

namespace GalaxExplorer
{

  template<typename T>
  class DataStorageSpecific; 

  class DataStorage
  {
  public:
    virtual ~DataStorage() { }

    virtual void resize(long numElements) = 0;
    long getNumElements() const { return numElements; }

    template<typename T>
    T& getValue(long i) {
      assert (typeid(*this) == typeid(DataStorageSpecific<T>));
      return ((T*)data)[i];
    }

    template<typename T>
    const T& getValue(long i) const {
      assert (typeid(*this) == typeid(DataStorageSpecific<T>));
      return ((T*)data)[i];
    }

    virtual void writeTo(QDataStream& data) const = 0;
    virtual void loadFrom(QDataStream& data) = 0;
    int32_t getStorageType() const;
    virtual const std::type_info& getStorageComparator() const = 0;

  protected:
    void *data;
    long fullsize, numElements;
  };

  template<typename T>
  class DataStorageSpecific: public DataStorage
  {
  public:
     DataStorageSpecific(long initialSize) {
        data = new T[initialSize];
        fullsize = initialSize*sizeof(T);
        numElements = initialSize;
     }

     virtual ~DataStorageSpecific() { delete[] (T*)data;}
     
     virtual void resize(long numElements) { 
        T *newArray = new T[numElements];
        memcpy(newArray, data, sizeof(T)*this->numElements);
        this->numElements = numElements;
        data = newArray;
        fullsize = sizeof(T)*numElements;
     }

     virtual void writeTo(QDataStream& s) const {
        for (long i = 0; i < numElements; i++)
          s << ((T*)data)[i];
     }

     virtual void loadFrom(QDataStream& s) {
        for (long i = 0; i < numElements; i++)
          s >> ((T*)data)[i];
     }

     virtual const std::type_info& getStorageComparator() const { return typeid(T); }
  };

  template<typename T>
  DataStorage *createStorage(long numElements) 
  { 
    return new DataStorageSpecific<T>(numElements);
  }

  DataStorage  *dataStorageFromType(int32_t typeId, long numElements);
};

#endif
