/*+
This is GalaxExplorer (./src/datasets.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __DATASETS_HPP
#define __DATASETS_HPP

#include <QObject>
#include <QString>
#include <QVector>
#include <QMap>
#include <exception>
#include <inttypes.h>
#include <vector>
#include <list>
#include <QDataStream>
#include "points.hpp"
#include "datasource.hpp"
#include "datastorage.hpp"

namespace GalaxExplorer 
{ 
#define getLuminosityAttribute(cur) getAttributeValue<float>("lum", cur)
#define getVelocityXAttribute(cur) getAttributeValue<float>("velX", cur)
#define getVelocityYAttribute(cur) getAttributeValue<float>("velY", cur)
#define getVelocityZAttribute(cur) getAttributeValue<float>("velZ", cur)

  class DataSet;
  
  // =========================
  // Data Set selector
  //
  
  class DataSelectorDescriptor;
  class DataSetSelector : public QObject
  {
    Q_OBJECT
    
    Q_PROPERTY(QString name READ getName WRITE setName)
  public:
    DataSetSelector(DataSelectorDescriptor *d = 0);
    virtual ~DataSetSelector();
    
    const QString& getName() const { return myName; }
    void setName(const QString& n) { myName = n; }

    virtual DataSelectorDescriptor *getDescriptor() { return descriptor; }

    virtual uint32_t getNext(const DataSet& d, uint32_t cur) const;
    virtual uint32_t getFirst(const DataSet& d, uint32_t cur) const;

    virtual void saveState(QDataStream& data);
    virtual void loadState(QDataStream& data);
  private:
    QString myName;
    DataSelectorDescriptor *descriptor;
  };

  class DataJoinSelector
  {
  public:
    DataJoinSelector();
    virtual ~DataJoinSelector();

    void addSelector(const DataSetSelector& a) { selectors.push_back(&a); }

    virtual uint32_t getFirst(const DataSet& d, uint32_t cur) const;
  protected:
    typedef std::vector<const DataSetSelector *> SelectorArray;
    SelectorArray selectors;
  };

  

  // ==========================
  // Iterators
  class NoSuchAttributeError {};
  class InvalidStorageError {};

  class DataSetIterator
  {
  public:    
    DataSetIterator(DataSet& data, const DataSetSelector *sel = 0) : d(data), selector(sel) {}
    DataSetIterator(const DataSetIterator& b) :  current(b.current), d(b.d), selector(b.selector) {}
    ~DataSetIterator() {}
  
    DataSetIterator& operator++ ();
    BarePoint operator* ();

    template<typename T>
    T& getAttribute(const QString& s) throw (NoSuchAttributeError); 
    template<typename T>
    T& getAttribute(int attrid); 

    int getAttributeID(const QString& s) const;

    bool operator== (const DataSetIterator& b) const { return current == b.current; }
    bool operator!= (const DataSetIterator& b) const { return current != b.current; }

    friend class DataSet;
    friend class DataSetConstIterator;

  protected:
    uint32_t current;
    DataSet& d;
    const DataSetSelector *selector;
  };

  class DataSetConstIterator
  {
  public:
    DataSetConstIterator(const DataSet& data, const DataSetSelector *sel = 0) : d(data), selector(sel) {}
    DataSetConstIterator(const DataSetConstIterator& b) : current(b.current), d(b.d), selector(b.selector) {}
    DataSetConstIterator(const DataSetIterator& b) : current(b.current), d(b.d), selector(b.selector) {}
    ~DataSetConstIterator() {}
  
    DataSetConstIterator& operator++ ();
    BarePoint operator* ();
    bool operator== (const DataSetConstIterator& b) const { return current == b.current; }
    bool operator!= (const DataSetConstIterator& b) const { return current != b.current; }

    template<typename T>
    const T& getAttribute(const QString& s) const throw (NoSuchAttributeError); 
    template<typename T>
    const T& getAttribute(int attrid) const;

    int getAttributeID(const QString& s) const;

    DataSetConstIterator& operator=(const DataSetConstIterator& i);
    
    friend class DataSet;

  protected:
    uint32_t current;  
    BarePoint p;
    const DataSet& d;
    const DataSetSelector *selector;
  };

  // ========================
  // Data Set

  class DataSet: public DataSource 
  {
    Q_OBJECT

  public:
    typedef DataSetIterator iterator;
    typedef DataSetConstIterator const_iterator;

    typedef DataStorage *(*StorageCreator)(long);

    friend class DataSetIterator;
    friend class DataSetConstIterator;

    DataSet(const std::vector<DataPoint>& points);
    DataSet(const std::vector<BarePoint>& points);
    DataSet(BarePoint *points, uint32_t numPoints);
    DataSet(const std::list<DataPoint>& points);
    DataSet();
    virtual ~DataSet();

    const_iterator begin() const;
    iterator begin();
    const_iterator end() const;
    iterator end();

    const_iterator beginSelected(const DataSetSelector& selector) const;
    iterator beginSelected(const DataSetSelector& selector);

    virtual uint32_t size() const { return numDataPoints; }

    BarePoint& operator[](uint32_t i) { return (dataPoints == 0) ? virtualGet(i) : dataPoints[i]; }
    BarePoint operator[](uint32_t i) const { return (dataPoints == 0) ? virtualGet(i) : dataPoints[i]; }

    template<typename T>
    T &getAttributeValue(int attrid, uint32_t i) { return allAttributes[attrid]->getValue<T>(i); }

    template<typename T>
    T &getAttributeValue(const QString& name, uint32_t i) 
      throw (NoSuchAttributeError)
    { 
       int attrid = getAttributeID(name);

       if (attrid < 0)
         throw NoSuchAttributeError();

       return allAttributes[attrid]->getValue<T>(i); 
    }

    template<typename T>
    const T &getAttributeValue(int attrid, uint32_t i) const { return allAttributes[attrid]->getValue<T>(i); }

    template<typename T>
    const T &getAttributeValue(const QString& name, uint32_t i) const
       throw(NoSuchAttributeError)
    {
       int attrid = getAttributeID(name);

       if (attrid < 0)
         throw NoSuchAttributeError();

       return allAttributes[attrid]->getValue<T>(i);
    }

    int getAttributeID(const QString& name) const { 
      if (attributeMap.contains(name)) 
        return attributeMap[name];
      else 
	return -1; 
    }

    void addAttribute(const QString& name, StorageCreator f);
    void addAttribute(const QString& name, DataStorage *storage)
      throw (InvalidStorageError);

    virtual void saveData(QDataStream& data);
    virtual void loadData(QDataStream& data);

  private:
    template<typename T>
    void createDefaultAttributes(T begin, T end, bool needColor, bool needVelocity, bool needName);

  signals:
    void dataUpdated();

  protected:
    QMap<QString,int> attributeMap;
    BarePoint *dataPoints;
    quint32 numDataPoints;
    std::vector<DataStorage *> allAttributes;

    virtual BarePoint& virtualGet(uint32_t i);
    virtual BarePoint virtualGet(uint32_t i) const;
  };


  typedef QMap<QString, DataSetSelector *> AllDataSelector;

  template<typename T>
  const T& DataSetConstIterator::getAttribute(const QString & s) const
     throw (NoSuchAttributeError)
    {
      int id = d.getAttributeID(s);
      if (id < 0)
        throw NoSuchAttributeError();

      return d.getAttributeValue<T>(id,current);
    }

  template<typename T>
  const T& DataSetConstIterator::getAttribute(int attrid) const
    {
      return d.getAttributeValue<T>(attrid,current);
    }

  template<typename T>
  T& DataSetIterator::getAttribute(const QString & s) 
     throw (NoSuchAttributeError)
    {
      int id = d.getAttributeID(s);
      if (id < 0)
        throw NoSuchAttributeError();

      return d.getAttributeValue<T>(id,current);
    }

  template<typename T>
  T& DataSetIterator::getAttribute(int attrid) 
    {
      return d.getAttributeValue<T>(attrid,current);
    }


  inline int DataSetIterator::getAttributeID(const QString& n) const {
    return d.getAttributeID(n);
  }

  inline int DataSetConstIterator::getAttributeID(const QString& n) const {
    return d.getAttributeID(n);
  }
};

inline GalaxExplorer::DataSetIterator& GalaxExplorer::DataSetIterator::operator++ ()
{
  current = (selector == 0) ? (current+1) : selector->getNext(d, current);  
  return *this;
}

inline GalaxExplorer::BarePoint GalaxExplorer::DataSetIterator::operator* () 
{ 
  return d[current]; 
}  

inline GalaxExplorer::DataSetConstIterator& GalaxExplorer::DataSetConstIterator::operator++ ()
{
  current = (selector == 0) ? (current+1) : selector->getNext(d, current);  
  return *this;
}

inline GalaxExplorer::BarePoint GalaxExplorer::DataSetConstIterator::operator* () 
{ 
  return d[current]; 
}  



#endif
