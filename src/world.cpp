/*+
This is GalaxExplorer (./src/world.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <QtCore>
#include "glwidget.hpp"
#include "world.hpp"
#include "save_load.hpp"
#include "window.hpp"
#include "skybox.hpp"
#include "inits.hpp"

using namespace GalaxExplorer;

#define HAS_VIEWSTATE (1<<0)

World::World(Window *parent)
  : QObject(parent), window(parent)
{
}

World::~World()
{
}

void World::finish_initialization()
{
  QSettings settings;
  GLRendererFactory *fac = getRendererByName(settings.value("renderer").toString());

  if (fac != 0)
    window->changeRenderer(fac);
}

void World::addNewConnector(Connector *c)
{
  allConnectors.push_back(*c);

  int newid = allConnectors.size()-1;

  window->addConnectorToList(c->getName(), newid);
  
  universeWidget->setActiveConnectors(allConnectors);
}

bool World::removeConnector(const QString& name)
{
  int row = 0;
  bool done = false;

  for (ConnectorVector::iterator i = allConnectors.begin();
       i != allConnectors.end() && !done;
       ++i,++row)
    {
      if (i->getName() != name)
	continue;

      allConnectors.remove(row);
      window->recreateConnectorList();
      universeWidget->setActiveConnectors(allConnectors);
      done = true;
    }

  return done;
}

QStringList World::getConnectorList()
{
  QStringList l;

  for (ConnectorVector::iterator i = allConnectors.begin();
       i != allConnectors.end();
       ++i)
    {
      l.push_back(i->getName());
    }
  return l;
}

void World::setShowConnector(const QString& name, bool visible)
{
  for (ConnectorVector::iterator i = allConnectors.begin();
       i != allConnectors.end();
       ++i)
   {
     if (i->getName() == name) {
       i->setShow(visible);
       if (visible)
	 universeWidget->showConnector(name);
       else
	 universeWidget->hideConnector(name);
       return;
     }
   }
}

void World::installConnectors(const ConnectorVector& c)
{
  allConnectors = c;
  universeWidget->setActiveConnectors(c);  
}

void World::cleanupSelectors()
{
  AllDataSelector::iterator iter = selectors.begin();
  while (iter != selectors.constEnd())
    {
      delete (*iter);
      iter++;
    }

  selectors.clear();
}

void World::loadState(QString fname)
{
  QFile file(fname);
  if (!file.open(QIODevice::ReadOnly))
    {
      qCritical(qPrintable(tr("Cannot open ") + fname + tr(" for reading.")));
      return;
    }

  QDataStream sdata(&file);

  cleanupSelectors();
  allConnectors.clear();
  try
    {
      ::loadState(sdata, selectors, allConnectors);
    }
  catch (const IncompatibleVersion& e)
    {
      qCritical(qPrintable(tr("Invalid data file ") + fname + QString(".")));
    }

  universeWidget->setActiveConnectors(allConnectors);

  {
    qint32 extendedAttributes;
    sdata >> extendedAttributes;
    if (extendedAttributes & HAS_VIEWSTATE)
      {
        QMap<QString,QVariant> qmap;
        sdata >> qmap;
        universeWidget->getState()->loadState(qmap);
     }
  }
  postLoadState();
}

void World::refreshConnectors()
{
  universeWidget->setActiveConnectors(allConnectors);
}

void World::saveState(QString fname)
{
  QFile file(fname);
  if (!file.open(QFile::WriteOnly | QFile::Truncate))
    {
      qCritical(qPrintable(tr("Cannot open ") + fname + tr(" for writing.")));
      return;
    }

  QDataStream sdata(&file);
  
  ::saveState(sdata, selectors, allConnectors);
  qint32 extendedAttributes = HAS_VIEWSTATE;
  sdata << extendedAttributes;
  {
    QMap<QString,QVariant> qmap;
    universeWidget->getState()->saveState(qmap);

    sdata << qmap;
  }
}

