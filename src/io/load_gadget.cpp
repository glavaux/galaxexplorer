/*+
This is GalaxExplorer (./src/io/load_gadget.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <QtCore>
#include <iostream>
#include "points.hpp"
#include "load_gadget.hpp"
#include "load_data.hpp"
#include "datasets.hpp"
#include "datastorage.hpp"
#include <CosmoTool/loadGadget.hpp>
#include <CosmoTool/loadSimu.hpp>
#include <QErrorMessage>
#include <QMessageBox>
#include "virtualDataSetTrajectory.hpp"
#include "progress.hpp"

using namespace GalaxExplorer;
using namespace std;
using namespace CosmoTool;

static DataSet *loadGadgetBase(const QString& fname, bool override);

EvolvingGadgetLoader::EvolvingGadgetLoader(const QString& base_name, int Nmax)
  throw (VirtualDataSetException)
  : g_name(base_name)
{
  cerr << "Initializing gadget loader with pattern " << base_name.toAscii().constData() << endl;
  QString name0 = base_name.arg(0,3,10,QLatin1Char('0'));
  QByteArray fname_data = name0.toLatin1();
  SimuData *s;
  
  multi = false;

  if (name0.endsWith(".0"))
    {
      multi = true;
      name0.chop(2);
    }
  
  times.clear();
  for (N=0; (Nmax<=0 || N<Nmax); N++)
    {
      name0 = base_name.arg(N,3,10,QLatin1Char('0'));
      fname_data = name0.toLatin1();
      try
	{
	  cerr << "Attempting to load " << fname_data.data() << endl;
	  s = loadGadgetMulti(fname_data.data(), multi ? 0 : -1, 0);
	}
      catch (const NoSuchFileException& e)
	{
	  s = 0;
	}


      if (s == 0 && N==0)
	throw VirtualDataSetException("Error while preliminary loading");

      if (s==0)
	break;

      Ndata = s->TotalNumPart;
      boxsize = s->BoxSize;
      times.push_back(s->time);

      delete s;
    }

}

EvolvingGadgetLoader::~EvolvingGadgetLoader()
{
}

DataSet *EvolvingGadgetLoader::loadDataSet(int id)
{
  if (id < 0 || id >= N)
    return 0;

  QString name = g_name.arg(id,3,10,QLatin1Char('0'));
  
  cout << "Loading gadget snapshot " << name.toAscii().constData() << endl;

  return loadGadgetBase(name, true);
}

float EvolvingGadgetLoader::getDataSetTimeStart(int id)
{
  return times[id];
}

static DataSet *loadGadgetBase(const QString& fname, bool override)
{
  bool multi = false;
  SimuData *s;
  QString fname2 = fname;
  int numFiles = 0;
  BarePoint *data;
  int choice;
  float max_extent = -INFINITY, min_extent = INFINITY;

  
  // We want to detect the first of a serie if possible.
  if (fname2.endsWith(".0"))
    {
      multi = true;
      fname2.chop(2);
    }

  QByteArray fname_data = fname2.toAscii();
  s = loadGadgetMulti(fname_data.constData(), multi ? 0 : -1, 0);

  if (s == 0)
    {
      QErrorMessage::qtHandler()->showMessage(QString("Cannot load gadget file %1").arg(fname2));
      return 0;
    }

  if (multi)
    {
      for (numFiles = 0; ; numFiles++)
	{
	  s = loadGadgetMulti(fname_data.constData(), numFiles, 0);
	  if (s != 0)
	    delete s;
	  else
	    break;
	}

    }
  else
    numFiles=1;
  
  // Inspect memory requirement.
  uint32_t memRequired = (9*sizeof(float)+sizeof(int))*s->TotalNumPart/1024/1024;
  long totalNumPart = s->TotalNumPart;

  delete s;

  if (!override)
    {
      choice = 
	QMessageBox::question(0, QObject::tr("Memory requirement"), 
			      QString(QObject::tr("%1 Megabytes of memory required to load the gadget file. Continue ?")).arg(memRequired),
			      QMessageBox::Yes|QMessageBox::No);
      
      if (choice == QMessageBox::No)
	return 0;
    }

  long cur = 0;
  int *datatype = new int[totalNumPart];
  data = new BarePoint[totalNumPart];
  UserIOHandler ioh;

  ioh.startProgress(QObject::tr("Loading gadget data..."), 2*numFiles);

  for (int id=0;id<numFiles;id++)
    {
      ioh.setProgress(id);

      s = loadGadgetMulti(fname_data.constData(), multi ? id : -1, NEED_POSITION|NEED_TYPE|NEED_GADGET_ID); 

      if (s == 0)
	{
          ioh.doneProgress();
	  delete[] data;
	  delete[] datatype;
	  return 0;
	}
  
      for (long j = 0; j < s->NumPart; j++,cur++)
	{
	  int q = s->Id[j]-1;
	  data[q].x = s->Pos[0][j];
	  data[q].y = s->Pos[1][j];
	  data[q].z = s->Pos[2][j];
	  datatype[q] = s->type[j];

	  max_extent = max(max_extent, data[q].x);
	  max_extent = max(max_extent, data[q].y);
	  max_extent = max(max_extent, data[q].z);

	  min_extent = min(min_extent, data[q].x);
	  min_extent = min(min_extent, data[q].y);
	  min_extent = min(min_extent, data[q].z);
	}

      delete s;

      if (ioh.wasProgressCanceled())
	{
          ioh.doneProgress();
	  delete[] data;
	  delete[] datatype;
	  return 0;
	}      
    }

  ioh.doneProgress();

  if (cur != totalNumPart)
    {
      int choice = 
	QMessageBox::question(0, "Memory requirement", 
			      QString("only %1 out of %2 particles loaded. Continue ?").arg(cur).arg(totalNumPart),
			      QMessageBox::Yes|QMessageBox::No);
      if (choice == QMessageBox::No)
	{
	  delete[] data;
	  delete[] datatype;
	  return 0;
	}
    }

  
  float total_extent = max(max_extent,-min_extent);
  float multiplier = 10./total_extent;

  DataSet *universeData = new DataSet(data, totalNumPart);
  universeData->setDistanceMultiplier(multiplier);
  universeData->addAttribute("particle_type", createStorage<int>);

  int& base_type = universeData->getAttributeValue<int>("particle_type", 0);
  memcpy(&base_type, datatype, sizeof(int)*totalNumPart);

  delete[] datatype;

  return universeData;
}

void GalaxExplorer::loadGadget(const QString& fname)
{
  DataSet *universeData;
  
  universeData = loadGadgetBase(fname, false);
  if (universeData == 0)
    return;

  addDataSource(universeData);
}
