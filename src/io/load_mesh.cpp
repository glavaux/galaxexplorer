/*+
This is GalaxExplorer (./src/io/load_mesh.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <QtCore>
#include <netcdfcpp.h>
#include "dataSetTrajectory.hpp"
#include "load_mesh.hpp"
#include "load_data.hpp"
#include "datamesh.hpp"
#include "datasets.hpp"
#include <cmath>
#include "glwidget.hpp"

using namespace GalaxExplorer;
using namespace std;

template<typename L>
void prepareData(L& l, double& multiplier)
{
  typename L::iterator iter;
  typename L::iterator last = l.end();


  double minCoord = INFINITY;
  double maxCoord = -INFINITY;

  for (iter = l.begin(); iter != last; ++iter)
    {
      const BarePoint& p = *iter;

      if (p.x < minCoord)
	minCoord = p.x;
      if (p.y < minCoord)
	minCoord = p.y;
      if (p.z < minCoord)
	minCoord = p.z;
      if (p.x > maxCoord)
	maxCoord = p.x;
      if (p.y > maxCoord)
	maxCoord = p.y;
      if (p.z > maxCoord)
	maxCoord = p.z;
    }

  double val = max(maxCoord,-minCoord);
  
  multiplier = UNIVERSE_BOXSIZE / val;
}

void GalaxExplorer::loadMeshDataFromFile(const QString& fname)
{
  uint32_t numTime;
  float *timeSampling;

  NcFile f(qPrintable(fname), NcFile::ReadOnly);
  if (!f.is_valid())
    {
      qCritical(qPrintable(QObject::tr("Cannot open ") + fname + QObject::tr(" for reading")));
    }

  
  NcVar *vTri = f.get_var("array");

  if (vTri->num_dims() != 2)
    {
      qCritical("Problems with dimensions");
    }

  long *numPoints = vTri->edges();
  long N = numPoints[0];  
  
  if (numPoints[1] != 3)
    {
      qCritical("Problems with number of components");
    }


  long numTriangles = N/3;
  if ((N % 3) != 0)
    {
      qCritical("Should be 3-triangles");
    }

  float *points = new float[3*N];

  vTri->get(points, N, 3);

  BarePoint *b_points = new BarePoint[N];

  for (int i = 0; i < N; i++)
    {
      b_points[i].x = points[3*i+0];
      b_points[i].y = points[3*i+1];
      b_points[i].z = points[3*i+2];
    }
  delete[] points;

  DataSet *myset = new DataSet(b_points, N);
  myset->setDistanceMultiplier(10.0/200.);
  addDataSource(myset);
  
  int *plist = new int[N];
  for (int i = 0; i < N; i++)
    {
      plist[i] = i;
    }

  DataMesh *mesh = new DataMesh(plist, numTriangles);
  addDataSource(mesh);
  
}
