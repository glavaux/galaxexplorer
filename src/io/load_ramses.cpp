/*+
This is GalaxExplorer (./src/io/load_ramses.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <QtCore>
#include <iostream>
#include "points.hpp"
#include "load_ramses.hpp"
#include "load_data.hpp"
#include "datasets.hpp"
#include "datastorage.hpp"
#include <CosmoTool/loadRamses.hpp>
#include <CosmoTool/loadSimu.hpp>
#include <QErrorMessage>
#include <QMessageBox>
#include "virtualDataSetTrajectory.hpp"
#include "progress.hpp"

using namespace GalaxExplorer;
using namespace std;
using namespace CosmoTool;

static const bool DOUBLE_PREC = true;

static DataSet *loadRamsesBase(const QString& basepath, int id, bool override);

EvolvingRamsesLoader::EvolvingRamsesLoader(const QString& base_name, int Nmax)
  throw (VirtualDataSetException)
  : g_name(base_name)
{
  cerr << "Initializing ramses loader with base path " << base_name.toAscii().constData() << endl;
  SimuData *s;

  times.clear();
  for (N=0; (Nmax<=0 || N<Nmax); N++)
    {
      int N0 = N+1;
      QString name = base_name + QString("/output_%0/").arg(N0,5,10,QLatin1Char('0'));
      try
	{
	  cout << "Attempting " << name.toAscii().constData() << " N=" << N << endl;
	  s = loadRamsesSimu(name.toAscii().constData(), N0, 1, DOUBLE_PREC, 0);
	}
      catch (const NoSuchFileException& e)
	{
	  s = 0;
	}


      if (s == 0 && N==0)
	throw VirtualDataSetException("Error while preliminary loading");

      if (s==0)
	break;

      Ndata = s->TotalNumPart;
      boxsize = s->BoxSize;
      times.push_back(s->time);

      delete s;
    }
  this->basepath = base_name;
}

EvolvingRamsesLoader::~EvolvingRamsesLoader()
{
}

DataSet *EvolvingRamsesLoader::loadDataSet(int id)
{
  if (id < 0 || id >= N)
    return 0;

  QString name = basepath + QString("/output_%0/").arg(id+1,5,10,QLatin1Char('0'));

  return loadRamsesBase(name.toAscii().constData(), id+1, true);
}

float EvolvingRamsesLoader::getDataSetTimeStart(int id)
{
  return times[id];
}

static DataSet *loadRamsesBase(const QString& basepath, int id, bool override)
{
  bool multi = false;
  SimuData *s;
  QString fname2 = basepath;
  int numFiles = 0;
  BarePoint *data;
  int choice;
  float max_extent = -INFINITY, min_extent = INFINITY;
  UserIOHandler ioh;

  QByteArray fname_data = basepath.toAscii();
  s = loadRamsesSimu(fname_data.constData(), id, 1, DOUBLE_PREC, NEED_POSITION);

  if (s == 0)
    {
      QErrorMessage::qtHandler()->showMessage(QString("Cannot load ramses output %1 (id %2)").arg(basepath).arg(id));
      return 0;
    }

  // Inspect memory requirement.
  uint32_t memRequired = (9*sizeof(float)+sizeof(int))*s->TotalNumPart/1024/1024;
  long totalNumPart = s->TotalNumPart;

  delete s;

  if (!override)
    {
      choice = 
	QMessageBox::question(0, "Memory requirement", 
			      QString("%1 Megabytes of memory required to load the ramses file. Continue ?").arg(memRequired),
			      QMessageBox::Yes|QMessageBox::No);
      
      if (choice == QMessageBox::No)
	return 0;
    }

  long cur = 0;
  int *datatype = new int[totalNumPart];
  data = new BarePoint[totalNumPart];


  ioh.startProgress("Loading ramses data...", totalNumPart);

  int cpu_id = 0;
  while (1)
    {
      ioh.setProgress(cur);

      s = loadRamsesSimu(fname_data.constData(), id, cpu_id, DOUBLE_PREC, NEED_POSITION|NEED_GADGET_ID); 

      if (s == 0)
	{
	  break;
	}
  
      for (long j = 0; j < s->NumPart; j++,cur++)
	{
	  int q = s->Id[j]-1;
	  data[q].x = s->Pos[0][j];
	  data[q].y = s->Pos[1][j];
	  data[q].z = s->Pos[2][j];
	  //	  datatype[q] = s->type[j];

	  max_extent = max(max_extent, data[q].x);
	  max_extent = max(max_extent, data[q].y);
	  max_extent = max(max_extent, data[q].z);

	  min_extent = min(min_extent, data[q].x);
	  min_extent = min(min_extent, data[q].y);
	  min_extent = min(min_extent, data[q].z);
	}

      delete s;

      if (ioh.wasProgressCanceled())
	{
	  delete[] data;
	  delete[] datatype;
	  return 0;
	}      
      cpu_id++;
    }

  ioh.setProgress(totalNumPart);

  if (cur != totalNumPart)
    {
      int choice = 
	QMessageBox::question(0, "Memory requirement", 
			      QString("only %1 out of %2 particles loaded. Continue ?").arg(cur).arg(totalNumPart),
			      QMessageBox::Yes|QMessageBox::No);
      if (choice == QMessageBox::No)
	{
	  delete[] data;
	  delete[] datatype;
	  return 0;
	}
    }

  
  float total_extent = max(max_extent,-min_extent);
  float multiplier = 10./total_extent;

  DataSet *universeData = new DataSet(data, cur);
  universeData->setDistanceMultiplier(multiplier);

  return universeData;
}

void GalaxExplorer::loadRamses(const QString& fname, int id)
{
  DataSet *universeData;
  
  universeData = loadRamsesBase(fname, id, false);
  if (universeData == 0)
    return;

  addDataSource(universeData);
}

void GalaxExplorer::loadRamses(const QString& name, const QString& fname, int id)
{
  DataSet *universeData;
  
  universeData = loadRamsesBase(fname, id, true);
  if (universeData == 0)
    return;

  addDataSource(name, universeData);
}
