/*+
This is GalaxExplorer (./src/io/load_simulation.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <QtGui>
#include "datasets.hpp"
#include "load_simulation.hpp"
#include "ui_simulationDialog.h"
#include "inits.hpp"

using namespace GalaxExplorer;

struct SimulationDescriptor
{
  const char *name;
  DataSet *(*creator)(const QString& fname, int decimation, float scaling,
		      float *translation, bool load_velocities);
};


DataSet *simLoadGadget(const QString& fname, int decimation, float scaling,
		       float *translation, bool load_velocities)
{
}

static const SimulationDescriptor descriptors[] = {
  { "Gadget", simLoadGadget }
};

static const int numDescriptors = 1;

#define tr QObject::tr

void GalaxExplorer::loadSimulation(const QString& fname)
{
  QDialog dialog;
  Ui::SimulationDialog ui;
  float scaleFactor, translate[3];
  int decimation;
  bool loadVelocities;
  QString dataSetName;

  ui.setupUi(&dialog);
  
  ui.filename->setText(fname);
  for (int i = 0; i < numDescriptors; i++)
    ui.simuFileType->addItem(descriptors[i].name);
  
  ui.scaleFactor->setText("1.0");
  ui.decimation->setText("1");
  ui.x_translate->setText("0.0");
  ui.y_translate->setText("0.0");
  ui.z_translate->setText("0.0");
  ui.loadVelocities->setChecked(true);
  
  do
    {
      bool ok;
      if (!dialog.exec())
	return;
    
      scaleFactor = ui.scaleFactor->text().toFloat(&ok);
      if (!ok)
	{
	  QMessageBox::critical(&dialog, tr("Invalid number"), tr("Invalid entered scale factor number"));
	  continue;
	}
      
      decimation = ui.decimation->text().toInt(&ok);
      if (!ok)
	{
	  QMessageBox::critical(&dialog, tr("Invalid number"), tr("Invalid entered decimation number"));
	  continue;
	}

      translate[0] = ui.x_translate->text().toFloat(&ok);
      if (!ok)
	{
	  QMessageBox::critical(&dialog, tr("Invalid number"), tr("Invalid entered x translation value"));
	  continue;
	}
      
      translate[1] = ui.y_translate->text().toFloat(&ok);
      if (!ok)
	{
	  QMessageBox::critical(&dialog, tr("Invalid number"), tr("Invalid entered y translation value"));
	  continue;
	}
      
      translate[2] = ui.z_translate->text().toFloat(&ok);
      if (!ok)
	{
	  QMessageBox::critical(&dialog, tr("Invalid number"), tr("Invalid entered z translation value"));
	  continue;
	}

      loadVelocities  = ui.loadVelocities->isChecked();

      dataSetName = ui.dataSetName->text();
      if (dataSetName == "")
	{ 
	  QMessageBox::critical(&dialog, tr("Invalid name"), tr("Invalid entered name for data set"));
	  continue;
	}
    }
  while(1);

  int chosenSimu = ui.simuFileType->currentIndex();

  DataSet *data = 
    descriptors[chosenSimu].creator(fname, decimation, 
				    scaleFactor, translate, loadVelocities);

  data->setName(dataSetName);
  g_allDataSources[data->getName()] = data;
}
