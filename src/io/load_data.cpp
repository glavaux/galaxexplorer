/*+
This is GalaxExplorer (./src/io/load_data.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <QtGlobal>
#include <QtDebug>
#include <QFile>
#include <QTextStream>
#include <QInputDialog>
#include <cstdlib>
#include <cmath>
#include <list>
#include "points.hpp"
#include "datasets.hpp"
#include "dataSetTrajectory.hpp"
#include "inits.hpp"
#include "load_data.hpp"
#include <iostream>
#include "glwidget.hpp"
#include "array3d.hpp"
#include "vector3d.hpp"
#ifdef HDF5_PRESENT
#include "makImport/hdfcatalog.hpp"
#endif
#include "window.hpp"

using namespace std;
using namespace GalaxExplorer;  

bool GalaxExplorer::dropDataSource(const QString& name, bool force)
{
  DataSource *s;

  if (!g_allDataSources.contains(name))
    return true;

  s = g_allDataSources[name];

  ConnectorVector cv = main_window->getWorld()->getConnectors();

  for (ConnectorVector::iterator i_conn = cv.begin();
       i_conn != cv.end();
       ++i_conn)
    {
      if (s == i_conn->getConnector()->getDataSource())
	{
	  if (!force)
	    return false;
	  main_window->getWorld()->removeConnector(i_conn->getName());
	}
    }

  delete s;
  g_allDataSources.remove(name);
  return true;
}

void GalaxExplorer::addDataSource(const QString& name, DataSource *data)
{
  QMutexLocker locker(&g_allDataSourcesMutex);
  data->setName(name);
  g_allDataSources[name] = data;
}

void GalaxExplorer::addDataSource(DataSource *universeData)
{
  bool ok;
  QString text = QInputDialog::getText(0, QObject::tr("DataSource name"),
				       QObject::tr("Name to give to the datasource (must be unique)"), QLineEdit::Normal,
				       QString(""), &ok);
  if (!ok || text.isEmpty())
    {
      delete universeData;
      return;
    }

  addDataSource(text, universeData);
}

static list<PointStatus> loadPosData(const QString& fname)
{
  QFile f(fname);
  list<PointStatus> l;

  if (!f.open(QFile::ReadOnly))
    {
      qCritical(qPrintable(QObject::tr("Cannot open ") + fname + QObject::tr(" for reading")));
    }

  QTextStream ts(&f);
  int textFormat;

  ts >> textFormat;
  
  while (!ts.atEnd())
    {
      PointStatus p;

      ts >> p.x >> p.y >> p.z;

      p.selected = 0;
      p.velPresent = 0;
      p.colPresent = 0;
      p.namePresent = 0;
      switch (textFormat) {
      case 1:
          ts >> p.lum >> p.radius;
          break;
      case 2:
  	  p.radius = 10.0;
	  ts >> p.lum >> p.vx >> p.vy >> p.vz;
  	  p.velPresent = 1;
          break;
      case 3:
          p.colPresent = 1;
          ts >> p.radius >> p.color[0] >> p.color[1] >> p.color[2];
          break;
      case 4:
          p.namePresent = 1;
          ts >> p.radius >> p.name;
          break;
        }
      if (ts.atEnd())
	  break;
      l.push_back(p);
    }
  qDebug() << "Read " << l.size() << " points" << endl;
  return l;
}

template<typename L>
void prepareData(L& l, double& multiplier)
{
  typename L::iterator iter;
  typename L::iterator last = l.end();


  double minCoord = INFINITY;
  double maxCoord = -INFINITY;

  for (iter = l.begin(); iter != last; ++iter)
    {
      const BarePoint& p = *iter;

      if (p.x < minCoord)
	minCoord = p.x;
      if (p.y < minCoord)
	minCoord = p.y;
      if (p.z < minCoord)
	minCoord = p.z;
      if (p.x > maxCoord)
	maxCoord = p.x;
      if (p.y > maxCoord)
	maxCoord = p.y;
      if (p.z > maxCoord)
	maxCoord = p.z;
    }

  double val = max(maxCoord,-minCoord);
  
  multiplier = UNIVERSE_BOXSIZE / val;
}

#ifdef HDF5_PRESENT
void GalaxExplorer::loadHDFCatalog(const QString& fname)
{
  double multiplier;

  list<PointStatus> points;

  MAK::FullHDFCatalog catalog;

  try
    {
      MAK::readGenHDFCatalog(qPrintable(fname), catalog);
    }
  catch (const MAK::NoSuchFileException& e)
    {
      qWarning() << "No such file or directory " << fname;
    }


  double minLum = INFINITY, maxLum = -INFINITY;

  for (uint32_t i = 0; i < catalog.numEntries; i++)
    {
      PointStatus p;
      MAK::HDFCatalogEntry& e = catalog.entries[i];
      double l = e.GalLongit, b = e.GalLat;

      if (e.groupId != MAK::noGroup)
	continue;

      p.x = cos(l) * cos(b) * e.Redshift;
      p.y = sin(l) * cos(b) * e.Redshift;
      p.z = sin(b) * e.Redshift;

      p.lum = e.Mass;
      p.radius = pow(e.Mass / 1e13,1./3)*100;
      p.selected = 0;

      if (p.lum < minLum)
	minLum = p.lum;
      if (p.lum > maxLum)
	maxLum = p.lum;

      p.velPresent = 0;
      p.colPresent = 0;

      points.push_back(p);
    }

  cout << "minLum = " << minLum << endl;
  cout << "maxLum = " << maxLum << endl;

  for (uint32_t i = 0; i < catalog.numGroupEntries; i++)
    {
      PointStatus p;
      MAK::HDFCatalogEntry& e = catalog.groupEntries[i];
      double l = e.GalLongit, b = e.GalLat;
      
      p.x = cos(l) * cos(b) * e.Redshift;
      p.y = sin(l) * cos(b) * e.Redshift;
      p.z = sin(b) * e.Redshift;

      p.lum = e.Mass;
      p.radius = pow(e.Luminosity / 1e13,1./3)*100;

      p.selected = 0;

      p.velPresent = 0;
      p.colPresent = 0;
      
      points.push_back(p);
    }

  prepareData(points, multiplier);

  DataSet *universeData = new DataSet(points);
  universeData->setDistanceMultiplier(multiplier);

  addDataSource(universeData);
}
#endif

void GalaxExplorer::loadDataFromFile(const QString& fname)
{
  double multiplier;

  list<PointStatus> points = loadPosData(fname);
  
  prepareData(points, multiplier);

  DataSet *universeData = new DataSet(points);

  universeData->setDistanceMultiplier(multiplier);
  
  addDataSource(universeData);
}

void GalaxExplorer::loadGridData(const QString& fname)
{
  DataGrid *g = loadGridDataBase(fname);

  if (g != 0)
    addDataSource(g);
}

DataGrid *GalaxExplorer::loadGridDataBase(const QString& fname)
{
  QFile f(fname);
  if (!f.open(QIODevice::ReadOnly))
    return 0;

  QDataStream d(&f);
  Array3D<double> data;
  Array3D<Vector3D<double> > displacement;

  uint32_t N;
  double boundaries[6];
  d.readRawData((char *)&N, sizeof(N));
  cout << "Grid " << N << endl;
  d.readRawData((char *)boundaries, sizeof(boundaries));
  cout << "B=[" << boundaries[0] << " " << boundaries[1] << " " << boundaries[2] << " " << boundaries[3] << " " << boundaries[4] << " " << boundaries[5] << "]" << endl;

  data.alloc(N, N, N);
  displacement.alloc(N, N, N);
  d.readRawData((char *)&data(0,0,0), N*N*N*sizeof(double));
  for (int i = 0; i < N*N*N; i++)
    {
      double psi[3];
      if (d.readRawData((char *)psi, 3*sizeof(double)) < 0)
       {
         displacement.alloc(0,0,0);
         break;
       }
      displacement[i](0) = psi[0];
      displacement[i](1) = psi[1];
      displacement[i](2) = psi[2];
    }
  DataGrid *datagrid = new DataGrid(data, displacement, boundaries);
  double mul;

  mul = max(-boundaries[0],boundaries[1]);
  mul = max(mul, -boundaries[2]);
  mul = max(mul, boundaries[3]);
  mul = max(mul, -boundaries[4]);
  mul = max(mul, boundaries[5]);
  
  datagrid->setDistanceMultiplier(UNIVERSE_BOXSIZE/mul);
  return datagrid;
}

void GalaxExplorer::loadParticles(const QString& fname)
{
  double multiplier;

  QFile f(fname);

  if (!f.open(QIODevice::ReadOnly))
    return;

  QDataStream d(&f);
  uint32_t N;
  float one_point[3];
  d.readRawData((char *)&N, sizeof(N));
  vector<BarePoint> points(N);
  for (int i = 0; i < N; i++)
    {
      BarePoint p;

      d.readRawData((char*)one_point, sizeof(one_point));

      points[i].x = one_point[0];
      points[i].y = one_point[1];
      points[i].z = one_point[2];
      
    }
  
  prepareData(points, multiplier);

  DataSet *universeData = new DataSet(points);
  universeData->setDistanceMultiplier(multiplier);
  addDataSource(universeData);
}
