/*+
This is GalaxExplorer (./src/io/load_trajectory.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <QtCore>
#include <netcdfcpp.h>
#include "dataSetTrajectory.hpp"
#include "load_trajectory.hpp"
#include "load_data.hpp"
#include <cmath>

using namespace GalaxExplorer;
using namespace std;

static void prepareData(DataTrajectory *points, uint32_t numPoints, float& multiplier)
{
  double minCoord = INFINITY;
  double maxCoord = -INFINITY;

  for (uint32_t i = 0; i < numPoints; i++)
    {
      const DataTrajectory& p = points[i];

      if (p.x < minCoord)
	minCoord = p.x;
      if (p.y < minCoord)
	minCoord = p.y;
      if (p.z < minCoord)
	minCoord = p.z;
      if (p.x > maxCoord)
	maxCoord = p.x;
      if (p.y > maxCoord)
	maxCoord = p.y;
      if (p.z > maxCoord)
	maxCoord = p.z;
    }

  double val = max(maxCoord,-minCoord);
  
  multiplier = 10.0 / val;
}

void GalaxExplorer::loadTrajectoryDataFromFile(const QString& fname)
{
  uint32_t numTime;
  float *timeSampling;

  NcFile f(qPrintable(fname), NcFile::ReadOnly);
  if (!f.is_valid())
    {
      qCritical(qPrintable(QObject::tr("Cannot open ") + fname + QObject::tr(" for reading")));
    }

  NcVar *vTime = f.get_var("timeSampling");
  int rank = vTime->num_dims();
  if (rank != 1)
    {
      qCritical(qPrintable(QObject::tr("Rank of timeSampling should be 1")));
      return;
    }
  
  long *sizeTime = vTime->edges();
  numTime = sizeTime[0];
  timeSampling = new float[numTime];
  
  vTime->get(timeSampling, sizeTime);

  NcVar *vTraj = f.get_var("trajectory");
  if (vTraj->num_dims() != 3)
    {
      qCritical(qPrintable(QObject::tr("Rank of trajectory should be 3")));
      delete[] timeSampling;
      return;
    }

  long *sizeTraj = vTraj->edges();
  if (sizeTraj[0] != numTime)
    {
      qCritical(qPrintable(QObject::tr("Third dimension of trajectory should be equal to the number of time steps")));
      delete[] timeSampling;
      return;
    }

  if (sizeTraj[2] != 6)
    {     
      qCritical(qPrintable(QObject::tr("First dimension of trajectory should be 6")));
      delete[] timeSampling;
      return;
    }

  NcVar *vType = f.get_var("type");

  uint32_t numTracers = sizeTraj[1];
  float *trajectory = new float[numTracers*numTime*6];
  int *type = 0;
  long *sizeType = 0;

  if (vType != 0) {
    sizeType = vType->edges();
    if (vType->num_dims() != 1 || sizeType[0] != numTracers)
      {
         qCritical(qPrintable(QObject::tr("The 'type' array must be monodimensional and the dimension equal to the number of particles.")));
         delete[] trajectory;
         delete[] timeSampling;
         return;
      }

    type = new int[numTracers];
    vType->get(type, sizeType);
  }

  vTraj->get(trajectory, sizeTraj);

  DataTrajectory *points = new DataTrajectory[numTime*numTracers];
  for (uint32_t i = 0; i < numTime; i++)
    {
      for (uint32_t j = 0; j < numTracers; j++)
	{
	  uint32_t idx = j + i * numTracers;
	  
	  points[idx].x  = trajectory[0 + idx * 6];
	  points[idx].y  = trajectory[1 + idx * 6];
	  points[idx].z  = trajectory[2 + idx * 6];
	  points[idx].vx = trajectory[3 + idx * 6];
	  points[idx].vy = trajectory[4 + idx * 6];
	  points[idx].vz = trajectory[5 + idx * 6];
	}
    }
  
  delete[] trajectory;
  delete[] sizeTime;
  delete[] sizeTraj;

  float multiplier;
  prepareData(points, numTracers*numTime, multiplier);

  SimpleDataSetTrajectory *dataTraj = new SimpleDataSetTrajectory(points, numTracers, timeSampling, numTime); 
  dataTraj->setDistanceMultiplier(multiplier);
  if (type != 0) {
    dataTraj->addAttribute("particle_type", createStorage<int>);
    int attrid = dataTraj->getAttributeID("particle_type");
    for (int i = 0; i < numTracers; i++)
      dataTraj->getAttributeValue<int>(attrid, i) = type[i];

    delete[] type;
  }

  addDataSource(dataTraj);
}
