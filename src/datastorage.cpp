/*+
This is GalaxExplorer (./src/datastorage.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <typeinfo>
#include <QtGui>
#include "datastorage.hpp"

using namespace GalaxExplorer;
using namespace std;

class StorageComparatorBase {
public:
   virtual const type_info& getStorageComparator() = 0;
};

template<typename T> class StorageComparator: public StorageComparatorBase
{
   virtual const type_info& getStorageComparator() { return typeid(T); }
};

static StorageComparatorBase *typeDb[] = {
   new StorageComparator<int>(),
   new StorageComparator<float>(),
   new StorageComparator<double>(),
   new StorageComparator<QColor>(),
   new StorageComparator<QString>()
};

typedef DataStorage *(*CreateDbType)(long);

static CreateDbType createDb[] = {
   createStorage<int>,
   createStorage<float>,
   createStorage<double>,
   createStorage<QColor>,
   createStorage<QString>
};

static int numTypes = sizeof(typeDb)/sizeof(StorageComparatorBase *);

int32_t DataStorage::getStorageType() const
{
  for (int i = 0; i < numTypes; i++)
    if (getStorageComparator() == typeDb[i]->getStorageComparator())
      return i;
  return -1;
}

DataStorage *GalaxExplorer::dataStorageFromType(int32_t t, long numElements)
{
  if (t < 0 || t >= numTypes)
    return 0;

  return createDb[t](numElements);
}
