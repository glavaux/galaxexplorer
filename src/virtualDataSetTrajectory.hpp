/*+
This is GalaxExplorer (./src/virtualDataSetTrajectory.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef _GALAX_DATASET_VTRAJ_HPP
#define _GALAX_DATASET_VTRAJ_HPP

#include <exception>
#include <string>
#include <boost/shared_ptr.hpp>
#include "dataSetTrajectory.hpp"

namespace GalaxExplorer
{
  class VirtualDataSetException: virtual std::exception
  {
  public:
    VirtualDataSetException(const std::string& s) : msg(s) {}
    virtual ~VirtualDataSetException() throw() {}

    virtual const char *what() const throw() { return msg.c_str(); }
  private:
    std::string msg;
  };
  
  class EvolvingDataSetLoader
  {
  public:
    EvolvingDataSetLoader();
    virtual ~EvolvingDataSetLoader();

    virtual uint32_t getNumberParticles() = 0;
    virtual DataSet *loadDataSet(int id) = 0;
    virtual int getNumDataSets() = 0;
    virtual bool isTimeLog() = 0;
    virtual float getDataSetTimeStart(int id) = 0;
    virtual float getBoxSize() = 0;
  };

  class VirtualDataSetTrajectory:  public DataSet, public DataSetTrajectory
  {
    Q_OBJECT
  public:
    VirtualDataSetTrajectory(boost::shared_ptr<EvolvingDataSetLoader> &loader, float sx, float sy, float sz)
      throw (VirtualDataSetException);
    virtual ~VirtualDataSetTrajectory();
 
    virtual void saveData(QDataStream& d);
    virtual void loadData(QDataStream& d);

    virtual uint32_t size() const { return numTracers; }

  public slots:
    virtual void setTimePosition(float t)
      throw (VirtualDataSetException);
    
  protected:
    uint32_t numTracers;
    DataSet *trajectory_current;       
    DataSet *trajectory_next;
    float view_start, view_end, sx, sy, sz;
    float alpha;
    float L;
    int currentSnapshot;
    BarePoint currentPoint;
    
    boost::shared_ptr<EvolvingDataSetLoader> main_loader;    

    virtual BarePoint& virtualGet(uint32_t i);
    virtual BarePoint virtualGet(uint32_t i) const;
  };

};

#endif
