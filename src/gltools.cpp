/*+
This is GalaxExplorer (./src/gltools.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include "includeGL.hpp"
#include <cmath>
#include "gltools.hpp"

using namespace std;

void GalaxExplorer::buildMatrixLookAt(GLdouble *M, 
				      GLdouble ux,
				      GLdouble uy, 
				      GLdouble uz)
{
  double norm = sqrt(ux*ux+uy*uy+uz*uz);
  GLdouble u[3] = { ux/norm, uy/norm, uz/norm };
  GLdouble v[3];
  GLdouble w[3];
  int zeroIndix = 0;
  GLdouble minVal = fabs(u[0]);

  for (int i = 1; i < 3; i++)
    {
      double v = fabs(u[i]);
      if (minVal > v)
	{
	  minVal = v;
	  zeroIndix = i;
	}
    }

  int indix_max, indix_med;
  indix_max = (zeroIndix == 0) ? 2 : 0;
  indix_med = (zeroIndix == 1) ? 2 : 1;
  
  if (fabs(u[indix_max]) < fabs(u[indix_med]))
    {
      int tmp = indix_med;
      indix_med = indix_max;
      indix_max = tmp;
    }

  double alpha = u[indix_med] / u[indix_max];
  double a = 1/sqrt(1 + alpha*alpha);
  double b = -alpha * a;

  v[indix_med] = a;
  v[indix_max] = b;
  v[zeroIndix] = 0;

  w[0] = u[1] * v[2] - u[2] * v[1];
  w[1] = u[2] * v[0] - u[0] * v[2];
  w[2] = u[0] * v[1] - u[1] * v[0];

  M[0] = v[0];
  M[1] = v[1];
  M[2] = v[2];
  M[3] = 0;
  
  M[4] = w[0];
  M[5] = w[1];
  M[6] = w[2];
  M[7] = 0;

  M[8] = u[0];
  M[9] = u[1];
  M[10] = u[2];
  M[11] = 0;
  
  M[12] = 0;
  M[13] = 0;
  M[14] = 0;
  M[15] = 1;
}

void GalaxExplorer::drawCone(GLdouble x, GLdouble y, GLdouble z, 
			     GLdouble h, GLdouble R,
			     GLdouble ux, GLdouble uy, GLdouble uz,
			     int NumFacets)
{
  GLdouble M[16];

  glPushMatrix();
  buildMatrixLookAt(M, ux, uy, uz);
  glTranslated(x, y, z);
  glMultMatrixd(M);

  glBegin(GL_TRIANGLE_FAN);
  glVertex3f(0, 0, 0);
  for (int i = 0; i <= NumFacets; i++)
    {
      glVertex3d(R * cos(2 * M_PI / NumFacets * i),
		 R * sin(2 * M_PI / NumFacets * i),
		 - h);
    }
  glEnd();

  glBegin(GL_TRIANGLE_FAN);
  glVertex3f(0, 0, -h);
  for (int i = 0; i <= NumFacets; i++)
    {
      glVertex3d(R * cos(-2 * M_PI / NumFacets * i),
		 R * sin(-2 * M_PI / NumFacets * i),
		 - h);
    }
  glEnd();

  glPopMatrix();
}

void GalaxExplorer::drawQuad(int w, int h)
{
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  glColor3f(1.0,1.0,1.0);
  glBegin(GL_QUADS);
  {
    glTexCoord2f(0, 0); 
    glVertex2f(-1.0, -1.0);

    glTexCoord2f(w, 0); 
    glVertex2f(1.0, -1.0);

    glTexCoord2f(w, h); 
    glVertex2f(1.0, 1.0);

    glTexCoord2f(0, h); 
    glVertex2f(-1.0, 1.0);
  }
  glEnd();
}

