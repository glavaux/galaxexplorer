/*+
This is GalaxExplorer (./src/progress.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <cassert>
#include <QApplication>
#include <QProgressDialog>
#include <QObject>
#include <QString>
#include "progress.hpp"

using namespace std;
using namespace GalaxExplorer;

RealHandler::RealHandler()
{
  pdialog = 0;
}

RealHandler::~RealHandler()
{
  assert(pdialog == 0);
}

void RealHandler::startProgress(const QString& title, int max_value)
{
  pdialog = new QProgressDialog(title, "Cancel", 0, max_value);
  pdialog->setAutoReset(true);
  pdialog->setAutoClose(true);

  connect(pdialog, SIGNAL(canceled()), this, SLOT(canceled()));
}

void RealHandler::canceled()
{
  emit canceledProgress();
}

void RealHandler::setProgress(int value)
{
  pdialog->setValue(value);
}

void RealHandler::doneProgress()
{
  pdialog->deleteLater();
  pdialog = 0;
}



UserIOHandler::UserIOHandler()
{
  rh = new RealHandler();

  connect(this, SIGNAL(s_startProgress(const QString&, int)), rh, SLOT(startProgress(const QString&, int)));
  connect(this, SIGNAL(s_setProgress(int)), rh, SLOT(setProgress(int)));
  connect(rh, SIGNAL(canceledProgress()), this, SLOT(canceledProgress()));

  // Insure that the handler is receving signal from the main thread.
  rh->moveToThread(QApplication::instance()->thread());
}

UserIOHandler::~UserIOHandler()
{
  doneProgress();
}

void UserIOHandler::canceledProgress()
{
  canceled_progress = true;
}

void UserIOHandler::startProgress(const QString& title, int max_value)
{
  canceled_progress = false;
  emit s_startProgress(title, max_value);
}

void UserIOHandler::setProgress(int value)
{
  emit s_setProgress(value);
}

void UserIOHandler::doneProgress()
{
  emit s_doneProgress();
}
