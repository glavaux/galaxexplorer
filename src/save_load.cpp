/*+
This is GalaxExplorer (./src/save_load.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <QDebug>
#include <QDataStream>
#include <cstring>
#include "save_load.hpp"
#include "inits.hpp"
#include "datasets.hpp"
#include "dataSetTrajectory.hpp"

using namespace std;

//#define LOAD_COMPATIBILITY2

#ifdef LOAD_COMPATIBILITY2
#define LOAD_VERSION 0x0002
#else
#define LOAD_VERSION VERSION_DATAFILE
#endif

#define VERSION_DATAFILE 0x0003

static const char *SIGNATURE = "GALAXEXPLORER";
static const int SIGNATURE_LEN = strlen(SIGNATURE);

void GalaxExplorer::saveState(QDataStream& data, const AllDataSelector& allSelectors,  const ConnectorVector& allConnectors)
{
  data.setByteOrder(QDataStream::LittleEndian);

  data.writeRawData(SIGNATURE, SIGNATURE_LEN);
  data << (qint16)VERSION_DATAFILE;

  quint32 setsize = g_allDataSources.size();
  data << setsize;
  {
    DataSourceMap::iterator iter = g_allDataSources.begin();

    while (iter != g_allDataSources.end())
      {
	data << QString((*iter)->metaObject()->className());
	data << iter.key();
	(*iter)->saveData(data);
	iter++;
      }
  }

  data << (qint32)g_allRepresentations.size();
  {
    DataRepresentationMap::iterator iter = g_allRepresentations.begin();

    while (iter != g_allRepresentations.end())
      {
	data << iter.key();
	data << (*iter)->getRepresentationName();

	(*iter)->saveState(data);

	iter++;
      }
  }

  data << (quint32)allSelectors.size();

  {
    AllDataSelector::const_iterator iter = allSelectors.constBegin();
    while (iter != allSelectors.constEnd())
    {
      data << iter.key() << (*iter)->getDescriptor()->getName();
      (*iter)->saveState(data);
      iter++;
    }
  }
  data << (qint32)allConnectors.size();

  {
    ConnectorVector::const_iterator iter = allConnectors.constBegin();
    while (iter != allConnectors.constEnd())
      {
	data << iter->getName();
	
	data << iter->getConnector()->getDataSource()->getName();
	data << iter->getConnector()->getRepresentation()->getName();
	if (const SetConnector *sc = qobject_cast<const SetConnector *>(iter->getConnector()))
	  data << sc->getSelector()->getName();      
	else
	  data << QString("Noname");
	data << (qint8)iter->isShown();
	
	iter++;
      }
  }
}


void GalaxExplorer::loadState(QDataStream& data, AllDataSelector& allSelectors, ConnectorVector& allConnectors) throw (IncompatibleVersion)
{
  char signature[SIGNATURE_LEN];
  data.setByteOrder(QDataStream::LittleEndian);

  data.readRawData(signature, SIGNATURE_LEN);
  if (strncmp(signature, SIGNATURE, SIGNATURE_LEN) != 0)
    throw IncompatibleVersion();

  qint16 verCheck;
  data >> verCheck;

  if (verCheck != LOAD_VERSION)
    throw IncompatibleVersion();

  quint32 setsize;
  data >> setsize;
  
  for (uint32_t i = 0; i < setsize; i++)
    {
      QString setname;
      QString classname;
      
      DataSource *myset = 0;
#ifdef LOAD_COMPATIBILITY2
      myset = new DataSet();
#else
      data >> classname;
      if (classname == "GalaxExplorer::SimpleDataSetTrajectory")
	myset = new SimpleDataSetTrajectory();
      else if (classname == "GalaxExplorer::DataSet")
	myset = new DataSet();
      else if (classname == "GalaxExplorer::DataGrid")
	myset = new DataGrid();
      else
	{
	  QByteArray msg = ("Do not know anything datasets " + classname).toAscii();
	  qFatal("%s", msg.constData());
	}
#endif
      data >> setname;
      myset->loadData(data);
      myset->setName(setname);

      g_allDataSources[setname] = myset;
    }
 
  qint32 numRepresentations;
  data >> numRepresentations;
  {
    for (qint32 i = 0; i < numRepresentations; i++)
      {
	QString repName;
	QString name;
	cout <<  i << " / " << numRepresentations << endl;

	data >> name;	
	data >> repName;

	if (g_allRepresentations[name] == 0)
	  {
	    cout << repName.toAscii().constData() << endl;
	    cout << name.toAscii().constData() << endl;
	    assert(g_allRepresentations[repName]);

	    DataRepresentation *r = g_allRepresentations[repName]->duplicate();
	    r->loadState(data);
	    r->setName(name);

	    g_allRepresentations[name] = r;
	  }
	else
	  {
	    if (g_allRepresentations[name]->getRepresentationName() != name)
	      {
		delete g_allRepresentations[name];
		g_allRepresentations[name] = g_allRepresentations[repName]->duplicate();
		g_allRepresentations[name]->setName(name);
	      }	    

	    g_allRepresentations[name]->loadState(data);
	  }
      }
  }

  quint32 selectorsize;
  data >> selectorsize;

  for (uint32_t i = 0; i < selectorsize; i++)
    {
      QString selectorName, descriptorName;
      
      data >> selectorName;
      data >> descriptorName;

      DataSetSelector *selector = g_allDataSelectors[descriptorName]->newSelector();
      selector->setName(selectorName);
      selector->loadState(data);

      allSelectors[selectorName] = selector;
    }

  quint32 connectorsize;

  data >> connectorsize;

  for (uint32_t i = 0; i < connectorsize; i++)
    {
      QString connectorName;
      data >> connectorName;
	
      QString representationName;
      QString selectorName;
      QString dataName;
      qint8 show;

      data >> dataName >> representationName >> selectorName >> show;
      cout << dataName.toAscii().constData() << " " << representationName.toAscii().constData() << " " << selectorName.toAscii().constData() << endl;
      
      Connector connector;
      if (DataGrid *dg = qobject_cast<DataGrid *>(g_allDataSources[dataName]))
	connector = buildConnector(representationName, connectorName, dg);
      else if (DataSet *ds = qobject_cast<DataSet *>(g_allDataSources[dataName]))	
	connector = buildConnector(representationName, connectorName, ds,
				   allSelectors[selectorName]);

      connector.setShow(show);

      allConnectors.push_back(connector);
    }

 
}
