/*+
This is GalaxExplorer (./src/GLSLProgramObject.h) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
//
// GLSLProgramObject.h - Wrapper for GLSL program objects
//
// Author: Louis Bavoil
// Email: sdkfeedback@nvidia.com
//
// Copyright (c) NVIDIA Corporation. All rights reserved.
////////////////////////////////////////////////////////////////////////////////

#ifndef GLSL_PROGRAM_OBJECT_H
#define GLSL_PROGRAM_OBJECT_H

#include "includeGL.hpp"
#include <string>
#include <iostream>
#include <vector>
#include <exception>

namespace GalaxExplorer {

  class ShaderCompileError: public std::exception
  {
  public:
    ShaderCompileError(QString path)
      : errorPath(path)
    {      
    }

    ShaderCompileError(const ShaderCompileError& e)
      : errorPath(e.errorPath)
      {
      }
    virtual ~ShaderCompileError() throw() {}

    virtual const char *what() const throw() {
      return "Shader compile error";
    }    

    const QString& getPath() const { return errorPath; }
  private:
    QString errorPath;
  };

  class GLSLShaderObject
  {
  public:
    GLSLShaderObject()
      throw();
    GLSLShaderObject(const GLSLShaderObject& s)
      throw()
      {
	used = s.used;
	if (used != 0)
	  (*used)++;
	shaderId = s.shaderId;
	type = s.type;
      }
    GLSLShaderObject(const std::string& filename, GLuint type)
      throw (ShaderCompileError);
    ~GLSLShaderObject()
      throw();
    
    void release();
    
    GLSLShaderObject& operator=(const GLSLShaderObject& s)
      {
	if (used != 0)
	  {
	    (*used)--;
	    if ((*used) == 0)
	      release();
	  }
	used = s.used;
	if (used != 0)
	  (*used)++;
	shaderId = s.shaderId;
	type = s.type;
	
	return *this;
      }
    
    GLuint getId() { return shaderId; }
    GLuint getType() { return type; }
    
  protected:
    GLuint shaderId;
    GLuint type;
    int *used;
  };

  class GLSLTextureUnitAllocator 
  {
  public:
    virtual ~GLSLTextureUnitAllocator();

    virtual GLuint alloc() = 0;
    virtual void free_all() = 0;
    virtual GLSLTextureUnitAllocator* duplicate() const = 0;
  };

  class GLSLSimpleAllocator: public GLSLTextureUnitAllocator
  {
  public:
    GLSLSimpleAllocator(int numReserved) : reserved(numReserved), init_reserved(numReserved) {}
    virtual ~GLSLSimpleAllocator() {}
    virtual GLuint alloc() { return reserved++; }
    virtual void free_all() { reserved = init_reserved; }
    virtual GLSLTextureUnitAllocator* duplicate() const { 
      GLSLSimpleAllocator *alloc = new GLSLSimpleAllocator(reserved);
      alloc->init_reserved = init_reserved;
      return alloc;
    }
  private:
    GLuint reserved, init_reserved;
  };
  
  class GLSLProgramObject
  {
  public:
    GLSLProgramObject() throw();
    GLSLProgramObject(const GLSLProgramObject& p) throw();
    ~GLSLProgramObject() throw();
    
    void destroy() throw();
  
    void bind();
  
    void unbind();
    
    void setUniform(const std::string& name, const GLfloat* val, int count);
    void setIntUniform(const std::string& name, const GLint* val, int count);
    void setMatrixUniform(std::string name, const GLfloat* val, int rank);
    
    void setTextureUnit(const std::string& texname, int texunit);
 
    GLint getAttribLocation(const std::string& attribname);
    
    void bindTexture(GLenum target, const std::string& texname, GLuint texid);

    void bindTexture1D(const std::string& texname, GLuint texid) {
      bindTexture(GL_TEXTURE_1D, texname, texid);
    }
    
    void bindTexture2D(const std::string& texname, GLuint texid) {
      bindTexture(GL_TEXTURE_2D, texname, texid);
    }
    
    void bindTexture3D(const std::string& texname, GLuint texid) {
      bindTexture(GL_TEXTURE_3D, texname, texid);
    }
    
    void bindTextureRECT(const std::string& texname, GLuint texid) {
      bindTexture(GL_TEXTURE_RECTANGLE_ARB, texname, texid);
    }

    void bindTextureCUBE(const std::string& texname, GLuint texid) {
      bindTexture(GL_TEXTURE_CUBE_MAP, texname, texid);
    }

    void setAllocator(GLSLTextureUnitAllocator *allocator);

    void attachShader(GLSLShaderObject& s);
    
    void link();
    
    inline GLuint getProgId() { return _progId; }
    
    void pushShaders(GLSLProgramObject& p);
    
    GLSLProgramObject& operator=(const GLSLProgramObject& p);
    
  protected:
    std::vector<GLSLShaderObject>		_vertexShaders;
    std::vector<GLSLShaderObject>		_fragmentShaders;
    std::vector<GLSLShaderObject>               _geometryShaders;
    std::map<std::string,GLuint>                   _texunitMap;
    GLSLTextureUnitAllocator *texunit_allocator;
    GLuint					_progId;
  };
  
};

#endif
  
