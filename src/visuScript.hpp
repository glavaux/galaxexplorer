/*+
This is GalaxExplorer (./src/visuScript.hpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __GALAX_SCRIPT_HPP
#define __GALAX_SCRIPT_HPP

#include "glwidget.hpp"

#include <QtCore>
#include <QtScript>
#include "base.hpp"
#include "world.hpp"

namespace GalaxExplorer
{

  class ConnectionScripted: public QObject
  {
    Q_OBJECT

  public:
    ConnectionScripted(UniverseWidget *w, 
		       Connector *c, int id, QObject *parent = 0);
    virtual ~ConnectionScripted();

  public slots:
    void show(bool val);
    void setMaster();
    QObject *getDataSet();
    QObject *getRepresentation();
    QObject *getDataSetSelector();
    void recompile();

  private:
    UniverseWidget *universe;
    Connector *connector;
    int connectionId;
  };

  class VisuScript: public QObject
  {
    Q_OBJECT

  public:
    VisuScript(const QString& programFile, World *world, QObject *parent = 0)
      throw (NoSuchFileException);

    virtual ~VisuScript();

    void execute();

  public slots:
    QObject *getConnection(const QString& connectionName);
    void hideAllConnectors();
    void loadState(QString n);
    void setDistanceMultiplier(float f);

  private:
    QScriptEngine engine;
    ConnectorVector *existingConnections;
    World *world;
    QMap<QString,ConnectionScripted *> connectionMap;
    QString program;
    QString programFile;
    QObject *connectionPool;
  };

};

#endif
