/*+
This is GalaxExplorer (./src/surfaceCleaner.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include "vboObject.hpp"
#include "surfaceCleaner.hpp"

using namespace GalaxExplorer;

SurfaceCleaner::SurfaceCleaner(bool smooth)
{
  smoothSurface = smooth;
  all_normals = 0;
}

SurfaceCleaner::~SurfaceCleaner()
{
  if (all_normals)
    delete[] all_normals;
}

void SurfaceCleaner::begin(int numFaces, int numPoints)
{
  int activeNum = smoothSurface ? numPoints : numFaces;

  if (all_normals != 0)
    delete[] all_normals;

  all_normals = new Normal[activeNum];
  this->numPoints = activeNum;
  
  for (int i = 0; i < activeNum; i++)
    {
      for (int j = 0; j < 3; j++)
	all_normals[i].direction.xyz[j] = 0;
      all_normals[i].accum = 0;
    }
}

void SurfaceCleaner::end()
{
  if (all_normals == 0)
    return;

  for (int i = 0; i < numPoints; i++)
    {
      for (int j = 0; j < 3; j++)
	all_normals[i].direction.xyz[j] /= all_normals[i].accum;
    }
}

void SurfaceCleaner::getNormal(int t, int p, Point& n)
{
  n = all_normals[smoothSurface ? p : t].direction;
}
