/*+
This is GalaxExplorer (./src/virtualDataSetTrajectory.cpp) -- Copyright (C) Guilhem Lavaux (2007-2013)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to interpret and
represent astromical data or N-body simulations in three-dimensions.
It is a visualization tool.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <boost/shared_ptr.hpp>
#include "datasets.hpp"
#include "virtualDataSetTrajectory.hpp"

#include <iostream>

using namespace std;

using namespace GalaxExplorer;


EvolvingDataSetLoader::EvolvingDataSetLoader()
{
}

EvolvingDataSetLoader::~EvolvingDataSetLoader()
{
}


VirtualDataSetTrajectory::VirtualDataSetTrajectory(boost::shared_ptr<EvolvingDataSetLoader> &loader, float sx, float sy, float sz)
  throw (VirtualDataSetException)
  : main_loader(loader)
{
  if (main_loader->getNumDataSets() < 2)
    throw VirtualDataSetException("need at least two datasets !");

  numTracers = main_loader->getNumberParticles();
  currentSnapshot = 0;
  
  view_start = 0.;
  view_end = main_loader->getDataSetTimeStart(1);

  L = main_loader->getBoxSize();
  this->sx = sx;
  this->sy = sy;
  this->sz = sz;

  if ((trajectory_current = main_loader->loadDataSet(0)) == 0)
    throw VirtualDataSetException("Error while loading first dataset");

  if ((trajectory_next = main_loader->loadDataSet(1)) == 0)
    throw VirtualDataSetException("Error while loading second dataset");
}

VirtualDataSetTrajectory::~VirtualDataSetTrajectory()
{
  delete trajectory_current;
  delete trajectory_next;
}
 
void VirtualDataSetTrajectory::saveData(QDataStream& d)
{
}

void VirtualDataSetTrajectory::loadData(QDataStream& d)
{
}

void VirtualDataSetTrajectory::setTimePosition(float t)
  throw (VirtualDataSetException)
{
  int N = main_loader->getNumDataSets();
  bool changed = false;
  int oldSnapshot = currentSnapshot;
  float old_start = view_start, old_end = view_end;
  
  //  cerr << "view_start = " << view_start << endl;
  //  cerr << "view_end = " << view_end << endl;
  //  cerr << "snap=" << currentSnapshot << endl;
  //  cerr << "Got t=" << t << endl;
  if (t > view_end)
    {
      while (view_end < t)
	{
	  view_start = view_end;
	  currentSnapshot++;
	  changed = true;
	  if (currentSnapshot == (N-1))
	    {
	      view_start = old_start;
	      view_end = old_end;
	      currentSnapshot = oldSnapshot;
	      throw VirtualDataSetException("Time does not exist");
	    }

	  view_end = main_loader->getDataSetTimeStart(currentSnapshot+1);
	}
    }
  else
    {
      while (view_start > t)
	{
	  view_end = view_start;
	  currentSnapshot--;
	  changed = true;
	  if (currentSnapshot == 0)
	    {
	      view_start = old_start;
	      view_end = old_end;
	      currentSnapshot = oldSnapshot;
	      throw VirtualDataSetException("Time does not exist");
	    }
	  view_start = main_loader->getDataSetTimeStart(currentSnapshot);	
	}
    }

  assert(view_start <= t);
  assert(view_end >= t);

  //  cerr << "After..." <<endl
  //       << "snap=" << currentSnapshot << endl
  //       << "view_start = " << view_start << endl
  //       << "view_end = " << view_end << endl;
    

  if (changed)
    {
      if (oldSnapshot == currentSnapshot+1)
	{
	  delete trajectory_next;
	  trajectory_next = trajectory_current;
	  if ((trajectory_current = main_loader->loadDataSet(currentSnapshot)) == 0)
	    throw VirtualDataSetException("Error while loading first dataset");
	}
      else if (oldSnapshot == currentSnapshot-1)
	{
	  delete trajectory_current;
	  trajectory_current = trajectory_next;
	  if ((trajectory_next = main_loader->loadDataSet(currentSnapshot+1)) == 0)
	    throw VirtualDataSetException("Error while loading second dataset");
	}
      else
	{
	  delete trajectory_current;
	  delete trajectory_next;

	  if ((trajectory_current = main_loader->loadDataSet(currentSnapshot)) == 0)
	    throw VirtualDataSetException("Error while loading first dataset");
	  
	  if ((trajectory_next = main_loader->loadDataSet(currentSnapshot+1)) == 0)
	    throw VirtualDataSetException("Error while loading second dataset");	  
	}
    }
  
  alpha = (t-view_start)/(view_end-view_start);

  //  cerr << "alpha=" << alpha << endl;

  emit dataUpdated();
}

BarePoint& VirtualDataSetTrajectory::virtualGet(uint32_t i)
{
  const BarePoint& p0 = (*trajectory_current)[i];
  const BarePoint& p1 = (*trajectory_next)[i];
  
  currentPoint = p0;
  currentPoint.x += alpha*(p1.x-p0.x)+sx;
  currentPoint.y += alpha*(p1.y-p0.y)+sy;
  currentPoint.z += alpha*(p1.z-p0.z)+sz;

  return currentPoint;
}

static float periodize(float x, float p)
{
  while (x > 0.5*p)
    x -= p;
  while (x < -0.5*p)
    x += p;
  return x;
}

BarePoint VirtualDataSetTrajectory::virtualGet(uint32_t i) const
{
  const BarePoint& p0 = (*trajectory_current)[i];
  const BarePoint& p1 = (*trajectory_next)[i];
  BarePoint p = p0;

  p.x += alpha*periodize(p1.x-p0.x, L)+sx;
  p.y += alpha*periodize(p1.y-p0.y, L)+sy;
  p.z += alpha*periodize(p1.z-p0.z, L)+sz;

  return p;
}
