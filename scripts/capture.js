if (typeof(captureConfigured) == "undefined")
  {
     print("Must configure !");
     CAPTURE_MODE = 0;
     CAPTURE_WIDTH = 800;
     CAPTURE_HEIGHT = 600;
     CAPTURE_BASE_FILENAME = "capture";
     CAPTURE_COUNTER = 0;
     MOVIE_SLEEP = 30.0;
     captureConfigured = 1;
  }


captureNextImage = function()
{
   captureFilename = CAPTURE_BASE_FILENAME + CAPTURE_COUNTER + ".png";
   universe.capture(captureFilename, CAPTURE_WIDTH, CAPTURE_HEIGHT);
   CAPTURE_COUNTER++;
}


print("Movie captures extension.");
