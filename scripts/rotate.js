include("capture.js");
include("clone.js");

doRotate = function(angle,numSteps)
{
   s = universe.getState();
   s0 = clone(s);

   for (i = 1; i <= numSteps; i++)
    {
       s.zRotation = s0.zRotation + i * angle / numSteps;
       universe.setState(s);
       if (CAPTURE_MODE)
         captureNextImage();
       else
         sleep(MOVIE_SLEEP);
    }
};

rotateToZAngle = function(angle, numSteps)
{
    s = universe.getState();
    s0 = clone(s);

    print("Current zRot = " + s0.zRotation);

    for (i = 1; i <= numSteps; i++)
	{
	    s.zRotation = s0.zRotation + i * (angle-s0.zRotation) / numSteps;
	    universe.setState(s);
	    if (CAPTURE_MODE)
		captureNextImage();
	    else
		sleep(MOVIE_SLEEP);
	    
	}
}

doXRotate = function(angle,numSteps)
{
   s = universe.getState();
   s0 = clone(s);

   for (i = 1; i <= numSteps; i++)
    {
       s.xRotation = s0.xRotation + i * angle / numSteps;
       universe.setState(s);
       if (CAPTURE_MODE)
         captureNextImage();
       else
         sleep(MOVIE_SLEEP);
    }    
}
;
rotateToXAngle = function(angle, numSteps)
{
    s = universe.getState();
    s0 = clone(s);

    for (i = 1; i <= numSteps; i++)
	{
	    s.xRotation = s0.xRotation + i * (angle-s0.xRotation) / numSteps;
	    universe.setState(s);
	    if (CAPTURE_MODE)
		captureNextImage();
	    else
		sleep(MOVIE_SLEEP);
	    
	}
}

doZoom = function(endZoom, numSteps)
{
   s = universe.getState();
   s0 = clone(s);

   for (i = 1; i <= numSteps; i++)
    {
	s.distance = s0.distance + (endZoom-s0.distance) * i / numSteps;
	universe.setState(s);
	if (CAPTURE_MODE)
	    captureNextImage();
	else
	    sleep(MOVIE_SLEEP);
    }
}

print("Included rotation of figures extensions.");
