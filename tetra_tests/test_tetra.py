#+
# This is GalaxExplorer (./tetra_tests/test_tetra.py) -- Copyright (C) Guilhem Lavaux (2007-2013)
#
# guilhem.lavaux@gmail.com
#
# This software is a computer program whose purpose is to interpret and
# represent astromical data or N-body simulations in three-dimensions.
# It is a visualization tool.
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#+
import numpy as np
from numpy import genfromtxt
import GalaxExplorer as ge

f = file("points.txt")
f.readline()
f.readline()
points = genfromtxt(f, dtype=[('x','f'),('y','f'),('z','f')])
f.close()

f = file("facets.txt")
f.readline()
mesh = genfromtxt(f,dtype=np.int)

dsh=ge.DataSetHandler()
ch =ge.Connectors()
vs=ge.ViewState()
ss=ge.SkyBox()
rh=ge.RepresentationHandler()

try:
    ch.destroy('pm')
except ValueError:
    pass

dsh.dropData('p')
dsh.newData('p', np.array([points['x'],points['y'],points['z']]))
dsh.dropData('m')
dsh.newMesh('m', mesh.transpose())

ch.createMesh('pm', 'm','p','TetrahedronRepresentation')
ch.create('p','p','No selection', 'BWPointRepresentation')

ss.add('cmb','/home/guilhem/testcmb.fits')

rep_points = ge.Representation('BWPointRepresentation')
rep_points.setParameter('pointSize', 4.0)

t_rep = ge.Representation('TetrahedronRepresentation')
t_rep.setParameter('surfaceColor', '#ee1100')
t_rep.setParameter('surfaceAlpha', 0.7)

vs.changeScaling(10.0)
vs.recompileAll()
