#+
# This is GalaxExplorer (./data/drawDwarfs.py) -- Copyright (C) Guilhem Lavaux (2007-2013)
#
# guilhem.lavaux@gmail.com
#
# This software is a computer program whose purpose is to interpret and
# represent astromical data or N-body simulations in three-dimensions.
# It is a visualization tool.
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#+
import sys
sys.path.append("../python")


import GalaxExplorer as ge
import numpy as np

sys.path.append("../python")

#pos = np.genfromtxt("dwarfs.txt", dtype=[('name','S20'),('l','f'),('b','f'),('mu','f')],delimiter='|')
pos = np.genfromtxt("dwarfs_Watkins.txt", dtype=[('name','S20'),('l','f'),('b','f'),('r','f')],delimiter='|')

l=pos['l']*np.pi/180
b=pos['b']*np.pi/180
#d=10**(0.2*(pos['mu']-25))
d=pos['r']

x=np.cos(l)*np.cos(b)*d
y=np.sin(l)*np.cos(b)*d
z=np.sin(b)*d


dsh = ge.DataSetHandler()
rh = ge.RepresentationHandler()
ch = ge.Connectors()
vs = ge.ViewState()


try:
    dsh.newData('dwarfs', np.array([x,y,z]),multiplier=10.0/300.)
    dsh.waitData('dwarfs')
    
    attribs = np.array([[0],[0],[1]])

    dsh.newData('mw', np.array([[0],[0],[0]]), attribute_names=np.array(['normal_x','normal_y','normal_z']), attribute_data=attribs, multiplier=10.0/300.)
    dsh.waitData('mw')
except ValueError:
    pass

if not rh.duplicateRepresentation('GalaxyRepresentation', 'dwarfRep'):
    print "Impossible to duplicate GalaxyRepresentation to dwarfRep"

if not rh.duplicateRepresentation('GalaxyRepresentation', 'MilkyWay'):
    print "Impossible to duplicate GalaxyRepresentation to dwarfRep"

dwarfRep = ge.Representation('dwarfRep')
mwRep = ge.Representation('MilkyWay')

dwarfRep.setParameter('galaxySize', 10.0)
dwarfRep.setParameter('galaxyTexture', 'm31.jpg')
dwarfRep.setParameter('autoTransparency', 1)
dwarfRep.setParameter('alphaPower', 1.0)
mwRep.setParameter('billboard', 0)
mwRep.setParameter('galaxyTexture.', 'galaxy.png')
mwRep.setParameter('galaxySize', 100.0);

ch.create('dwarfs','dwarfs','No selection','dwarfRep')
ch.create('mw','mw','No selection','MilkyWay')

vs.changeScaling(10.0/300)
vs.recompileAll()
vs.refresh()
