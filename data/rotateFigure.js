include("../scripts/rotate.js");

FRAME_RATE = 2;
MOVIE_SLEEP = 10000.0/FRAME_RATE;

s = universe.getState();
s.distance = 25.0;
s.zRotation = 0;
s.yRotation = 0;
s.xRotation = 270;
universe.setState(s);


CAPTURE_MODE=0;

print("Initialized. Rotating...");

doRotate(360, 30*FRAME_RATE);
